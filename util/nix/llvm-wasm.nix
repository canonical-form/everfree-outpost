pkgs:

let newScope' = auto: path: args:
        let orig = pkgs.newScope auto path args;
            name = builtins.parseDrvName orig.name;
            major = builtins.elemAt (builtins.splitVersion name.version) 0;
        in
        if name.name == "llvm" && major == "6" then
            orig.overrideAttrs (oldAttrs: {
                cmakeFlags = oldAttrs.cmakeFlags ++
                    ["-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=WebAssembly"]; }
            )
        else
            orig;

    llvm = pkgs.llvmPackages_6.override {
        stdenv = pkgs.stdenv_32bit;
        newScope = newScope';
    };

    version = "6.0.0";
    release_version = version;

    # Need stdenv with 32-bit support, otherwise we get the wrong glibc headers
    compiler-rt-wasm = pkgs.stdenv.mkDerivation (rec {
        name = "compiler-rt-wasm-${version}";

        buildInputs = [
            pkgs.cmake pkgs.python
            llvm.llvm llvm.clang
        ];

        cmakeFlags = [
            "-DCMAKE_BUILD_TYPE=Release"
            "-DCMAKE_C_COMPILER=clang"
            "-DCMAKE_CXX_COMPILER=clang++"
            "-DCOMPILER_RT_DEFAULT_TARGET_TRIPLE=wasm32-unknown-unknown-wasm"
            "-DCOMPILER_RT_BAREMETAL_BUILD=ON"
            "-DCOMPILER_RT_EXCLUDE_ATOMIC_BUILTIN=ON"
            "--target" "../lib/builtins"
        ];

        enableParallelBuilding = true;

        src = pkgs.fetchurl {
            url = "http://releases.llvm.org/${release_version}/compiler-rt-${version}.src.tar.xz";
            sha256 = "16m7rvh3w6vq10iwkjrr1nn293djld3xm62l5zasisaprx117k6h";
        };
    });

in llvm // { inherit compiler-rt-wasm; }
