#!/bin/bash
set -e

cd "$(dirname "$0")/.."
mkdir -p run
cd run

cp ../util/outpost_bugreport.ini .

echo "starting bugreport server..."
python3 ../dist/bugreport/server.py
