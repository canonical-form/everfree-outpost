#include "server.hpp"
#include "websocket.hpp"

#include <iostream>

using namespace std;
// Avoid conflict between std and boost placeholders.
namespace ph = std::placeholders;
using namespace boost::asio;
using websocketpp::connection_hdl;


websocket::websocket(server& owner,
                     boost::asio::io_service& ios,
                     boost::asio::ip::tcp::endpoint addr)
    : owner(owner),
      ws_server(),
      next_id(1),
      id_to_client(),
      clients() {
    ws_server.init_asio(&ios);
    ws_server.set_reuse_addr(true);

    ws_server.set_open_handler(bind(&websocket::handle_open, this, ph::_1));
    ws_server.set_message_handler(bind(&websocket::handle_message, this, ph::_1, ph::_2));
    ws_server.set_close_handler(bind(&websocket::handle_close, this, ph::_1));

    ws_server.listen(addr);
    ws_server.start_accept();
}

void websocket::handle_open(connection_hdl conn) {
    while (next_id == 0 || id_to_client.count(next_id)) {
        ++next_id;
    }

    uint16_t id = next_id++;
    id_to_client.insert(make_pair(id, conn));
    client_data data;
    data.id = id;

    // Do this before `emplace` because data.proto is null afterwards.
    occ_protocol_open(data.proto);

    clients.emplace(conn, move(data));

    auto conn_ptr = ws_server.get_con_from_hdl(conn);
    cout << "connection from " << conn_ptr->get_remote_endpoint() << ", wire id " << id << endl;
}

void websocket::handle_message(connection_hdl conn, ws_server_asio::message_ptr msg) {
    auto data_iter = clients.find(conn);
    if (data_iter == clients.end()) {
        return;
    }
    auto& data(data_iter->second);

    const string& payload = msg->get_payload();
    occ_protocol_incoming(data.proto, (uint8_t*)&*payload.begin(), payload.size());
    process_events(conn, data);
}

void websocket::handle_close(connection_hdl conn) {
    auto data_iter = clients.find(conn);
    if (data_iter == clients.end()) {
        return;
    }
    auto& data(data_iter->second);

    data.client_connected = false;
    if (data.dead()) {
        id_to_client.erase(data.id);
        clients.erase(data_iter);
    } else {
        // Shut down the backend side as well.
        owner.handle_websocket_disconnect(data.id);
    }
}

void websocket::send_message(message msg) {
    auto conn_iter = id_to_client.find(msg.client_id);
    if (conn_iter == id_to_client.end()) {
        return;
    }
    auto& conn(conn_iter->second);

    auto data_iter = clients.find(conn);
    if (data_iter == clients.end()) {
        return;
    }
    auto& data(data_iter->second);

    if (!data.client_connected) {
        return;
    }

    vector<uint8_t> buf;
    buf.reserve(2 + msg.data.size());
    buf.resize(2);
    *(uint16_t*)&buf[0] = msg.opcode;
    buf.insert(buf.end(), msg.data.begin(), msg.data.end());

    occ_protocol_send(data.proto, &*buf.begin(), buf.size());
    process_events(conn, data);
}

/*

    std::error_code ec;
    ws_server.send(conn, buf.data(), buf.size(), websocketpp::frame::opcode::binary, ec);
    if (ec) {
        cerr << "error sending to " << msg.client_id << ": " << ec << endl;
    }
}
*/

void websocket::handle_client_removed(uint16_t client_id) {
    auto conn_iter = id_to_client.find(client_id);
    if (conn_iter == id_to_client.end()) {
        return;
    }
    auto& conn(conn_iter->second);

    auto data_iter = clients.find(conn);
    if (data_iter == clients.end()) {
        return;
    }
    auto& data(data_iter->second);

    data.backend_connected = false;
    if (data.dead()) {
        id_to_client.erase(data.id);
        clients.erase(data_iter);
    } else {
        // Shut down the client connection as well.
        std::error_code ec;
        ws_server.close(conn, websocketpp::close::status::normal, "", ec);
        if (ec) {
            cerr << "error closing " << client_id << ": " << ec << endl;
        }
        // NB: handle_close may have invalidated one or both of the iterators.
    }
}

void websocket::process_events(connection_hdl conn, client_data& data) {
    int kind;
    while ((kind = occ_protocol_next_event(data.proto)) != OCC_EVENT_NONE) {
        switch (kind) {
            case OCC_EVENT_RECV: {
                if (!data.backend_connected) {
                    return;
                }
                size_t len;
                uint8_t* begin = occ_protocol_recv_data(data.proto, &len);
                uint8_t* end = begin + len;

                if (len < 2) {
                    cerr << "client " << data.id << ": message has no opcode" << endl;
                    // TODO: send a kick message and shut down
                    return;
                }

                uint16_t opcode = *(uint16_t*)begin;
                vector<uint8_t> msg_data(begin + 2, end);

                owner.handle_websocket_request(message(data.id, opcode, move(msg_data)));
                break;
            }

            case OCC_EVENT_OUTGOING: {
                size_t len;
                uint8_t* begin = occ_protocol_outgoing_data(data.proto, &len);
                std::error_code ec;
                ws_server.send(conn, begin, len, websocketpp::frame::opcode::binary, ec);
                if (ec) {
                    cerr << "error sending to " << data.id << ": " << ec << endl;
                }
                break;
            }

            case OCC_EVENT_HANDSHAKE_FINISHED: {
                uint8_t* begin =
                    occ_protocol_handshake_finished_channel_binding_token(data.proto);
                uint8_t* end = begin + OCC_CHANNEL_BINDING_TOKEN_LEN;
                vector<uint8_t> cb_token(begin, end);
                data.backend_connected = true;
                owner.handle_websocket_connect(data.id, move(cb_token));
                break;
            }

            case OCC_EVENT_ERROR: {
                size_t len;
                uint8_t* begin = occ_protocol_error_message(data.proto, &len);
                string msg(begin, begin + len);
                cerr << "protocol error for " << data.id << ": " << msg << endl;
                break;
            }
        }
    }
}
