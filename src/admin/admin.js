var CELLS = {};

function add_row(tbl, name, key) {
    var tr = document.createElement('tr');
    var td1 = document.createElement('td');
    var td2 = document.createElement('td');

    td1.textContent = name;
    td1.classList.add('name');
    td2.textContent = '--';
    td2.classList.add('value');
    CELLS[key] = td2;

    tr.appendChild(td1);
    tr.appendChild(td2);
    tbl.appendChild(tr);
}

function add_sep(tbl) {
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    var hr = document.createElement('hr');

    td.setAttribute('colspan', '2');

    td.appendChild(hr);
    tr.appendChild(td);
    tbl.appendChild(tr);
}

function start_ajax() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'stats');
    xhr.responseType = 'json';
    xhr.onload = function(evt) {
        update_cells(xhr.response);
    };
    xhr.send();

    setTimeout(function() {
        window.requestAnimationFrame(function() {
            start_ajax();
        })
    }, 5000);
}

function format_padded(x, n) {
    var s = '' + x;
    while (s.length < n) {
        s = '0' + s;
    }
    return s;
}

function format_time(t) {
    var s = (t / 1000)|0;
    var ms = (t % 1000)|0;
    return s + '.' + format_padded(ms, 3);
}

function format_long_time(t) {
    var ms = (t % 1000)|0;
    t = (t / 1000)|0;
    var s = (t % 60)|0;
    t = (t / 60)|0;
    var m = (t % 60)|0;
    t = (t / 60)|0;
    var h = (t % 24)|0;
    t = (t / 24)|0;
    var d = t;

    var str = format_padded(s, 2) + '.' + format_padded(ms, 3);
    if (m > 0) {
        str = format_padded(m, 2) + ':' + str;
    }
    if (h > 0) {
        str = format_padded(h, 2) + ':' + str;
    }
    if (d > 0) {
        str = d + 'd ' + str;
    }
    return str;
}

function update_cells(j) {
    var eng = j['engine'];

    for (var k of Object.getOwnPropertyNames(eng)) {
        if (CELLS[k] != null) {
            CELLS[k].textContent = eng[k];
        }
    }

    CELLS['now_sec'].textContent = format_long_time(eng['now']);

    var hr = format_time(eng['now'] % (24 * 60 * 1000) / 60);
    CELLS['now_day'].textContent = hr;

    var client_list = CELLS['client_list_text'];
    while (client_list.lastChild) {
        client_list.removeChild(client_list.lastChild);
    }
    eng['client_list'].sort();
    for (var name of eng['client_list']) {
        client_list.appendChild(document.createTextNode(name));
        client_list.appendChild(document.createElement('br'));
    }
}

document.addEventListener('DOMContentLoaded', function() {
    add_row(tbl, 'World time', 'now_sec');
    add_row(tbl, 'Time of day', 'now_day');
    add_sep(tbl);

    add_row(tbl, 'Clients', 'num_clients');
    add_row(tbl, 'Entities', 'num_entities');
    add_row(tbl, 'Inventories', 'num_inventories');
    add_row(tbl, 'Planes', 'num_planes');
    add_row(tbl, 'Terrain chunks', 'num_terrain_chunks');
    add_row(tbl, 'Structures', 'num_structures');
    add_sep(tbl);

    add_row(tbl, 'Client list', 'client_list_text');

    start_ajax();
});
