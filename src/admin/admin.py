import os
import socket
import sys
import time

import tornado.ioloop
import tornado.web


SERVER_PATH, = sys.argv[1:]


class StatsHandler(tornado.web.RequestHandler):
    def get(self):
        s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect(os.path.join(SERVER_PATH, 'stats'))
        stats = s.makefile().read()

        self.set_header('Content-type', 'application/json')
        self.write(stats)

DEBUG = int(os.environ.get('OUTPOST_ADMIN_DEBUG', 0)) > 0

if __name__ == '__main__':
    application = tornado.web.Application([
        (r'/stats', StatsHandler),
        (r'/(.*)', tornado.web.StaticFileHandler, {
            'path': os.path.dirname(sys.argv[0]),
            'default_filename': 'admin.html',
            }),
    ], debug=DEBUG)

    application.listen(8893, '127.0.0.1')
    tornado.ioloop.IOLoop.instance().start()

