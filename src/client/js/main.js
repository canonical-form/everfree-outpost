var $ = document.getElementById.bind(document);

var loader = require('./loader');
var Config = require('./config').Config;

var handleResize = require('./resize').handleResize;

var Keyboard = require('./keyboard').Keyboard;
var asmKeyHandler = require('./keyboard').asmKeyHandler;
var Dialog = require('./ui/dialog').Dialog;
var Banner = require('./ui/banner').Banner;
var ChatWindow = require('./ui/chat').ChatWindow;
var Iframe = require('./ui/iframe').Iframe;
var KeyDisplay = require('./ui/keydisplay').KeyDisplay;
var Menu = require('./ui/menu').Menu;
var ConfigEditor = require('./ui/configedit').ConfigEditor;
var MusicTest = require('./ui/musictest').MusicTest;
var KeybindingEditor = require('./ui/keybinding').KeybindingEditor;
var widget = require('./ui/widget');
var ErrorList = require('./ui/errorlist').ErrorList;
var DIALOG_TYPES = require('./ui/dialogs').DIALOG_TYPES;

var Input = require('./input').Input;

var LOCAL_SIZE = require('./consts').LOCAL_SIZE;

var DynAsm = require('./asmlibs').DynAsm;
var AsmClientInput = require('./asmlibs').AsmClientInput;

var net = require('./net');
var Timing = require('./time').Timing;

var util = require('./util/misc');

var EXPORTS = require('./export').EXPORTS;


// Client objects

var asm_client;

var assets;

var conn;

var canvas;
var ui_div;
var dialog;
var banner;
var keyboard;
var chat;
var error_list;
var music_test;

var input;

var main_menu;
var debug_menu;

var timing;
var synced = net.SYNC_OK;


/** @constructor */
function OutpostClient() {
    this._earlyInit();
}
window['OutpostClient'] = OutpostClient;

OutpostClient.prototype._earlyInit = function() {
    // Set up error_list first to catch errors in other parts of init.
    error_list = new ErrorList();
    error_list.attach(window);
    document.body.appendChild(error_list.container);
};

OutpostClient.prototype['initData'] = function(blob) {
    return new Promise(function(resolve, reject) {
        var old_onerror = window.onerror;

        window.onerror = function(msg, url, line, col, err) {
            reject(err + ' (while reading game data)');
            window.onerror = old_onerror;
        };

        loader.loadPack(blob, function(assets_) {
            assets = assets_;
            resolve(null);
            window.onerror = old_onerror;
        });
    });
};

OutpostClient.prototype['initAsm'] = function(wasm_modules) {
    var m = wasm_modules['asmlibs.wasm'];
    if (m == null) {
        throw 'Missing file asmlibs.wasm';
    }

    asm_client = new DynAsm();
    EXPORTS['asm'] = asm_client;
    return asm_client.init(m);
};

OutpostClient.prototype['handoff'] = function(old_canvas, ws, queued_messages) {
    ui_div = util.element('div', ['ui-container']);
    banner = new Banner();
    keyboard = new Keyboard(asm_client);
    dialog = new Dialog(keyboard);
    chat = new ChatWindow();
    music_test = new MusicTest();

    input = new Input();

    initMenus();


    canvas = document.createElement('canvas');

    canvas.addEventListener('webglcontextlost', function(evt) {
        throw 'context lost!';
    });

    asm_client.initClient(canvas.getContext('webgl'), assets);

    // Don't handle any input until the client is inited.
    keyboard.attach(document);
    setupKeyHandler();
    input.handlers.push(new AsmClientInput(asm_client));
    input.attach(document);


    // This should only happen after client init.
    function doResize() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        asm_client.resizeWindow(window.innerWidth, window.innerHeight);
        handleResize(null, ui_div, window.innerWidth, window.innerHeight);
    }
    window.addEventListener('resize', doResize);
    doResize();


    conn = new net.Connection(ws);
    //conn.onOpen = next;   // TODO - probably remove?
    conn.onClose = handleClose;
    conn.onChatUpdate = handleChatUpdate;

    /*
    conn.onSyncStatus = function(new_synced) {
        // The first time the status becomes SYNC_OK, swap out the canvas and
        // start the requestAnimationFrame loop.
        if (new_synced == net.SYNC_OK) {
            document.body.replaceChild(canvas, old_canvas);
            buildUI();
            conn.onSyncStatus = handleSyncStatus;
            window.requestAnimationFrame(frame);
        }
        handleSyncStatus(new_synced);
    };
    */
    conn.onSyncStatus = handleSyncStatus;

    setTimeout(function() {
        document.body.replaceChild(canvas, old_canvas);
        buildUI();
        conn.onSyncStatus = handleSyncStatus;
        window.requestAnimationFrame(frame);
        banner.hide();
    }, 10);

    // NB: create Timing (which sends a Ping) before sendReady().  This ensures
    // the slight delay of loading chunks won't impact the initial ping time.
    timing = new Timing(asm_client, conn);
    timing.scheduleUpdates(2, 5);
    asm_client.conn = conn;
    conn._asm = asm_client;

    conn.sendReady();

    for (var i = 0; i < queued_messages.length; ++i) {
        conn.feedEvent(queued_messages[i]);
    }


    // Set up exports
    EXPORTS['getScreenshot'] = getScreenshot;


    console.log('handoff complete');

    /*
    maybeRegister(info, function() {
        conn.sendLogin(Config.login_name.get(), Config.login_secret.get());

        // Show "Loading World..." banner.
        handleSyncStatus(net.SYNC_LOADING);
        canvas.start();
    });
    */
};

// Initialization helpers

function buildUI() {
    var key_list = $('key-list');

    ui_div.appendChild(key_list);
    ui_div.appendChild(chat.container);
    ui_div.appendChild(banner.container);
    ui_div.appendChild(dialog.container);

    if (Config.show_key_display.get()) {
        var key_display = new KeyDisplay();
        ui_div.appendChild(key_display.container);
        keyboard.monitor = function(down, evt) {
            if (down) {
                key_display.onKeyDown(evt);
            } else {
                key_display.onKeyUp(evt);
            }
        };
    }

    if (!Config.show_controls.get()) {
        key_list.classList.add('hidden');
    }

    banner.show('Loading...', 0, keyboard, function() { return false; });

    document.body.appendChild(ui_div);
}

function initMenus() {
    main_menu = new Menu([
            ['&Instructions', function() {
                dialog.show(new widget.Form(new Iframe('instructions.html', keyboard)));
            }],
            ['&Keyboard Controls', function() {
                dialog.show(new KeybindingEditor(keyboard));
            }],
            ['&Debug Menu', function() { dialog.show(debug_menu); }],
            ['&Credits', function() {
                dialog.show(new widget.Form(new Iframe('credits.html', keyboard)));
            }],
    ]);

    debug_menu = new Menu([
            ['&Config Editor', function() { dialog.show(new ConfigEditor()); }],
            ['&Music Test', function() { dialog.show(music_test); }],
    ]);
}



var INPUT_LEFT =    0x0001;
var INPUT_RIGHT =   0x0002;
var INPUT_UP =      0x0004;
var INPUT_DOWN =    0x0008;
var INPUT_RUN =     0x0010;

var ACTION_USE =        1;
var ACTION_INVENTORY =  2;
var ACTION_USE_ITEM =   3;

function setupKeyHandler() {
    keyboard.pushHandler(function(down, evt) {
        if (down && evt.repeat) {
            return true;
        }

        var shouldStop = alwaysStop(evt);

        var binding = Config.keybindings.get()[evt.keyCode];
        if (binding == null || evt.ctrlKey || evt.altKey || evt.metaKey) {
            return shouldStop;
        }

        if (down) {
            switch (binding) {
                // UI actions
                case 'show_controls':
                    var show = Config.show_controls.toggle();
                    $('key-list').classList.toggle('hidden', !show);
                    break;
                case 'debug_test':
                    if (!evt.shiftKey) {
                        asm_client.debugExport();
                    } else {
                        asm_client.debugImport();
                    }
                    break;
                case 'chat':
                    chat.startTyping(keyboard, conn, '');
                    break;
                case 'chat_command':
                    chat.startTyping(keyboard, conn, '/');
                    break;
                case 'show_menu':
                    dialog.show(main_menu);
                    break;

                default:
                    return asmKeyHandler(asm_client, down, evt) || shouldStop;
            }

            return true;
        } else {
            return asmKeyHandler(asm_client, down, evt) || shouldStop;
        }
    });

    function alwaysStop(evt) {
        // Allow Ctrl + anything
        if (evt.ctrlKey) {
            return false;
        }
        // Allow F5-F12
        if (evt.keyCode >= 111 + 5 && evt.keyCode <= 111 + 12) {
            return false;
        }

        // Stop all other events.
        return true;
    }
}


// Connection message callbacks

function handleClose(evt, reason) {
    var reason_elt = document.createElement('p');
    if (reason != null) {
        reason_elt.textContent = 'Reason: ' + reason;
    }

    var w = new widget.Template('disconnected', {'reason': reason_elt});
    var f = new widget.Form(w);
    f.oncancel = function() {};
    dialog.show(f);
}

function handleChatUpdate(msg) {
    chat.addMessage(msg);
}

function handleMainInventory(iid) {
    asm_client.inventoryMainId(iid);
}

function handleAbilityInventory(iid) {
    asm_client.inventoryAbilityId(iid);
}

function handleSyncStatus(new_synced) {
    synced = new_synced;
    if (synced == net.SYNC_REFRESH) {
        window.location.reload(true);
    } else if (synced == net.SYNC_LOADING) {
        banner.show('Loading World...', 0, keyboard, function() { return false; });
    } else if (synced == net.SYNC_RESET) {
        banner.show('Server restarting...', 0, keyboard, function() { return false; });

        if (synced == net.SYNC_RESET) {
            resetAll();
        }
    } else {
        banner.hide();
    }
}

// Reset (nearly) all client-side state to pre-login conditions.
function resetAll() {
    if (dialog.isVisible()) {
        dialog.hide();
    }

    asm_client.resetClient();
}


// Rendering

function frame(fine_now) {
    if (synced != net.SYNC_OK) {
        return;
    }

    asm_client.renderFrame();

    window.requestAnimationFrame(frame);
}

function getScreenshot(callback) {
    asm_client.renderFrame();
    return canvas.toBlob(callback, 'image/png');
}
