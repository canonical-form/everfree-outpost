use super::{V2, V3};
use {TILE_BITS, CHUNK_BITS};

impl V3 {
    pub fn px_to_tile(self) -> V3 {
        self >> TILE_BITS
    }

    pub fn tile_to_px(self) -> V3 {
        self << TILE_BITS
    }

    pub fn tile_to_cpos(self) -> V2 {
        self.reduce() >> CHUNK_BITS
    }

    pub fn tile_to_cpos_offset(self) -> (V2, V3) {
        (self.reduce() >> CHUNK_BITS,
         self & ((1 << CHUNK_BITS) - 1))
    }

    pub fn from_cpos_offset(cpos: V2, offset: V3) -> V3 {
        cpos.cpos_to_tile() + offset
    }

    pub fn px_to_cpos(self) -> V2 {
        self.px_to_tile().tile_to_cpos()
    }
}

impl V2 {
    pub fn cpos_to_tile(self) -> V3 {
        self.extend(0) << CHUNK_BITS
    }

    pub fn cpos_to_px(self) -> V3 {
        self.cpos_to_tile().tile_to_px()
    }
}
