use std::cmp::{min, max};
use std::fmt;
use std::ops::{Add, Sub, Mul, Div, Rem, Neg, Shl, Shr, BitAnd, BitOr, BitXor, Not};
use syntax_exts::for_each_obj_named;


mod ext;
mod region;

pub use self::region::{Region, Region2, Region3, RegionPoints, Align};


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Axis {
    X,
    Y,
    Z,
}

pub type DirAxis = (Axis, bool);

#[allow(non_snake_case, non_upper_case_globals)]
pub const PosX: DirAxis = (Axis::X, false);
#[allow(non_snake_case, non_upper_case_globals)]
pub const PosY: DirAxis = (Axis::Y, false);
#[allow(non_snake_case, non_upper_case_globals)]
pub const PosZ: DirAxis = (Axis::Z, false);
#[allow(non_snake_case, non_upper_case_globals)]
pub const NegX: DirAxis = (Axis::X, true);
#[allow(non_snake_case, non_upper_case_globals)]
pub const NegY: DirAxis = (Axis::Y, true);
#[allow(non_snake_case, non_upper_case_globals)]
pub const NegZ: DirAxis = (Axis::Z, true);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct V3 {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl fmt::Debug for V3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        (self.x, self.y, self.z).fmt(f)
    }
}

impl V3 {
    #[inline]
    pub fn new(x: i32, y: i32, z: i32) -> V3 {
        V3 { x: x, y: y, z: z }
    }

    #[inline]
    pub fn with_x(self, val: i32) -> V3 {
        self.with(Axis::X, val)
    }

    #[inline]
    pub fn with_y(self, val: i32) -> V3 {
        self.with(Axis::Y, val)
    }

    #[inline]
    pub fn with_z(self, val: i32) -> V3 {
        self.with(Axis::Z, val)
    }

    #[inline]
    pub fn reduce(self) -> V2 {
        V2::new(self.x, self.y)
    }

    #[inline]
    pub fn cross(self, other: V3) -> V3 {
        V3::new(self.y * other.z - self.z * other.y,
                self.z * other.x - self.x * other.z,
                self.x * other.y - self.y * other.z)
    }
}

impl Vn for V3 {
    type Axis = Axis;

    #[inline]
    fn unfold<T, F: FnMut(Axis, T) -> (i32, T)>(val: T, mut f: F) -> (V3, T) {
        let (x, val) = f(Axis::X, val);
        let (y, val) = f(Axis::Y, val);
        let (z, val) = f(Axis::Z, val);
        (V3::new(x, y, z), val)
    }

    #[inline]
    fn get(self, axis: Axis) -> i32 {
        match axis {
            Axis::X => self.x,
            Axis::Y => self.y,
            Axis::Z => self.z,
        }
    }

    #[inline]
    fn fold_axes<T, F: FnMut(Axis, T) -> T>(val: T, mut f: F) -> T {
        let val = f(Axis::X, val);
        let val = f(Axis::Y, val);
        let val = f(Axis::Z, val);
        val
    }
}

impl From<i32> for V3 {
    fn from(x: i32) -> V3 { scalar(x) }
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Axis2 {
    X,
    Y,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct V2 {
    pub x: i32,
    pub y: i32,
}

impl fmt::Debug for V2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        (self.x, self.y).fmt(f)
    }
}

impl V2 {
    #[inline]
    pub fn new(x: i32, y: i32) -> V2 {
        V2 { x: x, y: y }
    }

    #[inline]
    pub fn with_x(self, val: i32) -> V2 {
        self.with(Axis2::X, val)
    }

    #[inline]
    pub fn with_y(self, val: i32) -> V2 {
        self.with(Axis2::Y, val)
    }

    #[inline]
    pub fn extend(self, val: i32) -> V3 {
        V3::new(self.x, self.y, val)
    }

    #[inline]
    pub fn cross(self, other: V2) -> i32 {
        self.x * other.y - self.y * other.x
    }
}

impl Vn for V2 {
    type Axis = Axis2;

    #[inline]
    fn unfold<T, F: FnMut(Axis2, T) -> (i32, T)>(val: T, mut f: F) -> (V2, T) {
        let (x, val) = f(Axis2::X, val);
        let (y, val) = f(Axis2::Y, val);
        (V2::new(x, y), val)
    }

    #[inline]
    fn get(self, axis: Axis2) -> i32 {
        match axis {
            Axis2::X => self.x,
            Axis2::Y => self.y,
        }
    }

    #[inline]
    fn fold_axes<T, F: FnMut(Axis2, T) -> T>(val: T, mut f: F) -> T {
        let val = f(Axis2::X, val);
        let val = f(Axis2::Y, val);
        val
    }
}

impl From<i32> for V2 {
    fn from(x: i32) -> V2 { scalar(x) }
}


pub trait Vn:
    Sized + Copy +
    From<i32> +
    Add<Self, Output=Self> +
    Sub<Self, Output=Self> +
    Mul<Self, Output=Self> +
    Div<Self, Output=Self> +
    Rem<Self, Output=Self> +
    Neg<Output=Self> +
    Shl<usize, Output=Self> +
    Shr<usize, Output=Self> +
    BitAnd<Self, Output=Self> +
    BitOr<Self, Output=Self> +
    BitXor<Self, Output=Self> +
    Not<Output=Self>
{
    type Axis: Eq+Copy;

    fn unfold<T, F: FnMut(Self::Axis, T) -> (i32, T)>(val: T, f: F) -> (Self, T);
    fn get(self, axis: <Self as Vn>::Axis) -> i32;
    fn fold_axes<T, F: FnMut(Self::Axis, T) -> T>(init: T, f: F) -> T;

    #[inline]
    fn from_fn<F: FnMut(<Self as Vn>::Axis) -> i32>(mut f: F) -> Self {
        <Self as Vn>::unfold((), |a, ()| (f(a), ())).0
    }

    #[inline]
    fn on_axis(axis: <Self as Vn>::Axis, mag: i32) -> Self {
        <Self as Vn>::from_fn(|a| if a == axis { mag } else { 0 })
    }

    #[inline]
    fn on_dir_axis(dir_axis: (<Self as Vn>::Axis, bool), mag: i32) -> Self {
        let (axis, neg) = dir_axis;
        <Self as Vn>::on_axis(axis, if neg { -mag } else { mag })
    }

    #[inline]
    fn map<F: FnMut(i32) -> i32>(self, mut f: F) -> Self {
        <Self as Vn>::from_fn(|a| f(self.get(a)))
    }

    #[inline]
    fn zip<F: FnMut(i32, i32) -> i32>(self, other: Self, mut f: F) -> Self {
        <Self as Vn>::from_fn(|a| f(self.get(a), other.get(a)))
    }

    #[inline]
    fn zip3<F: FnMut(i32, i32, i32) -> i32>(self, other1: Self, other2: Self, mut f: F) -> Self {
        <Self as Vn>::from_fn(|a| f(self.get(a), other1.get(a), other2.get(a)))
    }

    #[inline]
    fn dot(self, other: Self) -> i32 {
        <Self as Vn>::fold_axes(0, |a, sum| sum + self.get(a) * other.get(a))
    }

    #[inline]
    fn mag2(self) -> i32 {
        self.dot(self)
    }

    #[inline]
    fn get_dir(self, dir_axis: (<Self as Vn>::Axis, bool)) -> i32 {
        let (axis, neg) = dir_axis;
        if neg { -self.get(axis) } else { self.get(axis) }
    }

    #[inline]
    fn get_if_pos(self, dir_axis: (<Self as Vn>::Axis, bool)) -> i32 {
        let (axis, neg) = dir_axis;
        if neg { 0 } else { self.get(axis) }
    }

    #[inline]
    fn only(self, axis: <Self as Vn>::Axis) -> Self {
        <Self as Vn>::on_axis(axis, self.get(axis))
    }

    #[inline]
    fn abs(self) -> Self {
        self.map(|x| x.abs())
    }

    #[inline]
    fn signum(self) -> Self {
        self.map(|x| x.signum())
    }

    #[inline]
    fn is_positive(self) -> Self {
        self.map(|x| (x > 0) as i32)
    }

    #[inline]
    fn is_negative(self) -> Self {
        self.map(|x| (x < 0) as i32)
    }

    #[inline]
    fn is_zero(self) -> Self {
        self.map(|x| (x == 0) as i32)
    }

    #[inline]
    fn choose(self, a: Self, b: Self) -> Self {
        self.zip3(a, b, |x, a, b| if x != 0 { a } else { b })
    }

    #[inline]
    fn clamp(self, low: i32, high: i32) -> Self {
        self.map(|x| max(low, min(high, x)))
    }

    #[inline]
    fn with(self, axis: <Self as Vn>::Axis, mag: i32) -> Self {
        <Self as Vn>::from_fn(|a| if a == axis { mag } else { self.get(a) })
    }

    #[inline(always)]
    fn div_floor(self, other: impl Into<Self>) -> Self {
        self.zip(other.into(), |a, b| div_floor(a, b))
    }

    #[inline(always)]
    fn mod_floor(self, other: impl Into<Self>) -> Self {
        self.zip(other.into(), |a, b| mod_floor(a, b))
    }

    #[inline]
    fn min(self) -> i32 {
        <Self as Vn>::fold_axes(None, |a, x| {
            match x {
                None => Some(self.get(a)),
                Some(x) => Some(min(x, self.get(a))),
            }
        }).unwrap()
    }

    #[inline]
    fn max(self) -> i32 {
        <Self as Vn>::fold_axes(None, |a, x| {
            match x {
                None => Some(self.get(a)),
                Some(x) => Some(max(x, self.get(a))),
            }
        }).unwrap()
    }

    #[inline]
    fn add(self, other: Self) -> Self {
        self.zip(other, |a, b| a + b)
    }

    #[inline]
    fn sub(self, other: Self) -> Self {
        self.zip(other, |a, b| a - b)
    }

    #[inline]
    fn mul(self, other: Self) -> Self {
        self.zip(other, |a, b| a * b)
    }

    #[inline]
    fn div(self, other: Self) -> Self {
        self.zip(other, |a, b| a / b)
    }

    #[inline]
    fn rem(self, other: Self) -> Self {
        self.zip(other, |a, b| a % b)
    }

    #[inline]
    fn neg(self) -> Self {
        self.map(|x| -x)
    }

    #[inline]
    fn shl(self, amount: usize) -> Self {
        self.map(|x| x << amount)
    }

    #[inline]
    fn shr(self, amount: usize) -> Self {
        self.map(|x| x >> amount)
    }

    #[inline]
    fn bitand(self, other: Self) -> Self {
        self.zip(other, |a, b| a & b)
    }

    #[inline]
    fn bitor(self, other: Self) -> Self {
        self.zip(other, |a, b| a | b)
    }

    #[inline]
    fn bitxor(self, other: Self) -> Self {
        self.zip(other, |a, b| a ^ b)
    }

    #[inline]
    fn not(self) -> Self {
        self.map(|x| !x)
    }
}

#[inline]
fn div_floor(a: i32, b: i32) -> i32 {
    if b < 0 {
        return div_floor(-a, -b);
    }

    // In the common case (dividing by a power-of-two constant), we'd like this to turn into a
    // single right-shift instruction.
    if (b as u32).is_power_of_two() {
        let bits = b.trailing_zeros();
        return a >> bits;
    }

    if a < 0 {
        (a - (b - 1)) / b
    } else {
        a / b
    }
}

#[inline]
fn mod_floor(a: i32, b: i32) -> i32 {
    let m = a % b;
    if m < 0 {
        m + b
    } else {
        m
    }
}

#[inline]
pub fn scalar<V: Vn>(w: i32) -> V {
    <V as Vn>::from_fn(|_| w)
}


macro_rules! impl_Vn_binop {
    ($vec:ty, $op:ident, $method:ident) => {
        impl $op<$vec> for $vec {
            type Output = $vec;
            #[inline]
            fn $method(self, other: $vec) -> $vec {
                <$vec as Vn>::$method(self, other)
            }
        }

        impl $op<i32> for $vec {
            type Output = $vec;
            #[inline]
            fn $method(self, other: i32) -> $vec {
                <$vec as Vn>::$method(self, scalar(other))
            }
        }

        impl $op<$vec> for i32 {
            type Output = $vec;
            #[inline]
            fn $method(self, other: $vec) -> $vec {
                <$vec as Vn>::$method(scalar(self), other)
            }
        }
    };
}

macro_rules! impl_Vn_unop {
    ($vec:ty, $op:ident, $method:ident) => {
        impl $op for $vec {
            type Output = $vec;
            #[inline]
            fn $method(self) -> $vec {
                <$vec as Vn>::$method(self)
            }
        }
    };
}

macro_rules! impl_Vn_shift_op {
    ($vec:ty, $op:ident, $method:ident) => {
        impl $op<usize> for $vec {
            type Output = $vec;
            #[inline]
            fn $method(self, amount: usize) -> $vec {
                <$vec as Vn>::$method(self, amount)
            }
        }
    };
}

macro_rules! impl_Vn_ops {
    ($vec:ty) => {
        impl_Vn_binop!($vec, Add, add);
        impl_Vn_binop!($vec, Sub, sub);
        impl_Vn_binop!($vec, Mul, mul);
        impl_Vn_binop!($vec, Div, div);
        impl_Vn_binop!($vec, Rem, rem);
        impl_Vn_unop!($vec, Neg, neg);

        impl_Vn_shift_op!($vec, Shl, shl);
        impl_Vn_shift_op!($vec, Shr, shr);

        impl_Vn_binop!($vec, BitAnd, bitand);
        impl_Vn_binop!($vec, BitOr, bitor);
        impl_Vn_binop!($vec, BitXor, bitxor);
        impl_Vn_unop!($vec, Not, not);
    };
}


impl_Vn_ops!(V3);
impl_Vn_ops!(V2);

for_each_obj_named! { { v2; v3; }
    impl PartialEq<i32> for $Obj {
        fn eq(&self, other: &i32) -> bool {
            *self == scalar::<$Obj>(*other)
        }

        fn ne(&self, other: &i32) -> bool {
            *self != scalar::<$Obj>(*other)
        }
    }

    impl PartialEq<$Obj> for i32 {
        fn eq(&self, other: &$Obj) -> bool {
            scalar::<$Obj>(*self) == *other
        }

        fn ne(&self, other: &$Obj) -> bool {
            scalar::<$Obj>(*self) != *other
        }
    }
}
