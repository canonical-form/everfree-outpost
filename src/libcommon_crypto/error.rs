use std::result;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Error {
    ScalarMult,
    ScalarMultBase,
    AeadEncrypt,
    AeadDecrypt,
    Noise(&'static str),
}

pub type Result<T> = result::Result<T, Error>;


