use types::*;
use libcommon_proto::ExtraArg;
use libcommon_proto::types::LocalPos;
use libphysics::{TILE_BITS, TILE_SIZE, CHUNK_BITS, LOCAL_BITS};

use engine::Engine;
use util::StrResult;
use world::object::*;


pub fn interact(eng: &mut Engine, cid: ClientId, args: Option<ExtraArg>) {
    warn_on_err!(eng.script_hooks.call_client_interact(eng, cid, args));
}

pub fn use_item(eng: &mut Engine, cid: ClientId, item_id: ItemId, args: Option<ExtraArg>) {
    warn_on_err!(eng.script_hooks.call_client_use_item(eng, cid, item_id, args));
}

pub fn use_ability(eng: &mut Engine, cid: ClientId, item_id: ItemId, args: Option<ExtraArg>) {
    warn_on_err!(eng.script_hooks.call_client_use_ability(eng, cid, item_id, args));
}


fn convert_pos_inner(eng: &mut Engine, cid: ClientId, pos: LocalPos) -> StrResult<V3> {
    let c = unwrap!(eng.world.get_client(cid));
    let e = unwrap!(c.pawn());
    let pos = pos.to_global_bits(e.pos(eng.now),
                                 TILE_BITS + CHUNK_BITS + LOCAL_BITS);
    Ok(pos)
}

// TODO: this is kind of ugly
fn convert_pos(eng: &mut Engine, cid: ClientId, pos: LocalPos) -> Option<V3> {
    match convert_pos_inner(eng, cid, pos) {
        Ok(x) => Some(x),
        Err(e) => {
            warn!("convert_pos({:?}): {}",
                  cid,
                  ::std::error::Error::description(&e));
            None
        },
    }
}

pub fn point_interact(eng: &mut Engine, cid: ClientId, pos_px: LocalPos) {
    let pos = unwrap_or!(convert_pos(eng, cid, pos_px)).div_floor(scalar(TILE_SIZE));
    warn_on_err!(eng.script_hooks.call_client_point_interact(eng, cid, pos));
}

pub fn point_destroy(eng: &mut Engine, cid: ClientId, pos_px: LocalPos) {
    let pos = unwrap_or!(convert_pos(eng, cid, pos_px)).div_floor(scalar(TILE_SIZE));
    warn_on_err!(eng.script_hooks.call_client_point_destroy(eng, cid, pos));
}

pub fn point_use_item(eng: &mut Engine, cid: ClientId, pos_px: LocalPos, item_id: ItemId) {
    let pos = unwrap_or!(convert_pos(eng, cid, pos_px)).div_floor(scalar(TILE_SIZE));
    warn_on_err!(eng.script_hooks.call_client_point_use_item(eng, cid, pos, item_id));
}


pub fn open_inventory(_eng: &mut Engine, cid: ClientId) {
    error!("UNIMPLEMENTED: open_inventory - called by {:?}", cid);
}

pub fn chat(eng: &mut Engine, cid: ClientId, msg: String) {
    if msg.starts_with("/") && !msg.starts_with("/l ") {
        warn_on_err!(eng.script_hooks.call_client_chat_command(eng, cid, &msg));
        return;
    }

    let (msg, local) =
        if msg.starts_with("/l ") { (&msg[3..], true) }
        else { (&msg as &str, false) };
    if msg.len() > 400 {
        warn!("{:?}: bad request: chat message too long ({})", cid, msg.len());
        return;
    }

    if !local {
        // TODO: use an engine_part!()?
        eng.chat.send_global(&eng.world, &mut eng.messages, cid, msg);
    } else {
        eng.chat.send_local(&eng.world, &mut eng.messages, cid, msg);
    }
}
