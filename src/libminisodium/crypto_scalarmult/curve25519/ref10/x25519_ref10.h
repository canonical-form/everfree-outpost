#ifndef x25519_ref10_H
#define x25519_ref10_H

int
crypto_scalarmult_curve25519_ref10(unsigned char *q,
                                   const unsigned char *n,
                                   const unsigned char *p);
int
crypto_scalarmult_curve25519_ref10_base(unsigned char *q,
                                        const unsigned char *n);

#endif
