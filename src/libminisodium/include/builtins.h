#ifndef MINISODIUM_BUILTINS
#define MINISODIUM_BUILTINS

#include <stddef.h>

void abort(void) __attribute__((noreturn));
void *memcpy(void *dest, const void *src, size_t n);
void *memset(void *s, int c, size_t n);

#endif // MINISODIUM_BUILTINS
