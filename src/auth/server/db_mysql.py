import MySQLdb
import MySQLdb.constants.ER

class Database:
    def __init__(self, cfg):
        args = ()
        if cfg['db_connstr'] is not None:
            args += (cfg['db_connstr'],)

        kwargs = {
            'db': cfg['db_name'],
            'user': cfg['db_user'],
            'passwd': cfg['db_pass'],
        }
        if cfg['db_host'] is not None:
            kwargs['host'] = cfg['db_host']

        self._db_args = args
        self._db_kwargs = kwargs
        self._connect()

    def _connect(self):
        self.db = MySQLdb.connect(*self._db_args, **self._db_kwargs)

    def _refresh(self):
        try:
            self.db.ping()
        except MySQLdb.OperationalError as e:
            print('ping() received %s; reconnecting...' % (e,))
            self._connect()

    def next_id(self):
        self._refresh()

        # This needs to be a single transaction.  This code follows the new way
        # of doing things, since newer versions remove the ability to use the
        # connection object as a context manager to handle transactions
        # (https://stackoverflow.com/a/8074341).
        try:
            curs = self.db.cursor()
            curs.execute('SELECT value FROM counter;')
            uid, = curs.fetchone()
            curs.execute('UPDATE counter SET value = %s;', (uid + 1,))
        except:
            self.db.rollback()
            raise
        else:
            self.db.commit()
        return uid

    def lookup_user(self, name):
        self._refresh()
        with self.db as curs:
            curs.execute('SELECT id, name, password FROM users '
                    'WHERE name_lower = %s;',
                    (name.lower(),))
            rows = curs.fetchall()
        if len(rows) == 0:
            return None
        elif len(rows) == 1:
            return rows[0]
        else:
            assert False, 'UNIQUE constraint should forbid >1 row in result'

    def lookup_user_by_id(self, uid):
        self._refresh()
        with self.db as curs:
            curs.execute('SELECT name, password FROM users '
                    'WHERE id = %s;',
                    (uid,))
            rows = curs.fetchall()
        if len(rows) == 0:
            return None
        elif len(rows) == 1:
            return rows[0]
        else:
            assert False, 'UNIQUE constraint should forbid >1 row in result'

    def user_email(self, uid):
        self._refresh()
        with self.db as curs:
            curs.execute('SELECT email FROM users '
                    'WHERE id = %s;',
                    (uid,))
            rows = curs.fetchall()
        if len(rows) == 0:
            return None
        elif len(rows) == 1:
            return rows[0]
        else:
            assert False, 'UNIQUE constraint should forbid >1 row in result'

    def register(self, uid, name, pass_hash, email):
        self._refresh()
        try:
            with self.db as curs:
                curs.execute('INSERT INTO users (id, name, name_lower, password, email) '
                    'VALUES (%s, %s, %s, %s, %s)',
                    (uid, name, name.lower(), pass_hash, email))
                return True
        except MySQLdb.IntegrityError as e:
            if e.args[0] == MySQLdb.constants.ER.DUP_ENTRY:
                return False
            else:
                raise

    def reset_password(self, uid, pass_hash):
        with self.db as curs:
            curs.execute('UPDATE users '
                'SET password = %s '
                'WHERE id = %s',
                (pass_hash, uid))
