import configparser
import functools
import inspect
import json
import os
import re
import struct
import sys
import time

import bcrypt
import nacl.exceptions
import nacl.secret
import nacl.signing
from nacl.encoding import RawEncoder, URLSafeBase64Encoder

from server import db


def get_pwreset_key():
    key = os.environ.get('OUTPOST_AUTH_PWRESET_KEY') or input('pwreset secret key: ')
    sk = nacl.signing.SigningKey(URLSafeBase64Encoder.decode(key.encode('ascii')))
    return sk

if __name__ == '__main__':
    name, = sys.argv[1:]
    try:
        uid = int(name)
        name, pw_hash = db.lookup_user_by_id(uid)
    except ValueError:
        uid, name, pw_hash = db.lookup_user(name)

    key = get_pwreset_key()

    print('uid = %d' % uid)
    print('name = %r' % name)

    email, = db.user_email(uid)
    print('email = %r' % email)

    lifetime = 86400 * 7
    expire_time = int(time.time()) + lifetime
    print('token expires at %d (in %d seconds, or %.1f days)' %
            (expire_time, lifetime, lifetime / 86400))

    fmt_bytes = struct.pack('<IQ', uid, expire_time)
    signed_bytes = fmt_bytes + pw_hash.encode('ascii')
    sig_bytes = key.sign(signed_bytes).signature

    token = URLSafeBase64Encoder.encode(fmt_bytes + sig_bytes).decode('ascii')
    print('token = %s' % token)


