//! Orphan impls of `multipython` conversion traits.

#![feature(
    optin_builtin_traits,
    proc_macro_hygiene,
)]

extern crate common;
#[macro_use] extern crate multipython;
extern crate physics;
extern crate server_config;
extern crate server_extra;
extern crate server_types;
extern crate server_world_types;
extern crate syntax_exts;

mod attach;
mod data;
mod extra;
mod id;
mod v3;

use std::collections::HashMap;
use std::hash::Hash;
use multipython::{PyBox, PyRef, PyResult};
use multipython::Interp;
use multipython::api as py;


python_conv_definitions!();

pub struct ServerTypes {
    v3: v3::Types,
    id: id::Types,
    attach: attach::Types,
}

impl ServerTypes {
    pub fn new() -> ServerTypes {
        // TODO: proper error handling
        ServerTypes {
            v3: v3::register().unwrap(),
            id: id::register().unwrap(),
            attach: attach::register().unwrap(),
        }
    }
}


/// Add all Python types defined in this crate as attributes of `m`.
pub fn init_module(m: PyRef) -> PyResult<()> {
    let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                        runtime_error, "ServerTypes not available");
    v3::init_module(m, &tys.v3)?;
    id::init_module(m, &tys.id)?;
    attach::init_module(m, &tys.attach)?;
    Ok(())
}
