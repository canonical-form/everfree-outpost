use common::types::*;
use common::util::Bytes;
use multipython::{PyBox, PyRef, PyResult};
use multipython::Interp;
use multipython::api as py;
use server_extra::Value;
use server_types::Stable;
use syntax_exts::expand_objs_named;

use {Pack, Unpack};
use ServerTypes;


impl Pack for Value {
    fn pack(self) -> PyResult<PyBox> {
        expand_objs_named! {
            {
                client, clients, Raw = u16;
                entity, entities, Raw = u32;
                inventory, inventories, Raw = u32;
                plane, planes, Raw = u32;
                terrain_chunk, terrain_chunks, Raw = u32;
                structure, structures, Raw = u32;
            }

            match self {
                Value::Null => Ok(py::none().to_box()),
                Value::Bool(b) => Ok(py::bool::from_bool(b).to_box()),
                Value::Int(i) => py::int::from_i64(i),
                Value::Float(f) => py::float::from_f64(f),
                Value::Str(s) => py::unicode::from_str(&s),
                Value::Bytes(b) => py::bytes::from_bytes(&b),

                $(
                    Value::$ObjId(id) => <$ObjId as Pack>::pack(id),
                    Value::$StableObjId(id) => <Stable<$ObjId> as Pack>::pack(id),
                )*

                Value::V2(v) => <V2 as Pack>::pack(v),
                Value::V3(v) => <V3 as Pack>::pack(v),
                Value::Region2(r) => <Region<V2> as Pack>::pack(r),
                Value::Region3(r) => <Region<V3> as Pack>::pack(r),
            }
        }
    }
}

impl Unpack for Value {
    fn unpack(obj: PyRef) -> PyResult<Value> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");

        expand_objs_named! {
            {
                client, clients, Raw = u16;
                entity, entities, Raw = u32;
                inventory, inventories, Raw = u32;
                plane, planes, Raw = u32;
                terrain_chunk, terrain_chunks, Raw = u32;
                structure, structures, Raw = u32;
            }

            if obj == py::none() {
                return Ok(Value::Null);
            }
            if py::bool::check(obj) {
                return py::bool::as_bool(obj).map(Value::Bool);
            }
            if py::int::check(obj) {
                return py::int::as_i64(obj).map(Value::Int);
            }
            if py::float::check(obj) {
                return py::float::as_f64(obj).map(Value::Float);
            }
            if py::unicode::check(obj) {
                return py::unicode::as_string(obj).map(Value::Str);
            }
            if py::bytes::check(obj) {
                return py::bytes::as_bytes(obj).map(Bytes).map(Value::Bytes);
            }

            $(
                if py::object::is_instance(obj, tys.id.$obj_id.borrow()) {
                    return <$ObjId as Unpack>::unpack(obj).map(Value::$ObjId);
                }
                if py::object::is_instance(obj, tys.id.$stable_obj_id.borrow()) {
                    return <Stable<$ObjId> as Unpack>::unpack(obj).map(Value::$StableObjId);
                }
            )*

            if py::object::is_instance(obj, tys.v3.v2.borrow()) {
                return <V2 as Unpack>::unpack(obj).map(Value::V2);
            }
            if py::object::is_instance(obj, tys.v3.v3.borrow()) {
                return <V3 as Unpack>::unpack(obj).map(Value::V3);
            }
            if py::object::is_instance(obj, tys.v3.region2.borrow()) {
                return <Region<V2> as Unpack>::unpack(obj).map(Value::Region2);
            }
            if py::object::is_instance(obj, tys.v3.region3.borrow()) {
                return <Region<V3> as Unpack>::unpack(obj).map(Value::Region3);
            }

            pyraise!(type_error, "expected any Value type");
        }
    }
}
