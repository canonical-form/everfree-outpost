use server_config::data::RecipeItem;
use multipython::{PyBox, PyResult}; 

use Pack;


impl Pack for RecipeItem {
    fn pack(self) -> PyResult<PyBox> {
        Pack::pack((self.item, self.quantity))
    }
}
