#![crate_name = "client_data"]

#[macro_use] extern crate bitflags;
#[macro_use] extern crate log;

extern crate common;


use std::mem;
use std::ops::Deref;
use std::ops::Range;
use std::slice;
use std::str;

use common::data::{self, Section, ChdParams, chd_lookup};
use common::types::{AnimId, BlockFlags, BlockId, TemplateId, TerrainId};
use common::types::{V3, V2};
use common::util::ByteCast;

pub mod ui;


#[derive(Clone, Copy, Debug)]
#[repr(C, align(4))]
pub struct ExtInfo {
    raw: [u8; 4],
}
unsafe impl ByteCast for ExtInfo {}

impl ExtInfo {
    pub fn tag(&self) -> u8 {
        self.raw[0]
    }

    pub fn args(&self) -> (u8, u8, u8) {
        (self.raw[1],
         self.raw[2],
         self.raw[3])
    }

    pub fn args_u16(&self) -> (u8, u16) {
        let a = self.raw[1];
        let b = unsafe {
            *(&self.raw[2] as *const u8 as *const u16)
        };
        (a, b)
    }
}


bitflags! {
    #[repr(C)]
    pub flags BlockDisplayFlags: u8 {
        const HAS_TERRAIN =     0x01,
        const HAS_STRUCTURE =   0x02,
    }
}

/// Tile numbers used to display a particular block.
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct BlockDef {
    flags: u16,
    display_flags: u8,

    pub terrain: TerrainId,
    pub structure: TemplateId,
}
unsafe impl ByteCast for BlockDef {}

impl BlockDef {
    pub fn flags(&self) -> BlockFlags {
        BlockFlags::from_bits_truncate(self.flags)
    }

    pub fn display_flags(&self) -> BlockDisplayFlags {
        BlockDisplayFlags::from_bits_truncate(self.display_flags)
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TerrainDef {
    pub priority: u8,
    has_alt: u8,
    pub index: u16,
}
unsafe impl ByteCast for TerrainDef {}

impl TerrainDef {
    pub fn has_alt(&self) -> bool {
        self.has_alt != 0
    }
}


bitflags! {
    pub flags ItemFlags: u16 {
        const I_USE_AT_POINT =      0x0001,
        const I_USE_ON_SELF =       0x0002,
    }
}
// TODO: this impl is bogus, only 0x00 - 0x01 are valid bit patterns
unsafe impl ByteCast for ItemFlags {}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct RawItemDef {
    pub name_off: u32,
    pub name_len: u32,
    pub ui_name_off: u32,
    pub ui_name_len: u32,
    pub desc_off: u32,
    pub desc_len: u32,
    pub image_pos: (u16, u16),
    pub flags: ItemFlags,
}
unsafe impl ByteCast for RawItemDef {}

impl RawItemDef {
    pub fn image_pos(&self) -> V2 {
        V2::new(self.image_pos.0 as i32,
                self.image_pos.1 as i32)
    }
}

pub struct ItemDef<'a> {
    def: &'a RawItemDef,
    data: &'a Data,
}

impl<'a> Deref for ItemDef<'a> {
    type Target = RawItemDef;
    fn deref(&self) -> &RawItemDef {
        self.def
    }
}

impl<'a> ItemDef<'a> {
    pub fn name(&self) -> &'a str {
        self.data.string_slice(self.def.name_off as usize,
                               self.def.name_len as usize)
    }

    pub fn ui_name(&self) -> &'a str {
        self.data.string_slice(self.def.ui_name_off as usize,
                               self.def.ui_name_len as usize)
    }

    pub fn desc(&self) -> &'a str {
        self.data.string_slice(self.def.desc_off as usize,
                               self.def.desc_len as usize)
    }
}


bitflags! {
    pub flags TemplateFlags: u8 {
        const HAS_SHADOW =      0x01,
        const HAS_ANIM =        0x02,
        const HAS_LIGHT =       0x04,
    }
}
// TODO: this impl is bogus, only 0x00 - 0x07 are valid bit patterns
unsafe impl ByteCast for TemplateFlags {}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct StructureTemplate {
    pub size: (u8, u8, u8),
    pub layer: u8,
    pub shape_idx: u16,
    pub part_idx: u16,
    pub part_count: u16,
    pub flags: TemplateFlags,
    draw_mode: u8,
    draw_args: [u8; 4],

    // TODO: would be nice to have some sanity checking on `swizzle` - values should be in the
    // range indicated by part_idx + part_count.
    swizzle_off: u16,
    swizzle_len: u16,

    depth_mask_off: u16,
    depth_mask_len: u16,

    pub light_pos: (u8, u8, u8),
    pub light_color: (u8, u8, u8),
    pub light_radius: u16,

    ext_info_off: u16,
    ext_info_len: u16,
}
unsafe impl ByteCast for StructureTemplate {}

impl StructureTemplate {
    pub fn size(&self) -> V3 {
        V3::new(self.size.0 as i32,
                self.size.1 as i32,
                self.size.2 as i32)
    }

    pub fn draw_mode(&self) -> DrawMode {
        let args = &self.draw_args;
        match self.draw_mode {
            0 => DrawMode::Normal,
            1 => DrawMode::Fence { cls: args[0], fixed: args[1], use_corners: args[2] != 0 },
            2 => DrawMode::Tree { fixed: args[0] },
            3 => DrawMode::Fence16 { cls: args[0] },
            4 => DrawMode::Door { cls: args[0] },
            _ => panic!("invalid draw mode: {}", self.draw_mode),
        }
    }
}

#[derive(Clone, Copy)]
pub struct StructureTemplateRef<'a> {
    obj: &'a StructureTemplate,
    base: &'a Data,
}

impl<'a> Deref for StructureTemplateRef<'a> {
    type Target = StructureTemplate;
    fn deref(&self) -> &StructureTemplate {
        self.obj
    }
}

impl<'a> StructureTemplateRef<'a> {
    pub fn shape(&self) -> &'a [BlockFlags] {
        let base = self.shape_idx as usize;
        let volume = self.size.0 as usize *
                     self.size.1 as usize *
                     self.size.2 as usize;
        &self.base.template_shapes()[base .. base + volume]
    }

    pub fn swizzle(&self) -> &'a [u8] {
        let off = self.swizzle_off as usize;
        let len = self.swizzle_len as usize;
        &self.base.template_swizzle()[off .. off + len]
    }

    pub fn iter_swizzle(&self) -> TemplatePartIndexes {
        let s = self.swizzle();

        if s.len() == 0 {
            TemplatePartIndexes::Default(0 .. self.part_count as u8)
        } else {
            TemplatePartIndexes::Swizzled(s.iter())
        }
    }

    pub fn depth_mask(&self) -> &'a [u8] {
        let off = self.depth_mask_off as usize;
        let len = self.depth_mask_len as usize;
        &self.base.template_depth_mask()[off .. off + len]
    }

    pub fn ext_info(&self) -> &'a [ExtInfo] {
        let off = self.ext_info_off as usize;
        let len = self.ext_info_len as usize;
        &self.base.ext_info()[off .. off + len]
    }

    pub fn find_ext_info(&self, tag: u8) -> Option<&'a ExtInfo> {
        self.ext_info().iter().find(|x| x.tag() == tag)
    }
}

pub enum TemplatePartIndexes<'a> {
    Default(Range<u8>),
    Swizzled(slice::Iter<'a, u8>),
}

impl<'a> Iterator for TemplatePartIndexes<'a> {
    type Item = usize;
    fn next(&mut self) -> Option<usize> {
        match *self {
            TemplatePartIndexes::Default(ref mut inner) => {
                inner.next().map(|x| x as usize)
            },
            TemplatePartIndexes::Swizzled(ref mut inner) => {
                inner.next().map(|&x| x as usize)
            },
        }
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TemplatePart {
    // 0
    pub vert_idx: u16,
    pub vert_count: u16,
    pub offset: (i16, i16),
    pub sheet: u8,
    pub flags: TemplateFlags,

    // 10
    pub anim_length: i8,
    pub anim_rate: u8,
    pub anim_step: u16,     // x-size of each frame

    // 14
}
unsafe impl ByteCast for TemplatePart {}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TemplateVertex {
    pub x: i16,
    pub y: i16,
    pub z: i16,
}
unsafe impl ByteCast for TemplateVertex {}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum DrawMode {
    Normal,
    Fence { cls: u8, fixed: u8, use_corners: bool },
    Tree { fixed: u8 },
    Fence16 { cls: u8 },
    Door { cls: u8 },
}

impl DrawMode {
    pub fn is_fence(&self, match_cls: u8) -> bool {
        match *self {
            DrawMode::Fence { cls, .. } => cls == match_cls,
            DrawMode::Fence16 { cls, .. } => cls == match_cls,
            _ => false,
        }
    }

    pub fn is_door(&self, match_cls: u8) -> bool {
        match *self {
            DrawMode::Door { cls, .. } => cls == match_cls,
            _ => false,
        }
    }

    pub fn is_tree(&self) -> bool {
        match *self {
            DrawMode::Tree { .. } => true,
            _ => false,
        }
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct RawRecipeDef {
    pub ui_name_off: u32,
    pub ui_name_len: u32,
    pub inputs_off: u16,
    pub inputs_len: u16,
    pub outputs_off: u16,
    pub outputs_len: u16,
    pub ability: u16,
    pub time: u16,
    pub crafting_class: u8,
    _pad0: u8,
}
unsafe impl ByteCast for RawRecipeDef {}

pub struct RecipeDef<'a> {
    def: &'a RawRecipeDef,
    data: &'a Data,
}

impl<'a> RecipeDef<'a> {
    pub fn ui_name(&self) -> &'a str {
        self.data.string_slice(self.def.ui_name_off as usize,
                               self.def.ui_name_len as usize)
    }

    pub fn inputs(&self) -> &'a [RecipeItem] {
        let off = self.def.inputs_off as usize;
        let len = self.def.inputs_len as usize;
        self.data.recipe_item_slice(off, len)
    }

    pub fn outputs(&self) -> &'a [RecipeItem] {
        let off = self.def.outputs_off as usize;
        let len = self.def.outputs_len as usize;
        self.data.recipe_item_slice(off, len)
    }
}

impl<'a> Deref for RecipeDef<'a> {
    type Target = RawRecipeDef;
    fn deref(&self) -> &RawRecipeDef {
        self.def
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct RecipeItem {
    pub item: u16,
    pub quantity: u16,
}
unsafe impl ByteCast for RecipeItem {}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct DayNightPhase {
    pub start_time: u16,
    pub end_time: u16,
    pub start_color: u8,
    pub end_color: u8,
}
unsafe impl ByteCast for DayNightPhase {}


pub struct BlockFlagsArray([BlockFlags]);

unsafe impl Section for BlockFlagsArray {
    unsafe fn from_bytes(ptr: *const u8, len: usize) -> *const BlockFlagsArray {
        assert!(mem::size_of::<u16>() == mem::size_of::<BlockFlags>());
        let raw = slice::from_raw_parts(ptr as *const u16,
                                        len / mem::size_of::<u16>());
        assert!(raw.iter().all(|&x| x & !BlockFlags::all().bits() == 0),
                "found invalid bits in BlockFlags array");
        mem::transmute(raw as *const [u16])
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Sprite {
    pub src_pos: (i16, i16),
    pub dest_pos: (i16, i16),
    pub size: (u16, u16),
    pub sheet: u8,
    pub anim_length_oneshot: i8,
    pub anim_rate: u8,
    pub anim_step: u16,
}
unsafe impl ByteCast for Sprite {}

impl Sprite {
    pub fn src_pos(&self) -> V2 {
        V2::new(self.src_pos.0 as i32,
                self.src_pos.1 as i32)
    }

    pub fn dest_pos(&self) -> V2 {
        V2::new(self.dest_pos.0 as i32,
                self.dest_pos.1 as i32)
    }

    pub fn size(&self) -> V2 {
        V2::new(self.size.0 as i32,
                self.size.1 as i32)
    }

    pub fn anim_length(&self) -> u8 {
        self.anim_length_oneshot.abs() as u8
    }

    pub fn anim_oneshot(&self) -> bool {
        self.anim_length_oneshot < 0
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct LayerSprite {
    pub sprite_id: u16,
    pub flip: u8,
}
unsafe impl ByteCast for LayerSprite {}

#[derive(Clone, Copy)]
pub struct LayerSpriteRef<'a> {
    obj: &'a LayerSprite,
    base: &'a Data,
}

impl<'a> Deref for LayerSpriteRef<'a> {
    type Target = LayerSprite;
    fn deref(&self) -> &LayerSprite {
        self.obj
    }
}

impl<'a> LayerSpriteRef<'a> {
    pub fn sprite(&self) -> &'a Sprite {
        self.base.sprite(self.sprite_id)
    }

    pub fn flip(&self) -> bool {
        self.flip != 0
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct PonyLayer {
    anims_off: u16,
    anims_len: u16,
}
unsafe impl ByteCast for PonyLayer {}

#[derive(Clone, Copy)]
pub struct PonyLayerRef<'a> {
    obj: &'a PonyLayer,
    base: &'a Data,
}

impl<'a> Deref for PonyLayerRef<'a> {
    type Target = PonyLayer;
    fn deref(&self) -> &PonyLayer {
        self.obj
    }
}

impl<'a> PonyLayerRef<'a> {
    pub fn anims(&self) -> &'a [LayerSprite] {
        let off = self.anims_off as usize;
        let len = self.anims_len as usize;
        &self.base.layer_sprites()[off .. off + len]
    }

    pub fn anim(&self, id: AnimId) -> LayerSpriteRef<'a> {
        self.base.layer_sprite(self.anims_off + id)
    }
}


data::gen_data! {
    version = (2, 8);

    strings (b"Strings\0"): str,

    ext_info (b"ExtnInfo"): [ExtInfo],

    blocks (b"Blocks\0\0"): [BlockDef],
    terrains (b"Terrains"): [TerrainDef],
    raw_items (b"Items\0\0\0"): [RawItemDef],

    templates (b"StrcDefs"): [StructureTemplate],
    template_parts (b"StrcPart"): [TemplatePart],
    template_verts (b"StrcVert"): [TemplateVertex],
    _template_shapes (b"StrcShap"): BlockFlagsArray,
    template_swizzle (b"StrcSwzl"): [u8],
    template_depth_mask (b"StrcDpth"): [u8],

    sprites (b"Sprites\0"): [Sprite],
    layer_sprites (b"PonySprt"): [LayerSprite],
    pony_layers (b"PonyLayr"): [PonyLayer],

    recipes (b"RcpeDefs"): [RawRecipeDef],
    recipe_items (b"RcpeItms"): [RecipeItem],

    day_night_phases (b"DyNtPhas"): [DayNightPhase],
    day_night_colors (b"DyNtColr"): [(u8, u8, u8)],

    ui_cards (b"UICard\0\0"): [ui::UICard],
    ui_borders (b"UIBorder"): [ui::UIBorder],

    fonts (b"Fonts\0\0\0"): [ui::RawFont],
    font_spans (b"FontSpan"): [ui::RawFontSpan],
    font_xs (b"FontXs\0\0"): [u16],

    inventory_layouts (b"InvLay\0\0"): [ui::RawInventoryLayout],
    inventory_layout_slots (b"InvLSlot"): [ui::InventoryLayoutSlot],

    pony_layer_table (b"XPonLayr"): [u8],
    physics_anim_table (b"XPhysAnm"): [[u16; 8]],
    anim_dir_table (b"XAnimDir"): [u8],
    special_anims (b"XSpcAnim"): [u16],
    special_layers (b"XSpcLayr"): [u8],
    special_graphics (b"XSpcGrfx"): [u16],

    index_table_items (b"IxTbItem"): [u16],
    index_params_items (b"IxPrItem"): ChdParams<u16>,
}

impl Data {
    fn string_slice(&self, off: usize, len: usize) -> &str {
        &self.strings()[off .. off + len]
    }

    fn recipe_item_slice(&self, off: usize, len: usize) -> &[RecipeItem] {
        &self.recipe_items()[off .. off + len]
    }


    pub fn template_shapes(&self) -> &[BlockFlags] {
        &self._template_shapes().0
    }


    pub fn block(&self, id: BlockId) -> &BlockDef {
        &self.blocks()[id as usize]
    }

    pub fn terrain(&self, id: TerrainId) -> &TerrainDef {
        &self.terrains()[id as usize]
    }


    fn make_item_def<'a>(&'a self, raw: &'a RawItemDef) -> ItemDef<'a> {
        ItemDef {
            def: raw,
            data: self,
        }
    }

    pub fn item_def(&self, id: u16) -> ItemDef {
        self.make_item_def(&self.raw_items()[id as usize])
    }

    pub fn find_item_id(&self, name: &str) -> Option<u16> {
        if let Some(id) = chd_lookup(name, self.index_table_items(), self.index_params_items()) {
            if (id as usize) < self.raw_items().len() && self.item_def(id).name() == name {
                return Some(id)
            }
        }
        None
    }


    pub fn recipe(&self, id: u16) -> RecipeDef {
        RecipeDef {
            def: &self.recipes()[id as usize],
            data: self,
        }
    }

    pub fn get_recipe(&self, id: u16) -> Option<RecipeDef> {
        if (id as usize) < self.recipes().len() {
            Some(self.recipe(id))
        } else {
            None
        }
    }


    pub fn template(&self, id: u32) -> StructureTemplateRef {
        StructureTemplateRef {
            obj: &self.templates()[id as usize],
            base: self,
        }
    }


    pub fn sprite(&self, id: u16) -> &Sprite {
        &self.sprites()[id as usize]
    }

    pub fn layer_sprite(&self, idx: u16) -> LayerSpriteRef {
        LayerSpriteRef {
            obj: &self.layer_sprites()[idx as usize],
            base: self,
        }
    }

    pub fn pony_layer(&self, id: u8) -> PonyLayerRef {
        PonyLayerRef {
            obj: &self.pony_layers()[id as usize],
            base: self,
        }
    }


    pub fn day_night_phase(&self, idx: u8) -> &DayNightPhase {
        &self.day_night_phases()[idx as usize]
    }


    pub fn index_font(&self, idx: usize) -> ui::Font {
        ui::Font::new(self, &self.fonts()[idx])
    }

    pub fn index_inventory_layout(&self, idx: usize) -> ui::InventoryLayout {
        ui::InventoryLayout::new(self, &self.inventory_layouts()[idx])
    }


    pub fn anim_dir(&self, anim: u16) -> Option<u8> {
        match self.anim_dir_table().get(anim as usize) {
            None | Some(&255) => None,
            Some(&x) => Some(x),
        }
    }

    pub fn default_anim(&self) -> u16 {
        self.special_anims()[0]
    }

    pub fn editor_anim(&self) -> u16 {
        self.special_anims()[1]
    }
}
