use std::ops::Deref;
use std::str;

use common::types::{V2, Region};
use common::util::ByteCast;

use Data;


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct UICard {
    pub pos: (u16, u16),
    pub size: (u16, u16),
}
unsafe impl ByteCast for UICard {}

impl UICard {
    pub fn pos(&self) -> V2 {
        V2::new(self.pos.0 as i32,
                self.pos.1 as i32)
    }

    pub fn size(&self) -> V2 {
        V2::new(self.size.0 as i32,
                self.size.1 as i32)
    }

    pub fn bounds(&self) -> Region<V2> {
        Region::sized(self.size()) + self.pos()
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct UIBorder {
    pub pos: (u16, u16),
    pub size: (u16, u16),
    pub inset: (u8, u8, u8, u8),
}
unsafe impl ByteCast for UIBorder {}

impl UIBorder {
    pub fn pos(&self) -> V2 {
        V2::new(self.pos.0 as i32,
                self.pos.1 as i32)
    }

    pub fn size(&self) -> V2 {
        V2::new(self.size.0 as i32,
                self.size.1 as i32)
    }

    pub fn bounds(&self) -> Region<V2> {
        Region::sized(self.size()) + self.pos()
    }

    pub fn inset(&self) -> (i32, i32, i32, i32) {
        (self.inset.0 as i32,
         self.inset.1 as i32,
         self.inset.2 as i32,
         self.inset.3 as i32)
    }

    pub fn min_size(&self) -> V2 {
        let (x0, x1, y0, y1) = self.inset();
        V2::new(x0 + x1, y0 + y1)
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct RawFont {
    spans_off: u16,
    spans_len: u16,
    pub height: u8,
    pub spacing: u8,
    pub space_width: u8,
}
unsafe impl ByteCast for RawFont {}

#[derive(Clone, Copy)]
pub struct Font<'a> {
    def: &'a RawFont,
    data: &'a Data,
}

impl<'a> Font<'a> {
    pub fn new(data: &'a Data, def: &'a RawFont) -> Font<'a> {
        Font {
            def: def,
            data: data,
        }
    }

    fn raw_spans(&self) -> &'a [RawFontSpan] {
        let off = self.def.spans_off as usize;
        let len = self.def.spans_len as usize;
        &self.data.font_spans()[off .. off + len]
    }

    pub fn char_glyph(&self, ch: char) -> Option<Region<V2>> {
        let code = ch as u32;
        for span in self.raw_spans() {
            if span.last_char <= code {
                continue;
            }

            if code < span.first_char() {
                // If spans are properly sorted, then all later `first_char`s will be >=
                // `last_char`, and `last_char > code`.
                break;
            }

            // Now we know `first_char <= code < last_char`.

            let idx = (code - span.first_char()) as usize;
            let xs = span.data_xs(self.data);

            let pos = span.pos() + V2::new(xs[idx] as i32, 0);
            let size = V2::new((xs[idx + 1] - xs[idx]) as i32,
                               self.height as i32);
            return Some(Region::sized(size) + pos);
        }

        None
    }

    pub fn iter_glyphs<'b>(&self, s: &'b str) -> GlyphIter<'a, 'b> {
        GlyphIter {
            font: *self,
            chars: s.chars(),
            x: 0,
        }
    }
}

impl<'a> Deref for Font<'a> {
    type Target = RawFont;
    fn deref(&self) -> &RawFont {
        self.def
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct RawFontSpan {
    pub last_char: u32,
    pub len: u16,
    xs_off: u16,
    pub pos: (u16, u16),
}
unsafe impl ByteCast for RawFontSpan {}

impl RawFontSpan {
    pub fn pos(&self) -> V2 {
        V2::new(self.pos.0 as i32,
                self.pos.1 as i32)
    }

    pub fn first_char(&self) -> u32 {
        self.last_char - self.len as u32
    }

    fn data_xs<'a>(&self, data: &'a Data) -> &'a [u16] {
        let off = self.xs_off as usize;
        // Note the list of `xs` includes an entry for one-past-the-end.
        let len = self.len as usize + 1;
        &data.font_xs()[off .. off + len]
    }
}

#[derive(Clone, Copy)]
pub struct FontSpan<'a> {
    def: &'a RawFontSpan,
    data: &'a Data,
}

impl<'a> Deref for FontSpan<'a> {
    type Target = RawFontSpan;
    fn deref(&self) -> &RawFontSpan {
        self.def
    }
}

impl<'a> FontSpan<'a> {
    pub fn xs(&self) -> &'a [u16] {
        self.data_xs(self.data)
    }
}


pub struct GlyphOutput {
    pub x_offset: i32,
    pub src: Region<V2>,
}

pub struct GlyphIter<'a, 'b> {
    font: Font<'a>,
    chars: str::Chars<'b>,
    x: i32,
}

impl<'a, 'b> Iterator for GlyphIter<'a, 'b> {
    type Item = GlyphOutput;

    fn next(&mut self) -> Option<GlyphOutput> {
        'top: while let Some(ch) = self.chars.next() {
            if ch == ' ' {
                self.x += self.font.space_width as i32;
                continue;
            }

            if let Some(src) = self.font.char_glyph(ch) {
                let result = GlyphOutput {
                    x_offset: self.x,
                    src: src,
                };
                self.x += src.size().x + self.font.spacing as i32;
                return Some(result);
            } else {
                self.x += self.font.space_width as i32;
            }
        }

        None
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct RawInventoryLayout {
    size: (u16, u16),
    slots_off: u16,
    slots_len: u16,
}
unsafe impl ByteCast for RawInventoryLayout {}

impl RawInventoryLayout {
    pub fn size(&self) -> V2 {
        V2::new(self.size.0 as i32,
                self.size.1 as i32)
    }

    pub fn len(&self) -> usize {
        self.slots_len as usize
    }
}

#[derive(Clone, Copy)]
pub struct InventoryLayout<'a> {
    def: &'a RawInventoryLayout,
    data: &'a Data,
}

impl<'a> InventoryLayout<'a> {
    pub fn new(data: &'a Data, def: &'a RawInventoryLayout) -> InventoryLayout<'a> {
        InventoryLayout {
            def: def,
            data: data,
        }
    }

    pub fn slots(&self) -> &'a [InventoryLayoutSlot] {
        let off = self.def.slots_off as usize;
        let len = self.def.slots_len as usize;
        &self.data.inventory_layout_slots()[off .. off + len]
    }
}

impl<'a> Deref for InventoryLayout<'a> {
    type Target = RawInventoryLayout;
    fn deref(&self) -> &RawInventoryLayout {
        self.def
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct InventoryLayoutSlot {
    display_pos: (u16, u16),
    image_src: (u16, u16),
}
unsafe impl ByteCast for InventoryLayoutSlot {}

impl InventoryLayoutSlot {
    pub fn display_pos(&self) -> V2 {
        V2::new(self.display_pos.0 as i32,
                self.display_pos.1 as i32)
    }

    pub fn image_src(&self) -> V2 {
        V2::new(self.display_pos.0 as i32,
                self.display_pos.1 as i32)
    }
}
