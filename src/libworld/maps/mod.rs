pub mod id_map;
pub use self::id_map::IdMap;
pub use self::id_map::Reserved;

#[macro_use] pub mod stable_id_map;
pub use self::stable_id_map::{StableIdMap, IntrusiveStableId};
