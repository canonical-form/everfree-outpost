use server_types::*;
use std::collections::hash_set;

use Ext;
use ext::Update;
use objects::*;
use object_ref::{ObjectRef, ObjectMut};
use world::StructuresById;


simple_getters! {
    object_type = TerrainChunk;
    obj_ident = obj;

    fn plane_id: PlaneId = obj.plane;
    fn cpos: V2 = obj.cpos;
    fn blocks: &BlockChunk = &obj.blocks;
}

// Setters and modifiers

impl<'a, E: Ext> ObjectMut<'a, E, TerrainChunk> {
    pub fn modify_blocks<F: FnOnce(&mut BlockChunk) -> R, R>(&mut self, f: F) -> R {
        self.world.update.terrain_chunk_blocks(self.id, &self.obj().blocks);
        f(&mut self.obj_mut().blocks)
    }
}

// Non-mut methods.

impl<'a, E: Ext> ObjectRef<'a, E, TerrainChunk> {
    pub fn plane(&self) -> ObjectRef<'a, E, Plane> {
        self.world().plane(self.plane_id())
    }

    pub fn child_structures(&self) -> StructuresById<'a, E, hash_set::Iter<'a, StructureId>> {
        StructuresById::new(self.world(), self.obj().child_structures.iter())
    }
}

impl<'a, E: Ext> ObjectMut<'a, E, TerrainChunk> {
    pub fn plane(&self) -> ObjectRef<E, Plane> {
        self.world().plane(self.plane_id())
    }

    pub fn child_structures(&self) -> StructuresById<E ,hash_set::Iter<StructureId>> {
        StructuresById::new(self.world(), self.obj().child_structures.iter())
    }
}

// Mut methods.

impl<'a, E: Ext> ObjectMut<'a, E, TerrainChunk> {
    pub fn plane_mut(&mut self) -> ObjectMut<E, Plane> {
        let pid = self.plane_id();
        self.world_mut().plane_mut(pid)
    }

    // TODO: replace modify_blocks with set_block, passing position to Ext hook

    // TODO: move_to(PlaneId, V2)
}
