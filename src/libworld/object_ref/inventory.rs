use common::types::NO_ITEM;

use Ext;
use ext::Update;
use objects::*;
use object_ref::{ObjectRef, ObjectMut};


simple_getters! {
    object_type = Inventory;
    obj_ident = obj;

    fn contents: &[Item] = &obj.contents;
    fn attachment: InventoryAttachment = obj.attachment;
}

// Setters and modifiers

impl<'a, E: Ext> ObjectMut<'a, E, Inventory> {
    pub fn modify_contents<F: FnOnce(&mut [Item]) -> R, R>(&mut self, f: F) -> R {
        self.world.update.inventory_contents(self.id, &self.obj().contents);
        f(&mut self.obj_mut().contents)
    }
}

// Non-mut methods.

impl<'a, E: Ext> ObjectRef<'a, E, Inventory> {
    pub fn is_empty(&self) -> bool {
        self.contents().iter().all(|i| i.id == NO_ITEM)
    }
}

// Mut methods.

impl<'a, E: Ext> ObjectMut<'a, E, Inventory> {
    // TODO replace modify_contents with a setter, passing index to Ext hook

    /// Changes the parent of this inventory.  The new parent must refer to an existing object.
    pub fn set_attachment(&mut self, attach: InventoryAttachment) {
        let id = self.id;
        let old_attach = self.attachment();

        self.world.update.inventory_attachment(self.id, old_attach);

        match old_attach {
            InventoryAttachment::World => {},
            InventoryAttachment::Client(cid) => {
                self.world_mut().client_mut(cid).obj_mut().child_inventories.remove(&id);
            },
            InventoryAttachment::Entity(eid) => {
                self.world_mut().entity_mut(eid).obj_mut().child_inventories.remove(&id);
            },
            InventoryAttachment::Structure(sid) => {
                self.world_mut().structure_mut(sid).obj_mut().child_inventories.remove(&id);
            },
        }

        self.obj_mut().attachment = attach;

        match attach {
            InventoryAttachment::World => {},
            InventoryAttachment::Client(cid) => {
                self.world_mut().client_mut(cid).obj_mut().child_inventories.insert(id);
            },
            InventoryAttachment::Entity(eid) => {
                self.world_mut().entity_mut(eid).obj_mut().child_inventories.insert(id);
            },
            InventoryAttachment::Structure(sid) => {
                self.world_mut().structure_mut(sid).obj_mut().child_inventories.insert(id);
            },
        }
    }
}
