bitflags! {
    pub flags ClientFlags: u32 {
        // Need at least one const in here, apparently
        const C_UNUSED              = 0x00000000,
    }
}

bitflags! {
    pub flags EntityFlags: u32 {
        const E_DUMMY = 0,
    }
}

bitflags! {
    pub flags InventoryFlags: u32 {
        const I_HAS_CHANGE_HOOK     = 0x00000001,
        const I_HAS_FILTER_HOOK     = 0x00000002,
        const I_PUBLIC              = 0x00000004,
    }
}

bitflags! {
    pub flags PlaneFlags: u32 {
        const P_UNUSED              = 0x00000000,
    }
}

bitflags! {
    pub flags TerrainChunkFlags: u32 {
        /// Terrain generation for this chunk is still running in a background thread.
        const TC_LOADING            = 0x00000001,
        const TC_GENERATION_PENDING = TC_LOADING.bits,  // obsolete alias
    }
}

bitflags! {
    pub flags StructureFlags: u32 {
        const S_HAS_IMPORT_HOOK     = 0x00000001,
        // const S_HAS_EXPORT_HOOK     = 0x00000002, // unimplemented
    }
}
