//! A container widget that places each child at the position computed by a callback.  Each child
//! is given its minimum size, and there is no automatic alignment or spacing handling.

use std::prelude::v1::*;
use std::cell::Cell;
use std::cmp;
use std::marker::PhantomData;

use context::Context;
use event::{KeyEvent, MouseEvent, UIResult};
use geom::*;
use widget::Widget;

use super::ChildWidget;
use super::contents::{Contents, Visitor};


pub struct Computed<'s, Ctx: Context, R, C: Contents<Ctx, R>, F>
        where F: Fn(usize) -> Point {
    focus: &'s Cell<usize>,
    contents: C,
    hover_focus: bool,
    position: F,
    _marker: PhantomData<(Ctx, R)>,
}

impl<'s, Ctx: Context, R, C: Contents<Ctx, R>, F> Computed<'s, Ctx, R, C, F>
        where F: Fn(usize) -> Point {
    pub fn new(focus: &'s Cell<usize>, contents: C, position: F) -> Computed<'s, Ctx, R, C, F> {
        Computed {
            focus: focus,
            contents: contents,
            hover_focus: false,
            position: position,
            _marker: PhantomData,
        }
    }

    pub fn hover_focus(self, hover_focus: bool) -> Self {
        Computed {
            hover_focus: hover_focus,
            .. self
        }
    }
}

impl<'s, Ctx, R, C, F> Widget<Ctx> for Computed<'s, Ctx, R, C, F>
        where Ctx: Context, C: Contents<Ctx, R>, F: Fn(usize) -> Point {
    type Event = R;

    fn min_size(&self, ctx: &Ctx) -> Point {
        struct MinSizeVisitor<'c, Ctx: Context+'c, G>
                where G: Fn(usize) -> Point {
            ctx: &'c Ctx,
            position: G,
            idx: usize,
            size: Point,
        }
        impl<'c, Ctx: Context, R, G> Visitor<Ctx, R> for MinSizeVisitor<'c, Ctx, G>
                where G: Fn(usize) -> Point {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let pos = (self.position)(self.idx);
                let size = cw.w.min_size(self.ctx);
                self.size = Point::new(cmp::max(self.size.x, (pos + size).x),
                                       cmp::max(self.size.y, (pos + size).y));

                self.idx += 1;
            }
        }

        let mut v = MinSizeVisitor {
            ctx: ctx,
            position: |idx| (self.position)(idx),
            idx: 0,
            size: Point::new(0, 0),
        };
        self.contents.accept(&mut v);

        v.size
    }

    fn requested_visibility(&self, ctx: &Ctx) -> Option<Rect> {
        let idx = self.focus.get();
        if idx >= self.contents.len() {
            return None;
        }

        struct RequestedVisibilityVisitor<'c, Ctx: Context+'c, G>
                where G: Fn(usize) -> Point {
            ctx: &'c Ctx,
            position: G,
            idx: usize,
            bounds: Rect,
        }
        impl<'c, Ctx: Context, R, G> Visitor<Ctx, R> for RequestedVisibilityVisitor<'c, Ctx, G>
                where G: Fn(usize) -> Point {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let pos = (self.position)(self.idx);
                let size = cw.w.min_size(self.ctx);
                self.bounds = Rect::sized(size) + pos;
            }
        }

        let idx = self.focus.get();
        let mut v = RequestedVisibilityVisitor {
            ctx: ctx,
            position: |idx| (self.position)(idx),
            idx: idx,
            bounds: Rect::new(0, 0, 0, 0),
        };
        self.contents.accept_nth(&mut v, idx);

        Some(v.bounds)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        struct PaintVisitor<'c, Ctx: Context+'c, G>
                where G: Fn(usize) -> Point {
            ctx: &'c mut Ctx,
            position: G,
            idx: usize,
            focus: usize,
        }
        impl<'c, Ctx: Context, R, G> Visitor<Ctx, R> for PaintVisitor<'c, Ctx, G>
                where G: Fn(usize) -> Point {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let pos = (self.position)(self.idx);
                let size = cw.w.min_size(self.ctx);
                let bounds = Rect::sized(size) + pos;
                self.ctx.with_focus(self.idx == self.focus, |ctx| {
                    ctx.with_bounds(bounds, |ctx| {
                        cw.w.on_paint(ctx);
                    })
                });
                self.idx += 1;
            }
        }

        let mut v = PaintVisitor {
            ctx: ctx,
            position: |idx| (self.position)(idx),
            idx: 0,
            focus: self.focus.get(),
        };
        self.contents.accept(&mut v);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<R> {
        struct KeyVisitor<'c, Ctx: Context+'c, R, G>
                where G: Fn(usize) -> Point {
            ctx: &'c mut Ctx,
            position: G,
            idx: usize,
            event: Option<KeyEvent<Ctx::Key>>,
            result: UIResult<R>,
        }
        impl<'c, Ctx, R, G> Visitor<Ctx, R> for KeyVisitor<'c, Ctx, R, G>
                where Ctx: Context, G: Fn(usize) -> Point {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let pos = (self.position)(self.idx);
                let size = cw.w.min_size(self.ctx);
                let bounds = Rect::sized(size) + pos;
                let evt = self.event.take().unwrap();
                self.result = self.ctx.with_bounds(bounds, |ctx| {
                    cw.w.on_key(ctx, evt).map(|e| (cw.f)(e))
                });
            }
        }

        let mut v: KeyVisitor<Ctx, R, _> = KeyVisitor {
            ctx: ctx,
            position: |idx| (self.position)(idx),
            idx: self.focus.get(),
            event: Some(evt.clone()),
            result: UIResult::Unhandled,
        };
        self.contents.accept_nth(&mut v, self.focus.get());
        try_handle!(v.result);

        let ctx = v.ctx;
        match ctx.interp_key(evt) {
            // TODO
            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<R> {
        struct MouseVisitor<'c, Ctx: Context+'c, R, G>
                where G: Fn(usize) -> Point {
            ctx: &'c mut Ctx,
            position: G,
            idx: usize,
            event: Option<MouseEvent<Ctx::Button>>,
            result: UIResult<R>,
            found_target: bool,
        }
        impl<'c, Ctx, R, G> Visitor<Ctx, R> for MouseVisitor<'c, Ctx, R, G>
                where Ctx: Context, G: Fn(usize) -> Point {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                if self.found_target {
                    return;
                }

                // Start at `contents.len()` and pre-decrement so that we don't underflow past 0.
                self.idx -= 1;

                let pos = (self.position)(self.idx);
                let size = cw.w.min_size(self.ctx);
                let bounds = Rect::sized(size) + pos;

                let event = &mut self.event;
                let result = &mut self.result;
                let found_target = &mut self.found_target;

                self.ctx.with_bounds(bounds, |ctx| {
                    if ctx.mouse_target() {
                        let evt = event.take().unwrap();
                        *result = cw.w.on_mouse(ctx, evt).map(|e| (cw.f)(e));
                        *found_target = true;
                    }
                });
            }
        }

        let mut v: MouseVisitor<Ctx, R, _> = MouseVisitor {
            ctx: ctx,
            position: |idx| (self.position)(idx),
            idx: self.contents.len(),
            event: Some(evt.clone()),
            result: UIResult::Unhandled,
            found_target: false,
        };
        self.contents.accept_rev(&mut v);

        if v.found_target {
            if v.result.is_handled() || self.hover_focus {
                self.focus.set(v.idx);
            }
        }

        v.result
    }

    fn check_drop(&self, _ctx: &mut Ctx, _data: &Ctx::DropData) -> bool {
        unimplemented!()
    }

    fn on_drop(&self, _ctx: &mut Ctx, _data: &Ctx::DropData) -> UIResult<R> {
        unimplemented!()
    }
}


