#[macro_use] pub mod macros;

pub mod context;
pub mod event;
pub mod geom;
pub mod param;
pub mod widget;
pub mod widgets;


// TODO:
// - Remove drag/drop events.  Better handled in user code.
// - Remove built-in event_map from group Contents.
// - Remove current focus system.  Add new keyboard focus tracking & highlighting.
// - Move toward stateless widgets.
// - Better support for compound widgets (`fn inner()`)
// - Better way of subdividing complex widget state into state for sub-widgets
