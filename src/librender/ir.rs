use std::fmt;

pub use ast::{IntSize, PrimTy, TexFormat, BlendMode, DepthTest, RenderConfig};
use util::IndexSlice;


define_id!(PassId(u32));

define_id!(VarId(u32)); // Pass-local


#[derive(Clone, Copy, PartialOrd, Ord, Hash, Debug)]
pub struct Symbol<'a>(pub &'a &'a str);

impl<'a> PartialEq for Symbol<'a> {
    fn eq(&self, other: &Symbol<'a>) -> bool {
        self.0 as *const _ == other.0 as *const _
    }
}

impl<'a> Eq for Symbol<'a> {}

impl<'a> fmt::Display for Symbol<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", *self.0)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Ty<'a>(pub &'a TyKind<'a>);

impl<'a> Ty<'a> {
    pub fn resolve(self) -> &'a TyKind<'a> {
        match *self.0 {
            TyKind::Alias(_, ty) => ty.resolve(),
            ref tk => tk,
        }
    }

    /// Check two types for structural equivalence.  Ignores type aliases, as well as `Geometry`
    /// field names.
    pub fn equiv(self, other: Ty<'a>) -> bool {
        match (self.resolve(), other.resolve()) {
            (&TyKind::Prim(p1), &TyKind::Prim(p2)) => p1 == p2,
            (&TyKind::Vec(p1, l1), &TyKind::Vec(p2, l2)) => p1 == p2 && l1 == l2,
            (&TyKind::Texture(fmt1), &TyKind::Texture(fmt2)) => fmt1 == fmt2,
            (&TyKind::Geometry(fs1), &TyKind::Geometry(fs2)) =>
                fs1.len() == fs2.len() &&
                fs1.iter().zip(fs2.iter()).all(|(f1, f2)| f1.ty.equiv(f2.ty)),

            (&TyKind::Alias(..), _) => unreachable!(),
            (_, &TyKind::Alias(..)) => unreachable!(),
            _ => false,
        }
    }
}


#[derive(Clone, Copy, Debug)]
pub struct Pipeline<'a> {
    pub passes: &'a IndexSlice<PassId, Pass<'a>>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Field<'a> {
    pub name: Symbol<'a>,
    pub ty: Ty<'a>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum TyKind<'a> {
    Prim(PrimTy),
    Vec(PrimTy, u8),
    Texture(TexFormat),
    Geometry(&'a [Field<'a>]),
    Alias(Symbol<'a>, Ty<'a>),
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Var<'a> {
    pub name: Symbol<'a>,
    pub ty: Ty<'a>,
    pub origin: VarOrigin,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum VarOrigin {
    Input(usize),
    Output(usize),
    Stmt(usize),
}

#[derive(Clone, Copy, Debug)]
pub struct Pass<'a> {
    pub name: Symbol<'a>,
    pub public: bool,
    pub inputs: &'a [VarId],
    pub outputs: &'a [VarId],
    pub stmts: &'a [Stmt<'a>],
    pub vars: &'a IndexSlice<VarId, Var<'a>>,
}

#[derive(Clone, Copy, Debug)]
pub enum Stmt<'a> {
    Local(LocalStmt<'a>),
    Clear(ClearStmt<'a>),
    Render(RenderStmt<'a>),
    Call(CallStmt<'a>),
}

#[derive(Clone, Copy, Debug)]
pub struct LocalStmt<'a> {
    pub var: VarId,
    pub insn: Insn<'a>,
}

#[derive(Clone, Copy, Debug)]
pub struct ClearStmt<'a> {
    pub color: Option<VarId>,
    pub depth: Option<VarId>,
    pub outputs: DrawOutputs<'a>,
}

#[derive(Clone, Copy, Debug)]
pub struct CallStmt<'a> {
    pub pass: PassId,
    pub inputs: &'a [VarId],
    pub outputs: &'a [VarId],
}


#[derive(Clone, Copy, Debug)]
pub struct RenderStmt<'a> {
    pub shader: Shader<'a>,
    pub uniforms: &'a [UniformBinding<'a>],
    pub vert_attribs: &'a [AttribBinding<'a>],
    pub inst_attribs: &'a [AttribBinding<'a>],
    pub outputs: DrawOutputs<'a>,
    pub config: RenderConfig,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Shader<'a> {
    pub vertex: Symbol<'a>,
    pub fragment: Symbol<'a>,
    pub defs: Symbol<'a>,
}

#[derive(Clone, Copy, Debug)]
pub struct UniformBinding<'a> {
    pub name: Symbol<'a>,
    pub ty: Ty<'a>,
    pub var: VarId,
}

#[derive(Clone, Copy, Debug)]
pub struct AttribBinding<'a> {
    pub name: Symbol<'a>,
    pub ty: Ty<'a>,
    pub var: VarId,
    /// Index of the field in `var`'s `Geometry` type.
    pub field: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct DrawOutputs<'a> {
    pub color: &'a [VarId],
    pub depth: Option<VarId>,
    pub viewport: Option<VarId>,
}

#[derive(Clone, Copy, Debug)]
pub enum Insn<'a> {
    ConstI(IntSize, i32),
    ConstU(IntSize, u32),
    ConstF(f32),

    Copy(VarId),

    // Fields
    VecIndex(VarId, u8),
    VecSwizzle(VarId, &'a [u8]),
    VecLen(VarId),
    TextureSize(VarId),
    GeomField(VarId, Symbol<'a>),

    // Ctors
    VecCtor(PrimTy, &'a [VarId]),
    TextureCtor(TexFormat, [VarId; 2]),

    // Numeric coercion
    Coerce(VarId, PrimTy),
}
