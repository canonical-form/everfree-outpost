use std::fmt;

use intern::Interners;
use ir::*;

pub mod inline;
pub mod replace_depth;
pub mod split_buffers;
pub mod stmts;
pub mod subst;

pub fn copy_propagate<'a>(intern: &Interners<'a>,
                          pass: Pass<'a>) -> Pass<'a> {
    stmts::rewrite_pass(intern, pass, |mut curs| {
        if let &Stmt::Local(l) = curs.get() {
            if let Insn::Copy(v) = l.insn {
                curs.replace_with_var(v);
                return;
            }
        }
        curs.keep()
    })
}

fn add_def<'a, D: fmt::Display>(intern: &Interners<'a>, rs: &mut RenderStmt<'a>, def: D) {
    let s = format!(
        "{}{}{}\n",
        rs.shader.defs,
        if rs.shader.defs.0.len() == 0 || rs.shader.defs.0.ends_with("\n") { "" } else { "\n" },
        def,
    );
    rs.shader.defs = intern.symbol(&s);
}
