use std::iter;

use ir::*;
use util::IndexVec;


#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum VarClass {
    /// The value does not change once the pass has been compiled.
    Const,
    /// The value may vary on every invocation of the pass.
    Uniform,
    /// The value may vary for each vertex in the geometry buffer.
    Attrib,
}

impl VarClass {
    /// Join/LUB operation on the lattice of variable classes
    ///
    ///          attrib
    ///            |
    ///         uniform
    ///            |
    ///          const
    pub fn join(self, other: VarClass) -> VarClass {
        use self::VarClass::*;
        match (self, other) {
            (Const, x) => x,
            (Uniform, Const) => Uniform,
            (Uniform, x) => x,
            (Attrib, _) => Attrib,
        }
    }

    pub fn le(self, other: VarClass) -> bool {
        use self::VarClass::*;
        match (self, other) {
            (Const, _) => true,
            (Uniform, Const) => false,
            (Uniform, _) => true,
            (Attrib, Attrib) => true,
            (Attrib, _) => false,
        }
    }
}

pub fn classify_vars(pass: Pass) -> IndexVec<VarId, VarClass> {
    let mut class: IndexVec<_, _> =
        iter::repeat(VarClass::Uniform).take(pass.vars.len()).collect();

    for &id in pass.inputs.iter().chain(pass.outputs.iter()) {
        class[id] = VarClass::Uniform;
    }

    for &s in pass.stmts {
        let l = match s {
            Stmt::Local(l) => l,
            _ => continue,
        };
        class[l.var] = match l.insn {
            Insn::ConstI(_, _) => VarClass::Const,
            Insn::ConstU(_, _) => VarClass::Const,
            Insn::ConstF(_) => VarClass::Const,
            Insn::Copy(v) => class[v],
            Insn::VecIndex(v, _) => class[v],
            Insn::VecSwizzle(v, _) => class[v],
            Insn::VecLen(_) => VarClass::Const,
            Insn::TextureSize(v) => class[v],
            Insn::GeomField(_, _) => VarClass::Attrib,
            Insn::VecCtor(_, vs) =>
                vs.iter().cloned().fold(VarClass::Const, |cls, v| cls.join(class[v])),
            Insn::TextureCtor(_, vs) =>
                vs.iter().cloned().fold(VarClass::Const, |cls, v| cls.join(class[v])),
            Insn::Coerce(v, _) => class[v],
        };
    }

    class
}
