use std::cell::RefCell;
use std::collections::HashMap;
use std::str;

use rustc_arena::DroplessArena;

use ir::*;
use util::IndexSlice;

pub struct Interners<'a> {
    arena: &'a DroplessArena,
    symbol_lookup: RefCell<HashMap<&'a str, Symbol<'a>>>,
    ty_lookup: RefCell<HashMap<&'a TyKind<'a>, Ty<'a>>>,
}

impl<'a> Interners<'a> {
    pub fn new(arena: &'a DroplessArena) -> Interners<'a> {
        Interners {
            arena,
            symbol_lookup: RefCell::new(HashMap::new()),
            ty_lookup: RefCell::new(HashMap::new()),
        }
    }

    #[allow(dead_code)]
    pub fn intern<T>(&self, x: T) -> &'a T {
        self.arena.alloc(x)
    }

    pub fn slice<T: Copy>(&self, x: &[T]) -> &'a [T] {
        if x.len() == 0 {
            &[]
        } else {
            self.arena.alloc_slice(x)
        }
    }


    pub fn index_slice<K, V>(&self, x: &IndexSlice<K, V>) -> &'a IndexSlice<K, V>
            where K: From<usize>+Into<usize>, V: Copy {
        IndexSlice::new(self.slice(x.as_slice()))
    }

    pub fn symbol(&self, s: &str) -> Symbol<'a> {
        let mut symbol_lookup = self.symbol_lookup.borrow_mut();

        if let Some(&sym) = symbol_lookup.get(&s) {
            return sym;
        }

        let arena_bytes = self.slice(s.as_bytes());
        let arena_str = str::from_utf8(arena_bytes).unwrap();
        let sym = Symbol(self.arena.alloc(arena_str));
        symbol_lookup.insert(arena_str, sym);
        sym
    }

    pub fn ty(&self, tk: &TyKind<'a>) -> Ty<'a> {
        let mut ty_lookup = self.ty_lookup.borrow_mut();

        if let Some(&ty) = ty_lookup.get(tk) {
            return ty;
        }

        let ty = Ty(self.arena.alloc(tk.clone()));
        ty_lookup.insert(ty.0, ty);
        ty
    }
}
