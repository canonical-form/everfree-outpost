#![allow(dead_code)]
use ir::*;

struct PrintCx<'a> {
    pipeline: Option<&'a Pipeline<'a>>,
    pass: Option<&'a Pass<'a>>,
}

fn join<I: Iterator<Item=String>>(it: I) -> String {
    let mut s = String::new();
    for x in it {
        if s.len() > 0 {
            s.push_str(", ");
        }
        s.push_str(&x)
    }
    s
}

fn indent(s: &str) -> String {
    let mut s2 = String::new();
    for line in s.lines() {
        s2.push_str("  ");
        s2.push_str(line);
        s2.push_str("\n");
    }
    s2
}

impl<'a> PrintCx<'a> {
    fn print_insn(&self, i: &Insn) -> String {
        match *i {
            Insn::ConstI(sz, x) => format!("{}_i{}", x, sz.size() * 8),
            Insn::ConstU(sz, x) => format!("{}_u{}", x, sz.size() * 8),
            Insn::ConstF(x) => format!("{}_f32", x),

            Insn::Copy(v) => self.print_var(v),

            Insn::VecIndex(v, idx) => format!("{}[{}]", self.print_var(v), idx),
            Insn::VecSwizzle(v, idxs) => format!("{}~{:?}", self.print_var(v), idxs),
            Insn::VecLen(v) => format!("{}{{vec}}.len", self.print_var(v)),
            Insn::TextureSize(v) => format!("{}{{tex}}.size", self.print_var(v)),
            Insn::GeomField(v, f) => format!("{}{{geom}}.{}", self.print_var(v), f),

            Insn::VecCtor(p, vs) => {
                format!("{}x{}({})",
                        self.print_prim_ty(p), vs.len(),
                        join(vs.iter().map(|&v| self.print_var(v))))
            },
            Insn::TextureCtor(fmt, vs) => {
                format!("texture<{:?}>({})",
                        fmt,
                        join(vs.iter().map(|&v| self.print_var(v))))
            },

            Insn::Coerce(v, p) =>
                format!("{} as {}", self.print_var(v), self.print_prim_ty(p)),
        }
    }

    fn print_var(&self, v: VarId) -> String {
        if let Some(p) = self.pass {
            let var = p.vars[v];
            format!("{}#{}", var.name, v.0)
        } else {
            format!("_#{}", v.0)
        }
    }

    fn print_prim_ty(&self, p: PrimTy) -> String {
        match p {
            PrimTy::Int(sz) => format!("i{}", sz.size() * 8),
            PrimTy::Uint(sz) => format!("u{}", sz.size() * 8),
            PrimTy::Float => format!("f32"),
        }
    }

    fn print_ty(&self, t: Ty) -> String {
        match *t.resolve() {
            TyKind::Prim(p) => self.print_prim_ty(p),
            TyKind::Vec(p, len) => format!("{}x{}", self.print_prim_ty(p), len),
            TyKind::Texture(fmt) => format!("texture<{:?}>", fmt),
            TyKind::Geometry(fields) => {
                format!("geometry {{ {:?} }}", join(fields.iter().map(|f| self.print_field(f))))
            },
            TyKind::Alias(..) => unreachable!(),
        }
    }

    fn print_field(&self, f: &Field) -> String {
        format!("{}: {}", f.name, self.print_ty(f.ty))
    }

    fn print_stmt(&self, s: &Stmt) -> String {
        match *s {
            Stmt::Local(ref l) => {
                if let Some(p) = self.pass {
                    let var = p.vars[l.var];
                    format!("local {}: {} = {};\n",
                            self.print_var(l.var),
                            self.print_ty(var.ty),
                            self.print_insn(&l.insn))
                } else {
                    format!("local {} = {};\n",
                            self.print_var(l.var),
                            self.print_insn(&l.insn))
                }
            },

            Stmt::Clear(ref cs) => {
                let mut s = String::new();
                s.push_str(&format!("clear (\n"));
                if let Some(c) = cs.color {
                    s.push_str(&format!("  color = {},\n", self.print_var(c)));
                }
                if let Some(d) = cs.depth {
                    s.push_str(&format!("  depth = {},\n", self.print_var(d)));
                }
                s.push_str(&format!(") -> (\n"));
                s.push_str(&self.print_outputs(&cs.outputs));
                s.push_str(&format!(");\n"));
                s
            },

            Stmt::Render(ref rs) => {
                let mut s = String::new();
                s.push_str(&format!("render {:?} {:?}", rs.shader.vertex, rs.shader.fragment));
                if rs.shader.defs.0.len() > 0 {
                    for line in rs.shader.defs.0.lines() {
                        s.push_str("\n  ");
                        s.push_str(line);
                    }
                    s.push_str("\n(\n");
                } else {
                    s.push_str(" (\n");
                }
                for u in rs.uniforms {
                    s.push_str(&format!("  uniform {}: {} = {},\n",
                                        u.name,
                                        self.print_ty(u.ty),
                                        self.print_var(u.var)));
                }
                for a in rs.vert_attribs {
                    s.push_str(&format!("  vertex {}: {} = {}.{},\n",
                                        a.name,
                                        self.print_ty(a.ty),
                                        self.print_var(a.var),
                                        a.field));
                }
                for a in rs.inst_attribs {
                    s.push_str(&format!("  instance {}: {} = {}.{},\n",
                                        a.name,
                                        self.print_ty(a.ty),
                                        self.print_var(a.var),
                                        a.field));
                }
                s.push_str(&format!(") -> (\n"));
                s.push_str(&self.print_outputs(&rs.outputs));
                s.push_str(&format!(") config (\n"));
                s.push_str(&format!("  blend_mode = {:?},\n", rs.config.blend_mode));
                s.push_str(&format!("  depth_test = {:?},\n", rs.config.depth_test));
                s.push_str(&format!(");\n"));
                s
            },

            Stmt::Call(ref cs) => {
                let mut s = String::new();
                if let Some(pipe) = self.pipeline {
                    let p = &pipe.passes[cs.pass];
                    s.push_str(&format!("call {} (\n", self.print_pass_id(cs.pass)));
                    for (&formal, &v) in p.inputs.iter().zip(cs.inputs.iter()) {
                        s.push_str(&format!("  {} = {},\n",
                                            p.vars[formal].name,
                                            self.print_var(v)));
                    }
                    s.push_str(&format!(") -> (\n"));
                    for (&formal, &v) in p.outputs.iter().zip(cs.outputs.iter()) {
                        s.push_str(&format!("  {} = {},\n",
                                            p.vars[formal].name,
                                            self.print_var(v)));
                    }
                    s.push_str(&format!(");\n"));
                } else {
                    s.push_str(&format!("call {} (\n", self.print_pass_id(cs.pass)));
                    for &v in cs.inputs {
                        s.push_str(&format!("  {},\n", self.print_var(v)));
                    }
                    s.push_str(&format!(") -> (\n"));
                    for &v in cs.outputs {
                        s.push_str(&format!("  {},\n", self.print_var(v)));
                    }
                    s.push_str(&format!(");\n"));
                }
                s
            },
        }
    }

    fn print_pass_id(&self, id: PassId) -> String {
        if let Some(p) = self.pipeline {
            format!("{}", p.passes[id].name)
        } else {
            format!("pass#{}", id.0)
        }
    }

    fn print_outputs(&self, o: &DrawOutputs) -> String {
        let mut s = String::new();
        for (i, &c) in o.color.iter().enumerate() {
            s.push_str(&format!("  color[{}] = {},\n", i, self.print_var(c)));
        }
        if let Some(d) = o.depth {
            s.push_str(&format!("  depth = {},\n", self.print_var(d)));
        }
        s
    }

    fn print_pass(&self, p: &Pass) -> String {
        let cx = PrintCx { pass: Some(p), ..*self };
        let mut s = String::new();
        s.push_str(&format!("{} pass {} (\n",
                            if p.public { "pub" } else { "" },
                            p.name));
        for &v in p.inputs {
            let var = p.vars[v];
            s.push_str(&format!("  {}: {},\n", cx.print_var(v), cx.print_ty(var.ty)));
        }
        s.push_str(&format!(") -> (\n"));
        for &v in p.outputs {
            let var = p.vars[v];
            s.push_str(&format!("  {}: {},\n", cx.print_var(v), cx.print_ty(var.ty)));
        }
        s.push_str(&format!(") {{\n"));
        for stmt in p.stmts {
            s.push_str(&indent(&cx.print_stmt(stmt)));
        }
        s.push_str(&format!("}}\n"));
        s
    }

    fn print_pipeline(&self, p: &Pipeline) -> String {
        let cx = PrintCx { pipeline: Some(p), ..*self };
        let mut s = String::new();
        for (_, pass) in p.passes.iter() {
            s.push_str(&cx.print_pass(pass));
            s.push_str("\n");
        }
        s
    }
}

pub fn print_pass(pipeline: Option<&Pipeline>, pass: &Pass) -> String {
    PrintCx { pipeline, pass: None }.print_pass(pass)
}

pub fn print_pipeline(pipeline: &Pipeline) -> String {
    PrintCx { pipeline: None, pass: None }.print_pipeline(pipeline)
}
