/// Small non-generic wrapper around PNG reading.  Because its entry points are not generic, we can
/// compile this library with `opt-level=3` and get fast PNG loading regardless of the downstream
/// optimization level.
extern crate png;

use std::fs::File;
use std::io::{Cursor, Read};
use std::iter;
use std::path::Path;
use png::HasParameters;

fn read_png<R: Read>(r: R) -> Result<(png::OutputInfo, Box<[u8]>), png::DecodingError> {
    let mut decoder = png::Decoder::new(r);
    decoder.set(png::TRANSFORM_EXPAND);
    let (info, mut reader) = try!(decoder.read_info());
    assert!(info.color_type == png::ColorType::RGBA);
    assert!(info.bit_depth == png::BitDepth::Eight);
    let size = reader.output_buffer_size();
    let mut buf = iter::repeat(0).take(size).collect::<Vec<_>>().into_boxed_slice();
    try!(reader.next_frame(&mut buf));

    Ok((info, buf))
}

pub fn read_png_file(path: &Path) -> Result<(png::OutputInfo, Box<[u8]>), png::DecodingError> {
    let f = try!(File::open(path));
    read_png(f)
}

pub fn read_png_bytes(bytes: &[u8]) -> Result<(png::OutputInfo, Box<[u8]>), png::DecodingError> {
    read_png(Cursor::new(bytes))
}
