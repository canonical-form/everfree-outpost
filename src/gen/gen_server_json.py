import base64
import hashlib
import json
import os
import sys
import time

if __name__ == '__main__':
    pack_file, = sys.argv[1:]

    with open(pack_file, 'rb') as f:
        b = f.read()
    size = len(b)
    h = hashlib.sha256(b).digest()
    h_str = base64.b64encode(h).decode('ascii')

    obj = {
            'url': 'ws://localhost:8888/ws',
            'version': 'dev',
            'pack': ['outpost.pack', size, h_str],
            }
    json.dump(obj, sys.stdout)
