import argparse
import base64
import io
import json
import os
from pprint import pprint
import struct
import sys
import xml.etree.ElementTree as ET


TILE_SIZE = 16


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('mode', choices=('tmx-to-omx', 'omx-to-tmx', 'omx-to-json'))

    args.add_argument('--in-file', metavar='FILE',
            help='read input from FILE')
    args.add_argument('--in-dir', metavar='DIR',
            help='read input from each file found inside DIR')
    args.add_argument('--out-file', metavar='FILE',
            help='write generated output to FILE')
    args.add_argument('--out-dir', metavar='DIR',
            help='write one output to DIR for each input file')

    args.add_argument('--dep-file', metavar='FILE',
            help='write Makefile-style dependencies to FILE')
    args.add_argument('--stamp-file', metavar='FILE',
            help='touch FILE to indicate the conversion is complete')

    args.add_argument('--json-dir',
            help='directory containing data json files')

    args.add_argument('--tileset-rel-dir',
            help='output-relative directory path for constructing tileset '
                'source paths')
    args.add_argument('--tileset-suffix',
            help='name suffix for constructing tileset source paths')

    return args


_DEPS = set()

def read_file(path):
    _DEPS.add(path)
    with open(path) as f:
        return f.read()

def read_json_file(path):
    _DEPS.add(path)
    with open(path) as f:
        return json.load(f)

def read_xml_file(path):
    _DEPS.add(path)
    with open(path, 'rb') as f:
        return ET.parse(f)

def iter_dir(path):
    _DEPS.add(path)
    for f in os.listdir(path):
        p = os.path.join(path, f)
        yield p
        if os.path.isdir(p):
            yield from iter_dir(p)

def get_deps():
    return sorted(_DEPS)


def collect_text(x):
    parts = []
    if x.text is not None:
        parts.append(x.text)
    for y in x:
        if x.tail is not None:
            parts.append(x.tail)
    return ''.join(parts)

def remove_all(x, tag):
    ys = list(x.iterfind(tag))
    for y in ys:
        x.remove(y)

def decode_data(x, enc, comp):
    if enc is None:
        return [int(x_tile.get('gid')) for x_tile in x.iterfind('tile')]
    elif enc == 'csv':
        s = ''.join(collect_text(x).split())
        if s == '':
            return []
        return [int(t) for t in s.split(',')]
    elif enc == 'base64':
        s = ''.join(collect_text(x).split())
        b = base64.b64decode(s.encode('ascii'))
        if comp is None:
            pass
        elif comp == 'gzip':
            b = gzip.decompress(b)
        elif comp == 'zlib':
            b = zlib.decompress(b)
        else:
            raise ValueError('unknown compression mode %r' % comp)
        return [t[0] for t in struct.iter_unpack('<I', b)]
    else:
        raise ValueError('unknown tile data encoding %r' % enc)

def encode_data(x, ts):
    '''Set the tile data contained in `x` to `ts`.  This always uses encoding
    `base64` and no compression.  The enclosing <data> element should be
    updated accordingly.'''
    remove_all(x, 'tile')
    b = b''.join(struct.pack('<I', t) for t in ts)
    x.text = base64.b64encode(b).decode('ascii')

def find_used_tiles(x):
    used = set()
    for x_layer in x.iter('layer'):
        for x_data in x_layer.iterfind('data'):
            enc = x_data.get('encoding')
            comp = x_data.get('compression')

            used.update(decode_data(x_data, enc, comp))
            for x_chunk in x_data.iterfind('chunk'):
                used.update(decode_data(x_chunk, enc, comp))

    for x_obj in x.iter('object'):
        gid_str = x_obj.get('gid')
        if gid_str is not None:
            used.add(int(gid_str))

    return used

def renumber_tiles(x, dct):
    for x_layer in x.iter('layer'):
        for x_data in x_layer.iterfind('data'):
            old_enc = x_data.get('encoding')
            old_comp = x_data.get('compression')

            x_data.set('encoding', 'base64')
            if old_comp is not None:
                del x_data.attrib['compression']

            ts = decode_data(x_data, old_enc, old_comp)
            encode_data(x_data, [dct[t] for t in ts])

            for x_chunk in x_data.iterfind('chunk'):
                ts = decode_data(x_chunk, old_enc, old_comp)
                encode_data(x_chunk, [dct[t] for t in ts])

    for x_obj in x.iter('object'):
        gid_str = x_obj.get('gid')
        if gid_str is not None:
            x_obj.set('gid', str(dct[int(gid_str)]))

def iter_points(bbox):
    x0, y0, x1, y1 = bbox
    for y in range(y0, y1):
        for x in range(x0, x1):
            yield (x, y)

def iter_tiles(x):
    for x_layer in x.iter('layer'):
        for x_data in x_layer.iterfind('data'):
            enc = x_data.get('encoding')
            comp = x_data.get('compression')

            data = decode_data(x_data, enc, comp)
            if len(data) > 0:
                x1 = int(x_layer.get('width'))
                y1 = int(x_layer.get('height'))
                yield from zip(data, iter_points((0, 0, x1, y1)))

            for x_chunk in x_data.iterfind('chunk'):
                data = decode_data(x_data, enc, comp)
                if len(data) > 0:
                    x0 = int(x_chunk.get('x'))
                    y0 = int(x_chunk.get('x'))
                    x1 = x0 + int(x_chunk.get('width'))
                    y1 = y0 + int(x_chunk.get('height'))
                    yield from zip(data, iter_points((x0, y0, x1, y1)))

def tileset_to_dataset(x, path, ts):
    '''Remove all <tileset>s from `x`, and replace them with a <dataset>
    containing the objects referenced by `ts`, in order.'''

    # Map from global tile IDs to <tile> elements
    x_tiles = {}
    for x_ts_ref in x.getroot().iterfind('tileset'):
        firstgid = int(x_ts_ref.get('firstgid'))

        source = x_ts_ref.get('source')
        if source is None:
            x_ts = x_ts_ref
        else:
            ts_path = os.path.join(os.path.dirname(path), source)
            # Turn `foo/../bar.tsx` into `bar.tsx`.  Otherwise we get problems
            # due to tiled assuming `foo` is not a symlink when building paths.
            ts_path = os.path.normpath(ts_path)
            # TODO: assert ts_path is in the same directory as path
            x_ts = read_xml_file(ts_path)

        for x_tile in x_ts.iterfind('tile'):
            gid = int(x_tile.get('id')) + firstgid
            x_tiles[gid] = x_tile

    x_dataset = ET.Element('dataset')
    for t in ts:
        if t == 0:
            continue
        if t not in x_tiles:
            raise KeyError('no tile data for %r in %r' % (t, path))
        x_tile = x_tiles[t]

        kind = None
        name = None
        for x_props in x_tile.iterfind('properties'):
            for x_prop in x_props.iterfind('property'):
                k = x_prop.get('name')
                v = x_prop.text
                if k == 'kind':
                    kind = v
                elif k == 'name':
                    name = v

        if kind is None or name is None:
            raise ValueError('incomplete tile data for %r in %r' % (t, path))

        if kind not in ('structure', 'block'):
            raise ValueError('unknown tile kind %rkindfor %r in %r' % (kind, t, path))

        x_data = ET.SubElement(x_dataset, kind)
        x_data.set('name', name)

        # TODO: may also want to store sx,sy,sz for structures for better
        # handling / error messages on size changes

    remove_all(x.getroot(), 'tileset')
    x.getroot().insert(0, x_dataset)

def tmx_to_omx(args, name, path, x):
    used = find_used_tiles(x)
    used.add(0)
    # When all tile indices are >= 0, then tile 0 will be at index 0.
    assert all(t >= 0 for t in used)
    assert all(t & 0xe0000000 == 0 for t in used), \
            'flipped tiles are not supported'
    used = sorted(used)

    tileset_to_dataset(x, path, used)

    dct = {t: i for i, t in enumerate(sorted(used))}
    renumber_tiles(x, dct)

    return x

def build_omx_data_map(x, tag, datas):
    '''Build a map from omx tile IDs to indices into `datas`.  For each
    <dataset> entry whose tag is `tag`, the corresponding entry in the map will
    contain the index of the entry in `datas` of the same name.'''
    data_ids = {b['name']: i for i, b in enumerate(datas)}
    data_map = [None]
    for x_dataset in x.getroot().iterfind('dataset'):
        for x_data in x_dataset:
            name = x_data.get('name')
            if x_data.tag == tag:
                data_map.append(data_ids[name])
            else:
                data_map.append(None)
    return data_map

def build_omx_tile_map(x, structures, blocks):
    '''Build a map from omx tile IDs (entries in <dataset>) to the appropriate
    tile IDs for tmx blocks.tsx/structures.tsx tilesets.'''
    # global 0, (local 0, blocks), (local 0, structures)
    first_block = 1 + 1
    first_structure = 1 + (1 + len(blocks)) + 1

    block_map = build_omx_data_map(x, 'block', blocks)
    structure_map = build_omx_data_map(x, 'structure', structures)

    tile_map = [None] * len(block_map)
    tile_map[0] = 0
    for i, j in enumerate(block_map):
        if j is not None:
            tile_map[i] = first_block + j
    for i, j in enumerate(structure_map):
        if j is not None:
            tile_map[i] = first_structure + j

    return tile_map

def tileset_path(args, map_name, basename):
    name = basename
    if args.tileset_suffix is not None:
        name = '%s-%s' % (name, args.tileset_suffix)
    name += '.tsx'
    if args.tileset_rel_dir is not None:
        name = os.path.join(args.tileset_rel_dir, name)
        # map_name is the path (minus extension) relative to --in-dir.  We
        # assume tileset_rel_dir is relative to --in-dir as well.
        name = os.path.relpath(name, os.path.dirname(map_name))
    return name

def tileset_xml(path, first_gid):
    x = ET.Element('tileset')
    x.set('firstgid', str(first_gid))
    x.set('source', path)
    return x

def omx_to_tmx(args, name, path, x):
    assert args.json_dir is not None, \
            '--json-dir must be set for mode omx-to-tmx'
    structures = read_json_file(os.path.join(args.json_dir, 'structures.json'))
    blocks = read_json_file(os.path.join(args.json_dir, 'blocks.json'))

    tile_map = build_omx_tile_map(x, structures, blocks)
    renumber_tiles(x, tile_map)

    remove_all(x.getroot(), 'dataset')
    x_tileset = tileset_xml(tileset_path(args, name, 'blocks'), 1)
    x.getroot().insert(0, x_tileset)
    x_tileset = tileset_xml(tileset_path(args, name, 'structures'), 2 + len(blocks))
    x.getroot().insert(1, x_tileset)

    return x

class BitMask:
    def __init__(self):
        self.data = set()
        self.bbox = (0, 0, 0, 0)

    def origin(self):
        return self.bbox[:2]

    def size(self):
        x0, y0, x1, y1 = self.bbox
        return (x1 - x0, y1 - y0)

    def expand(self, x, y):
        x0, y0, x1, y1 = self.bbox

        if x1 - x0 == 0 or y1 - y0 == 0:
            self.bbox = (x, y, x + 1, y + 1)
        else:
            self.bbox = (min(x0, x), min(y0, y), max(x1, x + 1), max(y1, y + 1))

    def set(self, x, y, val):
        if val:
            self.data.add((x, y))
        else:
            self.data.remove((x, y))
        self.expand(x, y)

    def get(self, x, y):
        return (x, y) in self.data

    def set_box(self, x0, y0, x1, y1, val):
        self.expand(x0, y0)
        self.expand(x1 - 1, y1 - 1)

        if val:
            for y in range(y0, y1):
                for x in range(x0, x1):
                    self.data.add((x, y))
        else:
            for y in range(y0, y1):
                for x in range(x0, x1):
                    self.data.remove((x, y))

    def dump(self, t='1', f='.'):
        s = ''
        x0, y0, x1, y1 = self.bbox
        for y in range(y0, y1):
            for x in range(x0, x1):
                if (x, y) in self.data:
                    s += t
                else:
                    s += f
            s += '\n'
        return s

    def translate(self, dx, dy):
        x0, y0, x1, y1 = self.bbox

        self.data = set((x + dx, y + dy) for (x, y) in self.data)
        self.bbox = (x0 + dx, y0 + dy, x1 + dx, y1 + dy)

    def as_bytes(self):
        x0, y0, x1, y1 = self.bbox
        sx, sy = x1 - x0, y1 - y0

        bsx = (sx + 7) // 8
        buf = bytearray(bsx * sy)

        for (x, y) in self.data:
            xb, xo = (x - x0) // 8, (x - x0) % 8
            buf[y * bsx + xb] |= 1 << xo

        return buf

def omx_to_json(args, name, path, xml):
    assert args.json_dir is not None, \
            '--json-dir must be set for mode omx-to-tmx'
    structures = read_json_file(os.path.join(args.json_dir, 'structures.json'))
    blocks = read_json_file(os.path.join(args.json_dir, 'blocks.json'))

    structure_map = build_omx_data_map(xml, 'structure', structures)
    block_map = build_omx_data_map(xml, 'block', blocks)


    covered = BitMask()

    for t, (x, y) in iter_tiles(xml):
        if t != 0:
            covered.set(x, y, True)

    j_structures = []
    for x_obj in xml.iter('object'):
        sid = structure_map[int(x_obj.get('gid'))]
        assert sid is not None and 0 <= sid < len(structures), \
                'invalid structure id %s' % x_obj.get('gid')
        s = structures[sid]

        sx, sy, sz = s['size']

        x = int(x_obj.get('x')) // TILE_SIZE
        # x_obj attrs give the screen coordinates of the bottom-left corner
        y = int(x_obj.get('y')) // TILE_SIZE - sy
        z = 0

        covered.set_box(x, y, x + sx, y + sy, True)

        j_structures.append({
            'desc': s['name'],
            # Positions will be adjusted later, once the vault's final bounding
            # box is known.
            'pos': (x, y, z),
        })


    ox, oy = covered.origin()
    covered.translate(-ox, -oy)

    no_trees_mask = covered.as_bytes()
    no_junk_mask = no_trees_mask

    for js in j_structures:
        x, y, z = js['pos']
        js['pos'] = x - ox, y - oy, z


    return {
            'name': name,
            'size': covered.size(),
            'tree_mask': list(no_trees_mask),
            'junk_mask': list(no_junk_mask),
            'structures': j_structures,
            }


def write_xml(path, x):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'wb') as f:
        x.write(f, xml_declaration=True)

def write_json(path, j):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as f:
        json.dump(j, f)

def read_inputs(args, expect_ext, read_func):
    if args.in_file is not None and args.in_dir is not None:
        raise ValueError('can\'t specify both --in-file and --in-dir')
    if args.in_file is None and args.in_dir is None:
        raise ValueError('must specify either --in-file or --in-dir')

    if args.in_file is not None:
        path = args.in_file
        name = os.path.splitext(os.path.basename(path))[0]
        val = read_func(path)
        return [(name, path, val)]

    if args.in_dir is not None:
        vals = []
        for path in iter_dir(args.in_dir):
            name, ext = os.path.splitext(os.path.relpath(path, args.in_dir))
            if ext == expect_ext:
                val = read_func(path)
                vals.append((name, path, val))
        return vals

def transform_values(args, vals, conv_func):
    new_vals = []
    for name, path, val in vals:
        new_vals.append((name, path, conv_func(args, name, path, val)))
    return new_vals

def write_outputs(args, vals, ext, write_func, combine=False):
    '''Write values `vals` to files, according to the output settings in
    `args`.  Generated filenames will end with `ext` (which should include the
    `.` in `.foo`, if desired).  Returns the path of the first output file, or
    None if there were no outputs.'''

    if args.out_file is not None and args.out_dir is not None:
        raise ValueError('can\'t specify both --out-file and --out-dir')
    if args.out_file is None and args.out_dir is None:
        raise ValueError('must specify either --out-file or --out-dir')

    if args.out_file is not None:
        if combine:
            vals_only = [val for name, path, val in vals]
            write_func(args.out_file, vals_only)
            return args.out_file
        else:
            if args.in_file is None:
                raise ValueError('can\'t combine multiple inputs '
                        'into one output in mode %r' % args.mode)
            assert len(vals) == 1
            name, path, val = vals[0]
            write_func(args.out_file, val)
            return args.out_file

    if args.out_dir is not None:
        first_output = None
        for (name, path, val) in vals:
            out_path = os.path.join(args.out_dir, name) + ext
            write_func(out_path, val)
            if first_output is None:
                first_output = out_path
        return first_output


def main(argv):
    args = build_parser().parse_args(argv)

    if args.mode == 'tmx-to-omx':
        vals = read_inputs(args, '.tmx', read_xml_file)
        vals = transform_values(args, vals, tmx_to_omx)
        first_output = write_outputs(args, vals, '.omx', write_xml)
    elif args.mode == 'omx-to-tmx':
        vals = read_inputs(args, '.omx', read_xml_file)
        vals = transform_values(args, vals, omx_to_tmx)
        first_output = write_outputs(args, vals, '.tmx', write_xml)
    elif args.mode == 'omx-to-json':
        vals = read_inputs(args, '.omx', read_xml_file)
        vals = transform_values(args, vals, omx_to_json)
        first_output = write_outputs(args, vals, '.json', write_json, combine=True)
    else:
        assert False, 'unknown mode %r' % args.mode

    if args.stamp_file is not None:
        with open(args.stamp_file, 'w'):
            pass
        # If --stamp-file is present, we always use that as the depfile output
        # path.
        first_output = args.stamp_file

    if args.dep_file is not None:
        if first_output is None:
            raise ValueError('can\'t generate depfile with no output paths')

        with open(args.dep_file, 'w') as f:
            f.write('%s: \\\n' % first_output)
            for dep in get_deps():
                f.write('    %s \\\n' % dep)
            f.write('\n\n')

if __name__ == '__main__':
    main(sys.argv[1:])
