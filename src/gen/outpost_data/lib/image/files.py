from contextlib import contextmanager
import os

from outpost_data.lib.image.image import Image, Anim
from outpost_data.lib.image.cache import CachedImage

import loadxcf


_SEARCH_PATH = ()

@contextmanager
def search_dirs(path):
    global _SEARCH_PATH

    if isinstance(path, str):
        path = (path,)

    old = _SEARCH_PATH
    _SEARCH_PATH = _SEARCH_PATH + tuple(path)
    yield
    _SEARCH_PATH = old


_DEPENDENCIES = set()

def find_file(rel):
    for base in _SEARCH_PATH:
        path = os.path.join(base, rel)
        if os.path.isfile(path):
            _DEPENDENCIES.add(path)
            return path
        else:
            # Find the deepest existing parent directory of `base/rel`.  If
            # this directory is modified, it could mean that `base/rel` now
            # exists.
            parent = os.path.split(rel)[0]
            while parent != '':
                if os.path.exists(os.path.join(base, parent)):
                    break
                else:
                    parent = os.path.split(parent)[0]
            _DEPENDENCIES.add(os.path.join(base, parent))

    raise OSError('not found: %s' % rel)

def get_dependencies():
    return _DEPENDENCIES


# Cache of images indexed by their real file path.
_LOAD_CACHE = {}

def load(rel, unit=1, layers=False, anim=None):
    k = (rel, unit, layers)
    if k not in _LOAD_CACHE:
        path = find_file(rel)
        if not layers:
            _LOAD_CACHE[k] = Image(img=CachedImage.open(path), unit=unit or 1)
        else:
            if rel.endswith('.xcf'):
                dct = CachedImage.open_layers(path)
                dct = {k: Image(img=v, unit=unit or 1) for k,v in dct.items()}
            elif rel.endswith('.aseprite') or rel.endswith('.ase'):
                dct = CachedImage.open_aseprite(path)
                dct = {k: [Image(img=i, unit=unit or 1) for i in v]
                        for k,v in dct.items()}
                if anim is not None:
                    dct = {k: Anim(v, **anim) for k,v in dct.items()}
                elif all(len(v) == 1 for v in dct.values()):
                    dct = {k: v[0] for k,v in dct.items()}

            _LOAD_CACHE[k] = dct
    return _LOAD_CACHE[k]


def load_layer_info(rel):
    path = find_file(rel)
    with open(path, 'rb') as f:
        return loadxcf.XcfReader(f).read_layer_paths()
