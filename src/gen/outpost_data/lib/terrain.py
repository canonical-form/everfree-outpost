r'''
This module is responsible for adapting terrain graphics from their
editing-friendly layout to one more suitable for the engine.

The input (for a given terrain type) is a set of 16 tile images.  The current
terrain may be present (opaque) or absent (transparent) at each of the 4
corners of the tile, giving rise to 16 combinations.  However, for gameplay
purposes, we assign terrain types to the centers of cells, not the corners.
The difference can be handled by drawing tile images with a 1/2-tile offset
from the cell positions, but this makes terrain and structure handling somewhat
tricky.  It would be more convenient to render only a single image at the
center of each cell where the terrain is present.  This module produces images
suitable for that approach.

Here is a diagram of 1/2-tile offset drawing.  The center `X` is a cell center
where the terrain is present, and the surrounding `O`s are cell centers where
it is absent.  Dots represent the terrain borders in the tile images.  Note
that the borders can be somewhat irregular relative to the cell boundaries.

    O---O---O
    |  .|.. |
    |.. +  .|
    |.  | . |
    O-+-X-+-O
    |.  |  .|
    | ..+  .|
    |   |...|
    O---O---O

If the borders in the tile images remained strictly within 1/2 cell of the
tile's "present" corners, we could simply render each combination of adjacent
cell assignments (presence/absence), extract the center tile from the resulting
image, and use that tile image when the same combination appears in-game.  But
the tile images can extend beyond the midpoint (the `+` symbols in the above
diagram).  Since we draw only in "present" cells, the output images for some
combinations may need to extend beyond a single tile size.  For example, in the
diagram above, the in-game rendering of the center `X` tile must contain the
entire 2x2-tile region shown in the diagram, because none of the adjacent `O`
tiles will be rendered, and thus they cannot contribute to drawing the more
distant parts of the terrain border.

The approach taken in this module works as follows:

    1. Take each combination of terrain assignments to adjacent cells (256 in
       total).  Then for each one...
    2. Render the 2x2 tile region surrounding the center cell.
    3. Determine which portions of that 2x2 region the center cell is
       "responsible for" - that is, those portions which will not be drawn by
       any other "present" cell.
    4. Extract from the image in (2) the region found in (3), and use that as
       the image for the center tile under the current terrain assignment.

For determining "responsibility" of a cell, we divide the 2x2 region into 8
octants, then divide each octant into four smaller triangles, as shown:

    *---+---*
    |\ 2|3 / 
    | \ | /  
    |1 \|/   
    +---+
    |0 /
    | /
    |/
    X

The central cell will be assigned 1-4 triangles, depending on the neighboring
terrain assignments:

        O---+---O        O---+ - O        O - - - X        X - - - *
        |\  |  /         |\  |            |\                        
        | \ | / |        | \ |   |        | \     |        |       |
        |  \|/           |  \|            |  \                      
        +---+   |        +---+   |        +---+   |        +---+   |
        |  /             |  /             |  /             |  /     
        | /     |        | /     |        | /     |        | /     |
        |/               |/               |/               |/       
        X - - - O        X - - - X        X - - - *        X - - - *

The lower-left `X` is the central cell.  Other corners are marked with their
cell assignments, or `*` to indicate "don't care".
'''
from outpost_data.lib.consts import *
from outpost_data.lib.image import load, Image

TILE_SIZE = TILE_SIZE2


def _separate_segment_masks():
    '''
    Load `tile-segment-mask.png`, and extract a black-and-white mask for each
    of the 8 tile segments.
    '''
    orig = load('misc/tile-segment-mask-16.png')
    chans = []
    for i in range(3):
        chan = orig.channel(i)
        inv = chan.invert()
        chans.append((inv, chan))

    masks = []
    for i in range(8):
        # Use `i` as a bitmask to select a single colored section from the
        # image.  For example, when `i` is `1` (`0b001`), we select only the
        # red portion of the image, by computing `r & ~g & ~b`.
        bits = [(i << b) & 1 for b in range(3)]
        mask = chans[0][(i >> 0) & 1]
        mask = mask.multiply(chans[1][(i >> 1) & 1])
        mask = mask.multiply(chans[2][(i >> 2) & 1])
        masks.append(mask)

    return masks


def _mask_to_rgba_impl(img):
    assert img.mode == 'L'
    rgba = img.convert('RGBA')
    rgba.putalpha(img)
    return rgba

def mask_to_rgba(img):
    return img.modify(_mask_to_rgba_impl)

def _build_octant_resp_masks():
    r'''
    Construct the responsibility masks for each octant.  Returns a list with an
    entry for each octant.  Each entry contains its octant's four masks, each a
    tile-sized black-and-white image.  The first mask in each octant entry
    corresponds to the 1-triangle version shown above, and so on up to 4.

    Octant 0 is the one shown in the diagrams above, and the remaining octants
    are numbered in clockwise order.
    '''

    segs = _separate_segment_masks()

    # Gives the start and step for each octant.  The resulting numbers (0-7)
    # are used as indices into `segs`.
    OCTANT_INFO = [
            (0, -1), (1, 1),
            (6, -1), (7, 1),
            (4, -1), (5, 1),
            (2, -1), (3, 1),
            ]

    result = []
    for o, (start, step) in enumerate(OCTANT_INFO):
        oct_result = []

        mask = segs[start]
        oct_result.append(mask)

        for i in range(1, 4):
            mask = mask.add(segs[(start + i * step) & 7])
            oct_result.append(mask)

        result.append(oct_result)

    return result

_OCTANT_RESP_MASKS = None

def octant_resp_masks():
    global _OCTANT_RESP_MASKS
    if _OCTANT_RESP_MASKS is None:
        _OCTANT_RESP_MASKS = _build_octant_resp_masks()
    return _OCTANT_RESP_MASKS

def _compute_tile_variants():
    '''
    Construct a table indicating which tiles should be drawn in the 4
    quadrants, for each of the 256 adjacent tile assignments.

    The result is a list of 256 items, where each item is a list of 4 tile
    indices, starting from the northeast corner and proceeding clockwise.
    '''

    # Table giving the bit indices of the corners of each quadrant.  Quadrant
    # corners are numbered with 0 at the northwest, proceeding clockwise.  The
    # eight adjacent tiles are numbered starting with the one to the east and
    # proceeding clockwise.  If the bit number is `-1`, the result is always 1.
    # TODO: unify all these different numbering schemes
    QUADRANT_INFO = [
            (6, 7, 0, -1),
            (-1, 0, 1, 2),
            (4, -1, 2, 3),
            (5, 6, -1, 4),
            ]

    variants = []
    for i in range(256):
        tiles = []
        for info in QUADRANT_INFO:
            bits = [((i >> bit) & 1) if bit >= 0 else 1 for bit in info]
            tiles.append(sum(b << i for i,b in enumerate(bits)))
        variants.append(tiles)

    return variants

def _compute_mask_variants():
    '''
    Construct a table indicating which mask (0-3, corresponding to 1-4
    triangles) should be used in each octant, for each of the 256 adjacent tile
    assignments.
    '''

    # Table indicating which of the adjacent-tile bits are relevant to each of
    # the octants.  The first bit in each list is the bit across from the
    # center along the short leg of the octant (for example, "north" for octant
    # 0).  The other two bits are the bit across the long leg and the bit
    # that's in the same quadrant but doesn't lie on the octant.
    OCTANT_INFO = [
            (6, 7, 0), (0, 7, 6),
            (0, 1, 2), (2, 1, 0),
            (2, 3, 4), (4, 3, 2),
            (4, 5, 6), (6, 5, 4),
            ]

    results = []
    for i in range(256):
        masks = []
        for info in OCTANT_INFO:
            a, b, c = ((i >> bit) & 1 for bit in info)
            if a:
                mask = 0
            elif b:
                mask = 1
            elif c:
                mask = 2
            else:
                mask = 3
            masks.append(mask)
        results.append(masks)

    return results


def _compute_variants():
    '''
    Construct a table combining the tile and mask variant information.  Returns
    a list with an entry for each adjacent tile assignment (256 in total), with
    each entry containing `(tile, mask1, mask2)` for each quadrant.
    '''

    tiles = _compute_tile_variants()
    masks = _compute_mask_variants()

    results = []
    for i in range(256):
        entry = []
        for j in range(4):
            t = tiles[i][j]
            m1 = masks[i][j * 2 + 0]
            m2 = masks[i][j * 2 + 1]
            entry.append((t, m1, m2))
        results.append(tuple(entry))

    return results

def load_tileset(path):
    '''
    Load a terrain tileset, returning a list of tiles.  Indices 0-15 in the
    list contain the image of the correspondingly numbered tile.  When present,
    indices 16-18 contain alternate versions of tile 15.
    '''
    img = load(path, unit=TILE_SIZE)

    grid = [
            [ 5, 11,  7],
            [10, 13, 14],
            [ 4, 12,  8],
            [ 6, 15,  9],
            [ 2,  3,  1],
            ]
    num_tiles = 16

    if img.size[1] >= 6:
        # Tileset includes alternate center tiles
        grid.append([16, 17, 18])
        num_tiles += 3

    dct = img.chop_grid(grid)
    dct[0] = Image((1, 1), unit=TILE_SIZE)

    return [dct[i] for i in range(num_tiles)]

def build_tile_sheet(ts):
    ts = ts.copy()

    if len(ts) > 16:
        # Put the first alternate tile into slot 0
        ts[0] = ts[16]

    return Image.sheet((ts[i], (i * TILE_SIZE, 0)) for i in range(16))


def generate_masked_tileset(ts):
    masks = octant_resp_masks()

    QUAD_OFFSETS = [
            (TILE_SIZE, 0),
            (TILE_SIZE, TILE_SIZE),
            (0, TILE_SIZE),
            (0, 0),
            ]

    results = []
    variants = _compute_variants()
    for entry in variants:
        quads = []
        for i, (t, m1, m2) in enumerate(entry):
            o1 = i * 2 + 0
            o2 = i * 2 + 1
            mask = masks[o1][m1].add(masks[o2][m2])
            tile = ts[t].multiply(mask_to_rgba(mask))
            quads.append(tile)

        result = Image.sheet(zip(quads, QUAD_OFFSETS), unit=TILE_SIZE)
        results.append(result)

    return results


def test_tileset(mts):
    TEST_MAP = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0],
            [0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0],
            [0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0],
            [0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0],
            [0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            ]
    imgs = []
    for y, row in enumerate(TEST_MAP):
        for x, cell in enumerate(row):
            if not cell:
                continue

            adj = \
                    (TEST_MAP[y + 0][x + 1] << 0) | \
                    (TEST_MAP[y + 1][x + 1] << 1) | \
                    (TEST_MAP[y + 1][x + 0] << 2) | \
                    (TEST_MAP[y + 1][x - 1] << 3) | \
                    (TEST_MAP[y + 0][x - 1] << 4) | \
                    (TEST_MAP[y - 1][x - 1] << 5) | \
                    (TEST_MAP[y - 1][x + 0] << 6) | \
                    (TEST_MAP[y - 1][x + 1] << 7)

            print('mask at %d,%d: %s' % (x, y, bin(adj)))

            img_x = int((x - 0.5) * TILE_SIZE)
            img_y = int((y - 0.5) * TILE_SIZE)
            imgs.append((mts[adj], (img_x, img_y)))

    return Image.sheet(imgs)
            



