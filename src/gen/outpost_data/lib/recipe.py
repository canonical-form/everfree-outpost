def parse_item_list(s):
    items = []

    for line in s.splitlines():
        for chunk in line.split(','):
            chunk = chunk.strip()
            if chunk == '':
                continue

            parts = chunk.split()
            if len(parts) == 1:
                name, = parts
                items.append((name, 1))
            elif len(parts) == 2:
                count_str, name = parts
                items.append((name, int(count_str)))
            else:
                raise ValueError('bad item reference: %r' % chunk)

    return items

