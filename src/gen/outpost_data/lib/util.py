import functools
import hashlib
import importlib
import inspect
import os
import sys

from PIL import Image


SAW_ERROR = False

def err(s):
    global SAW_ERROR
    SAW_ERROR = True
    sys.stderr.write('error: ' + s + '\n')

def warn(s):
    sys.stderr.write('warning: ' + s + '\n')


def assign_ids(names, reserved=()):
    reserved_map = dict((name, i) for i, name in enumerate(reserved))

    id_map = {}
    next_id = len(reserved_map)
    for name in sorted(names):
        if name in reserved_map:
            id_map[name] = reserved_map.pop(name)
        else:
            id_map[name] = next_id
            next_id += 1

    assert len(reserved_map) == 0, \
            'some reserved names were not used: %s' % sorted(reserved_map.keys())

    return id_map

def order_by_id(objs, id_map):
    lst = []
    for name, idx in id_map.items():
        if idx >= len(lst):
            lst.extend([None] * (idx - len(lst) + 1))
        lst[idx] = objs[name]
    return lst


def dedupe(vals, keys):
    """Deduplicate a list of objects using their pre-computed unique keys.
    Returns a list of deduplicated outputs and a map from `id(val)` to the
    index where the equivalent object resides in the list."""
    key_idx = {}
    val_idx = {}
    result = []

    for k,v in zip(keys, vals):
        if k in key_idx:
            val_idx[id(v)] = key_idx[k]
        else:
            next_idx = len(result)
            result.append(v)
            key_idx[k] = next_idx
            val_idx[id(v)] = next_idx

    return result, val_idx

def hash_image(i):
    b = bytes(x for p in i.getdata() for x in p)
    return hashlib.sha1(b).hexdigest()

def project(p):
    x, y, z = p
    return (x, y - z)


def get_python_deps():
    deps = set()
    for k, v in sys.modules.items():
        if k.startswith('outpost_data.') or k == '__main__':
            path = getattr(v, '__file__', None)
            if path is not None:
                deps.add(path)
    return deps
