import argparse
import json
import os
import textwrap
import sys
import yaml

from outpost_data.lib.atlas import ImagePacker
from outpost_data.lib import image, util


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('--asset-dir', dest='asset_dirs', default=[], action='append',
            help='directory containing art assets')
    args.add_argument('--atlas-config',
            help='YAML file describing the atlases to be generated')

    args.add_argument('--atlas-out-dir',
            help='where to store generated atlas images')
    args.add_argument('--index-out',
            help='where to write the atlas index')

    args.add_argument('--dep-file',
            help='where to write makefile-style dependencies')
    args.add_argument('--cache-dir',
            help='directory to cache generated images')

    return args


class AtlasRef:
    def __init__(self, img, crop=True):
        # Size of the original image.  (`self.size` is the size of the image
        # after cropping.)
        self.orig_size = img.px_size

        if crop:
            # `crop_offset` is the offset of the cropped image within the original
            img, self.crop_offset = img.autocrop()
        else:
            self.crop_offset = (0, 0)

        if isinstance(img, image.Anim):
            self.anim = {
                    'length': img.length,
                    'rate': img.rate,
                    'oneshot': img.oneshot,
                    }
            self.image = img.flatten()
        else:
            self.anim = None
            self.image = img

        self.size = img.px_size

        # Sheet number and position in the output atlas.  These are set by
        # `atlas.build()`.
        self.sheet = None
        self.pos = None


class Atlas:
    def __init__(self, config):
        self.config = config
        self.refs = []

    def size(self):
        return tuple(self.config['size'])

    def place(self, img):
        # TODO: hack
        scale = self.config.get('hack_scale')
        if scale is not None:
            w, h = img.px_size
            img = img.scale((w // scale, h // scale), unit=1)

        ref = AtlasRef(img, crop=self.config.get('autocrop', True))
        self.refs.append(ref)
        return ref

    def build(self):
        '''Build an atlas from all images passed to `place`.  Returns the atlas
        image(s), and as a side effect, updates all associated `AtlasRef`s with
        sheet and position information.'''
        size = tuple(self.config['size'])

        all_imgs = [ref.image for ref in self.refs]
        imgs, idx_map = util.dedupe(all_imgs, [i.hash() for i in all_imgs])

        packer = ImagePacker(size, res=self.config.get('res', 16))
        offsets = packer.place(imgs)

        for ref in self.refs:
            i = idx_map[id(ref.image)]
            ref.sheet, ref.pos = offsets[i]

        sheet_imgs = packer.build_sheets()
        num_sheets = self.config.get('sheets', 1)
        while len(sheet_imgs) < num_sheets:
            sheet_imgs.append(Image(img=CachedImage.blank(size)))
        assert len(sheet_imgs) == num_sheets, \
                'wrong number of sheets in atlas: expected %d, but got %d' % \
                (num_sheets, len(sheet_imgs))

        return [img.raw().raw() for img in sheet_imgs]



class Atlases:
    def __init__(self, config):
        self._atlases = {}
        for name, atlas_config in config.items():
            self._atlases[name] = Atlas(atlas_config)

    def __getattr__(self, k):
        return self._atlases[k]

    def __iter__(self):
        return iter(self._atlases.items())


class RefEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, AtlasRef):
            x, y = obj.pos
            return {
                    'sheet': obj.sheet,
                    'pos': obj.pos,
                    'size': obj.size,
                    'crop_offset': obj.crop_offset,
                    'orig_size': obj.orig_size,
                    'anim': obj.anim,
                    }
        return super(RefEncoder, self).default(obj)


def gen_images(argv, funcs):
    args = build_parser().parse_args(argv)

    with open(args.atlas_config) as f:
        config = yaml.load(f)
    atlases = Atlases(config)

    # Initialize the image cache
    image.init_cache(args.cache_dir)

    # Run the functions and collect their results
    results = {}
    with image.search_dirs(args.asset_dirs):
        for f in funcs:
            results[f.__name__] = f(atlases)


    # Build and save atlas images.  Populates AtlasRefs as a side effect.
    outputs = []
    for name, atlas in atlases:
        imgs = atlas.build()

        if atlas.config.get('sheets') is None:
            assert len(imgs) == 1
            img = imgs[0]
            path = os.path.join(args.atlas_out_dir, '%s.png' % name)
            img.save(path)
            outputs.append(path)
        else:
            assert len(imgs) == atlas.config['sheets']
            for i, img in enumerate(imgs):
                path = os.path.join(args.atlas_out_dir, '%s%d.png' % (name, i))
                img.save(path)
                outputs.append(path)

    # AtlasRefs now contain accurate sheet and position data.  Save the index.
    with open(args.index_out, 'w') as f:
        json.dump(results, f, cls=RefEncoder)

    # Save dependencies
    deps = image.get_dependencies() | util.get_python_deps()
    with open(args.dep_file, 'w') as f:
        f.write('%s: %s\n' % (args.index_out, ' '.join(deps)))

    # Save newly generated images to the cache
    image.save_cache()
