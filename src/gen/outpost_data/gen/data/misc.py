from outpost_data.gen.data.typed import *
from outpost_data.lib.consts import *
from outpost_data.lib import util


class Item:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.ui_name = typed(dct, 'ui_name', str, self.name)
        self.desc = typed(dct, 'desc', str, '')
        self.flags = typed(dct, 'flags', int, 0)
        self.image = dct['image']

    def process(self):
        px_x, px_y = self.image['pos']
        return {
                'name': self.name,
                'ui_name': self.ui_name,
                'desc': self.desc,
                'flags': self.flags,
                'image_pos': [px_x // ICON_SIZE, px_y // ICON_SIZE],
                # Item atlas has autocrop disabled, so all icons are ICON_SIZE
                }

def conv_item(dct):
    return dct['name'], Item(dct)

def process_items(data, id_maps):
    items = util.order_by_id(data.item, id_maps.item)
    return [i.process() for i in items]


class Recipe:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.ui_name = typed(dct, 'ui_name', str, self.name)
        self.inputs = typed(dct, 'inputs', [(str, int)])
        self.outputs = typed(dct, 'outputs', [(str, int)])
        self.ability = typed(dct, 'ability', str, 'none')
        self.crafting_class = typed(dct, 'crafting_class', str, 'none')
        self.time = typed(dct, 'time', int, 1000)

    def process(self, id_maps):
        return {
                'name': self.name,
                'ui_name': self.ui_name,
                'inputs': [(id_maps.item[name], count) for name, count in self.inputs],
                'outputs': [(id_maps.item[name], count) for name, count in self.outputs],
                'ability': id_maps.item[self.ability],
                'crafting_class': id_maps.recipe_crafting_class[self.crafting_class],
                'time': self.time,
                }

def conv_recipe(dct):
    return dct['name'], Recipe(dct)

def process_recipes(data, id_maps):
    recipes = util.order_by_id(data.recipe, id_maps.recipe)
    return [r.process(id_maps) for r in recipes]


class Block:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.flags = typed(dct, 'flags', int)
        self.terrain = typed(dct, 'terrain', Optional(str))

    def process(self, id_maps):
        dct = {
                'name': self.name,
                'flags': self.flags,
                'display_flags': (
                    (0x01 if self.terrain is not None else 0) |
                    0  #(0x02 if b.structure_id is not None else 0)
                    ),
                }

        if self.terrain is not None:
            dct.update({
                'terrain': id_maps.terrain[self.terrain],
                })

        return dct

def conv_block(dct):
    return dct['name'], Block(dct)

def process_blocks(data, id_maps):
    blocks = util.order_by_id(data.block, id_maps.block)
    return [b.process(id_maps) for b in blocks]


class Terrain:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.priority = typed(dct, 'priority', int)
        self.has_alt = typed(dct, 'has_alt', bool, False)
        self.index = typed(dct['image'], 'pos', (int, int))[1] // TILE_SIZE 

    def process(self, id_maps):
        return {
                'name': self.name,
                'priority': self.priority,
                'has_alt': self.has_alt,
                'index': self.index,
                }

def conv_terrain(dct):
    return dct['name'], Terrain(dct)

def process_terrains(data, id_maps):
    terrains = util.order_by_id(data.terrain, id_maps.terrain)
    return [t.process(id_maps) for t in terrains]


class Sprite:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.image = dct['image']
        # Position of the sprite's reference point relative to the image.  Note this may be outside
        # the actual bounding box of `self.image`.
        self.ref = typed(dct, 'ref', (int, int))

    def process(self):
        ix, iy = self.image['crop_offset']
        rx, ry = self.ref

        dct = {
                'name': self.name,
                'sheet': self.image['sheet'],
                # Position of the image in the atlas sheet
                'src_pos': self.image['pos'],
                'size': self.image['size'],
                # Where to place the upper-left corner of the image, relative
                # to the sprite's reference point
                'dest_pos': (ix - rx, iy - ry),
                }

        if self.image['anim'] is not None:
            anim = self.image['anim']
            dct.update({
                'anim_length': anim['length'],
                'anim_rate': anim['rate'],
                'anim_oneshot': anim['oneshot'],
                'anim_step': self.image['size'][0],
                })

        return dct

def conv_sprite(dct):
    return dct['name'], Sprite(dct)

def process_sprites(data, id_maps):
    sprites = util.order_by_id(data.sprite, id_maps.sprite)
    return [s.process() for s in sprites]



class PonyLayer:
    '''A single layer of a pony entity, such as body, wings, or hat.  Each layer has a reference to
    one sprite definition for each supported animation.'''
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.anims = dct['anims']
        for v in self.anims.values():
            ensure_type(v, { 'sprite': str, 'flip': Optional(bool) })

    def anim_names(self):
        return self.anims.keys()

    def process(self, id_maps):
        anims = [None] * len(id_maps.pony_layer_anim)
        for i, k in enumerate(id_maps.pony_layer_anim):
            anim_def = self.anims.get(k)
            if anim_def is None:
                util.warn('PonyLayer %r has no definition for anim %r' % (self.name, k))
                anims[i] = { 'sprite': 0, 'flip': False }
                continue

            sprite_id = id_maps.sprite[anim_def['sprite']]
            anims[i] = {
                    'sprite': sprite_id,
                    'flip': anim_def.get('flip', False),
                    }

        return {
                'name': self.name,
                'anims': anims,
                }

def conv_pony_layer(dct):
    return dct['name'], PonyLayer(dct)

def process_pony_layers(data, id_maps):
    pony_layers = util.order_by_id(data.pony_layer, id_maps.pony_layer)
    return [pl.process(id_maps) for pl in pony_layers]



class Extra:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.func = dct['func']

    def process(self, id_maps):
        return self.func(id_maps)

def conv_extra(dct):
    return dct['name'], Extra(dct)

def process_extras(data, id_maps):
    return dict((k, v.process(id_maps)) for k, v in data.extra.objs.items())
