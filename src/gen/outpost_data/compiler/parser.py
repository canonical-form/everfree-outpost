from collections import namedtuple
import re


Header = namedtuple('Header', ('kind', 'arg'))
Assign = namedtuple('Assign', ('key', 'value'))
AssignBlock = namedtuple('AssignBlock', ('key', 'lines'))

class LineReader:
    def __init__(self, lines):
        self.lines = iter(lines)
        self.pushed = []
        self.iter_eof = False

    def take(self):
        if self.pushed:
            return self.pushed.pop()

        try:
            l = next(self.lines)
            if l.endswith('\n'):
                l = l[:-1]
            return l
        except StopIteration:
            self.iter_eof = True
            return ''

    def eof(self):
        return self.iter_eof and len(self.pushed) == 0

    def put_back(self, val):
        self.pushed.append(val)

    def take_joined(self):
        line = self.take()
        while line.endswith('\\'):
            line = line[:-1] + self.take()
        return line

def parse_lines(lines):
    lines = LineReader(lines)

    while not lines.eof():
        line = lines.take_joined()
        line, _, comment = line.partition('#')
        if not line or line.isspace():
            continue

        # Try to parse an Assign
        key, equal, value = line.partition('=')
        if equal == '=':
            yield Assign(key.strip(), value.strip())
            continue

        # Try to parse a block header
        key, colon, rest = line.partition(':')
        if colon == ':' and (not rest or rest.isspace()):
            # Collect lines that are part of the block (indent > outer_indent)
            outer_indent = len(line) - len(line.lstrip())
            block_lines = []
            block_indent = None

            while not lines.eof():
                l = lines.take()
                indent = len(l) - len(l.lstrip())

                if indent == len(l):
                    block_lines.append(l)
                    continue

                if indent > outer_indent:
                    if block_indent is None or indent < block_indent:
                        block_indent = indent
                    block_lines.append(l)
                else:
                    lines.put_back(l)
                    break

            # Dedent block_lines based on the lowest indent seen within
            block_lines = [l[block_indent:] for l in block_lines]
            yield AssignBlock(key.strip(), block_lines)
            continue

        # Try to parse a Header
        kind, space, arg = line.lstrip().partition(' ')
        yield Header(kind.strip(), arg.strip())


Defn = namedtuple('Defn', ('kind', 'name', 'args'))

def parse(lines):
    kind, name = None, None
    args = {}

    for x in parse_lines(lines):
        if isinstance(x, Header):
            if kind is not None:
                yield Defn(kind, name, args)
            kind = x.kind
            name = x.arg
            args = {'name': name}
        elif isinstance(x, Assign):
            if kind is None:
                raise ValueError('found assignment before first definition header')
            if x.key in args:
                raise ValueError('duplicate key %r in definition %r' % (x.key, name))
            args[x.key] = x.value
        elif isinstance(x, AssignBlock):
            if kind is None:
                raise ValueError('found assignment before first definition header')
            if x.key in args:
                raise ValueError('duplicate key %r in definition %r' % (x.key, name))
            args[x.key] = '\n'.join(x.lines)
        else:
            assert False, 'unrecognized parser event: %r' % (x,)

    if kind is not None:
        yield Defn(kind, name, args)
