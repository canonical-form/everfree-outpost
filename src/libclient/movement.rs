use std::prelude::v1::*;
use types::*;
use common::movement::InputBits;
use common::proto::game::Request;

pub struct Movement {
    prev_time: Time,
    prev_bits: InputBits,
    prev_dir: u8,
    prev_speed: u8,
}

impl Movement {
    pub fn new() -> Movement {
        Movement {
            prev_time: 0,
            prev_bits: InputBits::empty(),
            prev_dir: 0,
            prev_speed: 0,
        }
    }

    pub fn key_down(&mut self, now: Time, add_bits: InputBits) -> Option<Request> {
        let bits = self.prev_bits | add_bits;
        if bits == self.prev_bits {
            return None;
        }

        self.set_bits(now, bits)
    }

    pub fn key_up(&mut self, now: Time, remove_bits: InputBits) -> Option<Request> {
        let bits = self.prev_bits & !remove_bits;
        if bits == self.prev_bits {
            return None;
        }

        self.set_bits(now, bits)
    }


    fn set_bits(&mut self, now: Time, bits: InputBits) -> Option<Request> {
        let dir = bits.to_direction_index().unwrap_or(self.prev_dir);
        let speed = bits.to_speed();

        if dir == self.prev_dir && speed == self.prev_speed {
            return None;
        }

        let msg =
            // TODO: be smarter about setting the input delay (currently always 64ms)
            if self.prev_speed == 0 { Request::InputStart(64, dir, speed) }
            else { Request::InputChange((now - self.prev_time) as u16, dir, speed) };

        self.prev_time = now;
        self.prev_bits = bits;
        self.prev_dir = dir;
        self.prev_speed = speed;

        Some(msg)
    }
}
