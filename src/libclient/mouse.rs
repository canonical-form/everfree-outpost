use common::types::*;

use data::data;
use debug::Debug;
use state::structure::Structures;

pub struct MouseSelect {
    /// Current mouse position.  This could be made into an argument to `update`, but it has to be
    /// stored somewhere, and it might as well be here.
    pos: V2,

    /// Indicates whether the mouse is near the pawn's position (within NEAR_DIST).
    near_pawn: bool,

    /// Cell position where the mouse was last pressed.
    down_cell: Option<V3>,

    // The result of `find_structure_at(tile_pos)` is cached in `self.structure`, because it's
    // expensive to compute.  It's updated only when `tile_pos` or `near_pawn` changes, or when
    // invalidated by structure changes.

    /// Position of the selected cell, if any.
    hit_cell: Option<V3>,

    /// ID of the selected structure, if any.
    hit_structure: Option<StructureId>,
}

/// Max distance from center that is considered "near".
const NEAR_DIST: i32 = 64;
const NEAR_DIST2: i32 = NEAR_DIST * NEAR_DIST;

impl MouseSelect {
    pub fn new() -> MouseSelect {
        MouseSelect {
            pos: (-1).into(),
            near_pawn: false,
            down_cell: None,
            hit_cell: None,
            hit_structure: None,
        }
    }

    pub fn reset(&mut self) {
        *self = Self::new();
    }

    pub fn set_pos(&mut self, pos: V2) {
        self.pos = pos;
    }

    pub fn mouse_down(&mut self) {
        if self.down_cell.is_none() {
            self.down_cell = self.hit_cell;
        }
    }

    pub fn mouse_up(&mut self) {
        self.down_cell = None;
    }

    /// Update the mouse selection state.
    ///
    /// Note that this method consults `Structures::changes`, so it must be called every frame
    /// (since that is how often the `changes` get cleared).
    pub fn update(&mut self,
                  structures: &Structures,
                  viewport: Region<V2>,
                  pawn_pos: V3,
                  override_pos: Option<V3>) {
        // `self.pos` is relative to the viewport.  Convert it to world coordinates.
        let pos = self.pos + viewport.min;

        // We do some extra checks to avoid unnecessary updates to `self.structure`.  (The helper
        // function `find_structure_at` iterates over all structures, which may be expensive.)

        let mut block_selection = false;

        let cell_pos =
            if let Some(pos) = override_pos {
                self.near_pawn = true;
                pos
            } else {
                // Update self.near_pawn.  If it changed, invalidate self.structure.
                let pawn_pos2 = V2::new(pawn_pos.x, pawn_pos.y - pawn_pos.z);
                // TODO: hacky adjustment, don't know why we need this but structure2.frag is
                // similar
                let pawn_pos2 = pawn_pos2 + V2::new(16, -16);
                self.near_pawn = (pos - pawn_pos2).mag2() < NEAR_DIST2;
                if !self.near_pawn {
                    // The cursor is not near the pawn, so nothing can be selected.
                    // TODO: change this to use the reachability map once that's implemented
                    block_selection = true;
                }

                // Update self.hit_cell.
                let cell_xv = pos.div_floor(TILE_SIZE);
                let cell_z = pawn_pos.div_floor(TILE_SIZE).z;
                V3::new(cell_xv.x, cell_xv.y + cell_z, cell_z)
            };

        if let Some(down_pos) = self.down_cell {
            if cell_pos != down_pos {
                // The player has pressed the mouse button over a cell, then moved to a
                // different cell.  That means no cell is currently selected.
                block_selection = true;
            }
        }


        if block_selection {
            self.hit_cell = None;
            self.hit_structure = None;
            return;
        }

        // On the frame after block_selection transitions from `true` to `false`, `hit_cell` is
        // `None`, so the following check will always detect a change (`Some(cell_pos) != None`),
        // and `hit_structure` will be updated appropriately.

        let mut structure_invalid = structures.has_changes();

        if Some(cell_pos) != self.hit_cell {
            self.hit_cell = Some(cell_pos);
            structure_invalid = true;
        }

        if structure_invalid {
            // hit_cell has already been updated, so cell_pos == hit_cell.unwrap()
            self.hit_structure = find_structure_at(structures, cell_pos);
        }
    }

    pub fn update_debug(&self, debug: &mut Debug) {
        debug.mouse_hit_pos = self.hit_cell.unwrap_or((-1).into());
        debug.mouse_hit_structure = self.hit_structure.map(|s| s.unwrap());
        debug.mouse_near_pawn = self.near_pawn;
    }

    pub fn structure(&self) -> Option<StructureId> {
        self.hit_structure
    }

    pub fn cell_pos(&self) -> Option<V3> {
        self.hit_cell
    }

    pub fn near_pawn(&self) -> bool {
        self.near_pawn
    }
}

fn find_structure_at(structures: &Structures, pos: V3) -> Option<StructureId> {
    let data = data();
    // TODO: use the struct_pos cache instead
    let mut max_layer = -1;
    let mut max_sid = None;
    for (&sid, s) in structures.iter() {
        let t = data.template(s.template_id);

        // Wrapping point-in-region test
        const MASK: i32 = CHUNK_SIZE * LOCAL_SIZE - 1;
        let rel_pos = (pos - s.pos) & MASK;

        let bounds = Region::sized(t.size());
        if !bounds.contains(rel_pos) {
            continue;
        }

        let idx = bounds.index(rel_pos);
        if !t.shape()[idx].contains(B_OCCUPIED) {
            continue;
        }

        let layer = t.layer as i32;
        if layer > max_layer {
            max_layer = layer;
            max_sid = Some(sid);
        }
    }

    max_sid
}
