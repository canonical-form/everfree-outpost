#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Key {
    MoveLeft,
    MoveRight,
    MoveUp,
    MoveDown,
    Run,

    OpenInventory,
    OpenAbilities,
    OpenEquipment,
    Interact,
    Destroy,

    ToggleDebugPanel,
    ToggleCursor,

    Select,
    Cancel,

    Hotbar(i8),

    DebugLogSwitch,
}

impl Key {
    pub fn from_code(code: u8) -> Option<Key> {
        use self::Key::*;
        match code {
            0 => Some(MoveLeft),
            1 => Some(MoveRight),
            2 => Some(MoveUp),
            3 => Some(MoveDown),
            4 => Some(Run),

            10 => Some(OpenAbilities),
            11 => Some(OpenEquipment),
            12 => Some(OpenInventory),
            13 => Some(Interact),
            14 => Some(Destroy),

            20 => Some(ToggleDebugPanel),
            21 => Some(ToggleCursor),

            30 => Some(Select),
            31 => Some(Cancel),

            41 ... 49 => Some(Hotbar(code as i8 - 41)),

            99 => Some(DebugLogSwitch),

            _ => None,
        }
    }
}


bitflags! {
    pub flags Modifiers: u8 {
        const MOD_SHIFT =    0x01,
    }
}

impl Modifiers {
    pub fn shift(&self) -> bool {
        self.contains(MOD_SHIFT)
    }
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct KeyEvent {
    pub code: Key,
    pub mods: Modifiers,
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Button {
    Left,
    Middle,
    Right,
    WheelUp,
    WheelDown,
}

impl Button {
    pub fn from_code(code: u8) -> Option<Button> {
        match code {
            1 => Some(Button::Left),
            2 => Some(Button::Middle),
            3 => Some(Button::Right),
            4 => Some(Button::WheelUp),
            5 => Some(Button::WheelDown),

            _ => None,
        }
    }
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct ButtonEvent {
    pub button: Button,
    pub mods: Modifiers,
}
