use std::cell::{Cell, RefCell};
use std::marker::PhantomData;
use std::ops::Deref;
use colorspace::LchUv;
use common::appearance::{Appearance, AppearanceFlags};
use common::appearance::{APP_WING, APP_HORN};
use outpost_ui::context::Void;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::Button;
use outpost_ui::widgets::container::{Group, ChildWidget, Align};
use outpost_ui::widgets::map_event::MapEvent;

use ui2::atlas::{Card, Border};
use ui2::context::Context;
use ui2::widgets::{ToggleRow, LabeledSpinner};
use ui2::widgets::color;
use util;


const PALETTE: &'static [(u8, u8, u8)] = &[
    ( 71,  65,  65),
    (244, 245, 236),
    (201, 201, 214),
    (108, 133, 157),
    (161, 223, 231),
    (144, 108,  78),
    ( 81, 173, 169),
    (117, 185, 227),
    (207, 181, 131),
    (202, 172, 224),
    ( 78,  72, 121),
    (224, 146, 176),
    (237, 237, 163),
    (117, 228, 195),
    (102, 136, 209),
    (175, 124, 196),
    ( 96, 162,  81),
    (224, 148,  67),
    (238, 209,  96),
    (210,  81,  76),
    (186,  54, 127),
    (130,  75, 187),
    (136, 220,  90),
];

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
enum ColorSlot {
    Coat,
    Mane,
}

pub struct PonyEdit<'a> {
    state: &'a RefCell<PonyEditState>,
}

#[derive(Debug)]
pub struct PonyEditState {
    tribe: usize,
    mane: usize,
    tail: usize,

    coat_color: (u8, u8, u8),
    coat_precise: LchUv,
    mane_color: (u8, u8, u8),
    mane_precise: LchUv,

    picker_mode: color::multi::Mode,
    picker_target: ColorSlot,

    focus_colors: usize,
    focus_left: usize,
    focus_top: usize,
    focus_main: usize,
}

impl PonyEditState {
    pub fn new() -> PonyEditState {
        PonyEditState {
            tribe: 0,
            mane: 0,
            tail: 0,
            coat_color: (50, 100, 200),
            coat_precise: LchUv { l: 0., c: 0., h: 0. },
            mane_color: (35, 70, 140),
            mane_precise: LchUv { l: 0., c: 0., h: 0. },
            picker_mode: color::multi::Mode::Plot,
            picker_target: ColorSlot::Coat,

            focus_colors: 0,
            focus_left: 0,
            focus_top: 0,
            focus_main: 0,
        }
    }
}

impl<'a> PonyEdit<'a> {
    /// Build a new equipment dialog.  Inventory 0 is the "normal" inventory and inventory 1 is the
    /// equipment inventory.
    pub fn new(state: &'a RefCell<PonyEditState>) -> PonyEdit<'a> {
        PonyEdit {
            state: state,
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
            where Ctx: Context,
                  CtxRef: Deref<Target=Ctx>,
                  F: FnOnce(CtxRef, &Widget<Ctx, Event=Appearance>) -> R {
        let mut s = self.state.borrow_mut();
        let s = &mut *s;


        let flags =
            [AppearanceFlags::empty(),
             APP_WING,
             APP_HORN,
            ][s.tribe];
        let app = Appearance {
            flags: flags,
            coat_color: s.coat_color,
            mane_color: s.mane_color,
            mane: s.mane as u8,
            tail: s.tail as u8,
        };
        let right = CharDisplay::new(app);


        static TRIBE_ICONS: [Card; 3] =
            [Card::IconEarthPony, Card::IconPegasus, Card::IconUnicorn];

        let tribe = ToggleRow::new(&TRIBE_ICONS, s.tribe);
        let mane = LabeledSpinner::new("Mane", 3, s.mane);
        let tail = LabeledSpinner::new("Tail", 2, s.tail);

        let coat_box = color::ColorBox::new(s.coat_color, s.picker_target == ColorSlot::Coat);
        let mane_box = color::ColorBox::new(s.mane_color, s.picker_target == ColorSlot::Mane);

        let tribe_cell = util::mut_to_cell(&mut s.tribe);
        let mane_cell = util::mut_to_cell(&mut s.mane);
        let tail_cell = util::mut_to_cell(&mut s.tail);
        let target_cell = util::mut_to_cell(&mut s.picker_target);

        let focus_colors = util::mut_to_cell(&mut s.focus_colors);
        let focus_left = util::mut_to_cell(&mut s.focus_left);
        let focus_top = util::mut_to_cell(&mut s.focus_top);
        let focus_main = util::mut_to_cell(&mut s.focus_main);

        let colors = Group::horiz(contents![
            ChildWidget::new(coat_box, |()| target_cell.set(ColorSlot::Coat)),
            ChildWidget::new(mane_box, |()| target_cell.set(ColorSlot::Mane)),
        ]).focus(focus_colors).spacing(3);
        let left = Group::vert(contents![
            ChildWidget::new(tribe, |x| tribe_cell.set(x)).align(Align::Center),
            ChildWidget::new(mane, |x| mane_cell.set(x)).align(Align::Stretch),
            ChildWidget::new(tail, |x| tail_cell.set(x)).align(Align::Stretch),
            ChildWidget::new(colors, |()| ()).align(Align::Center),
        ]).focus(focus_left).spacing(4);


        let top = Group::horiz(contents![
            ChildWidget::new(left, |_| ()).align(Align::Center),
            ChildWidget::new(right, |_| ()).align(Align::Center),
        ]).focus(focus_top).spacing(6);


        let mode_cell = util::mut_to_cell(&mut s.picker_mode);

        let (precise_mut, color_mut) = match target_cell.get() {
            ColorSlot::Coat => (&mut s.coat_precise, &mut s.coat_color),
            ColorSlot::Mane => (&mut s.mane_precise, &mut s.mane_color),
        };
        let precise_cell = util::mut_to_cell(precise_mut);
        let color_cell = util::mut_to_cell(color_mut);
        let color_pane = ColorPane::new(color_cell, mode_cell, precise_cell);


        let done_btn = Button::new("Done");

        let w = Group::vert(contents![
            ChildWidget::new(top.ignore_event(), |v| match v {}).align(Align::Center),
            ChildWidget::new(color_pane.ignore_event(), |v| match v {}).align(Align::Stretch),
            ChildWidget::new(done_btn, |()| {
                info!("clicked done!");
                app
            }).align(Align::Center),
        ]).focus(focus_main).spacing(6);

        f(ctx, &w)
    }
}

const PADDING: i32 = 2;

impl<'a, Ctx: Context> Widget<Ctx> for PonyEdit<'a> {
    type Event = Appearance;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let extra = Point::new(PADDING * 2, PADDING * 2);
        self.with_inner(ctx, |ctx, w| w.min_size(ctx)) + extra
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let bounds = Rect::sized(ctx.cur_bounds().size())
            .inset(PADDING, PADDING, PADDING, PADDING);
        ctx.with_bounds(bounds, |ctx| {
            self.with_inner(ctx, |ctx, w| w.on_paint(ctx));
        })
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        let bounds = Rect::sized(ctx.cur_bounds().size())
            .inset(PADDING, PADDING, PADDING, PADDING);
        ctx.with_bounds(bounds, |ctx| {
            self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt))
        })
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let bounds = Rect::sized(ctx.cur_bounds().size())
            .inset(PADDING, PADDING, PADDING, PADDING);
        ctx.with_bounds(bounds, |ctx| {
            self.with_inner(&mut *ctx, |ctx, w| w.on_mouse(ctx, evt))
        })
    }
}


// TODO: somehow unify this with equipment::CharDisplay
pub struct CharDisplay<'a> {
    appearance: Appearance,
    _marker: PhantomData<&'a ()>,
}

impl<'a> CharDisplay<'a> {
    pub fn new(appearance: Appearance) -> CharDisplay<'a> {
        CharDisplay {
            appearance: appearance,
            _marker: PhantomData,
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for CharDisplay<'a> {
    type Event = Void;

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(96 + 2 * 2,
                   96 + 2 * 2)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_border(Border::Frame);
        ctx.draw_special_pony(self.appearance, Point::new(2, 2));
    }
}


pub struct ColorPane<'a> {
    color: &'a Cell<(u8, u8, u8)>,
    mode: &'a Cell<color::multi::Mode>,
    precise: &'a Cell<LchUv>,
}

impl<'a> ColorPane<'a> {
    fn new(color: &'a Cell<(u8, u8, u8)>,
           mode: &'a Cell<color::multi::Mode>,
           precise: &'a Cell<LchUv>) -> ColorPane<'a> {
        ColorPane {
            color: color,
            mode: mode,
            precise: precise,
        }
    }

    fn inner_mode<Ctx: Context+'a>(&self) -> impl Widget<Ctx, Event=()>+'a {
        static MODE_ICONS: [Card; 3] =
            [Card::IconColorSwatches, Card::IconColorPlot, Card::IconColorSliders];

        let mode = self.mode;
        ToggleRow::vert(&MODE_ICONS, mode.get() as usize)
            .map_event(move |_, idx| match idx {
                0 => mode.set(color::multi::Mode::Swatch),
                1 => mode.set(color::multi::Mode::Plot),
                2 => mode.set(color::multi::Mode::Slider),
                _ => {
                    error!("invalid mode index: {}", idx);
                },
            })
    }

    fn inner_picker<Ctx: Context+'a>(&self) -> impl Widget<Ctx, Event=()>+'a {
        let color = self.color;
        color::MultiColorPicker::new(color.get(),
                                     self.mode.get(),
                                     &PALETTE,
                                     self.precise)
            .map_event(move |_, c| color.set(c))
    }

    fn bounds<Ctx: Context>(&self, ctx: &Ctx) -> (Rect, Rect) {
        let full_bounds = Rect::sized(ctx.cur_bounds().size());

        let w_mode = self.inner_mode();
        let size = w_mode.min_size(ctx);
        let y = (full_bounds.size().y - size.y) / 2;
        let bounds_mode = Rect::sized(size) + full_bounds.min + Point::new(0, y);
        let full_bounds = full_bounds.inset(size.x + 2, 0, 0, 0);

        let bounds_picker = full_bounds;

        (bounds_mode,
         bounds_picker)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for ColorPane<'a> {
    type Event = ();

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(100, 50)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let (bounds_mode, bounds_picker) = self.bounds(ctx);

        {
            let w = self.inner_mode();
            ctx.with_bounds(bounds_mode, |ctx| w.on_paint(ctx));
        }

        {
            let w = self.inner_picker();
            ctx.with_bounds(bounds_picker, |ctx| w.on_paint(ctx));
        }
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        let (bounds_mode, bounds_picker) = self.bounds(ctx);

        {
            let w = self.inner_mode();
            try_handle!(ctx.with_bounds(bounds_mode, |ctx| w.on_key(ctx, evt)));
        }

        {
            let w = self.inner_picker();
            try_handle!(ctx.with_bounds(bounds_picker, |ctx| w.on_key(ctx, evt)));
        }

        UIResult::Unhandled
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let (bounds_mode, bounds_picker) = self.bounds(ctx);

        {
            let w = self.inner_mode();
            try_handle!(ctx.with_bounds(bounds_mode, |ctx| {
                if ctx.mouse_target() {
                    w.on_mouse(ctx, evt)
                } else {
                    UIResult::Unhandled
                }
            }));
        }

        {
            let w = self.inner_picker();
            try_handle!(ctx.with_bounds(bounds_picker, |ctx| {
                if ctx.mouse_target() {
                    w.on_mouse(ctx, evt)
                } else {
                    UIResult::Unhandled
                }
            }));
        }

        UIResult::Unhandled
    }
}


