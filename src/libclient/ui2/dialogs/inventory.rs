use common::types::*;
use std::cell::Cell;
use std::ops::Deref;
use outpost_ui::context::Void;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Group, ChildWidget};
use outpost_ui::widgets::text::WrappedLabel;

use data::data;
use input::Key;
use state::inventory;
use ui2::atlas::{Card, Border, Font};
use ui2::context::{Context, DragItem};
use ui2::widgets;


pub struct InfoPane {
    pub id: ItemId,
}

impl<Ctx: Context> Widget<Ctx> for InfoPane {
    type Event = Void;

    fn min_size(&self, ctx: &Ctx) -> Point {
        // Pane should be 6 item-slots wide
        let slot_size = ctx.card_size(Card::ItemSlotRoundInactive);
        let line_height = ctx.text_line_height(Font::Default);
        Point::new(slot_size.x * 6, 2 * line_height)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        // Inset by 1 px on top and bottom so that the frame's lines align with the item slot
        // lines.  (Item slots get an extra 1px padding to make room for the active-slot
        // highlight.)
        let draw_rect = Rect::sized(ctx.cur_bounds().size()).inset(0, 0, 1, 1);
        ctx.draw_border_at(Border::Frame, draw_rect);

        if self.id == NO_ITEM {
            return;
        }

        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(2, 2, 2, 2);
        ctx.with_bounds(bounds, |ctx| {
            let data = data();
            let item = data.item_def(self.id);

            let title = WrappedLabel::new(item.ui_name(), bounds.size().x)
                .style(Font::Bold);
            let body = WrappedLabel::new(item.desc(), bounds.size().x);
            let group = Group::vert(contents![
                ChildWidget::new(title, |_| ()),
                ChildWidget::new(body, |_| ()),
            ]).spacing(2);
            group.on_paint(ctx);
        });
    }
}


pub struct Inventory<'a> {
    inv: &'a inventory::Inventory,
    focus: &'a Cell<usize>,
}

pub enum InventoryEvent {
    Transfer {
        src_slot: u8,
        dest_slot: u8,
        quantity: u8,
    },
    SetHotbar {
        slot: u8,
        item: ItemId,
    },
}

impl<'a> Inventory<'a> {
    pub fn new(inv: &'a inventory::Inventory, focus: &'a Cell<usize>) -> Inventory<'a> {
        Inventory {
            inv: inv,
            focus: focus,
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
            where Ctx: Context,
                  CtxRef: Deref<Target=Ctx>,
                  F: FnOnce(CtxRef, &Widget<Ctx, Event=usize>) -> R {
        let hide_slot = ctx.drag_item().and_then(|item| {
            if item.inv_id == self.inv.id {
                Some(item.slot)
            } else {
                None
            }
        });

        let grid = widgets::Inventory::new(self.inv, self.focus)
            .hide_slot(hide_slot);
        let info = InfoPane {
            id: self.inv.items.get(self.focus.get()).map_or(NO_ITEM, |i| i.id),
        };
        let w = Group::horiz(contents![
            ChildWidget::new(grid, |i| i),
            ChildWidget::new(info, |v| match v {}),
        ]).spacing(2);

        f(ctx, &w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Inventory<'a> {
    type Event = InventoryEvent;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(ctx, |ctx, w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx, |ctx, w| w.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt)).map(|_| {
            unreachable!()
        }).or_else(|| {
            let key = match evt {
                KeyEvent::Down((k, _)) => k,
                _ => return UIResult::Unhandled,
            };

            match key {
                Key::Hotbar(slot) => {
                    if let Some(item) = self.inv.items.get(self.focus.get()) {
                        UIResult::Event(InventoryEvent::SetHotbar {
                            slot: slot as u8,
                            item: item.id,
                        })
                    } else {
                        UIResult::NoEvent
                    }
                },
                _ => UIResult::Unhandled,
            }
        })
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(&mut *ctx, |ctx, w| w.on_mouse(ctx, evt)).and_then(|idx| {
            if let Some(drag) = ctx.end_drag() {
                UIResult::Event(InventoryEvent::Transfer {
                    src_slot: drag.slot,
                    dest_slot: idx as u8,
                    quantity: drag.quantity,
                })
            } else {
                let item = unwrap_or!(self.inv.items.get(idx), {
                    error!("inventory click index out of range: {}", idx);
                    return UIResult::NoEvent;
                });

                if item.id == NO_ITEM {
                    return UIResult::NoEvent;
                }

                ctx.start_drag(DragItem {
                    item: item.id,
                    quantity: item.quantity,
                    inv_id: self.inv.id,
                    slot: idx as u8,
                });
                UIResult::NoEvent
            }
        })
    }
}
