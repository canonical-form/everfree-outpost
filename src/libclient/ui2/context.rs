use common::types::*;
use std::mem;
use std::ops::{Deref, DerefMut};
use colorspace::LchUv;
use common::Appearance;
use outpost_ui::context::{
    self,
    Context as ContextTrait,
    Void,
};
use outpost_ui::event::{KeyEvent, KeyInterp};
use outpost_ui::geom::{Point, Rect};

use data::data;
use input::{Key, Modifiers, MOD_SHIFT, Button};
use ui2::atlas::{Card, Border, Font, UIDataExt};
use ui2::util::*;

pub struct ContextState {
    state: context::CommonState,
    drag_item: Option<DragItem>,
}

pub trait ContextOutput {
    fn draw_ui2(&mut self, src: Region<V2>, pos: V2);
    fn draw_ui2_tiled(&mut self, src: Region<V2>, dest: Region<V2>);
    fn draw_item(&mut self, src: V2, dest: V2);

    /// Draw the entire scratch buffer at `dest`, for debug purposes.
    fn draw_special_debug(&mut self, dest: V2);
    fn draw_special_pony(&mut self, appearance: Appearance, dest: V2);
    fn draw_special_color_plot_xy(&mut self, color: LchUv, dest: Region<V2>);
    fn draw_special_color_plot_z(&mut self, color: LchUv, dest: Region<V2>);
    fn draw_special_color_slider(&mut self,
                                 color: (u8, u8, u8),
                                 channel: Channel,
                                 dest: Region<V2>);
    fn draw_special_palette_color(&mut self,
                                  palette: &[(u8, u8, u8)],
                                  index: usize,
                                  dest: Region<V2>);

    fn push_clip(&mut self, clip: Region<V2>) -> Option<Region<V2>>;
    fn pop_clip(&mut self, old: Option<Region<V2>>);
}

#[allow(unused_variables)]
impl ContextOutput for () {
    fn draw_ui2(&mut self, src: Region<V2>, pos: V2) { unimplemented!() }
    fn draw_ui2_tiled(&mut self, src: Region<V2>, dest: Region<V2>) { unimplemented!() }
    fn draw_item(&mut self, src: V2, dest: V2) { unimplemented!() }

    fn draw_special_debug(&mut self, dest: V2) { unimplemented!() }
    fn draw_special_pony(&mut self, appearance: Appearance, dest: V2) { unimplemented!() }
    fn draw_special_color_plot_xy(&mut self, color: LchUv, dest: Region<V2>) {
        unimplemented!()
    }
    fn draw_special_color_plot_z(&mut self, color: LchUv, dest: Region<V2>) {
        unimplemented!()
    }
    fn draw_special_color_slider(&mut self,
                                 color: (u8, u8, u8),
                                 channel: Channel,
                                 dest: Region<V2>) { unimplemented!() }
    fn draw_special_palette_color(&mut self,
                                  palette: &[(u8, u8, u8)],
                                  index: usize,
                                  dest: Region<V2>) { unimplemented!() }

    // These are no-ops instead of `unimplemented!()` because `with_surface` uses them, and widgets
    // may call `with_surface` even in non-paint event handlers to adjust mouse positioning.
    fn push_clip(&mut self, clip: Region<V2>) -> Option<Region<V2>> { None }
    fn pop_clip(&mut self, old: Option<Region<V2>>) {}
}

pub struct ContextImpl<'r, O: 'r> {
    inner: &'r mut ContextState,
    //renderer: &'a mut Renderer,
    output: O,
}

impl<'r, O> Deref for ContextImpl<'r, O> {
    type Target = ContextState;
    fn deref(&self) -> &ContextState {
        self.inner
    }
}

impl<'r, O> DerefMut for ContextImpl<'r, O> {
    fn deref_mut(&mut self) -> &mut ContextState {
        self.inner
    }
}

#[derive(Debug)]
pub struct DragItem {
    pub item: ItemId,
    pub quantity: u8,
    pub inv_id: InventoryId,
    pub slot: u8,
}

impl ContextState {
    pub fn new(bounds: Region<V2>) -> ContextState {
        ContextState {
            state: context::CommonState::new(from_region2(bounds)),
            drag_item: None,
        }
    }

    pub fn get<'r>(&'r mut self) -> ContextImpl<'r, ()> {
        ContextImpl::new(self, ())
    }

    pub fn with_output<'r, O>(&'r mut self, output: O) -> ContextImpl<'r, O>
            where O: ContextOutput {
        ContextImpl::new(self, output)
    }
}

impl<'r, O: ContextOutput> ContextImpl<'r, O> {
    pub fn new(state: &'r mut ContextState,
               output: O) -> ContextImpl<'r, O> {
        ContextImpl {
            inner: state,
            output: output,
        }
    }

    pub fn resize(&mut self, size: Point) {
        self.state.set_bounds(Rect::sized(size));
    }

    fn draw_ui(&mut self, src: Region<V2>, dest: V2) {
        let dest = dest + to_v2(self.cur_bounds().min);
        self.output.draw_ui2(src, dest);
    }

    fn draw_ui_tiled(&mut self, src: Region<V2>, dest: Region<V2>) {
        let dest = dest + to_v2(self.cur_bounds().min);
        self.output.draw_ui2_tiled(src, dest);
    }
}

impl<'r, O: ContextOutput> context::Context for ContextImpl<'r, O> {
    fn state(&self) -> &context::CommonState { &self.state }
    fn state_mut(&mut self) -> &mut context::CommonState { &mut self.state }

    type Key = (Key, Modifiers);
    fn interp_key(&self, evt: KeyEvent<Self::Key>) -> Option<KeyInterp> {
        let interp = match evt {
            KeyEvent::Down((key, mods)) => {
                let mag = if mods.contains(MOD_SHIFT) { 10 } else { 1 };
                match key {
                    Key::MoveLeft => KeyInterp::FocusX(-mag),
                    Key::MoveRight => KeyInterp::FocusX(mag),
                    Key::MoveDown => KeyInterp::FocusY(mag),
                    Key::MoveUp => KeyInterp::FocusY(-mag),

                    Key::Select => KeyInterp::Activate,

                    _ => return None,
                }
            },
            _ => return None,
        };
        Some(interp)
    }

    type Button = (Button, Modifiers);

    type DropData = Void;


    type TextStyle = Font;

    fn text_size(&self, s: &str, style: Font) -> Point {
        let data = data();
        let font = data.font(style);
        let mut w = 0;
        for g in font.iter_glyphs(s) {
            w = g.x_offset + g.src.size().x;
        }
        Point::new(w, font.height as i32)
    }

    fn text_space_width(&self, style: Font) -> i32 {
        let data = data();
        let font = data.font(style);
        font.space_width as i32
    }

    fn text_line_height(&self, style: Self::TextStyle) -> i32 {
        let data = data();
        let font = data.font(style);
        font.height as i32
    }

    fn draw_text(&mut self, s: &str, style: Font) {
        self.draw_text_at(s, style, Point::new(0, 0))
    }


    type ButtonStyle = ButtonStyle;

    fn button_border_size(&self, style: ButtonStyle) -> (Point, Point) {
        match style {
            ButtonStyle::Default => (Point::new(4, 3), Point::new(4, 3)),
            ButtonStyle::SpinnerLeft =>
                (self.card_size(Card::SpinnerButtonLeftUp), Point::new(0, 0)),
            ButtonStyle::SpinnerRight =>
                (self.card_size(Card::SpinnerButtonRightUp), Point::new(0, 0)),
        }
    }

    fn draw_button(&mut self, style: ButtonStyle, state: context::ButtonState) {
        match style {
            ButtonStyle::Default => {
                let border = match state {
                    context::ButtonState::Up => Border::ButtonUp,
                    context::ButtonState::Down => Border::ButtonDown,
                    context::ButtonState::Active |
                    context::ButtonState::Hover => Border::ButtonActive,
                };
                self.draw_border(border);
            },
            ButtonStyle::SpinnerLeft => {
                let card = match state {
                    context::ButtonState::Up => Card::SpinnerButtonLeftUp,
                    context::ButtonState::Down => Card::SpinnerButtonLeftDown,
                    context::ButtonState::Active |
                    context::ButtonState::Hover => Card::SpinnerButtonLeftActive,
                };
                self.draw_card(card);
            },
            ButtonStyle::SpinnerRight => {
                let card = match state {
                    context::ButtonState::Up => Card::SpinnerButtonRightUp,
                    context::ButtonState::Down => Card::SpinnerButtonRightDown,
                    context::ButtonState::Active |
                    context::ButtonState::Hover => Card::SpinnerButtonRightActive,
                };
                self.draw_card(card);
            },
        }
    }


    type ScrollBarStyle = ScrollBarStyle;

    fn scroll_bar_width(&self, _style: ScrollBarStyle) -> i32 {
        data().ui_border(Border::ScrollBar).min_size().x
    }

    fn scroll_bar_handle_height(&self, _style: ScrollBarStyle) -> i32 {
        data().ui_card(Card::ScrollBarThumb).size().x
    }

    fn scroll_bar_top_button_height(&self, _style: ScrollBarStyle) -> i32 {
        0
    }

    fn scroll_bar_bottom_button_height(&self, _style: ScrollBarStyle) -> i32 {
        0
    }

    fn draw_scroll_bar(&mut self,
                       style: ScrollBarStyle,
                       val: i16,
                       max: i16,
                       _top_pressed: bool,
                       _bottom_pressed: bool) {
        let width = self.scroll_bar_width(style);
        let bounds = to_region2(self.cur_bounds()).inset(0, -width, 0, 0);
        let size = bounds.size();

        match style {
            ScrollBarStyle::Default => {
                self.draw_border(Border::ScrollBar);

                // Caps are each 4px high, but can overlap by 1px with the handle.  The handle
                // itself is 5px high.  Valid handle offsets are 0 to max_offset, inclusive.
                let max_offset = size.y - 3 * 2 - 5;
                // Top handle is 4px, but 1px can overlap.
                let base_offset = 3;
                let offset = base_offset + 
                    if max != 0 { max_offset * val as i32 / max as i32 } else { 0 };
                self.draw_card_at(Card::ScrollBarThumb, Point::new(0, offset));
            },
        }
    }


    type DialogStyle = ();

    fn dialog_border_size(&self, _style: Self::DialogStyle) -> (Point, Point) {
        let title_h = self.border_min_size(Border::DialogTitle).y;
        let connector_h = 3;
        (Point::new(6, title_h + connector_h + 6),
         Point::new(6, 6))
    }

    fn draw_dialog(&mut self, title: &str, _style: Self::DialogStyle) {
        let data = data();

        let size = self.cur_bounds().size();
        let area = Rect::sized(size);

        // Title area calculation.  Connector positioning uses this, but connectors must be drawn
        // before the title area.
        let title_h = self.border_min_size(Border::DialogTitle).y;
        let title_area = area.inset(0, 0, 0, size.y - title_h);

        // Connectors
        let conn_size = data.ui_border(Border::DialogConnectors).size();
        let conn_max_w = size.x;
        // Round to nearest whole number of copies.
        let conn_w = conn_max_w / conn_size.x * conn_size.x;
        let conn_x = (conn_max_w - conn_w) / 2;
        // Connectors are 5px high, but there is only a 3px gap between the title and body.  This
        // makes sure we don't draw truncated connectors under the rounded parts of the title bar.
        let conn_y = title_area.max.y - 2;
        let conn_area = Rect::sized(Point::new(conn_w, conn_size.y)) + Point::new(conn_x, conn_y);
        self.draw_border_at(Border::DialogConnectors, conn_area);

        // Title background
        self.draw_border_at(Border::DialogTitle, title_area);

        // Title text
        let text_size = self.text_size(title, Font::DialogTitle);
        let text_pos = Point::new((title_area.size().x - text_size.x) / 2,
                                  (title_area.size().y - text_size.y) / 2 + 1);
        self.draw_text_at(title, Font::DialogTitle, text_pos);

        // Content area background
        let content_area = area.inset(0, 0, title_h + 3, 0);
        self.draw_border_at(Border::DialogContent, content_area);
    }


    // Custom impl of `with_surface` to set clipping on the `geom` as well.
    fn with_surface<F: FnOnce(&mut Self) -> R, R>(&mut self,
                                                  size: Point,
                                                  src_pos: Point,
                                                  dest_rect: Rect,
                                                  func: F) -> R {
        let old = self.state_mut().push_surface(size, src_pos, dest_rect);
        let clip = self.state().clip.unwrap();
        let geom_old = self.output.push_clip(to_region2(clip));

        let r = func(self);

        self.state_mut().pop_surface(old);
        self.output.pop_clip(geom_old);

        r
    }
}


/// This trait is essentially an inherent impl on `ContextImpl`, but it lets us hide the lifetimes
/// from users.  This simplifies a lot of signatures in `ui2::widgets`.
pub trait Context: context::Context<Key=(Key, Modifiers),
                                    Button=(Button, Modifiers),
                                    TextStyle=Font,
                                    ButtonStyle=ButtonStyle,
                                    ScrollBarStyle=ScrollBarStyle> {
    fn draw_item(&mut self, id: ItemId);
    fn draw_item_at(&mut self, id: ItemId, pos: Point);

    fn draw_text_at(&mut self, s: &str, style: Font, pos: Point);

    fn draw_card(&mut self, card: Card);
    fn draw_card_at(&mut self, card: Card, pos: Point);
    fn draw_card_partial(&mut self, card: Card, src: Rect, dest: Point);
    fn card_size(&self, card: Card) -> Point;

    fn draw_border(&mut self, border: Border);
    fn draw_border_at(&mut self, border: Border, rect: Rect);
    fn border_min_size(&self, border: Border) -> Point;

    fn start_drag(&mut self, drag_item: DragItem);
    fn end_drag(&mut self) -> Option<DragItem>;
    fn drag_item(&self) -> Option<&DragItem>;

    fn draw_special_debug(&mut self, dest: Point);
    fn draw_special_pony(&mut self, appearance: Appearance, dest: Point);
    fn draw_special_color_plot_xy(&mut self, color: LchUv);
    fn draw_special_color_plot_z(&mut self, color: LchUv);
    fn draw_special_color_slider(&mut self, color: (u8, u8, u8), channel: Channel);
    fn draw_special_palette_color(&mut self, palette: &[(u8, u8, u8)], index: usize);
}

impl<'r, O: ContextOutput> Context for ContextImpl<'r, O> {
    fn draw_item(&mut self, id: ItemId) {
        self.draw_item_at(id, Point::new(0, 0));
    }

    fn draw_item_at(&mut self, id: ItemId, pos: Point) {
        let data = data();
        let item = data.item_def(id);
        let dest = to_v2(self.cur_bounds().min + pos);
        self.output.draw_item(item.image_pos(), dest);
    }


    fn draw_text_at(&mut self, s: &str, style: Font, pos: Point) {
        let data = data();
        let pos = to_v2(pos);
        let font = data.font(style);
        for g in font.iter_glyphs(s) {
            let dest = pos + V2::new(g.x_offset, 0);
            self.draw_ui(g.src, dest);
        }
    }


    fn draw_card(&mut self, card: Card) {
        self.draw_card_at(card, Point::new(0, 0));
    }

    fn draw_card_at(&mut self, card: Card, pos: Point) {
        let data = data();
        let card = data.ui_card(card);
        let src = card.bounds();
        self.draw_ui(src, to_v2(pos));
    }

    fn draw_card_partial(&mut self, card: Card, src: Rect, dest: Point) {
        let data = data();
        let card = data.ui_card(card);
        let src = to_region2(src) + card.bounds().min;
        self.draw_ui(src, to_v2(dest));
    }

    fn card_size(&self, card: Card) -> Point {
        let data = data();
        let card = data.ui_card(card);
        from_v2(card.size())
    }


    fn draw_border(&mut self, border: Border) {
        let size = self.cur_bounds().size();
        self.draw_border_at(border, Rect::sized(size));
    }

    fn draw_border_at(&mut self, border: Border, rect: Rect) {
        let data = data();
        // TODO: properly handle cases where rect.size < border min size
        let border = data.ui_border(border);

        let src = border.bounds();
        let dest = to_region2(rect);
        let (x0, x1, y0, y1) = border.inset();

        // Coordinate adjustments for drawing sections
        #[derive(Clone, Copy, PartialEq, Eq, Debug)]
        enum Section { Start, Middle, End };

        #[inline]
        fn adj_range(c0: i32, c1: i32, in0: i32, in1: i32, sec: Section) -> (i32, i32) {
            match sec {
                Section::Start => (c0, c0 + in0),
                Section::Middle => (c0 + in0, c1 - in1),
                Section::End => (c1 - in1, c1),
            }
        }

        let adj_rect = |r: Region<V2>, x_sec, y_sec| {
            let (min_x, max_x) = adj_range(r.min.x, r.max.x, x0, x1, x_sec);
            let (min_y, max_y) = adj_range(r.min.y, r.max.y, y0, y1, y_sec);
            Region::new(V2::new(min_x, min_y),
                        V2::new(max_x, max_y))

        };

        let mut draw = |x_sec, y_sec| {
            let src = adj_rect(src, x_sec, y_sec);
            let dest = adj_rect(dest, x_sec, y_sec);
            self.draw_ui_tiled(src, dest);
        };

        let xm = src.size().x - x0 - x1;
        let ym = src.size().y - y0 - y1;

        if x0 > 0 && y0 > 0 { draw(Section::Start, Section::Start); }
        if xm > 0 && y0 > 0 { draw(Section::Middle, Section::Start); }
        if x1 > 0 && y0 > 0 { draw(Section::End, Section::Start); }

        if x0 > 0 && ym > 0 { draw(Section::Start, Section::Middle); }
        if xm > 0 && ym > 0 { draw(Section::Middle, Section::Middle); }
        if x1 > 0 && ym > 0 { draw(Section::End, Section::Middle); }

        if x0 > 0 && y1 > 0 { draw(Section::Start, Section::End); }
        if xm > 0 && y1 > 0 { draw(Section::Middle, Section::End); }
        if x1 > 0 && y1 > 0 { draw(Section::End, Section::End); }
    }

    fn border_min_size(&self, border: Border) -> Point {
        let data = data();
        let border = data.ui_border(border);
        let (x0, x1, y0, y1) = border.inset();
        Point::new(x0 + x1, y0 + y1)
    }


    fn start_drag(&mut self, drag_item: DragItem) {
        info!("start drag: {:?}", drag_item);
        self.drag_item = Some(drag_item);
    }

    fn end_drag(&mut self) -> Option<DragItem> {
        info!("end drag: {:?}", self.drag_item);
        mem::replace(&mut self.drag_item, None)
    }

    fn drag_item(&self) -> Option<&DragItem> {
        self.drag_item.as_ref()
    }


    fn draw_special_debug(&mut self, dest: Point) {
        let dest = to_v2(dest) + to_v2(self.cur_bounds().min);
        self.output.draw_special_debug(dest);
    }

    fn draw_special_pony(&mut self, appearance: Appearance, dest: Point) {
        let dest = to_v2(dest) + to_v2(self.cur_bounds().min);
        self.output.draw_special_pony(appearance, dest);
    }

    fn draw_special_color_plot_xy(&mut self, color: LchUv) {
        let dest = to_region2(self.cur_bounds());
        self.output.draw_special_color_plot_xy(color, dest);
    }

    fn draw_special_color_plot_z(&mut self, color: LchUv) {
        let dest = to_region2(self.cur_bounds());
        self.output.draw_special_color_plot_z(color, dest);
    }

    fn draw_special_color_slider(&mut self, color: (u8, u8, u8), channel: Channel) {
        let dest = to_region2(self.cur_bounds());
        self.output.draw_special_color_slider(color, channel, dest);
    }

    fn draw_special_palette_color(&mut self, palette: &[(u8, u8, u8)], index: usize) {
        let dest = to_region2(self.cur_bounds());
        self.output.draw_special_palette_color(palette, index, dest);
    }
}



#[derive(Clone, Copy, Debug)]
pub enum ButtonStyle {
    Default,
    SpinnerLeft,
    SpinnerRight,
}

impl Default for ButtonStyle {
    fn default() -> ButtonStyle {
        ButtonStyle::Default
    }
}

impl context::ButtonStyle for ButtonStyle {
    fn default_off() -> ButtonStyle {
        ButtonStyle::Default
    }

    fn default_on() -> ButtonStyle {
        ButtonStyle::Default
    }
}


#[derive(Clone, Copy, Debug)]
pub enum ScrollBarStyle {
    Default,
}

impl Default for ScrollBarStyle {
    fn default() -> ScrollBarStyle {
        ScrollBarStyle::Default
    }
}


/// Color channel enum, for `draw_special_color_slider`.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
#[repr(u8)]
pub enum Channel {
    Red,
    Green,
    Blue,
}

impl Channel {
    pub fn get(self, color: (u8, u8, u8)) -> u8 {
        let (r, g, b) = color;
        match self {
            Channel::Red => r,
            Channel::Green => g,
            Channel::Blue => b,
        }
    }

    pub fn set(self, color: (u8, u8, u8), x: u8) -> (u8, u8, u8) {
        let (r, g, b) = color;
        match self {
            Channel::Red => (x, g, b),
            Channel::Green => (r, x, b),
            Channel::Blue => (r, g, x),
        }
    }
}
