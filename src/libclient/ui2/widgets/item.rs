use common::types::*;
use std::cell::Cell;
use std::cmp;

use outpost_ui::context::Focus;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Grid, Computed, ChildWidget, GenWidgets};

use data::data;
use state::inventory;
use ui2::atlas::{Card, Font, InventoryLayout, UIDataExt};
use ui2::context::Context;
use ui2::util::*;


/// Displays a single item icon, with quantity.  Produces event `()` on click.
pub struct Item {
    id: ItemId,
    qty: Option<u16>,
}

impl Item {
    pub fn new(id: ItemId, qty: Option<u16>) -> Item {
        Item {
            id: id,
            qty: qty,
        }
    }
}

impl<Ctx: Context> Widget<Ctx> for Item {
    type Event = ();

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(16, 16)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_item(self.id);

        if let Some(qty) = self.qty {
            let text = quantity_string(qty);
            let style = Font::ItemCount;
            let text_size = ctx.text_size(&text, style);
            // Place text in the bottom-right corner.
            let bounds = Rect::sized(text_size) + ctx.cur_bounds().size() - text_size;
            ctx.with_bounds(bounds, |ctx| ctx.draw_text(&text, style));
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Up(_) if ctx.mouse_pressed_over() => UIResult::Event(()),
            _ => UIResult::Unhandled,
        }
    }
}

pub fn quantity_string(quantity: u16) -> String {
    if quantity < 1000 {
        format!("{}", quantity)
    } else if quantity < 10000 {
        let frac = quantity / 100 % 10;
        let whole = quantity / 1000;
        format!("{}.{}k", whole, frac)
    } else {
        let thousands = quantity / 1000;
        format!("{}k", thousands)
    }
}


#[derive(Clone, Copy, Debug)]
pub enum SlotStyle {
    Round,
    Square,
}

impl Default for SlotStyle {
    fn default() -> SlotStyle {
        SlotStyle::Round
    }
}

/// Displays a slot that holds an item.  The slot will be highlighted if focused.  Produces event
/// `()` on click.
pub struct ItemSlot {
    id: ItemId,
    qty: Option<u16>,
    style: SlotStyle,
    public: bool,
}

impl ItemSlot {
    pub fn new(id: ItemId, qty: Option<u16>) -> ItemSlot {
        ItemSlot {
            id: id,
            qty: qty,
            style: SlotStyle::default(),
            public: false,
        }
    }

    pub fn style(self, style: SlotStyle) -> Self {
        ItemSlot {
            style: style,
            .. self
        }
    }

    pub fn public(self, public: bool) -> Self {
        ItemSlot { public, ..self }
    }
}

impl<Ctx: Context> Widget<Ctx> for ItemSlot {
    type Event = ();

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(20, 20)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let focus = ctx.state().focus;
        ctx.draw_card(item_slot_card(self.style, self.public, focus));

        let bounds = Rect::new(2, 2, 18, 18);
        ctx.with_bounds(bounds, |ctx| Item::new(self.id, self.qty).on_paint(ctx));
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let bounds = Rect::new(2, 2, 18, 18);
        ctx.with_bounds(bounds, |ctx| Item::new(self.id, self.qty).on_mouse(ctx, evt))
    }
}

fn item_slot_card(style: SlotStyle, public: bool, focus: Focus) -> Card {
    match (style, public, focus) {
        (SlotStyle::Round, false, Focus::Inactive) => Card::ItemSlotRoundInactive,
        (SlotStyle::Round, false, Focus::Semiactive) => Card::ItemSlotRoundSemiactive,
        (SlotStyle::Round, false, Focus::Active) => Card::ItemSlotRoundActive,
        (SlotStyle::Square, false, Focus::Inactive) => Card::ItemSlotSquareInactive,
        (SlotStyle::Square, false, Focus::Semiactive) => Card::ItemSlotSquareSemiactive,
        (SlotStyle::Square, false, Focus::Active) => Card::ItemSlotSquareActive,

        (SlotStyle::Round, true, Focus::Inactive) => Card::ItemSlotPublicRoundInactive,
        (SlotStyle::Round, true, Focus::Semiactive) => Card::ItemSlotPublicRoundSemiactive,
        (SlotStyle::Round, true, Focus::Active) => Card::ItemSlotPublicRoundActive,
        (SlotStyle::Square, true, Focus::Inactive) => Card::ItemSlotPublicSquareInactive,
        (SlotStyle::Square, true, Focus::Semiactive) => Card::ItemSlotPublicSquareSemiactive,
        (SlotStyle::Square, true, Focus::Active) => Card::ItemSlotPublicSquareActive,
    }
}


#[derive(Clone, Copy, Debug)]
pub enum FrameStyle {
    Yellow,
    Green,
    Blue,
    Red,
}

impl Default for FrameStyle {
    fn default() -> FrameStyle {
        FrameStyle::Yellow
    }
}

/// Displays a larger frame that holds an item.  Produces event `()` on click.
pub struct ItemFrame {
    id: ItemId,
    qty: Option<u16>,
    style: FrameStyle,
}

impl ItemFrame {
    pub fn new(id: ItemId, qty: Option<u16>) -> ItemFrame {
        ItemFrame {
            id: id,
            qty: qty,
            style: FrameStyle::default(),
        }
    }

    pub fn style(self, style: FrameStyle) -> Self {
        ItemFrame {
            style: style,
            .. self
        }
    }
}

impl<Ctx: Context> Widget<Ctx> for ItemFrame {
    type Event = ();

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(24, 24)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_card(item_frame_card(self.style));

        let bounds = Rect::new(4, 4, 20, 20);
        ctx.with_bounds(bounds, |ctx| Item::new(self.id, self.qty).on_paint(ctx));
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let bounds = Rect::new(4, 4, 20, 20);
        ctx.with_bounds(bounds, |ctx| Item::new(self.id, self.qty).on_mouse(ctx, evt))
    }
}

fn item_frame_card(style: FrameStyle) -> Card {
    match style {
        FrameStyle::Yellow => Card::ItemFrameYellow,
        FrameStyle::Green => Card::ItemFrameGreen,
        FrameStyle::Blue => Card::ItemFrameBlue,
        FrameStyle::Red => Card::ItemFrameRed,
    }
}


/// Displays the current contents of an inventory as a grid of `ItemSlot`s.  Produces event `idx`
/// on click of slot `idx`.
pub struct Inventory<'a> {
    inv: &'a inventory::Inventory,
    cols: usize,
    focus: &'a Cell<usize>,

    /// If `Some`, the indicated slot will be shown as empty, regardless of its actual contents.
    hide_slot: Option<u8>,
}

impl<'a> Inventory<'a> {
    pub fn new(inv: &'a inventory::Inventory, focus: &'a Cell<usize>) -> Inventory<'a> {
        Inventory {
            inv: inv,
            cols: 6,
            focus: focus,
            hide_slot: None,
        }
    }

    pub fn cols(self, cols: usize) -> Inventory<'a> {
        Inventory {
            cols: cols,
            .. self
        }
    }

    pub fn hide_slot(self, hide_slot: Option<u8>) -> Inventory<'a> {
        Inventory {
            hide_slot: hide_slot,
            .. self
        }
    }


    fn inner<Ctx: Context+'a>(&self) -> impl Widget<Ctx, Event=usize>+'a {
        let items = &self.inv.items as &[_];
        let public = self.inv.is_public();
        let hide_slot = self.hide_slot;
        let contents = GenWidgets::new(0 .. items.len(), move |idx| {
            let slot = if hide_slot == Some(idx as u8) {
                ItemSlot::new(NO_ITEM, None).public(public)
            } else {
                let item: inventory::Item = items[idx];
                let qty = if item.id != NO_ITEM { Some(item.quantity as u16) } else { None };
                ItemSlot::new(item.id, qty).public(public)
            };
            ChildWidget::new(slot, move |()| idx)
        });
        Grid::new(self.focus, self.cols, contents)
            .hover_focus(true)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Inventory<'a> {
    type Event = usize;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.inner().min_size(ctx)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.inner().on_paint(ctx);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.inner().on_key(ctx, evt)
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.inner().on_mouse(ctx, evt)
    }
}


/// Displays the contents of an inventory using a custom `InventoryLayout`.  Produces event `idx`
/// on click of slot `idx`.
pub struct CustomInventory<'a> {
    inv: &'a inventory::Inventory,
    layout: InventoryLayout,
    focus: &'a Cell<usize>,

    /// If `Some`, the indicated slot will be shown as empty, regardless of its actual contents.
    hide_slot: Option<u8>,
}

impl<'a> CustomInventory<'a> {
    pub fn new(inv: &'a inventory::Inventory,
               layout: InventoryLayout,
               focus: &'a Cell<usize>) -> CustomInventory<'a> {
        CustomInventory {
            inv: inv,
            layout: layout,
            focus: focus,
            hide_slot: None,
        }
    }

    pub fn hide_slot(self, hide_slot: Option<u8>) -> CustomInventory<'a> {
        CustomInventory {
            hide_slot: hide_slot,
            .. self
        }
    }


    fn inner<Ctx: Context+'a>(&self) -> impl Widget<Ctx, Event=usize>+'a {
        let items = &self.inv.items as &[_];
        let layout_len = data().inventory_layout(self.layout).len();
        let len = cmp::min(items.len(), layout_len);

        let hide_slot = self.hide_slot;

        let contents = GenWidgets::new(0 .. len, move |idx| {
            let slot = if hide_slot == Some(idx as u8) {
                ItemSlot::new(NO_ITEM, None)
            } else {
                let item: inventory::Item = items[idx];
                let qty = if item.id != NO_ITEM { Some(item.quantity as u16) } else { None };
                ItemSlot::new(item.id, qty)
            };
            ChildWidget::new(slot, move |()| idx)
        });

        let layout_name = self.layout;
        Computed::new(self.focus, contents, move |idx| {
            let data = data();
            let layout = data.inventory_layout(layout_name);
            from_v2(layout.slots()[idx].display_pos())
        })
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for CustomInventory<'a> {
    type Event = usize;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.inner().min_size(ctx)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.inner().on_paint(ctx);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.inner().on_key(ctx, evt)
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.inner().on_mouse(ctx, evt)
    }
}
