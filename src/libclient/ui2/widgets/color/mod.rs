pub mod plot;
pub mod swatch;
pub mod slider;
pub mod multi;
pub mod box_;

pub use self::plot::PlotColorPicker;
pub use self::swatch::SwatchColorPicker;
pub use self::slider::SliderColorPicker;
pub use self::multi::MultiColorPicker;
pub use self::box_::ColorBox;
