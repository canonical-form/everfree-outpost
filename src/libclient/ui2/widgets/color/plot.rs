use std::cell::Cell;
use std::cmp;
use std::marker::PhantomData;

use colorspace::{Srgb, LchUv};
use outpost_ui::event::{MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;

use ui2::atlas::Border;
use ui2::context::Context;


fn clamp_u8(x: f32) -> u8 {
    if x < 0.0 {
        return 0;
    }
    if x > 1.0 {
        return 255;
    }
    (x * 255.) as u8
}

fn clamp_srgb(srgb: Srgb) -> (u8, u8, u8) {
    (clamp_u8(srgb.r),
     clamp_u8(srgb.g),
     clamp_u8(srgb.b))
}


trait PlotMode {
    fn min_size() -> Point;
    fn draw<Ctx: Context>(ctx: &mut Ctx, color: LchUv);
    fn new_color(old: LchUv, pos: Point, size: Point) -> LchUv;
}

struct XYMode;
impl PlotMode for XYMode {
    fn min_size() -> Point { Point::new(102, 52) }

    fn draw<Ctx: Context>(ctx: &mut Ctx, color: LchUv) {
        ctx.draw_special_color_plot_xy(color);
    }

    fn new_color(old: LchUv, pos: Point, size: Point) -> LchUv {
        LchUv {
            h: pos.x as f32 / (size.x - 1) as f32,
            c: pos.y as f32 / (size.y - 1) as f32,
            .. old
        }
    }
}

struct ZMode;
impl PlotMode for ZMode {
    // X size should be an odd number so that the crosshair/arrows will be centered
    fn min_size() -> Point { Point::new(13, 52) }

    fn draw<Ctx: Context>(ctx: &mut Ctx, color: LchUv) {
        ctx.draw_special_color_plot_z(color);
    }

    fn new_color(old: LchUv, pos: Point, size: Point) -> LchUv {
        LchUv {
            l: pos.y as f32 / (size.y - 1) as f32,
            .. old
        }
    }
}


struct Plot<'a, M: PlotMode> {
    color: (u8, u8, u8),

    /// Precise LCh(uv) version of the RGB `color`.  We update this when the user clicks a new
    /// position on the plot, and reuse it as long as it still matches `color`.  This lets us avoid
    /// repeated LCh(uv) -> RGB -> LCh(uv) conversions, which cause the color values to drift
    /// unexpectedly.
    precise: &'a Cell<LchUv>,

    _marker: PhantomData<M>,
}

impl<'a> Plot<'a, XYMode> {
    pub fn xy(color: (u8, u8, u8),
              precise: &'a Cell<LchUv>) -> Plot<'a, XYMode> {
        Plot {
            color: color,
            precise: precise,
            _marker: PhantomData,
        }
    }
}

impl<'a> Plot<'a, ZMode> {
    pub fn z(color: (u8, u8, u8),
             precise: &'a Cell<LchUv>) -> Plot<'a, ZMode> {
        Plot {
            color: color,
            precise: precise,
            _marker: PhantomData,
        }
    }
}

impl<'a, M: PlotMode> Plot<'a, M> {
    fn color_srgb(&self) -> Srgb {
        Srgb {
            r: self.color.0 as f32 / 255.,
            g: self.color.1 as f32 / 255.,
            b: self.color.2 as f32 / 255.,
        }
    }

    /// Like `self.precise.get()`, but check that the result still matches `color`, and refresh it
    /// if needed.
    fn get_precise(&self) -> LchUv {
        let lch_uv = self.precise.get();
        let clamped = clamp_srgb(lch_uv.to_xyz().to_srgb());

        if clamped != self.color {
            // The color was changed externally.  Our cached "precise color" is no longer valid.
            let new_lch_uv = self.color_srgb().to_xyz().to_lch_uv();
            self.precise.set(new_lch_uv);
            return new_lch_uv;
        }

        lch_uv
    }
}

impl<'a, Ctx: Context, M: PlotMode> Widget<Ctx> for Plot<'a, M> {
    type Event = (u8, u8, u8);

    fn min_size(&self, _ctx: &Ctx) -> Point {
        M::min_size()
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_border(Border::Frame);
        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(1, 1, 1, 1);
        ctx.with_bounds(bounds, |ctx| {
            M::draw(ctx, self.get_precise());
        });
    }

    fn on_mouse(&self, ctx: &mut Ctx, _evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(1, 1, 1, 1);
        ctx.with_bounds(bounds, |ctx| {
            if !ctx.mouse_pressed_over() {
                return UIResult::Unhandled;
            }
            let pos = ctx.mouse_pos().unwrap();
            let size = ctx.cur_bounds().size();

            let old_lch_uv = self.get_precise();
            let lch_uv = M::new_color(old_lch_uv, pos, size);
            self.precise.set(lch_uv);
            let srgb = lch_uv.to_xyz().to_srgb();

            UIResult::Event(clamp_srgb(srgb))
        })
    }
}


pub struct PlotColorPicker<'a> {
    color: (u8, u8, u8),
    precise: &'a Cell<LchUv>,
}

impl<'a> PlotColorPicker<'a> {
    pub fn new(color: (u8, u8, u8),
               precise: &'a Cell<LchUv>) -> PlotColorPicker<'a> {
        PlotColorPicker {
            color: color,
            precise: precise,
        }
    }

    fn inner1(&self) -> Plot<XYMode> {
        Plot::xy(self.color, self.precise)
    }

    fn bounds1<Ctx: Context>(&self, ctx: &Ctx) -> Rect {
        let size = ctx.cur_bounds().size();
        let s2 = self.inner2().min_size(ctx);

        Rect::sized(size).inset(0, s2.x + 2, 0, 0)
    }

    fn inner2(&self) -> Plot<ZMode> {
        Plot::z(self.color, self.precise)
    }

    fn bounds2<Ctx: Context>(&self, ctx: &Ctx) -> Rect {
        let size = ctx.cur_bounds().size();
        let s2 = self.inner2().min_size(ctx);

        Rect::new(size.x - s2.x, 0, size.x, size.y)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for PlotColorPicker<'a> {
    type Event = (u8, u8, u8);

    fn min_size(&self, ctx: &Ctx) -> Point {
        let s1 = self.inner1().min_size(ctx);
        let s2 = self.inner2().min_size(ctx);
        Point::new(s1.x + 2 + s2.x,
                   cmp::max(s1.y, s2.y))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let bounds1 = self.bounds1(ctx);
        ctx.with_bounds(bounds1, |ctx| {
            self.inner1().on_paint(ctx);
        });

        let bounds2 = self.bounds2(ctx);
        ctx.with_bounds(bounds2, |ctx| {
            self.inner2().on_paint(ctx);
        });
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let bounds1 = self.bounds1(ctx);
        try_handle!(ctx.with_bounds(bounds1, |ctx| {
            if ctx.mouse_target() {
                self.inner1().on_mouse(ctx, evt)
            } else {
                UIResult::Unhandled
            }
        }));

        let bounds2 = self.bounds2(ctx);
        try_handle!(ctx.with_bounds(bounds2, |ctx| {
            if ctx.mouse_target() {
                self.inner2().on_mouse(ctx, evt)
            } else {
                UIResult::Unhandled
            }
        }));

        UIResult::Unhandled
    }
}
