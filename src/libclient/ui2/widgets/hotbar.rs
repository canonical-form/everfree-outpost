use common::types::*;
use std::cell::Cell;

use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Grid, ChildWidget, GenWidgets};

use hotbar;
use input::Key;
use state::inventory;
use ui2::atlas::{Border, Font};
use ui2::context::Context;
use ui2::widgets::item::{ItemFrame, FrameStyle};


pub struct Hotbar<'a> {
    hotbar: &'a hotbar::Hotbar,
    inv: Option<&'a inventory::Inventory>,
    energy_cur: i32,
    energy_max: i32,
    energy_style: Border,
}

impl<'a> Hotbar<'a> {
    pub fn new(hotbar: &'a hotbar::Hotbar,
               inv: Option<&'a inventory::Inventory>,
               energy_cur: i32,
               energy_max: i32,
               energy_style: Border) -> Hotbar<'a> {
        Hotbar {
            hotbar: hotbar,
            inv: inv,
            energy_cur: energy_cur,
            energy_max: energy_max,
            energy_style: energy_style,
        }
    }

    fn with_inner<Ctx, F, R>(&self, f: F) -> R
        where Ctx: Context,
              F: FnOnce(&Widget<Ctx, Event=u8>) -> R {
        let contents = GenWidgets::new(0 .. 9, move |idx| {
            let idx = idx as u8;
            let item = self.hotbar.item_id(idx);
            let is_active = self.hotbar.active_index() == Some(idx);
            let is_ability = self.hotbar.is_ability(idx);

            let qty =
                if is_ability || item == NO_ITEM { None }
                else { Some(self.inv.map_or(0, |inv| inv.count(item))) };

            let style =
                if !is_active { FrameStyle::Yellow }
                else if is_ability { FrameStyle::Blue }
                else { FrameStyle::Green };

            ChildWidget::new(ItemFrame::new(item, qty).style(style), move |_| idx)
        });
        let focus = Cell::new(0);
        let w = Grid::new(&focus, 9, contents).spacing(1);

        f(&w)
    }
}

/// Size of item frames (both width and height)
const FRAME_SIZE: i32 = 24;
/// Size of gaps between item frames
const FRAME_SPACING: i32 = 1;
/// Distance from left edge to first item frame.
const FRAME_LEFT_MARGIN: i32 = 8;
/// Distance from last item frame to right edge.
const FRAME_RIGHT_MARGIN: i32 = 7;
const FRAME_TOP_MARGIN: i32 = 1;

/// Distance from left edge of widget to left edge of the energy bar.
const ENERGY_LEFT_MARGIN: i32 = 13;
/// Distance from right edge of the energy bar to the right edge of the widget.
const ENERGY_RIGHT_MARGIN: i32 = 13;
/// Position of the top of the energy bar.
const ENERGY_TOP: i32 = 29;
/// Height of the energy bar.
const ENERGY_HEIGHT: i32 = 4;

pub enum HotbarEvent {
    Set {
        slot: u8,
        item: ItemId,
    },
    Select {
        slot: u8,
    },
}

impl<'a, Ctx: Context> Widget<Ctx> for Hotbar<'a> {
    type Event = HotbarEvent;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let w = FRAME_LEFT_MARGIN + 9 * FRAME_SIZE + 8 * FRAME_SPACING + FRAME_RIGHT_MARGIN;
        let h = ctx.border_min_size(Border::HotbarBg).y;
        Point::new(w, h)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let size = self.min_size(ctx);
        let area = Rect::sized(size);

        // Hotbar background
        ctx.draw_border_at(Border::HotbarBg, area.inset(0, 0, 2, 0));

        // Energy bar
        let energy_rect = area.inset(ENERGY_LEFT_MARGIN, ENERGY_RIGHT_MARGIN,
                                     ENERGY_TOP, -(ENERGY_TOP + ENERGY_HEIGHT));
        let energy_w = {
            let w = energy_rect.size().x;
            let cur = self.energy_cur as i32;
            let max = self.energy_max as i32;
            if max == 0 {
                0
            } else {
                (w * cur + (max / 2)) / max
            }
        };
        if energy_w > 0 {
            let energy_fill = energy_rect.inset(0, energy_rect.size().x - energy_w, 0, 0);
            ctx.draw_border_at(self.energy_style, energy_fill);
        }

        // Energy amount
        let amt_str = format!("{} / {}", self.energy_cur, self.energy_max);
        let amt_size = ctx.text_size(&amt_str, Font::ItemCount);
        let amt_offset = (energy_rect.size().x - amt_size.x) / 2;
        let amt_rect = Rect::sized(amt_size) + energy_rect.min + Point::new(amt_offset, -1);
        ctx.with_bounds(amt_rect, |ctx| {
            ctx.draw_text(&amt_str, Font::ItemCount);
        });

        // Item frames
        ctx.with_bounds(area.inset(FRAME_LEFT_MARGIN, 0, FRAME_TOP_MARGIN, 0), |ctx| {
            self.with_inner(|w| w.on_paint(ctx));
        });
    }

    fn on_key(&self, _ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        let key = match evt {
            KeyEvent::Down((k, _)) => k,
            _ => return UIResult::Unhandled,
        };

        match key {
            Key::Hotbar(slot) if 0 <= slot && slot < 9 =>
                UIResult::Event(HotbarEvent::Select {
                    slot: slot as u8,
                }),
            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let area = Rect::sized(self.min_size(ctx));
        ctx.with_bounds(area.inset(FRAME_LEFT_MARGIN, 0, FRAME_TOP_MARGIN, 0), |ctx| {
            self.with_inner(|w| w.on_mouse(ctx, evt))
        }).map(|idx| {
            if let Some(drag) = ctx.end_drag() {
                HotbarEvent::Set {
                    slot: idx,
                    item: drag.item,
                }
            } else {
                HotbarEvent::Select {
                    slot: idx,
                }
            }
        })
    }
}
