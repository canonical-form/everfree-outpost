use common::types::*;
use std::collections::VecDeque;
use std::mem;
use common::{Activity, Appearance};
use common::proto::types::CondensedStructure;

use debug::Debug;
use util;

use self::inventory::Item;


pub mod entity;
pub mod inventory;
pub mod plane;
pub mod terrain_chunk;
pub mod structure;

pub mod pawn;
pub mod misc;

mod day_night;



#[derive(Clone, Debug)]
pub enum Delta {
    EntityAppear(EntityId, Box<Appearance>, Box<String>),
    EntityChange(EntityId, Box<Appearance>, Box<String>),
    EntityGone(EntityId),
    SetPawn(Option<EntityId>),

    EntityActivityPush(EntityId, Box<Activity>, Time),
    EntityActivityUnpush,
    EntityActivityPop,
    EntityActivityUnpop(EntityId, Box<Activity>, Time),
    EntityActivitySet(EntityId, Box<Activity>, Time),
    EntityActivityApply,

    InventoryAppear(InventoryId, Box<[Item]>, u32),
    InventoryContents(InventoryId, Box<[Item]>),
    InventoryFlags(InventoryId, u32),
    InventoryUpdate(InventoryId, usize, Item),
    InventoryGone(InventoryId),
    InventorySetMainId(Option<InventoryId>),
    InventorySetAbilityId(Option<InventoryId>),

    SetIsDark(bool),
    InitDayNight(Time, Time),

    TerrainChunkBlocks((u8, u8), Box<[u16]>),

    StructureAppear(StructureId, (u8, u8, u8), TemplateId),
    StructureAppearTimed(Box<(StructureId, (u8, u8, u8), TemplateId, Time)>),
    StructureGone(StructureId),
    StructureReplace(StructureId, TemplateId),
    StructureReplaceTimed(StructureId, TemplateId, Time),
    StructureAppearMulti(StructureId, TemplateId, Box<Vec<CondensedStructure>>),

    SetDialog(pawn::Dialog),

    CraftingStateIdle(StructureId),
    CraftingStatePaused(StructureId, RecipeId, u8, u32),
    CraftingStateActive(StructureId, RecipeId, u8, u32, Time),

    SetNow(Time),
}


pub trait Log<T> {
    fn record(&mut self, val: T);
}

struct NoLog;

impl<T> Log<T> for NoLog {
    fn record(&mut self, _val: T) {
        // No-op
    }
}

impl<T> Log<T> for Vec<T> {
    fn record(&mut self, val: T) {
        self.push(val)
    }
}


#[derive(Clone)]
struct Inner {
    now: Time,

    entities: entity::Entities,
    inventories: inventory::Inventories,
    plane: plane::PlaneInfo,
    terrain_chunks: terrain_chunk::TerrainChunks,
    structures: structure::Structures,

    pawn: pawn::Pawn,
    misc: misc::Misc,
}

impl Inner {
    fn new() -> Inner {
        Inner {
            now: 0,

            entities: entity::Entities::new(),
            inventories: inventory::Inventories::new(),
            plane: plane::PlaneInfo::new(),
            terrain_chunks: terrain_chunk::TerrainChunks::new(),
            structures: structure::Structures::new(),

            pawn: pawn::Pawn::new(),
            misc: misc::Misc::new(),
        }
    }

    fn apply<L: Log<Delta>>(&mut self, d: Delta, log: &mut L) {
        match d {
            Delta::EntityAppear(id, appearance, name) => {
                self.entities.appear(id, *appearance, *name, log);
                if self.pawn.is(id) {
                    // Move the entity's name into `pawn`.
                    let name = self.entities.set_name(id, String::new()).unwrap();
                    self.pawn.set_name(name);
                }
            },
            Delta::EntityChange(id, appearance, name) => {
                self.entities.change(id, *appearance, *name, log);
                if self.pawn.is(id) {
                    // Move the entity's name into `pawn`.
                    let name = self.entities.set_name(id, String::new()).unwrap();
                    self.pawn.set_name(name);
                }
            },
            Delta::EntityGone(id) => {
                if self.pawn.is(id) {
                    // Move the entity's name back out of `pawn`.
                    let name = self.pawn.set_name(String::new());
                    self.entities.set_name(id, name);
                }
                self.entities.gone(id, log);
            },

            Delta::SetPawn(opt_id) => {
                // Pawn name logic is a little tricky.  The invariant is this: if the pawn ID is
                // set to `Some(id)`, and an entity with that ID exists, then the entity's name is
                // set to the empty string, and `self.pawn.name` contains its original name.  This
                // requires special handling both when the pawn ID changes and when an entity is
                // created/destroyed with the appropriate ID.  Furthermore, to ensure that
                // rewinding works correctly, none of these name adjustments should be reflected in
                // the log.

                // If the entity already exists, take its name.
                let new_name = opt_id
                    .and_then(|id| self.entities.set_name(id, String::new()))
                    .unwrap_or(String::new());

                let (old_opt_id, old_name) = self.pawn.set(opt_id, new_name);

                if let Some(old_id) = old_opt_id {
                    // Move the old pawn's name back into `entities`.  This only has an effect if
                    // the entity actually exists, and if it exists, then we should definitely have
                    // a real name (i.e., we aren't overwriting the name with `String::new()`).
                    self.entities.set_name(old_id, old_name);
                }

                log.record(Delta::SetPawn(old_opt_id));
            },

            Delta::EntityActivityPush(id, act, start) =>
                self.entities.push_activity(id, act, start, log),
            Delta::EntityActivityUnpush =>
                self.entities.unpush_activity(log),
            Delta::EntityActivityPop =>
                self.entities.pop_activity(log),
            Delta::EntityActivityUnpop(id, act, start) =>
                self.entities.unpop_activity(id, act, start, log),
            Delta::EntityActivitySet(id, act, start) =>
                self.entities.set_activity(id, *act, start, log),
            Delta::EntityActivityApply =>
                self.entities.apply_activity(log),

            Delta::InventoryAppear(id, items, flags) =>
                self.inventories.appear(id, items, flags, log),
            Delta::InventoryContents(id, items) =>
                self.inventories.contents(id, items, log),
            Delta::InventoryFlags(id, flags) =>
                self.inventories.flags(id, flags, log),
            Delta::InventoryUpdate(id, slot, item) =>
                self.inventories.update(id, slot, item, log),
            Delta::InventoryGone(id) =>
                self.inventories.gone(id, log),
            Delta::InventorySetMainId(opt_id) =>
                self.inventories.set_main_id(opt_id, log),
            Delta::InventorySetAbilityId(opt_id) =>
                self.inventories.set_ability_id(opt_id, log),

            Delta::SetIsDark(is_dark) =>
                self.plane.set_is_dark(is_dark, log),
            Delta::InitDayNight(base_time, cycle_ms) =>
                self.plane.init_day_night(base_time, cycle_ms, log),

            Delta::TerrainChunkBlocks(cpos_u8, blocks_rle16) =>
                self.terrain_chunks.decode_into(cpos_u8, &blocks_rle16, log),

            Delta::StructureAppear(id, pos, template) =>
                self.structures.appear(id, util::unpack_v3(pos), template, self.now, log),
            Delta::StructureAppearTimed(args) => {
                let (id, pos, template, appear_time) = *args;
                self.structures.appear(id, util::unpack_v3(pos), template, appear_time, log);
            },
            Delta::StructureGone(id) =>
                self.structures.gone(id, log),
            Delta::StructureReplace(id, template) =>
                self.structures.replace(id, template, self.now, log),
            Delta::StructureReplaceTimed(id, template, appear_time) =>
                self.structures.replace(id, template, appear_time, log),
            Delta::StructureAppearMulti(base_id, template, structures) => {
                for s in structures.iter() {
                    let id = StructureId(base_id.unwrap() + s.rel_id as u32);
                    let pos = util::unpack_v3(s.pos);
                    self.structures.appear(id, pos, template, self.now, log);
                }
            },

            Delta::SetDialog(dialog) =>
                self.pawn.set_dialog(dialog, log),

            Delta::CraftingStateIdle(sid) =>
                self.misc.crafting_state_idle(sid, log),
            Delta::CraftingStatePaused(sid, recipe, count, progress) =>
                self.misc.crafting_state_paused(sid, recipe, count, progress, log),
            Delta::CraftingStateActive(sid, recipe, count, progress, start_time) =>
                self.misc.crafting_state_active(sid, recipe, count, progress, start_time, log),

            Delta::SetNow(time) =>
                self.set_now(time, log),
        }
    }

    fn apply_all<I, L>(&mut self, ds: I, log: &mut L)
            where I: IntoIterator<Item=Delta>, L: Log<Delta> {
        for d in ds {
            self.apply(d, log);
        }
    }

    fn set_now<L>(&mut self, now: Time, log: &mut L)
            where L: Log<Delta> {
        let old = mem::replace(&mut self.now, now);
        log.record(Delta::SetNow(old));
    }
}


#[derive(Clone)]
pub struct State {
    inner: Inner,

    /// Deltas that fall within the current tick.
    cur_deltas: Vec<Delta>,

    /// Pending updates received from the server.
    future: VecDeque<(Time, Vec<Delta>)>,

    /// Deltas to reverse any local predictions that were applied to the state.  Note that these
    /// are stored in the order they were generated, which is opposite the order they were applied.
    predict_rev: Vec<Delta>,
}

impl State {
    pub fn new() -> State {
        State {
            inner: Inner::new(),
            cur_deltas: Vec::new(),
            future: VecDeque::new(),
            predict_rev: Vec::new(),
        }
    }

    pub fn push(&mut self, delta: Delta) {
        debug!("push {:?}", delta);
        self.cur_deltas.push(delta);
    }

    /// Mark the end of the tick.  Transfer all current deltas to the `future` queues, where they
    /// can be applied by `advance`.
    pub fn tick_end(&mut self, time: Time) {
        debug!("tick_end {}", time);

        if self.cur_deltas.len() > 0 {
            let deltas = mem::replace(&mut self.cur_deltas, Vec::new());
            self.future.push_back((time, deltas));
        }
    }

    pub fn advance(&mut self, time: Time) {
        self.unpredict();
        trace!("advance to time {}", time);

        while self.future.front().map_or(false, |td| td.0 <= time) {
            let (tick_t, deltas) = self.future.pop_front().unwrap();

            // Advance entity activities to `tick_t` before proceeding.  This way, if an entity
            // gets destroyed at time `tick_t`, but also has activities queued in the interval
            // `inner.now .. tick_t`, the activities will be applied before the entity is
            // destroyed, avoiding a spurious error.
            //
            // Activities applied at time `tick_t` with start time also set to `tick_t` will be
            // handled by the final `entities.advance` following the loop.

            // TODO: We'll still get an error (desync: EntityAct for nonexistent ...) if an entity
            // gets created, queues some activity updates, and gets destroyed all in one tick.
            // Hopefully that doesn't happen too often...

            self.inner.set_now(tick_t, &mut NoLog);
            self.inner.entities.advance(tick_t, &mut NoLog);

            debug!("t={}: apply {} deltas", tick_t, deltas.len());
            self.inner.apply_all(deltas, &mut NoLog);
        }

        self.inner.set_now(time, &mut NoLog);
        self.inner.entities.advance(time, &mut NoLog);
    }

    /// Apply a delta immediately, not waiting for a server tick.  This does not advance the
    /// current time, and thus it also does not advance entity activities.
    pub fn apply_immediate(&mut self, d: Delta) {
        debug!("immediate {:?}", d);
        self.unpredict();
        self.inner.apply(d, &mut NoLog);
    }


    // Prediction

    /// Add a delta to the current prediction.
    pub fn predict(&mut self, d: Delta) {
        trace!("  record prediction: {:?}", d);
        self.inner.apply(d, &mut self.predict_rev);
    }

    /// Undo any previously applied prediction, reverting to the base state.
    pub fn unpredict(&mut self) {
        if self.predict_rev.len() > 0 {
            let ds = mem::replace(&mut self.predict_rev, Vec::new());
            // Note that `self.predict_rev` stores the reverse deltas in the order they were
            // generated, which is opposite the order in which they should be applied.
            self.inner.apply_all(ds.into_iter().rev(), &mut NoLog);
        }
    }


    // Misc

    /// Some components of the state may track a high-level summary of changes, for use by
    /// downstream caches.  This method clears the currently recorded changes - usually this means
    /// the downstream consumers have processed all the changes.
    pub fn clear_changes(&mut self) {
        self.inner.structures.clear_changes();
        self.inner.terrain_chunks.clear_changes();
    }


    // Helper functions for handling network messages.

    pub fn entity_appear(&mut self, id: EntityId, appearance: Appearance, name: String) {
        self.push(Delta::EntityAppear(id, Box::new(appearance), Box::new(name)));
    }

    pub fn entity_change(&mut self, id: EntityId, appearance: Appearance, name: String) {
        self.push(Delta::EntityChange(id, Box::new(appearance), Box::new(name)));
    }

    pub fn entity_gone(&mut self, id: EntityId) {
        self.push(Delta::EntityGone(id));
    }

    fn entity_act(&mut self, id: EntityId, time: Time, act: Activity) {
        self.push(Delta::EntityActivityPush(id, Box::new(act), time));
    }

    pub fn entity_act_stand(&mut self, id: EntityId, time: Time, pos: V3, dir: u8) {
        if time >= self.now() + 1000 {
            warn!("{:?} activity scheduled in the future: {} > {} (act = Stand {:?} {})",
                  id, time, self.now(), pos, dir);
        }
        self.entity_act(id, time, Activity::Stand { pos, dir });
    }

    pub fn entity_act_walk(&mut self, id: EntityId, time: Time,
                           pos: V3, velocity: V3, dir: u8) {
        if time >= self.now() + 1000 {
            warn!("{:?} activity scheduled in the future: {} > {} (act = Walk {:?} {:?} {})",
                  id, time, self.now(), pos, velocity, dir);
        }
        self.entity_act(id, time, Activity::Walk { pos, velocity, dir });
    }

    /*
    pub fn entity_act_busy(&mut self, id: EntityId, time: Time,
                           pos: V3, anim: AnimId, icon: AnimId) {
        self.entity_act(id, time, Activity::Busy { pos: pos, anim: anim, icon: icon });
    }
    */

    pub fn set_pawn_id(&mut self, id: Option<EntityId>) {
        self.push(Delta::SetPawn(id));
    }


    pub fn inventory_appear(&mut self,
                            id: InventoryId,
                            items: Box<[Item]>,
                            flags: u32) {
        self.push(Delta::InventoryAppear(id, items, flags));
    }

    pub fn inventory_contents(&mut self, id: InventoryId, items: Box<[Item]>) {
        self.push(Delta::InventoryContents(id, items));
    }

    pub fn inventory_flags(&mut self, id: InventoryId, flags: u32) {
        self.push(Delta::InventoryFlags(id, flags));
    }

    pub fn inventory_update(&mut self, id: InventoryId, slot: usize, item: Item) {
        self.push(Delta::InventoryUpdate(id, slot, item));
    }

    pub fn inventory_gone(&mut self, id: InventoryId) {
        self.push(Delta::InventoryGone(id));
    }

    pub fn inventory_set_main_id(&mut self, id: Option<InventoryId>) {
        self.push(Delta::InventorySetMainId(id));
    }

    pub fn inventory_set_ability_id(&mut self, id: Option<InventoryId>) {
        self.push(Delta::InventorySetAbilityId(id));
    }


    pub fn set_is_dark(&mut self, is_dark: bool) {
        self.push(Delta::SetIsDark(is_dark));
    }

    pub fn init_day_night(&mut self, base_time: Time, cycle_ms: Time) {
        self.push(Delta::InitDayNight(base_time, cycle_ms));
    }


    pub fn terrain_chunk_blocks(&mut self, cpos: V2, blocks_rle16: Box<[u16]>) {
        let cpos_u8 = (cpos.x as u8, cpos.y as u8);
        self.push(Delta::TerrainChunkBlocks(cpos_u8, blocks_rle16));
    }


    pub fn structure_appear(&mut self, id: StructureId, pos: V3, template: TemplateId) {
        self.push(Delta::StructureAppear(id, util::pack_v3(pos), template));
    }

    pub fn structure_gone(&mut self, id: StructureId) {
        self.push(Delta::StructureGone(id));
    }

    pub fn structure_replace(&mut self, id: StructureId, template: TemplateId) {
        self.push(Delta::StructureReplace(id, template));
    }


    pub fn set_dialog(&mut self, dialog: pawn::Dialog) {
        self.push(Delta::SetDialog(dialog));
    }


    pub fn crafting_state_idle(&mut self, sid: StructureId) {
        self.push(Delta::CraftingStateIdle(sid));
    }

    pub fn crafting_state_paused(&mut self,
                                 sid: StructureId,
                                 recipe: RecipeId,
                                 count: u8,
                                 progress: u32) {
        self.push(Delta::CraftingStatePaused(sid, recipe, count, progress));
    }

    pub fn crafting_state_active(&mut self,
                                 sid: StructureId,
                                 recipe: RecipeId,
                                 count: u8,
                                 progress: u32,
                                 start_time: Time) {
        self.push(Delta::CraftingStateActive(sid, recipe, count, progress, start_time));
    }


    // Debug support

    pub fn update_debug(&self, debug: &mut Debug) {
        debug.state_pending_count = self.future.len();
        debug.state_pending_time = self.future.back().map_or(0, |&(t, _)| t - self.now());
    }

    // Accessors

    pub fn now(&self) -> Time {
        self.inner.now
    }

    pub fn entities(&self) -> &entity::Entities {
        &self.inner.entities
    }

    pub fn inventories(&self) -> &inventory::Inventories {
        &self.inner.inventories
    }

    pub fn plane(&self) -> &plane::PlaneInfo {
        &self.inner.plane
    }

    pub fn terrain_chunks(&self) -> &terrain_chunk::TerrainChunks {
        &self.inner.terrain_chunks
    }

    pub fn structures(&self) -> &structure::Structures {
        &self.inner.structures
    }

    pub fn pawn(&self) -> &pawn::Pawn {
        &self.inner.pawn
    }

    pub fn misc(&self) -> &misc::Misc {
        &self.inner.misc
    }

    pub fn pawn_entity(&self) -> Option<&entity::Entity> {
        self.inner.pawn.id().and_then(|id| self.inner.entities.get(id))
    }

    pub fn entities_update_z_order<F>(&mut self, calc_z: F)
            where F: Fn(&entity::Entity) -> i32 {
        self.inner.entities.update_z_order(calc_z);
    }
}
