use common::types::*;
use std::collections::btree_map::{self, BTreeMap};
use std::ops::Index;
use std::u8;
use std::u32;

use util;

use super::{Delta, Log};


#[derive(Clone, Copy)]
pub struct Structure {
    /// Structure position in tiles.  u8 is enough to cover the entire local region.
    pub pos: V3,

    pub template_id: TemplateId,

    /// Timestamp indicating when to start the structure's one-shot animation.  This field is only
    /// relevant if the structure's template defines such an animation.
    pub oneshot_start: Time,
}


pub type ChangeMap = BTreeMap<StructureId, ((u8, u8, u8), TemplateId)>;


#[derive(Clone)]
pub struct Structures {
    map: BTreeMap<StructureId, Structure>,

    change_add: BTreeMap<StructureId, ((u8, u8, u8), TemplateId)>,
    change_remove: BTreeMap<StructureId, ((u8, u8, u8), TemplateId)>,
}

impl Structures {
    pub fn new() -> Structures {
        Structures {
            map: BTreeMap::new(),

            change_add: BTreeMap::new(),
            change_remove: BTreeMap::new(),
        }
    }

    // For each ID, we need to condense an arbitrary (valid) sequence of adds and removes down to a
    // single (remove, add) pair.  Note the client may fast-forward over multiple ticks in
    // sequence, so we can't rely on the server's ID-reuse policy to rule out certain interactions.
    //
    // - Initial state: structure exists
    //   - R => R
    //   - RA => RA
    //   - RAR => R     (the added structure is never visible to callers)
    //   - RARA => RA   (the intermediate structure is never visible)
    //
    // - Initial state: structure does not exist
    //   - A => A
    //   - AR => (empty)    (the added structure is never visible)
    //   - ARA => A         (only the second structure is visible)
    //   - ARAR => (empty)
    //
    // To be conservative, we also handle AA and RR sequences somewhat intelligently, so callers
    // still get some useful information even if the server misbehaves.

    fn cache_add(&mut self, id: StructureId, t: TemplateId, pos: V3) {
        // Always overwrite - keep the newest set of new info (conservative handling of AA)
        self.change_add.insert(id, (util::pack_v3(pos), t));
    }

    fn cache_remove(&mut self, id: StructureId, t: TemplateId, pos: V3) {
        // If the previous event was an `add`, cancel it out.
        if self.change_add.remove(&id).is_some() {
            return;
        }

        // Otherwise, add a remove.
        // Never overwrite - keep the oldest set of old info (conservative handling of AA)
        self.change_remove.entry(id).or_insert((util::pack_v3(pos), t));
    }

    pub fn clear(&mut self) {
        self.map.clear();
        self.change_add.clear();
        self.change_remove.clear();
    }

    pub fn appear<L>(&mut self,
                     id: StructureId,
                     pos: V3,
                     template_id: u32,
                     appear_time: Time,
                     log: &mut L)
            where L: Log<Delta> {
        if self.map.contains_key(&id) {
            error!("desync: StructureAppear for existing {:?}", id);
            return;
        }

        let s = Structure {
            pos: pos,
            template_id: template_id,
            oneshot_start: appear_time,
        };

        self.map.insert(id, s);
        self.cache_add(id, template_id, pos);
        log.record(Delta::StructureGone(id));
    }

    pub fn gone<L>(&mut self, id: StructureId, log: &mut L)
            where L: Log<Delta> {
        let s = unwrap_or_error!(self.map.remove(&id),
                                 "desync: StructureGone for nonexistent {:?}", id);
        self.cache_remove(id, s.template_id, s.pos);
        log.record(Delta::StructureAppearTimed(Box::new(
                    (id, util::pack_v3(s.pos), s.template_id, s.oneshot_start))));
    }

    pub fn replace<L>(&mut self,
                      id: StructureId,
                      template_id: u32,
                      appear_time: Time,
                      log: &mut L)
            where L: Log<Delta> {
        let old_template_id: TemplateId;
        let old_appear_time: Time;
        let pos: V3;
        {
            let s = unwrap_or_error!(self.map.get(&id),
                                     "desync: StructureReplace for nonexistent {:?}", id);
            // After this point, `id` is known to be valid
            old_template_id = s.template_id;
            old_appear_time = s.oneshot_start;
            pos = s.pos;
        }

        {
            let s = self.map.get_mut(&id).unwrap();
            s.template_id = template_id;
            s.oneshot_start = appear_time;
        }

        self.cache_remove(id, old_template_id, pos);
        self.cache_add(id, template_id, pos);

        log.record(Delta::StructureReplaceTimed(id, old_template_id, old_appear_time));
    }

    pub fn get(&self, id: StructureId) -> Option<&Structure> {
        self.map.get(&id)
    }

    pub fn iter(&self) -> Iter {
        self.map.iter()
    }

    pub fn changes(&self) -> (&ChangeMap, &ChangeMap) {
        (&self.change_remove,
         &self.change_add)
    }

    pub fn has_changes(&self) -> bool {
        self.change_remove.len() > 0 ||
        self.change_add.len() > 0
    }

    pub fn clear_changes(&mut self) {
        self.change_remove.clear();
        self.change_add.clear();
    }
}


pub type Iter<'a> = btree_map::Iter<'a, StructureId, Structure>;

impl Index<StructureId> for Structures {
    type Output = Structure;
    fn index(&self, idx: StructureId) -> &Structure {
        self.map.get(&idx).expect("no entry for given key")
    }
}
