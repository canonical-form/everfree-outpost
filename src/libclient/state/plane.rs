use common::types::*;
use std::mem;

use state::day_night::DayNight;

use super::{Delta, Log};


#[derive(Clone)]
pub struct PlaneInfo {
    day_night: DayNight,
    is_dark: bool,
}

impl PlaneInfo {
    pub fn new() -> PlaneInfo {
        PlaneInfo {
            day_night: DayNight::new(),
            is_dark: false,
        }
    }


    pub fn is_dark(&self) -> bool {
        self.is_dark
    }

    pub fn set_is_dark<L>(&mut self, is_dark: bool, log: &mut L)
            where L: Log<Delta> {
        let old_is_dark = mem::replace(&mut self.is_dark, is_dark);
        log.record(Delta::SetIsDark(old_is_dark));
    }

    pub fn day_night(&self) -> &DayNight {
        &self.day_night
    }

    pub fn init_day_night<L>(&mut self, base_time: Time, cycle_ms: Time, log: &mut L)
            where L: Log<Delta> {
        let (old_base_time, old_cycle_ms) = self.day_night.get_init_settings();
        self.day_night.init(base_time, cycle_ms);
        log.record(Delta::InitDayNight(old_base_time, old_cycle_ms));
    }
}
