use std::cmp;
use common::types::*;

use physics;
use common::Appearance;
use common::movement::InputBits;
use common::proto::game::{Request, Response};
use common::proto::types::{LocalTime, LocalPos};
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use render::AsmglBackend;

use cache::Caches;
use data::{data, has_data};
use data::{I_USE_AT_POINT, I_USE_ON_SELF};
use debug::{Debug, FrameTimes, Timer};
use graphics::renderer::{self, Renderer};
use graphics::renderer::Scene;
use graphics::renderer::Selection;
use graphics::types::HAS_LIGHT;
use input::{Key, Modifiers, Button};
use misc::Misc;
use mouse::MouseSelect;
use movement::Movement;
use platform::{Platform, PlatformObj};
use platform::{Config, ConfigKey};
use predictor::Predictor;
use state;
use state::inventory::{Inventories, Item};
use state::pawn;
use timing::Timing;
use util;
use ui2;
use ui2::root::{UI as UI2, RootEvent};


pub struct Client<P: Platform> {
    platform: P,

    state: state::State,
    misc: Misc,

    caches: Caches,

    debug: Debug,
    timing: Timing,
    movement: Movement,
    predictor: Predictor,

    ui2: UI2,

    renderer: Renderer<AsmglBackend>,

    default_camera_pos: V3,
    window_size: (u16, u16),
    view_size: (u16, u16),
    ui_scale: u16,

    cur_input: InputBits,
    mouse_select: MouseSelect,

    config: ClientConfig,
}

struct ClientConfig {
    predict_movement: bool,
}

impl ClientConfig {
    fn from_platform_config(c: &Config) -> ClientConfig {
        ClientConfig {
            predict_movement: c.get_int(ConfigKey::PredictMovement) != 0,
        }
    }
}


impl<P: Platform> Client<P> {
    pub fn new(platform: P) -> Client<P> {
        assert!(has_data(), "must call set_data() before initializing client");

        let mut renderer = Renderer::new(AsmglBackend::new());
        if platform.config().get_int(ConfigKey::RenderNames) == 0 {
            renderer.render_names = false;
        }
        let config = ClientConfig::from_platform_config(platform.config());

        // After decoding, the initial server time could be anywhere in the range +/-32768.  We set
        // the initial `state.now()` to ensure the first few events we get aren't "in the past".
        let mut state = state::State::new();
        state.advance(-65536);

        let mut c = Client {
            platform: platform,

            state: state,
            misc: Misc::new(),

            caches: Caches::new(),

            debug: Debug::new(),
            timing: Timing::new(),
            movement: Movement::new(),
            predictor: Predictor::new(),

            ui2: UI2::new(),

            renderer: renderer,

            default_camera_pos: V3::new(4096, 4096, 0),
            window_size: (640, 480),
            view_size: (640, 480),
            ui_scale: 1,

            cur_input: InputBits::empty(),
            mouse_select: MouseSelect::new(),

            config: config,
        };

        c.misc.hotbar.init(c.platform.config());
        c.debug.init(c.platform.config());
        // TODO: will need an init like this for ui2 as well
        //c.ui.root.init(c.platform.config());

        c
    }


    // Message handling

    pub fn handle_message(&mut self, resp: Response) {
        debug!("{} ({}): receive {:?}", self.now(), self.platform.get_time(), resp);
        match resp {
            Response::TerrainChunk(idx, data) => {
                let cpos = V2::new(idx as i32 % LOCAL_SIZE,
                                   idx as i32 / LOCAL_SIZE);
                self.load_terrain_chunk(cpos, &data);
            },

            Response::Pong(_cookie, _now) => error!("NYI: libclient Pong"),

            Response::Init(now, day_night_base, day_night_ms) => {
                info!("init: now = {:?}", now);

                // Timing handling on init used to be fancier, but now it's just a measurement like
                // any other.  The only exception is that we snap `time_base` directly to the
                // measured value, instead of doing a gradual adjustment all the way from 0 to
                // (potentially) +/-32,000.
                //
                // Note it's okay to `decode` here even if `time_base` is not initialized.  Since
                // all times we receive are relative, we can pick an initial `time_base` pretty
                // much arbitrarily.
                let now = self.decode_time(now);
                self.timing.add_measurement(self.platform.get_time(), now);
                self.timing.snap();

                self.init_day_night(now, day_night_base as Time, day_night_ms as Time);

            },

            Response::KickReason(_msg) => error!("NYI: libclient KickReason"),

            Response::UnloadChunk(_idx) => {},   // TODO (currently no-op)

            Response::OpenCrafting(station, class_mask, contents, main, ability) =>
                self.open_crafting_dialog(station, class_mask, contents, main, ability),

            Response::OpenEquipment(iid1, iid2) =>
                self.open_equipment_dialog(iid1, iid2),

            Response::OpenInventory(iid) =>
                self.open_inventory_dialog(iid),

            Response::OpenAbilities(iid) =>
                self.open_abilities_dialog(iid),

            Response::OpenContainer(iid1, iid2) =>
                self.open_container_dialog(iid1, iid2),

            Response::OpenPonyEdit(_name) =>
                self.open_pony_edit_dialog(),

            Response::ChatUpdate(msg) => error!("NYI: libclient ChatUpdate: {}", msg),

            Response::EntityAppear(id, app_bytes, name) => {
                let appearance = Appearance::from_bytes(&app_bytes).unwrap();
                self.state.entity_appear(id, appearance, name.clone());
            },

            Response::EntityChange(id, app_bytes, name) => {
                let appearance = Appearance::from_bytes(&app_bytes).unwrap();
                self.state.entity_change(id, appearance, name.clone());
            },

            Response::EntityGone(id, _time) => {
                self.state.entity_gone(id);
            },

            Response::StructureAppear(id, template, pos) => {
                let pos = V3::new(pos.0 as i32,
                                  pos.1 as i32,
                                  pos.2 as i32);
                self.structure_appear(id, pos, template);
            },

            Response::StructureGone(id) =>
                self.structure_gone(id),

            Response::StructureAppearMulti(base_id, template, structures) => {
                let mut prev_id = base_id;
                for s in structures {
                    let id = StructureId(prev_id.unwrap() + s.rel_id as u32);
                    let pos = V3::new(s.pos.0 as i32,
                                      s.pos.1 as i32,
                                      s.pos.2 as i32);
                    self.structure_appear(id, pos, template);
                    prev_id = id;
                }
            },

            Response::StructureGoneMulti(base_id, rel_ids) => {
                let mut prev_id = base_id;
                for rel_id in rel_ids {
                    let id = StructureId(prev_id.unwrap() + rel_id as u32);
                    self.structure_gone(id);
                    prev_id = id;
                }
            },

            Response::PlaneFlags(flags) =>
                self.set_plane_flags(flags),

            Response::SyncStatus(_status) => error!("NYI: libclient SyncStatus"),

            Response::StructureReplace(id, template) =>
                self.structure_replace(id, template),

            Response::InventoryAppear(id, raw_items, flags) => {
                let items = raw_items.into_iter()
                    .map(|(_, qty, item_id)| Item::new(item_id, qty))
                    .collect::<Vec<_>>().into_boxed_slice();
                self.state.inventory_appear(id, items, flags);
            },

            Response::InventoryContents(id, raw_items) => {
                let items = raw_items.into_iter()
                    .map(|(_, qty, item_id)| Item::new(item_id, qty))
                    .collect::<Vec<_>>().into_boxed_slice();
                self.state.inventory_contents(id, items);
                self.misc.inv_changes.set_needs_update();
            },

            Response::InventoryFlags(id, flags) =>
                self.state.inventory_flags(id, flags),

            Response::InventoryUpdate(id, slot, raw_item) => {
                let (_, qty, item_id) = raw_item;
                let item = Item::new(item_id, qty);
                self.state.inventory_update(id, slot as usize, item);
                self.misc.inv_changes.set_needs_update();
            },

            Response::InventoryGone(id) =>
                self.state.inventory_gone(id),

            Response::CancelDialog(()) =>
                self.close_dialog(),

            Response::EnergyUpdate(_cur, _max, _rate, _time) => {
                // TODO
            },

            Response::EntityActStand(id, time, pos, dir) => {
                let time = self.decode_time(time);
                self.state.entity_act_stand(id, time, pos.unwrap(), dir);
            },

            Response::EntityActWalk(id, time, pos, velocity, dir) => {
                let time = self.decode_time(time);
                self.state.entity_act_walk(id, time, pos.unwrap(), velocity.unwrap(), dir);
            },

            /*
            Response::EntityActBusy(id, time, pos, anim, icon) => {
                let time = self.decode_time(time);
                self.state.entity_act_busy(id, time, pos.unwrap(), anim, icon);
            },
            */

            Response::MotionMispredict(_time, _next_validity) => {
                // no-op - not used with server2
            },

            Response::CraftingIdle(sid) =>
                self.state.crafting_state_idle(sid),
            Response::CraftingPaused(sid, recipe, count, progress) =>
                self.state.crafting_state_paused(sid, recipe, count, progress),
            Response::CraftingActive(sid, recipe, count, progress, start_time) => {
                let start_time = self.decode_time(start_time);
                self.state.crafting_state_active(sid, recipe, count, progress, start_time)
            },

            Response::CharacterInventories(main, ability, _equip) => {
                self.state.inventory_set_main_id(Some(main));
                self.state.inventory_set_ability_id(Some(ability));
                // TODO: record equip somewhere
                self.misc.inv_changes.set_needs_update();
            },

            Response::TickBegin(_time) => {
                // TODO
            },

            Response::TickEnd(time) => {
                let time = self.decode_time(time);
                self.timing.add_measurement(self.platform.get_time(), time);
                self.state.tick_end(time);
            },

            Response::AckRequest(time) => {
                if self.config.predict_movement {
                    let time = self.decode_time(time);
                    self.predictor.ack_request(time);
                }
            },

            Response::NakRequest(()) => {
                if self.config.predict_movement {
                    self.predictor.nak_request();
                }
            },


            Response::CameraFixed(pos) => {
                self.default_camera_pos = pos.unwrap();
                self.state.set_pawn_id(None);
            },

            Response::CameraPawn(pawn) => {
                self.state.set_pawn_id(Some(pawn));
            },
        }
    }

    fn send_message(&mut self, req: Request) {
        let time = self.platform.get_time();
        debug!("{} ({}): send {:?}", self.now(), time, req);
        if self.config.predict_movement {
            self.predictor.request_sent(time, &req);
        }
        self.platform.send_message(req);
    }

    // Resetting client state

    pub fn reset_all(&mut self) {
        self.state = state::State::new();
        self.misc.reset();
        self.mouse_select.reset();

        // TODO: close any open dialog
        // TODO: clear caches

        self.renderer.invalidate_terrain_geometry();
        self.renderer.invalidate_structure_geometry();
        self.renderer.invalidate_structure_light_geometry();
    }

    pub fn reset_renderer(&mut self) {
        self.renderer = Renderer::new(AsmglBackend::new());
        self.renderer.invalidate_terrain_geometry();
        self.renderer.invalidate_structure_geometry();
        self.renderer.invalidate_structure_light_geometry();
    }

    // Terrain chunk tracking

    pub fn load_terrain_chunk(&mut self, cpos: V2, data: &[u16]) {
        // Update self.chunks
        self.state.terrain_chunk_blocks(cpos, data.to_owned().into_boxed_slice());
    }

    // Structure tracking

    pub fn structure_appear(&mut self,
                            id: StructureId,
                            pos: V3,
                            template_id: TemplateId) {
        const MASK: i32 = LOCAL_SIZE * CHUNK_SIZE - 1;
        let pos = pos & MASK;
        self.state.structure_appear(id, pos, template_id);
        // Terrain shape cache will be updated by the next `check_invalidate_structure_geometry`
        // call after the add message actually gets applied to the state.
    }

    pub fn structure_gone(&mut self,
                          id: StructureId) {
        self.state.structure_gone(id);
    }

    pub fn structure_replace(&mut self,
                             id: StructureId,
                             template_id: TemplateId) {
        self.state.structure_replace(id, template_id);
    }

    fn check_invalidate_geometry(&mut self) {
        let data = data();
        let (added, removed) = self.state.structures().changes();
        let mut inv = false;
        let mut inv_light = false;
        for &(_, template_id) in added.values().chain(removed.values()) {
            let t = data.template(template_id);
            inv = true;
            if t.flags.contains(HAS_LIGHT) {
                inv_light = true;
            }
        }

        // Handle draw mode updates not caused by adding/removing structures, such as doors
        // opening/closing due to entity movement.
        if self.caches.draw_mode().modes_changed() {
            inv = true;
        }

        if inv {
            self.renderer.invalidate_structure_geometry();
        }
        if inv_light {
            self.renderer.invalidate_structure_light_geometry();
        }


        if self.state.terrain_chunks().has_changes() {
            self.renderer.invalidate_terrain_geometry();
        }
    }

    // Entity tracking

    pub fn set_default_camera_pos(&mut self, pos: V3) {
        self.default_camera_pos = pos;
    }

    // UI input

    fn process_ui_result(&mut self, r: UIResult<RootEvent>) -> bool {
        match r {
            UIResult::Unhandled => false,
            UIResult::NoEvent => true,
            UIResult::Event(evt) => {
                match evt {
                    RootEvent::CloseDialog => {
                        self.platform().send_message(Request::CloseDialog(()));
                        self.state.set_dialog(pawn::Dialog::None);
                    },

                    RootEvent::CreateCharacter { appearance } => {
                        let app_bytes = appearance.to_bytes().into_vec();
                        info!("send appearance bytes {:?}", app_bytes);
                        self.platform().send_message(Request::CreateCharacter(app_bytes));
                    },

                    RootEvent::ItemTransfer { src_slot, dest_slot, quantity,
                                              src_inv, dest_inv } => {
                        self.platform().send_message(
                            Request::MoveItem(src_inv,
                                              src_slot,
                                              dest_inv,
                                              dest_slot,
                                              quantity));
                    },

                    RootEvent::CraftingStart { recipe_id, count } => {
                        self.platform.send_message(Request::CraftingStart(recipe_id, count));
                    },

                    RootEvent::CraftingStop => {
                        self.platform.send_message(Request::CraftingStop(()));
                    },

                    RootEvent::HotbarSet { slot, item } => {
                        self.misc.hotbar.set_slot(self.platform.config_mut(),
                                                  slot,
                                                  item,
                                                  false /* is_ability */);
                        self.misc.hotbar.select(slot as i8);
                    },

                    RootEvent::HotbarSelect { slot } => {
                        self.misc.hotbar.toggle_select(slot as i8);
                    },
                }

                true
            },
        }
    }

    fn with_ui2_args_and_renderer<F, R>(&mut self, f: F) -> R
            where F: FnOnce(&mut UI2, ui2::root::Args, &mut Renderer<AsmglBackend>) -> R {
        let args = ui2::root::Args {
            now: self.now(),
            pawn: self.state.pawn_entity(),
            invs: self.state.inventories(),
            inv_changes: &self.misc.inv_changes,
            misc: self.state.misc(),
            hotbar: &self.misc.hotbar,
            energy: self.state.pawn().energy(),
            debug: &self.debug,
            dialog: self.state.pawn().dialog(),
        };
        f(&mut self.ui2, args, &mut self.renderer)
    }

    fn with_ui2_args<F: FnOnce(&mut UI2, ui2::root::Args) -> R, R>(&mut self, f: F) -> R {
        self.with_ui2_args_and_renderer(|ui2, args, _renderer| f(ui2, args))
    }

    fn input_key_down_result(&mut self, code: u8, mods: u8) -> UIResult<RootEvent> {
        if let Some(key) = Key::from_code(code) {
            let mods = Modifiers::from_bits_truncate(mods);
            let evt = KeyEvent::Down((key, mods));

            try_handle!(self.with_ui2_args(|ui, args| ui.key_event(args, evt)));
            try_handle!(self.handle_ui_key(key, mods));
            try_handle!(self.handle_key_down(key, mods));
        }
        UIResult::Unhandled
    }

    pub fn input_key_down(&mut self, code: u8, mods: u8) -> bool {
        let r = self.input_key_down_result(code, mods);
        self.process_ui_result(r)
    }

    fn input_key_up_result(&mut self, code: u8, mods: u8) -> UIResult<RootEvent> {
        if let Some(key) = Key::from_code(code) {
            let mods = Modifiers::from_bits_truncate(mods);
            let evt = KeyEvent::Up((key, mods));

            try_handle!(self.with_ui2_args(|ui, args| ui.key_event(args, evt)));
            try_handle!(self.handle_key_up(key, mods));
        }
        UIResult::Unhandled
    }

    pub fn input_key_up(&mut self, code: u8, mods: u8) -> bool {
        let r = self.input_key_up_result(code, mods);
        self.process_ui_result(r)
    }

    fn handle_ui_key(&mut self, key: Key, _mods: Modifiers) -> UIResult<RootEvent> {
        use input::Key::*;

        match key {
            ToggleCursor => {
                self.misc.show_cursor = !self.misc.show_cursor;
            },
            OpenInventory => {
                self.send_message(Request::OpenInventory(()));
            },
            OpenAbilities => {
                self.send_message(Request::OpenAbilities(()));
            },
            OpenEquipment => {
                self.send_message(Request::OpenEquipment(()));
            },

            ToggleDebugPanel => {
                self.debug.mode = self.debug.mode.next();
                self.platform.config_mut()
                    .set_int(ConfigKey::DebugShowPanel, self.debug.mode as i32);
            },

            _ => return UIResult::Unhandled,
        }

        UIResult::NoEvent
    }

    fn handle_key_down(&mut self, key: Key, _mods: Modifiers) -> UIResult<RootEvent> {
        use common::movement::*;
        use input::Key::*;

        // Update input bits
        let old_input = self.cur_input;
        let bits =
            match key {
                MoveLeft =>     INPUT_LEFT,
                MoveRight =>    INPUT_RIGHT,
                MoveUp =>       INPUT_UP,
                MoveDown =>     INPUT_DOWN,
                Run =>          INPUT_RUN,
                Cancel =>
                    // Don't hold if the character is already moving
                    if (old_input & INPUT_DIR_MASK).is_empty() { INPUT_HOLD }
                    else { InputBits::empty() },

                Interact |
                Destroy => InputBits::empty(),
                DebugLogSwitch => {
                    info!(" --- MARK ---");
                    info!("now = {}, time = {}",
                          self.state.now(),
                          self.platform.get_time());
                    InputBits::empty()
                },

                _ => return UIResult::Unhandled,
            };

        if !bits.is_empty() {
            // Clock selection: why use raw platform time here instead of self.now()?
            //
            // First, the reason it's even possible to use platform time here is that the movement
            // input system operates purely on relative time.  It uses the delta between two
            // timestamps (in InputChange, specifically), but never uses absolute timestamps for
            // anything, and never compares its input times to game-state times.
            //
            // Second, the reason platform time is the correct choice is that movement input
            // requests are "latency-oblivious" to some extent.  If we send InputStart at time T,
            // and it's received at time T + delta, then we can later send InputChange(1000) at
            // time T + 1000, and it will be received at time T + 1000 + delta.  Since the client
            // sends the two messages 1000ms apart, the server receives them 1000ms apart, and the
            // rel_time field of the InputChange message is 1000ms, all the timing works out
            // properly, even without any knowledge of the value of `delta`.  (Aside from the delay
            // field of InputStart, which reflects the *variance* of delta, not its value.) The
            // value of self.now() can be skewed between requests to account for changes in
            // latency, which breaks the symmetry of timing between client and server.
            let now = self.platform.get_time();
            debug!("key_down({}, {:?})", now, bits);
            if let Some(req) = self.movement.key_down(now, bits) {
                self.send_message(req);
            }

            self.cur_input.insert(bits);
        }

        UIResult::NoEvent
    }

    fn handle_key_up(&mut self, key: Key, _mods: Modifiers) -> UIResult<RootEvent> {
        use common::movement::*;
        use input::Key::*;

        // Update input bits
        let bits =
            match key {
                MoveLeft =>     INPUT_LEFT,
                MoveRight =>    INPUT_RIGHT,
                MoveUp =>       INPUT_UP,
                MoveDown =>     INPUT_DOWN,
                Run =>          INPUT_RUN,
                Cancel =>       INPUT_HOLD,

                Interact |
                Destroy => InputBits::empty(),

                _ => return UIResult::Unhandled,
            };

        if !bits.is_empty() {
            let now = self.platform.get_time();
            debug!("key_up({}, {:?})", now, bits);
            if let Some(req) = self.movement.key_up(now, bits) {
                self.send_message(req);
            }

            self.cur_input.remove(bits);
        }

        // Also send action command, if needed
        match key {
            Interact => self.do_interact(),
            Destroy => self.do_destroy(),

            _ => {},
        }

        UIResult::NoEvent
    }

    fn convert_world_mouse_pos(&self, pos: V2) -> V2 {
        let x = pos.x * self.view_size.0 as i32 / self.window_size.0 as i32;
        let y = pos.y * self.view_size.1 as i32 / self.window_size.1 as i32;
        V2::new(x, y)
    }

    fn convert_ui_mouse_pos(&self, pos: V2) -> V2 {
        self.convert_world_mouse_pos(pos) / self.ui_scale as i32
    }

    fn update_mouse_select_pos(&mut self, pos: V2) {
        let pos = self.convert_world_mouse_pos(pos);
        self.mouse_select.set_pos(pos);
    }

    pub fn input_mouse_move(&mut self, pos: V2) -> bool {
        self.update_mouse_select_pos(pos);
        let pos = self.convert_ui_mouse_pos(pos);

        let evt = MouseEvent::Move;
        let r = self.with_ui2_args(|ui, args| ui.mouse_event(args, pos, evt));
        self.process_ui_result(r)
    }

    pub fn input_mouse_down_result(&mut self, pos: V2, button: u8, mods: u8) -> UIResult<RootEvent> {
        if let Some(button) = Button::from_code(button) {
            let mods = Modifiers::from_bits_truncate(mods);
            let evt = MouseEvent::Down((button, mods));

            try_handle!(self.with_ui2_args(|ui, args| ui.mouse_event(args, pos, evt)));
            try_handle!(self.handle_mouse_down(button, mods));
        }
        UIResult::Unhandled
    }

    pub fn input_mouse_down(&mut self, pos: V2, button: u8, mods: u8) -> bool {
        self.update_mouse_select_pos(pos);
        let pos = self.convert_ui_mouse_pos(pos);
        let r = self.input_mouse_down_result(pos, button, mods);
        self.process_ui_result(r)
    }

    pub fn input_mouse_up_result(&mut self, pos: V2, button: u8, mods: u8) -> UIResult<RootEvent> {
        if let Some(button) = Button::from_code(button) {
            let mods = Modifiers::from_bits_truncate(mods);
            let evt = MouseEvent::Up((button, mods));

            try_handle!(self.with_ui2_args(|ui, args| ui.mouse_event(args, pos, evt)));
            try_handle!(self.handle_mouse_up(button, mods));
        }
        UIResult::Unhandled
    }

    pub fn input_mouse_up(&mut self, pos: V2, button: u8, mods: u8) -> bool {
        self.update_mouse_select_pos(pos);
        let pos = self.convert_ui_mouse_pos(pos);
        let r = self.input_mouse_up_result(pos, button, mods);
        self.process_ui_result(r)
    }

    fn handle_mouse_down(&mut self, button: Button, _mods: Modifiers) -> UIResult<RootEvent> {
        match button {
            Button::Left | Button::Right => {
                self.mouse_select.mouse_down();
                UIResult::NoEvent
            },
            _ => UIResult::Unhandled
        }
    }

    fn handle_mouse_up(&mut self, button: Button, mods: Modifiers) -> UIResult<RootEvent> {
        match button {
            Button::Left | Button::Right => {
                self.do_point_action(button, mods);
                self.mouse_select.mouse_up();
                UIResult::NoEvent
            },
            _ => UIResult::Unhandled
        }
    }

    fn do_point_action(&mut self, button: Button, _mods: Modifiers) {
        if button == Button::Left {
            self.do_interact();
        } else if button == Button::Right {
            self.do_destroy();
        }
    }

    fn do_interact(&mut self) {
        if let Some(item_id) = self.misc.hotbar.active_item() {
            match self.calc_selection() {
                Selection::None => {},

                Selection::Cell(_) |
                Selection::Structure(_) => {
                    let point = unwrap_or!(self.mouse_select.cell_pos());
                    self.send_message(
                        Request::PointUseItem(LocalPos::from_global(point), item_id));
                },

                Selection::Entity(eid) => {
                    if self.state.pawn().is(eid) {
                        self.send_message(
                            Request::UseItem(LocalTime::from_global_32(0), item_id));
                    }
                },
            }
        } else {
            let point = unwrap_or!(self.mouse_select.cell_pos());
            self.send_message(
                Request::PointInteract(LocalPos::from_global(point)));
        }
    }

    fn do_destroy(&mut self) {
        if self.misc.hotbar.active_item().is_some() {
            // With an item selected, the "destroy" key/mouse binding cancels instead.
            self.misc.hotbar.select(-1);
        } else {
            let point = unwrap_or!(self.mouse_select.cell_pos());
            self.send_message(
                Request::PointDestroy(LocalPos::from_global(point)));
        }
    }

    pub fn open_container_dialog(&mut self, inv0: InventoryId, inv1: InventoryId) {
        self.state.set_dialog(pawn::Dialog::Container(inv0, inv1));
    }

    pub fn open_crafting_dialog(&mut self,
                                station: StructureId,
                                class_mask: u32,
                                contents: InventoryId,
                                main: InventoryId,
                                ability: InventoryId) {
        self.state.set_dialog(pawn::Dialog::Crafting(
                station, class_mask, contents, main, ability));
    }

    pub fn open_equipment_dialog(&mut self, inv0: InventoryId, inv1: InventoryId) {
        self.state.set_dialog(pawn::Dialog::Equipment(inv0, inv1));
    }

    pub fn open_inventory_dialog(&mut self, inv: InventoryId) {
        self.state.set_dialog(pawn::Dialog::Inventory(inv));
    }

    pub fn open_abilities_dialog(&mut self, inv: InventoryId) {
        self.state.set_dialog(pawn::Dialog::Abilities(inv));
    }

    pub fn open_pony_edit_dialog(&mut self) {
        self.state.set_dialog(pawn::Dialog::PonyEdit);
    }

    pub fn close_dialog(&mut self) {
        self.state.set_dialog(pawn::Dialog::None);
    }


    // Graphics

    fn prepare(&mut self, scene: &Scene) {
        // Do all mutations first

        // Update entity Z order
        {
            self.state.entities_update_z_order(|e| {
                let mut pos = e.pos(scene.now);
                if pos.y < scene.camera_pos.y - CHUNK_SIZE * TILE_SIZE {
                    pos.y += CHUNK_SIZE * TILE_SIZE * LOCAL_SIZE;
                }
                pos.y - pos.z
            });
        }

        // Refresh the UI buffer.
        self.with_ui2_args_and_renderer(|ui, args, renderer| {
            let mut output = renderer::ui::Output::new(renderer);

            output.reset();
            ui.paint(args.clone(), &mut output);
        });
        // TODO: cursor handling?

        // Check if structures or terrain have changed since last time.  If they have, invalidate
        // cached geometry.
        self.check_invalidate_geometry();


        self.renderer.update_framebuffers(&scene);

        let bounds = Region::sized(scene.camera_size) + scene.camera_pos;
        let tile_bounds = bounds.div_round_signed(TILE_SIZE);
        let chunk_bounds = bounds.div_round_signed(CHUNK_SIZE * TILE_SIZE);

        // Terrain from the chunk below can cover the current one.
        let terrain_bounds = Region::new(chunk_bounds.min - V2::new(0, 0),
                                         chunk_bounds.max + V2::new(0, 1));
        self.renderer.update_terrain_geometry(self.state.terrain_chunks(),
                                              terrain_bounds);

        // Structures from the chunk below can cover the current one, and also
        // structures from chunks above and to the left can extend into it.
        let structure_bounds = Region::new(chunk_bounds.min - V2::new(1, 1),
                                           chunk_bounds.max + V2::new(0, 1));
        self.renderer.update_structure_geometry(self.state.structures(),
                                                self.caches.draw_mode(),
                                                structure_bounds);

        // Light from any adjacent chunk can extend into the current one.
        let light_bounds = Region::new(chunk_bounds.min - V2::new(1, 1),
                                       chunk_bounds.max + V2::new(1, 1));
        self.renderer.update_structure_light_geometry(self.state.structures(),
                                                      light_bounds);

        // Entities can extend in any direction from their reference point.
        let entity_bounds = Region::new(chunk_bounds.min - V2::new(1, 1),
                                        chunk_bounds.max + V2::new(1, 1));
        self.renderer.update_entity_geometry(self.state.entities(),
                                             entity_bounds,
                                             scene.now);

        // Draw the selection outline to the scratch buffer
        self.renderer.prepare_selection(scene,
                                        self.state.structures(),
                                        self.caches.draw_mode(),
                                        self.state.entities());

        // Update the cavern map
        // TODO: cache this
        let mut grid = box [physics::floodfill::flags::Flags::empty(); 96 * 96];
        let slice_offset = scene.slice_center.reduce() - 48;
        // `grid_bounds` is a region the size of the grid, centered at slice_center.
        let grid_bounds = Region::sized(96) + slice_offset;
        let slice_bounds = Region::new(tile_bounds.min,
                                       tile_bounds.max + V2::new(0, 16));
        physics::floodfill::floodfill(scene.slice_center,
                                      slice_bounds.intersect(grid_bounds),
                                      self.caches.terrain_shape(),
                                      &mut *grid,
                                      grid_bounds);
        self.renderer.load_cavern_map(&*grid);
    }

    fn calc_selection(&self) -> Selection {
        let data = data();
        if let Some(item_id) = self.misc.hotbar.active_item() {
            let item = data.item_def(item_id);

            if item.flags.contains(I_USE_ON_SELF) {
                // Highlight pawn, regardless of mouse position
                if let Some(eid) = self.state.pawn().id() {
                    return Selection::Entity(eid);
                }
                Selection::None

            } else if item.flags.contains(I_USE_AT_POINT) {
                // Highlight structure or ground
                if let Some(sid) = self.mouse_select.structure() {
                    Selection::Structure(sid)
                } else if let Some(pos) = self.mouse_select.cell_pos() {
                    Selection::Cell(pos)
                } else {
                    Selection::None
                }

            } else {
                Selection::None
            }

        } else {
            // No item - highlight structures only
            if let Some(sid) = self.mouse_select.structure() {
                Selection::Structure(sid)
            } else {
                Selection::None
            }
        }
    }

    fn render_frame_timed(&mut self, ft: &mut FrameTimes) {
        // NB: `render_frame` (and `prepare`) should not have early exits.  There are a number of
        // components that expect to be called exactly once for each `state.reset_changes()`.

        let t_total = Timer::start(&mut ft.total, self.platform.get_time());

        // Update time_base before querying the current sim time (`self.now()`).
        self.timing.update(self.platform.get_time());

        let now = self.now();

        trace!(" --- begin frame @ {} ---", now);

        // Update state, then immediately update caches.
        let t_advance = Timer::start(&mut ft.advance, self.platform.get_time());
        self.state.advance(now);
        self.caches.update1(self.state.terrain_chunks(),
                            self.state.structures());
        t_advance.end(self.platform.get_time());

        // Update debug before predicting, so debug sees the right value of `now`.
        self.state.update_debug(&mut self.debug);

        let t_predict = Timer::start(&mut ft.predict, self.platform.get_time());
        if self.config.predict_movement {
            self.predictor.advance(now);
            self.predictor.predict(self.platform.get_time(),
                                   &mut self.state,
                                   self.caches.terrain_shape());
        }
        t_predict.end(self.platform.get_time());

        self.caches.update2(now,
                            self.state.entities(),
                            self.state.structures());

        let day_time = self.state.plane().day_night().time_of_day(now);

        self.misc.inv_changes.update(now, self.state.inventories());

        if self.debug.show_full() {
            self.debug.update_memory_stats();
            self.timing.update_debug(&mut self.debug);
            self.debug.day_time = day_time;
            self.debug.day_phase = self.state.plane().day_night().phase_delta(day_time).0;
            self.predictor.update_debug(now, &mut self.debug);
        }

        // Update player position
        // This needs to happen before the camera position is set, in case the motion changed
        // between the previous frame and now.
        let pos =
            if let Some(pawn) = self.state.pawn_entity() { pawn.pos(now) }
            else { self.default_camera_pos };
        // Wrap `pos` to 2k .. 6k region
        const WRAP_BASE: i32 = 1 << (TILE_BITS + CHUNK_BITS + LOCAL_BITS - 1);
        const WRAP_MASK: i32 = (1 << (TILE_BITS + CHUNK_BITS + LOCAL_BITS)) - 1;
        let pos = util::wrap_base(pos, V3::new(WRAP_BASE, WRAP_BASE, 0), WRAP_MASK.into());
        self.debug.pos = pos;
        trace!("camera pos: {:?}", pos);

        // Check the current mouse position
        let cursor_pos =
            if self.misc.show_cursor {
                self.state.pawn_entity() .and_then(|pawn| {
                    calc_cursor_pos(pos, pawn.activity.dir())
                })
            } else {
                None
            };
        self.mouse_select.update(self.state.structures(),
                                 Scene::calc_camera_bounds(pos, self.view_size),
                                 pos,
                                 cursor_pos);
        self.mouse_select.update_debug(&mut self.debug);

        let ambient_light =
            if self.state.plane().is_dark() { (0, 0, 0, 0) }
            else { self.state.plane().day_night().ambient_light(now) };
        let scene = Scene::new(now,
                               self.window_size,
                               self.view_size,
                               self.ui_scale,
                               pos,
                               ambient_light,
                               self.mouse_select.near_pawn(),
                               self.calc_selection());

        let t_prepare = Timer::start(&mut ft.prepare, self.platform.get_time());
        self.prepare(&scene);
        t_prepare.end(self.platform.get_time());

        self.state.clear_changes();

        let t_render = Timer::start(&mut ft.render, self.platform.get_time());
        self.renderer.render(&scene);
        t_render.end(self.platform.get_time());

        let t_display = Timer::start(&mut ft.display, self.platform.get_time());
        self.renderer.display(&scene);
        t_display.end(self.platform.get_time());

        t_total.end(self.platform.get_time());
    }

    pub fn render_frame(&mut self) {
        let mut frame_times = FrameTimes::new();
        self.render_frame_timed(&mut frame_times);
        self.debug.record_frame_times(self.platform.get_time(), frame_times);
    }


    // Misc

    fn decode_time(&self, server: LocalTime) -> Time {
        let client = self.platform.get_time();
        self.timing.decode(client, server)
    }

    fn now(&self) -> Time {
        let client = self.platform.get_time();
        self.timing.convert(client)
    }

    pub fn init_day_night(&mut self, now: Time, base_offset: Time, cycle_ms: Time) {
        // Compute the server time when the day/night cycle began
        let base_time = now - base_offset;
        self.state.init_day_night(base_time, cycle_ms);
    }

    pub fn set_plane_flags(&mut self, flags: u32) {
        self.state.set_is_dark(flags != 0);
    }

    pub fn handle_pong(&mut self, _client_send: Time, _client_recv: Time, _server: u16) {
        // TODO
    }

    pub fn calc_scale(&self, size: (u16, u16)) -> i16 {
        let scale_config = self.platform.config().get_int(ConfigKey::ScaleWorld) as i16;
        if scale_config != 0 {
            return scale_config;
        }

        let (w, h) = size;
        let max_dim = cmp::max(w, h);
        // This size target places the scale breakpoints at 900px, 1500px, and 2100px.  These are
        // >=100px below the most common screen sizes.  Camera size is in the range 500-700px for
        // all common screen sizes except 800x600 (where it is 800px).
        const TARGET: u16 = 600;
        if max_dim > TARGET {
            ((max_dim + TARGET / 2) / TARGET) as i16
        } else {
            -(((TARGET + max_dim / 2) / max_dim) as i16)
        }
    }

    pub fn resize_window(&mut self, size: (u16, u16)) {
        let scale = self.calc_scale(size);
        info!("set scale to {} ({:?})", scale, size);

        let view_size =
            if scale > 0 {
                let shrink = scale as u16;
                ((size.0 + shrink - 1) / shrink,
                 (size.1 + shrink - 1) / shrink)
            } else {
                let grow = (-scale) as u16;
                (size.0 * grow,
                 size.1 * grow)
            };

        self.window_size = size;
        self.view_size = view_size;

        self.ui_scale = match self.platform.config().get_int(ConfigKey::ScaleUI) {
            0 => 1,
            x => x as u16,
        };

        let ui_size = (view_size.0 / self.ui_scale,
                       view_size.1 / self.ui_scale);
        self.ui2.resize(ui_size);
    }

    pub fn bench(&mut self) {
        #![allow(warnings)]
        let mut counter = 0;
        for i in 0 .. 10000 {
            //let geom = self.ui2.generate_geom(&self.state.inventories);
            //counter += geom.len();
            //self.renderer.load_ui_geometry(&geom);
        }
        info!("counter {}", counter);
    }
}


pub trait ClientObj {
    fn inventories(&self) -> &Inventories;

    fn platform(&mut self) -> &mut PlatformObj;

    fn handle_hotbar_assign(&mut self, idx: u8, item_id: ItemId, is_ability: bool);
    fn handle_hotbar_drop(&mut self,
                          src_inv: InventoryId,
                          src_slot: usize,
                          dest_slot: u8);
    fn handle_hotbar_select(&mut self, idx: u8);
}

impl<P: Platform> ClientObj for Client<P> {
    fn inventories(&self) -> &Inventories { self.state.inventories() }
    fn platform(&mut self) -> &mut PlatformObj { &mut self.platform }

    fn handle_hotbar_assign(&mut self, idx: u8, item_id: u16, is_ability: bool) {
        self.misc.hotbar.set_slot(self.platform.config_mut(),
                                  idx,
                                  item_id,
                                  is_ability);
    }

    fn handle_hotbar_drop(&mut self,
                          src_inv: InventoryId,
                          src_slot: usize,
                          dest_slot: u8) {
        let item_id = match self.state.inventories().get(src_inv) {
            Some(x) => x.items[src_slot].id,
            None => return,
        };
        // TODO: hack
        let is_ability = Some(src_inv) == self.state.inventories().ability_id();
        self.handle_hotbar_assign(dest_slot, item_id, is_ability);
    }

    fn handle_hotbar_select(&mut self, idx: u8) {
        self.misc.hotbar.select(idx as i8);
    }
}


pub fn rle16_decode(input: &[u16], output: &mut [u16]) {
    let mut i = 0;
    let mut j = 0;
    while i < input.len() {
        let x = input[i];
        i += 1;

        if x & 0xf000 == 0 {
            output[j] = x;
            j += 1;
        } else {
            let count = x & 0x0fff;
            let value = input[i];
            i += 1;

            for _ in 0..count {
                output[j] = value;
                j += 1;
            }
        }
    }
}

static DIRS: [V2; 8] = [
    V2 { x:  1, y:  0 },
    V2 { x:  1, y:  1 },
    V2 { x:  0, y:  1 },
    V2 { x: -1, y:  1 },
    V2 { x: -1, y:  0 },
    V2 { x: -1, y: -1 },
    V2 { x:  0, y: -1 },
    V2 { x:  1, y: -1 },
];

fn calc_cursor_pos(pos: V3, dir: Option<u8>) -> Option<V3> {
    let dir = dir?;
    let tile = (pos + V3::new(16, 16, 16)).div_floor(TILE_SIZE);
    let pos = tile + DIRS[dir as usize].extend(0);
    Some(pos)
}
