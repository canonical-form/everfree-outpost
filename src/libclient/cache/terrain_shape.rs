use common::types::*;
use common::util;
use common::movement::OldShapeSource;

use data::data;
use state::structure::Structures;


pub const NUM_LAYERS: usize = 4;

pub type ShapeChunk = [Shape; 1 << (3 * CHUNK_BITS)];

pub struct ChunkShape {
    layers: [ShapeChunk; NUM_LAYERS],
    merged: ShapeChunk,
}

impl ChunkShape {
    fn new() -> ChunkShape {
        ChunkShape {
            layers: [[Shape::Empty; 1 << (3 * CHUNK_BITS)]; NUM_LAYERS],
            merged: [Shape::Empty; 1 << (3 * CHUNK_BITS)],
        }
    }

    fn clear(&mut self) {
        for layer in self.layers.iter_mut() {
            for shape in layer.iter_mut() {
                *shape = Shape::Empty;
            }
        }

        for shape in self.merged.iter_mut() {
            *shape = Shape::Empty;
        }
    }

    fn reset_structure_layers(&mut self) {
        for layer in self.layers.iter_mut().skip(1) {
            for shape in layer.iter_mut() {
                *shape = Shape::Empty;
            }
        }
    }

    fn refresh(&mut self, bounds: Region) {
        let chunk_bounds = Region::sized(CHUNK_SIZE);

        for p in bounds.intersect(chunk_bounds).points() {
            let idx = chunk_bounds.index(p);
            self.merged[idx] = self.layers[0][idx];

            for layer in self.layers.iter().skip(1) {
                if shape_overrides(self.merged[idx], layer[idx]) {
                    self.merged[idx] = layer[idx];
                }
            }
        }
    }

    fn set_shape_in_region_by<F>(&mut self, bounds: Region, layer: usize, f: F)
            where F: Fn(V3) -> Option<Shape> {
        let chunk_bounds = Region::sized(CHUNK_SIZE);
        let inner_bounds = bounds.intersect(chunk_bounds);
        for pos in inner_bounds.points() {
            if let Some(shape) = f(pos) {
                self.layers[layer][chunk_bounds.index(pos)] = shape;
            }
        }
    }
}

fn shape_overrides(old: Shape, new: Shape) -> bool {
    match (old, new) {
        (Shape::Empty, _) => true,

        (Shape::Floor, Shape::Empty) => false,
        (Shape::Floor, _) => true,

        (Shape::Solid, _) => false,

        _ => false,
    }
}


pub struct Cache {
    chunks: Box<[Box<ChunkShape>; 1 << (2 * LOCAL_BITS)]>,
    structures_valid: Box<[bool; 1 << (2 * LOCAL_BITS)]>,
    any_structures_invalid: bool,
}

impl Cache {
    pub fn new() -> Cache {
        // 0 == Shape::Empty
        let chunks = util::make_array_with(1 << (2 * LOCAL_BITS),
                                           || Box::new(ChunkShape::new()));
        let chunks = unsafe { util::size_array(chunks) };
        Cache {
            chunks: chunks,
            structures_valid: Box::new([true; 1 << (2 * LOCAL_BITS)]),
            any_structures_invalid: false,
        }
    }

    pub fn clear(&mut self) {
        for chunk in self.chunks.iter_mut() {
            chunk.clear();
        }
    }

    pub fn refresh_structures(&mut self, structures: &Structures) {
        if !self.any_structures_invalid {
            return;
        }

        let local_bounds = Region2::sized(LOCAL_SIZE);
        let chunk_bounds = Region::sized(CHUNK_SIZE);

        for cpos in local_bounds.points() {
            let idx = local_bounds.index(cpos & LOCAL_MASK);
            if !self.structures_valid[idx] {
                self.chunks[local_bounds.index(cpos)].reset_structure_layers();
            }
        }

        for (_, s) in structures.iter() {
            self.add_structure_impl(s.pos, s.template_id, true);
        }

        for cpos in local_bounds.points() {
            let idx = local_bounds.index(cpos & LOCAL_MASK);
            if !self.structures_valid[idx] {
                self.chunks[idx].refresh(chunk_bounds);
                self.structures_valid[idx] = true;
            }
        }
        self.any_structures_invalid = false;
    }

    pub fn set_terrain<F: Fn(V3) -> BlockId>(&mut self, cpos: V2, get_block: F) {
        let data = data();
        let local_bounds = Region::sized(LOCAL_SIZE);
        let chunk_bounds = Region::sized(CHUNK_SIZE);
        let idx = local_bounds.index(cpos & LOCAL_MASK);
        self.chunks[idx].set_shape_in_region_by(chunk_bounds, 0, |pos| {
            Some(data.block(get_block(pos)).flags().shape())
        });
        self.chunks[idx].refresh(chunk_bounds);
    }

    fn add_structure_impl(&mut self,
                          pos: V3,
                          template_id: TemplateId,
                          refreshing: bool) {
        let data = data();
        let t = data.template(template_id);
        let flags = t.shape();

        let bounds = Region::sized(t.size()) + pos;
        let cpos_bounds = bounds.reduce().div_round_signed(CHUNK_SIZE);
        let local_bounds = Region::sized(LOCAL_SIZE);

        for cpos in cpos_bounds.points() {
            let idx = local_bounds.index(cpos & LOCAL_MASK);
            if !refreshing {
                if !self.structures_valid[idx] { continue; }
            } else {
                if self.structures_valid[idx] { continue; }
            }

            let adj = (cpos * CHUNK_SIZE).extend(0);
            self.chunks[idx].set_shape_in_region_by(bounds - adj, 1 + t.layer as usize, |pos| {
                let f = flags[bounds.index(pos + adj)];
                if f.occupied() {
                    Some(f.shape())
                } else {
                    None
                }
            });
            if !refreshing {
                self.chunks[idx].refresh(bounds - adj);
            }
        }
    }

    pub fn add_structure(&mut self, pos: V3, template_id: TemplateId) {
        self.add_structure_impl(pos, template_id, false);
    }

    pub fn remove_structure(&mut self, pos: V3, template_id: TemplateId) {
        let data = data();
        let t = data.template(template_id);

        let bounds = Region::sized(t.size()) + pos;
        let cpos_bounds = bounds.reduce().div_round_signed(CHUNK_SIZE);
        let local_bounds = Region::sized(LOCAL_SIZE);

        for cpos in cpos_bounds.points() {
            let idx = local_bounds.index(cpos & LOCAL_MASK);
            self.structures_valid[idx] = false;
            self.any_structures_invalid = true;
        }
    }

    pub fn get_shape(&self, pos: V3) -> Shape {
        if pos.z < 0 || pos.z >= CHUNK_SIZE {
            return Shape::Empty;
        }

        let tile = pos & CHUNK_MASK;
        let chunk = (pos.reduce() >> CHUNK_BITS) & LOCAL_MASK;

        let local_bounds = Region2::sized(LOCAL_SIZE);
        let chunk_bounds = Region::sized(CHUNK_SIZE);

        self.chunks[local_bounds.index(chunk)].merged[chunk_bounds.index(tile)]
    }
}

impl OldShapeSource for Cache {
    fn get_shape(&self, pos: V3) -> Shape {
        <Cache>::get_shape(self, pos)
    }
}
