use std::prelude::v1::*;
use common::types::*;
use std::collections::{BTreeMap, BTreeSet};
use std::mem;
use common::util::ZOrdered;

use data::{data, DrawMode};
use state::structure::Structures;

use super::{entity_pos, struct_pos};


/// Cache for tracking the effects of structure `DrawMode`s.  For each structure with a nontrivial
/// `DrawMode`, this cache stores the computed part mask for that structure.
///
/// This cache updates in two phases.  In the first phase, calls to `add` and `remove` build up a
/// data structure indicating which structures need their part masks updated.  In the second phase,
/// a call to `update` computes new part masks for all the recorded structures.  Deferring the
/// part mask computations lets us avoid duplicate work in cases where a structure and several
/// neighbors all appear at once (for example, during chunk loading).  It also lets us easily avoid
/// accessing structures that have been removed from `Structures`, even if they are still listed in
/// `struct_pos::Cache` at the time of the `add`/`remove`.
pub struct Cache {
    map: BTreeMap<StructureId, u32>,

    /// Set of structures that need updating in the second phase.
    needs_update: BTreeSet<StructureId>,
    /// Set of positions that need updating in the second phase.  The second phase will update all
    /// structures at these positions whose display is affected by the indicated draw mode.
    pos_needs_update: BTreeSet<(ZOrdered<V3>, DrawMode)>,

    /// Flag indicating if any draw modes changed in the most recent update.  This is an
    /// approximation: if `true`, some modes may have changed; if `false`, none changed.
    modes_changed: bool,
}

impl Cache {
    pub fn new() -> Cache {
        Cache {
            map: BTreeMap::new(),

            needs_update: BTreeSet::new(),
            pos_needs_update: BTreeSet::new(),

            modes_changed: false,
        }
    }

    pub fn add(&mut self,
               id: StructureId,
               pos: V3,
               draw_mode: DrawMode) {
        // TODO: ignore DrawMode::Normal
        self.map.insert(id, 0);
        self.needs_update.insert(id);

        iter_affected_neighbors(draw_mode, |offset| {
            self.pos_needs_update.insert((ZOrdered(pos + offset), draw_mode));
        });
    }

    pub fn remove(&mut self,
                  id: StructureId,
                  pos: V3,
                  draw_mode: DrawMode) {
        self.map.remove(&id);
        self.needs_update.remove(&id);

        iter_affected_neighbors(draw_mode, |offset| {
            self.pos_needs_update.insert((ZOrdered(pos + offset), draw_mode));
        });
    }

    /// Schedule updates for any structure overlapping `pos` that has `DrawMode::Door`.
    pub fn touch_doors(&mut self,
                       structures: &Structures,
                       struct_pos: &struct_pos::Cache,
                       pos: V3) {
        let data = data();

        for id in struct_pos.at_pos(pos) {
            let s = &structures[id];
            let t = data.template(s.template_id);
            if let DrawMode::Door { .. } = t.draw_mode() {
                trace!("touch_doors: updating {:?} at {:?}", id, pos);
                self.needs_update.insert(id);
            }
        }
    }

    pub fn update(&mut self,
                  structures: &Structures,
                  struct_pos: &struct_pos::Cache,
                  entity_pos: &entity_pos::Cache) {
        let data = data();

        let pos_needs_update = mem::replace(&mut self.pos_needs_update, BTreeSet::new());
        for (ZOrdered(pos), mode) in pos_needs_update {
            for id in struct_pos.origin_at_pos(pos) {
                let s = &structures[id];
                let t = data.template(s.template_id);
                if affected_by(mode, t.draw_mode()) {
                    self.needs_update.insert(id);
                }
            }
        }

        self.modes_changed = self.needs_update.len() > 0;
        for id in mem::replace(&mut self.needs_update, BTreeSet::new()) {
            let s = &structures[id];
            let t = data.template(s.template_id);
            let mask = calc_part_mask(structures, struct_pos, entity_pos, t.draw_mode(), s.pos);
            *self.map.get_mut(&id).unwrap() = mask;
        }
    }

    pub fn part_mask(&self, id: StructureId) -> u32 {
        self.map[&id]
    }

    pub fn modes_changed(&self) -> bool {
        self.modes_changed
    }
}

const HORIZ_DIRS: [V3; 2] = [
    // Order: E, W
    V3 { x:  1, y:  0, z:  0 },
    V3 { x: -1, y:  0, z:  0 },
];

const ORTHO_DIRS: [V3; 4] = [
    // Order: N, E, S, W
    V3 { x:  0, y: -1, z:  0 },
    V3 { x:  1, y:  0, z:  0 },
    V3 { x:  0, y:  1, z:  0 },
    V3 { x: -1, y:  0, z:  0 },
];

const TREE_DIRS: [V3; 4] = [
    // Order: E, NE, NW, W
    V3 { x:  2, y:  0, z:  0},
    V3 { x:  1, y: -1, z:  0},
    V3 { x: -1, y: -1, z:  0},
    V3 { x: -2, y:  0, z:  0},
];

fn iter_affected_neighbors<F: FnMut(V3)>(mode: DrawMode, mut func: F) {
    match mode {
        DrawMode::Normal => {},

        DrawMode::Fence { .. } => {
            for &dir in &ORTHO_DIRS {
                func(dir);
            }
        },

        DrawMode::Tree { .. } => {
            for &dir in &TREE_DIRS {
                func(-dir);
            }
        },

        DrawMode::Fence16 { .. } => {
            for &dir in &ORTHO_DIRS {
                func(dir);
            }
        },

        DrawMode::Door { .. } => {
            for &dir in &HORIZ_DIRS {
                func(dir);
            }
        },
    }
}

/// Returns `true` iff a structure with mode `target` might change its part mask due to a change in
/// an adjacent structure with mode `changed`.
fn affected_by(changed: DrawMode, target: DrawMode) -> bool {
    use data::DrawMode::*;
    match target {
        Normal => false,

        Fence { cls: cls1, .. } |
        Fence16 { cls: cls1, .. } => match changed {
            Fence { cls: cls2, .. } |
            Fence16 { cls: cls2, .. } |
            Door { cls: cls2, .. } => cls1 == cls2,
            _ => false,
        },

        Tree { .. } => match changed {
            Tree { .. } => true,
            _ => false,
        },

        Door { .. } => false,
    }
}


fn calc_part_mask(structures: &Structures,
                  struct_pos: &struct_pos::Cache,
                  entity_pos: &entity_pos::Cache,
                  mode: DrawMode,
                  pos: V3) -> u32 {
    let data = data();

    match mode {
        DrawMode::Normal => 0xffffffff,

        DrawMode::Fence { cls, fixed, use_corners } => {
            let mut fence_bits = 0;
            for (i, &dir) in ORTHO_DIRS.iter().enumerate() {
                for id in struct_pos.origin_at_pos(pos + dir) {
                    let mode = data.template(structures[id].template_id).draw_mode();
                    if mode.is_fence(cls) {
                        fence_bits |= 1 << i;
                    }
                }
            }

            let mut door_bits = 0;
            for (i, &dir) in HORIZ_DIRS.iter().enumerate() {
                for id in struct_pos.origin_at_pos(pos + dir) {
                    let mode = data.template(structures[id].template_id).draw_mode();
                    if mode.is_door(cls) {
                        fence_bits |= 1 << (i * 2 + 1);
                        door_bits |= 1 << i;
                    }
                }
            }

            // Build the mask
            let mut mask = (1 << fixed) - 1;
            let mut idx = fixed;

            mask |= fence_bits << idx;
            idx += 4;

            if use_corners {
                // Diagonal parts are enabled only if two adjacent bits are set in `fence_bits`.
                for i in 0 .. 4 {
                    // When i = 0, check = 0b1001.  Other times, it's 0b0011 << (i - 1).
                    let check = (0b10011 << i >> 1) & 0xf;
                    if (fence_bits & check) == check {
                        mask |= 1 << (idx + i);
                    }
                }
                idx += 4;
            }

            mask |= door_bits << idx;
            idx += 2;

            let _ = idx;
            mask
        },

        DrawMode::Tree { fixed } => {
            let mut tree_bits = 0;
            for (i, &dir) in TREE_DIRS.iter().enumerate() {
                for id in struct_pos.origin_at_pos(pos + dir) {
                    let mode = data.template(structures[id].template_id).draw_mode();
                    if mode.is_tree() {
                        tree_bits |= 1 << i;
                    }
                }
            }

            // `extra` bits, from MSB to LSB, indicate the presence of other trees to the west,
            // northwest, northeast, and east.
            const MASKS: [u8; 4] = [
                //0b1000, 0b1100, 0b0100, 0b0110, 0b0010, 0b0011, 0b0001
                0b1000, 0b0100, 0b0010, 0b0001
            ];

            let mut mask = (1 << fixed) - 1;
            for i in 0 .. MASKS.len() {
                if tree_bits & MASKS[i] == 0 {
                    mask |= 1 << (i + fixed as usize);
                }
            }

            mask
        },

        DrawMode::Fence16 { cls } => {
            let mut fence_bits = 0;
            for (i, &dir) in ORTHO_DIRS.iter().enumerate() {
                for id in struct_pos.origin_at_pos(pos + dir) {
                    let mode = data.template(structures[id].template_id).draw_mode();
                    if mode.is_fence(cls) {
                        fence_bits |= 1 << i;
                    }
                }
            }

            for (i, &dir) in HORIZ_DIRS.iter().enumerate() {
                for id in struct_pos.origin_at_pos(pos + dir) {
                    let mode = data.template(structures[id].template_id).draw_mode();
                    if mode.is_door(cls) {
                        fence_bits |= 1 << (i * 2 + 1);
                    }
                }
            }

            let mask = 1 << fence_bits;
            mask
        },

        DrawMode::Door { .. } => {
            trace!("door at {:?}: {} overlapping entities", pos, entity_pos.count_at(pos));
            if entity_pos.count_at(pos) > 0 {
                0
            } else {
                0xffffffff
            }
        },
    }
}
