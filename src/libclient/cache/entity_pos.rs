use common::types::*;
use std::collections::HashSet;
use std::collections::hash_map::{HashMap, Entry};
use std::mem;

use state::entity::Entities;


#[derive(Default)]
pub struct Cache {
    last_pos: HashMap<EntityId, V3>,
    entity_count: HashMap<V3, usize>,
    changed: HashSet<V3>,
}

impl Cache {
    pub fn new() -> Self {
        Self::default()
    }

    fn inc_ref(&mut self, pos: V3) {
        match self.entity_count.entry(pos) {
            Entry::Vacant(e) => {
                e.insert(1);
                self.changed.insert(pos);
            },
            Entry::Occupied(e) => {
                *e.into_mut() += 1;
            },
        }
    }

    fn dec_ref(&mut self, pos: V3) {
        match self.entity_count.entry(pos) {
            Entry::Vacant(_) => {
                error!("dec_ref: {:?} not present", pos);
            },
            Entry::Occupied(e) => {
                if *e.get() == 1 {
                    e.remove();
                    self.changed.insert(pos);
                } else {
                    *e.into_mut() -= 1;
                }
            },
        }
    }

    pub fn update(&mut self,
                  now: Time,
                  entities: &Entities) {
        let mut seen = HashSet::with_capacity(entities.len());
        for (&eid, e) in entities.iter() {
            seen.insert(eid);
            let old_pos = self.last_pos.get(&eid).cloned();
            let new_pos = e.pos(now);
            if old_pos != Some(new_pos) {
                trace!("entity {:?} moved: {:?} -> {:?}", eid, old_pos, new_pos);
                for p in entity_tiles(new_pos) {
                    self.inc_ref(p);
                }

                if let Some(old_pos) = old_pos {
                    for p in entity_tiles(old_pos) {
                        self.dec_ref(p);
                    }
                }
            }

            self.last_pos.insert(eid, new_pos);
        }

        let mut removed = Vec::new();
        for &eid in self.last_pos.keys() {
            if !seen.contains(&eid) {
                removed.push(eid);
            }
        }

        for eid in removed {
            if let Some(old_pos) = self.last_pos.remove(&eid) {
                trace!("entity {:?} gone: {:?}", eid, old_pos);
                for p in entity_tiles(old_pos) {
                    self.dec_ref(p);
                }
            }
        }
    }

    pub fn take_changed(&mut self) -> HashSet<V3> {
        if self.changed.len() > 0 {
            trace!("changed entity counts at {:?}", self.changed);
        }
        mem::replace(&mut self.changed, HashSet::new())
    }

    pub fn count_at(&self, pos: V3) -> usize {
        self.entity_count.get(&pos).cloned().unwrap_or(0)
    }
}

fn entity_tiles(pos: V3) -> impl Iterator<Item=V3> {
    static MASK: i32 = (1 << (CHUNK_BITS + LOCAL_BITS)) - 1;
    (Region::sized(16) + pos).div_round_signed(TILE_SIZE)
        .points()
        .map(move |p| p & MASK)
}
