//! Note on time systems:
//!
//! For the purposes of this module, there are two relevant systems of time.  Client time starts
//! counting from zero at the moment the client was started.  Server time starts counting from the
//! timestamp of the first received packet at the moment that packet arrives.  Since packet
//! timestamps are 16 bits, that means server time starts roughly 0 - 65 seconds before client time
//! does.  One goal of this module is to compute a good approximation of this offset, so that we
//! can obtain the server time corresponding to any client time.
//!
//! Note that "server time" does not actually correspond to the precise time on the server.
//! Rather, it gives the time on the server plus the one-way travel time from server to client,
//! which is the timestamp of the newest state change the client can observe.  This means that
//! server time can drift due to changes in network conditions.

use std::cmp;
use std::collections::{VecDeque, BTreeMap};
use std::collections::btree_map::Entry;
use common::types::*;
use common::proto::types::LocalTime;

use debug::Debug;


pub const TICK_MS: Time = 32;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct HistoryEntry {
    insert_time: Time,
    value: Time,
}

struct QuantileWindow {
    history: VecDeque<HistoryEntry>,
    sorted: BTreeMap<Time, usize>,
}

impl QuantileWindow {
    pub fn new() -> QuantileWindow {
        QuantileWindow {
            history: VecDeque::new(),
            sorted: BTreeMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.history.len()
    }

    pub fn insert(&mut self, insert_time: Time, value: Time) {
        self.history.push_back(HistoryEntry { insert_time, value });
        match self.sorted.entry(value) {
            Entry::Vacant(e) => {
                e.insert(1);
            },
            Entry::Occupied(e) => {
                *e.into_mut() += 1;
            },
        }
    }

    /// Remove all entries with `insert_time` earlier than `first_insert_time`.
    pub fn expire_older(&mut self, first_insert_time: Time) {
        while self.history.front().map_or(false, |e| e.insert_time < first_insert_time) {
            let value = self.history.pop_front().unwrap().value;
            match self.sorted.entry(value) {
                Entry::Vacant(_) => {
                    panic!("popped value is not in sorted map?");
                },
                Entry::Occupied(e) => {
                    if *e.get() == 1 {
                        e.remove();
                    } else {
                        *e.into_mut() -= 1;
                    }
                },
            }
        }
    }

    pub fn query(&self, numer: usize, denom: usize) -> Option<Time> {
        if self.history.len() == 0 {
            return None;
        }

        let idx = self.history.len() * numer / denom;
        // This works best if `idx` is close to `len`, which should be the case if queries are
        // normally for the 90+ percentile.
        let mut cur = self.history.len();
        for (&value, &count) in self.sorted.iter().rev() {
            // Logical behavior: subtract count from cur.  `cur` now points just before the first
            // copy of `value`.  If idx >= cur, then idx refers to one of those copies.  Note that
            // cur > idx on entry.
            //
            // Actual behavior: do a trick to avoid overflow.
            //
            // idx >= cur - count
            // -cur >= -idx - count
            // cur <= idx + count
            if cur <= idx + count {
                return Some(value);
            }
            cur -= count;
        }
        unreachable!()
    }

    /// Like `query`, but optimized for accessing small quantiles (<50%).
    pub fn query_low(&self, numer: usize, denom: usize) -> Option<Time> {
        if self.history.len() == 0 {
            return None;
        }

        let idx = self.history.len() * numer / denom;
        let mut cur = 0;
        for (&value, &count) in self.sorted.iter() {
            cur += count;
            if cur >= idx {
                return Some(value);
            }
        }
        unreachable!()
    }
}


pub struct Timing {
    /// Client timestamp corresponding to the server time `0`.
    base: Time,

    quant: QuantileWindow,
}

const WINDOW_DUR: Time = 60 * 1000;

impl Timing {
    pub fn new() -> Timing {
        Timing {
            base: 0,
            quant: QuantileWindow::new(),
        }
    }

    pub fn snap(&mut self) {
        self.base = self.target_time_base().expect("snap: no measurements in history");
        trace!("snap time base to {}", self.base);
    }

    pub fn add_measurement(&mut self, client: Time, server: Time) {
        trace!("measurement: client {:?}, server {:?}, base {:?}", client, server, client - server);
        self.quant.insert(client, client - server);
    }

    /// Compute the target time base.  When this diverges from the actual value of `self.base`,
    /// `self.base` will be slowly pulled toward it by `update()`.
    fn target_time_base(&self) -> Option<Time> {
        // Take the 99th %ile value, then add a tick.
        self.quant.query(99, 100).map(|v| v + TICK_MS)
    }

    /// Update the timing base, pulling it into alignment with `self.target_time_base()`.
    pub fn update(&mut self, client: Time) {
        self.quant.expire_older(client - WINDOW_DUR);
        let target = unwrap_or!(self.target_time_base());
        let full_delta = target - self.base;
        if full_delta == 0 {
            return;
        }

        // We limit the effective duration of a tick to 90% .. 200% of TICK_MS.
        //
        // Suppose a tick starts at platform time = sim time = 0.  Then we set the time base to -1.
        // The next tick starts at platform time = 31, sim time = 32.  Thus adjusting the time base
        // downward shortens the real-time duration of the tick, and adjusting it upward lengthens
        // the tick.
        //
        // NB: If you edit these ranges, also adjust them in `update_debug` so it stays accurate.
        let delta = cmp::max(-TICK_MS / 10, cmp::min(full_delta, TICK_MS));
        trace!("update ({}): adjust time base by {} (full = {}, {} -> {})",
               client, delta, full_delta, self.base, target);
        self.base += delta;
    }

    /// Decode a `LocalTime` into a server time.
    pub fn decode(&self, client_now: Time, server_time: LocalTime) -> Time {
        // Interpret `server_time` relative to a sliding window centered on our current
        // approximation of server time.
        let center = client_now - self.base;
        let offset = server_time.unwrap().wrapping_sub(center as u16);
        center + offset as i16 as Time
    }

    /// Convert client time to server time.
    pub fn convert(&self, client_now: Time) -> Time {
        client_now - self.base
    }

    pub fn update_debug(&self, debug: &mut Debug) {
        debug.timing_low = self.quant.query_low(5, 100).unwrap_or(0);
        debug.timing_mid = self.quant.query_low(50, 100).unwrap_or(0);
        debug.timing_high = self.quant.query(95, 100).unwrap_or(0);
        debug.timing_highest = self.quant.query(99, 100).unwrap_or(0);
        debug.timing_samples = self.quant.len();

        let target = self.target_time_base().unwrap_or(self.base);
        let delta = target - self.base;
        debug.timing_delta = delta;
        if delta < 0 {
            debug.timing_skew_ticks = (-delta / (TICK_MS / 10)) as u32;
        } else {
            debug.timing_skew_ticks = (delta / TICK_MS) as u32;
        }
    }
}


#[cfg(test)]
mod test {
    use common::proto::types::LocalTime;
    use super::Timing;

    #[test]
    fn decode_range() {
        let mut t = Timing::new();
        // Initial time_base is 0

        assert_eq!(t.decode(0, LocalTime(0)), 0);

        assert_eq!(t.decode(0, LocalTime(10000)), 10000);
        assert_eq!(t.decode(0, LocalTime(65535 - 10000 + 1)), -10000);

        assert_eq!(t.decode(0, LocalTime(32767)), 32767);
        assert_eq!(t.decode(0, LocalTime(32768)), -32768);
    }

    #[test]
    fn bug_64_second_delta() {
        let mut t = Timing::new();

        // A previous bug sometimes caused target_time_base to jump by +/-65,000 around init time,
        // making it take a very long time for time_base to reach the target.  These event timings
        // are extracted from a log exhibiting the bug.

        // 0:DEBUG:client::client: 870 (870): receive TickEnd(LocalTime(63264))
        let tick_end_client = 870;
        let tick_end_server = LocalTime(63264);

        // 0:DEBUG:client::client: 912 (912): receive Init(LocalTime(63456), 84704, 1440000)
        let init_client = 912;
        let init_server = LocalTime(63456);

        // Now we replicate the timing effects of the `Client`'s handling of those two messages.
        let tick_end_now = t.decode(tick_end_client, tick_end_server);
        t.add_measurement(tick_end_client, tick_end_now);

        let init_now = t.decode(init_client, init_server);
        t.add_measurement(init_client, init_now);
        t.snap();

        // Check that the delta isn't too extreme
        let target = t.target_time_base().unwrap();
        eprintln!("delta = {}", target - t.base);
        assert!((target - t.base).abs() < 1000);
    }
}
