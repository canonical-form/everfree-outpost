use common::types::*;
use std::cell::Cell;
use std::intrinsics;
use std::mem;

pub use common::util::*;

pub unsafe fn zeroed_boxed_slice<T>(len: usize) -> Box<[T]> {
    let mut v = Vec::with_capacity(len);
    v.set_len(len);
    intrinsics::write_bytes(v.as_mut_ptr(), 0, v.len());
    v.into_boxed_slice()
}

pub fn contains_wrapped(bounds: Region<V2>, pos: V2, mask: V2) -> bool {
    let delta = pos.zip(bounds.min, |a, b| a.wrapping_sub(b)) & mask;
    let size = bounds.max - bounds.min;

    delta.x < size.x &&
    delta.y < size.y
}

pub fn wrap_base<V: Vn>(pos: V, base: V, mask: V) -> V {
    ((pos - base) & mask) + base
}

pub fn sqrt(x: f64) -> f64 {
    unsafe { intrinsics::sqrtf64(x) }
}

pub fn floor(x: f64) -> f64 {
    unsafe { intrinsics::floorf64(x) }
}

pub fn round(x: f64) -> f64 {
    floor(x + 0.5)
}

pub fn sqrtf(x: f32) -> f32 {
    unsafe { intrinsics::sqrtf32(x) }
}

pub fn floorf(x: f32) -> f32 {
    unsafe { intrinsics::floorf32(x) }
}

pub fn roundf(x: f32) -> f32 {
    floorf(x + 0.5)
}


/// Interleave the bits of `x` and `y`, producing the 16-bit Morton code for the pair `(x, y)`.
pub fn morton8(x: u8, y: u8) -> u16 {
    // Here's the idea, on 4-bit numbers:
    //  (1) Put `x` and `y` together into a single word:
    //               0  0  0  0 y3 y2 y1 y0  0  0  0  0 x3 x2 x1 x0
    //  (2) Split apart pairs of bits.  `**` denotes garbage.
    //      (or)     0  0 y3 y2 ** ** y1 y0  0  0 x3 x2 ** ** x1 x0
    //      (mask)   0  0 y3 y2  0  0 y1 y0  0  0 x3 x2  0  0 x1 x0
    //  (3) Split apart single bits.
    //      (or)     0 y3 ** y2  0 y1 ** y0  0 x3 ** x2  0 x1 ** x0
    //      (mask)   0 y3  0 y2  0 y1  0 y0  0 x3  0 x2  0 x1  0 x0
    //  (4) Mix `x` and `y` sections together.
    //                                      y3 x3 y2 x2 y1 x1 y0 x0
    let z = x as u32 | ((y as u32) << 16);
    let z = (z | (z << 4)) & 0x0f0f0f0f;
    let z = (z | (z << 2)) & 0x33333333;
    let z = (z | (z << 1)) & 0x55555555;
    (z | (z >> 15)) as u16
}


pub fn pack_v3(v: V3) -> (u8, u8, u8) {
    (v.x as u8,
     v.y as u8,
     v.z as u8)
}

pub fn unpack_v3(v: (u8, u8, u8)) -> V3 {
    V3::new(v.0 as i32,
            v.1 as i32,
            v.2 as i32)
}


pub fn mut_to_cell<'a, T: Copy>(x: &'a mut T) -> &'a Cell<T> {
    unsafe { mem::transmute(x) }
}
