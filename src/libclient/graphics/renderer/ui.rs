use common::types::*;
use std::mem;
use colorspace::LchUv;
use common::Appearance;
use common::util::ByteCast;
use render::{Backend, Geometry};

use graphics::renderer::{Renderer, SCRATCH_SIZE};
use ui2::context::{ContextOutput, Channel};


#[allow(dead_code)]
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(C)]
pub struct Vertex {
    src_pos: (u16, u16),
    src_size: (u8, u8),
    sheet: u8,
    _pad1: u8,

    dest: (i16, i16),
    offset: (u16, u16),
}
unsafe impl ByteCast for Vertex {}


pub struct Output<'r, B: Backend+'r> {
    renderer: &'r mut Renderer<B>,

    clip: Option<Region<V2>>,
}


/// Size in pixels of each item.
const ITEM_SIZE: i32 = 16;

const ITEM_SHEET: u8 = 0;
const UI2_SHEET: u8 = 1;
const SCRATCH_SHEET: u8 = 2;

impl<'r, B: Backend> Output<'r, B> {
    pub fn new(renderer: &'r mut Renderer<B>) -> Output<'r, B> {
        Output {
            renderer: renderer,
            clip: None,
        }
    }

    pub fn reset(&mut self) {
        self.renderer.ui_geom.set_len(0);
    }

    fn emit_vert(&mut self, v: Vertex) {
        self.renderer.ui_geom.push(&v);
    }

    fn emit_quad(&mut self,
                 src: Region<V2>,
                 sheet: u8,
                 dest: Region<V2>) {
        let clipped =
            if let Some(ref clip) = self.clip { clip.intersect(dest) }
            else { dest };
        if clipped.is_empty() {
            return;
        }

        let dx = dest.min.x as i16;
        let dy = dest.min.y as i16;

        let mut go = |ox, oy| {
            self.emit_vert(Vertex {
                src_pos: (src.min.x as u16,
                          src.min.y as u16),
                src_size: (src.size().x as u8,
                           src.size().y as u8),
                sheet: sheet,
                _pad1: 0,
                dest: (dx, dy),
                offset: (ox, oy),
            });
        };

        let off = clipped - dest.min;
        let ox0 = off.min.x as u16;
        let ox1 = off.max.x as u16;
        let oy0 = off.min.y as u16;
        let oy1 = off.max.y as u16;

        go(ox0, oy0);
        go(ox0, oy1);
        go(ox1, oy0);

        go(ox1, oy0);
        go(ox0, oy1);
        go(ox1, oy1);
    }
}

impl<'a, 'r, B: Backend> ContextOutput for &'a mut Output<'r, B> {
    fn draw_ui2(&mut self, src: Region<V2>, pos: V2) {
        let dest = Region::sized(src.size()) + pos;
        self.emit_quad(src, UI2_SHEET, dest);
    }

    fn draw_ui2_tiled(&mut self, src: Region<V2>, dest: Region<V2>) {
        self.emit_quad(src, UI2_SHEET, dest);
    }

    fn draw_item(&mut self, src: V2, dest: V2) {
        let base = Region::sized(ITEM_SIZE);
        let src = base + src * ITEM_SIZE;
        let dest = base + dest;
        self.emit_quad(src, ITEM_SHEET, dest);
    }


    fn draw_special_debug(&mut self, dest: V2) {
        // UI quad size is capped at 255.  Use 128x128 chunks to draw the whole scratch buffer.
        let total_size = V2::new(SCRATCH_SIZE.0 as i32,
                                 SCRATCH_SIZE.1 as i32);
        let tile_size: V2 = 128.into();
        let tiled_size = total_size.div_floor(tile_size);
        for p in Region2::sized(tiled_size).points() {
            let tile_pos = p * tile_size;
            self.emit_quad(Region::sized(tile_size) + tile_pos,
                           SCRATCH_SHEET,
                           Region::sized(tile_size) + tile_pos + dest);
        }
    }

    fn draw_special_pony(&mut self, appearance: Appearance, dest: V2) {
        if let Some(src) = self.renderer.get_special_pony(appearance) {
            let dest = Region::sized(src.size()) + dest;
            self.emit_quad(src, SCRATCH_SHEET, dest);
        }
    }

    fn draw_special_color_plot_xy(&mut self, color: LchUv, dest: Region<V2>) {
        if let Some(src) = self.renderer.get_special_color_plot_xy(dest.size(), color) {
            self.emit_quad(src, SCRATCH_SHEET, dest);
        }
    }

    fn draw_special_color_plot_z(&mut self, color: LchUv, dest: Region<V2>) {
        if let Some(src) = self.renderer.get_special_color_plot_z(dest.size(), color) {
            self.emit_quad(src, SCRATCH_SHEET, dest);
        }
    }

    fn draw_special_color_slider(&mut self,
                                 color: (u8, u8, u8),
                                 channel: Channel,
                                 dest: Region<V2>) {
        if let Some(src) = self.renderer.get_special_color_slider(dest.size(), color, channel) {
            self.emit_quad(src, SCRATCH_SHEET, dest);
        }
    }

    fn draw_special_palette_color(&mut self,
                                  palette: &[(u8, u8, u8)],
                                  index: usize,
                                  dest: Region<V2>) {
        if let Some(src) = self.renderer.get_special_palette_color(palette, index) {
            self.emit_quad(src, SCRATCH_SHEET, dest);
        }
    }


    fn push_clip(&mut self, clip: Region<V2>) -> Option<Region<V2>> {
        mem::replace(&mut self.clip, Some(clip))
    }

    fn pop_clip(&mut self, old: Option<Region<V2>>) {
        self.clip = old;
    }
}
