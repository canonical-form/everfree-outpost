use common::types::TILE_SIZE;
use render::ast::{Pipeline, DepthTest, BlendMode};
use render::ast_builder::*;

pub fn build_pipeline() -> Pipeline {
    pipeline(|pb| pb
        .type_alias("square01_geom", ty_square01_geom())
        .type_alias("terrain_geom", ty_terrain_geom())
        .type_alias("structure_geom", ty_structure_geom())
        .type_alias("entity_geom", ty_entity_geom())
        .type_alias("light_geom", ty_light_geom())
        .type_alias("ui_geom", ty_ui_geom())

        .pub_pass("main", pass_main)
        .pub_pass("output", pass_output)

        .pass("terrain", pass_terrain)
        .pass("structures", pass_structures)
        .pass("structure_shadows", pass_structure_shadows)
        .pass("entities", pass_entities)
        .pass("entities_ui", pass_entities_ui)
        .pass("lights", pass_lights)
        .pass("selection", pass_selection)
        .pass("postprocess", pass_postprocess)
        .pass("ui", pass_ui)

        .pub_pass("special_pony", pass_special_pony)
        .pub_pass("special_color_plot_xy", pass_special_color_plot_xy)
        .pub_pass("special_color_plot_z", pass_special_color_plot_z)
        .pub_pass("special_color_slider", pass_special_color_slider)
        .pub_pass("special_selection_cell", pass_special_selection_cell)
        .pub_pass("special_selection_structure", pass_special_selection_structure)
        .pub_pass("special_selection_entity", pass_special_selection_entity)

        .pass("blit_full", pass_blit_full)
        .pass("blit_full_alpha", pass_blit_full_alpha)
        .pass("blit_outline", pass_blit_outline)
    )
}


fn ty_square01_geom() -> TyB {
    // (u8, u8)
    geometry(|fv| fv
        .field("corner", u8().vec(2))
    )
}

fn ty_terrain_geom() -> TyB {
    // graphics::terrain::Vertex
    geometry(|fv| fv
        .field("corner", u8().vec(2))
        .field("block_pos", u8().vec(3))
        .field("side", u8())
        .field("tile_coord", u8().vec(2))
    )
}

fn ty_structure_geom() -> TyB {
    // graphics::structure::Vertex
    geometry(|fv| fv
        .field("vert_offset", i16().vec(3))
        .field("src_pos", i16().vec(2))
        .field("block_pos", u8().vec(3))
        .field("block_size", u8().vec(3))
        .field("anim_length", i8())
        .field("anim_rate", u8())
        .field("anim_oneshot_start", u16())
        .field("anim_step", u16())
    )
}

fn ty_entity_geom() -> TyB {
    // graphics::entity::Vertex
    geometry(|fv| fv
        .field("dest_pos", u16().vec(2))
        .field("src_pos", u16().vec(2))
        .field("sheet", i8())
        .field("color", u8().vec(3))
        // ref_pos + ref_size_z
        .field("ref_pos_size", u16().vec(4))
        // anim_length + rate + start + step
        .field("anim_info", u16().vec(4))
    )
}

fn ty_light_geom() -> TyB {
    // graphics::light::Vertex
    geometry(|fv| fv
        .field("corner", u8().vec(2))
        .field("center", u16().vec(3))
        .field("color", u8().vec(3))
        .field("_pad1", u8())
        .field("radius", u16())
        .field("_pad2", u16())
    )
}

fn ty_ui_geom() -> TyB {
    // graphics::entity::Vertex
    geometry(|fv| fv
        .field("src_pos", u16().vec(2))
        .field("src_size", u8().vec(2))
        .field("sheet", u8())
        .field("_pad1", u8())
        .field("dest", i16().vec(2))
        .field("offset", u16().vec(2))
    )
}


fn pass_main(pb: &mut PassB) -> &mut PassB {
    pb
        .input("camera_pos", f32().vec(2))
        .input("slice_center", f32().vec(2))
        .input("slice_z", f32())
        .input("anim_now", f32())
        .input("clip_enabled", u8())
        .input("ambient_light", f32().vec(4))
        .input("selection_src_region", i32().vec(4))
        .input("selection_dest_pos", i32().vec(2))

        .input("terrain_atlas", TyB::tex_rgba())
        .input("structure_atlas", TyB::tex_rgba())
        .input("entity_atlas", TyB::tex_rgba())
        .input("ui_atlas", TyB::tex_rgba())
        .input("item_atlas", TyB::tex_rgba())
        .input("cavern_map", TyB::tex_rgba())
        .input("scratch", TyB::tex_rgba())

        .input("square01_geom", TyB::alias("square01_geom"))
        .input("terrain_geom", TyB::alias("terrain_geom"))
        .input("structure_geom", TyB::alias("structure_geom"))
        .input("entity_geom", TyB::alias("entity_geom"))
        .input("structure_light_geom", TyB::alias("light_geom"))
        .input("entity_light_geom", TyB::alias("light_geom"))
        .input("ui_geom", TyB::alias("ui_geom"))
        .input("selection_geom", TyB::alias("square01_geom"))

        .output("final", TyB::tex_rgba())

        .local("world", TyB::tex_rgba(), |e| e.init_arg(var("final").field("size")))
        .local("world_depth", TyB::tex_depth(), |e| e.init_arg(var("final").field("size")))
        .local("world_entity_depth", TyB::tex_depth(), |e| e.init_arg(var("final").field("size")))
        .local("shadow", TyB::tex_rgba(), |e| e.init_arg(var("final").field("size")))
        .local("light", TyB::tex_rgba(), |e| e.init_arg(var("final").field("size")))

        .clear(|cs| cs
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .depth(f32_val(0.0))
            .out_color(var("world"))
            .out_depth(var("world_depth"))
        )

        .clear(|cs| cs
            // TODO: asmgl backend expects at least one color output
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .depth(f32_val(0.0))
            .out_color(var("world"))
            .out_depth(var("world_entity_depth"))
        )

        .clear(|cs| cs
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .out_color(var("shadow"))
        )

        .clear(|cs| cs
            .color(var("ambient_light"))
            .out_color(var("light"))
        )

        .call("terrain", |cs| cs
            .input("camera_pos", var("camera_pos"))
            .input("slice_center", var("slice_center"))
            .input("slice_z", var("slice_z"))

            .input("atlas", var("terrain_atlas"))
            .input("cavern_map", var("cavern_map"))

            .input("geom", var("terrain_geom"))

            .output("world", var("world"))
            .output("world_depth", var("world_depth"))
        )

        .call("structures", |cs| cs
            .input("camera_pos", var("camera_pos"))
            .input("slice_center", var("slice_center"))
            .input("slice_z", var("slice_z"))
            .input("anim_now", var("anim_now"))
            .input("clip_enabled", var("clip_enabled"))

            .input("atlas", var("structure_atlas"))
            .input("cavern_map", var("cavern_map"))

            .input("geom", var("structure_geom"))

            .output("world", var("world"))
            .output("world_depth", var("world_depth"))
        )

        .call("structure_shadows", |cs| cs
            .input("camera_pos", var("camera_pos"))
            .input("slice_center", var("slice_center"))
            .input("slice_z", var("slice_z"))
            .input("anim_now", var("anim_now"))

            .input("atlas", var("structure_atlas"))
            .input("cavern_map", var("cavern_map"))

            .input("geom", var("structure_geom"))

            .output("world", var("shadow"))
            .output("world_depth", var("world_depth"))
        )

        .call("blit_full_alpha", |cs| cs
            .input("src", var("shadow"))
            .input("geom", var("square01_geom"))
            .output("dest", var("world"))
        )

        .call("entities", |cs| cs
            .input("camera_pos", var("camera_pos"))
            .input("slice_center", var("slice_center"))
            .input("slice_z", var("slice_z"))
            .input("anim_now", var("anim_now"))

            .input("atlas", var("entity_atlas"))
            .input("world_depth", var("world_depth"))
            .input("cavern_map", var("cavern_map"))
            .input("ui_atlas", var("ui_atlas"))

            .input("geom", var("entity_geom"))

            .output("world", var("world"))
            .output("world_entity_depth", var("world_entity_depth"))
        )

        .call("lights", |cs| cs
            .input("camera_pos", var("camera_pos"))

            .input("world_depth", var("world_depth"))
            .input("world_entity_depth", var("world_entity_depth"))

            .input("geom", var("structure_light_geom"))

            .output("light", var("light"))
        )

        .call("lights", |cs| cs
            .input("camera_pos", var("camera_pos"))

            .input("world_depth", var("world_depth"))
            .input("world_entity_depth", var("world_entity_depth"))

            .input("geom", var("entity_light_geom"))

            .output("light", var("light"))
        )

        .call("postprocess", |cs| cs
            .input("world", var("world"))
            .input("light", var("light"))
            .input("geom", var("square01_geom"))
            .output("final", var("final"))
        )

        .call("selection", |cs| cs
            .input("src", var("scratch"))
            .input("geom", var("selection_geom"))
            .input("src_region", var("selection_src_region"))
            .input("dest_pos", var("selection_dest_pos"))
            .output("dest", var("final"))
        )

        .call("ui", |cs| cs
            .input("atlas", var("ui_atlas"))
            .input("items", var("item_atlas"))
            .input("scratch", var("scratch"))

            .input("geom", var("ui_geom"))

            .output("dest", var("final"))
        )

}

fn pass_output(pb: &mut PassB) -> &mut PassB {
    pb
        .input("final", TyB::tex_rgba())
        .input("geom", TyB::alias("square01_geom"))
        .output("screen", TyB::tex_rgba())

        .render("blit_fullscreen_flip.vert", "blit_output.frag", |rs| rs
            .uniform("image_tex", TyB::tex_rgba(), var("final"))
            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .out_color(var("screen"))
        )
}


fn pass_terrain(pb: &mut PassB) -> &mut PassB {
    pb
        .input("camera_pos", f32().vec(2))
        .input("slice_center", f32().vec(2))
        .input("slice_z", f32())

        .input("atlas", TyB::tex_rgba())
        .input("cavern_map", TyB::tex_rgba())

        .input("geom", TyB::alias("terrain_geom"))

        .output("world", TyB::tex_rgba())
        .output("world_depth", TyB::tex_depth())

        .local("camera_size", f32().vec(2), |e| e
            .init_arg(var("world").field("size"))
        )

        .render("terrain2.vert", "terrain2.frag", |rs| rs
            .uniform("cameraPos", f32().vec(2), var("camera_pos"))
            .uniform("cameraSize", f32().vec(2), var("camera_size"))
            .uniform("sliceCenter", f32().vec(2), var("slice_center"))
            .uniform("sliceZ", f32(), var("slice_z"))
            .uniform("atlasTex", TyB::tex_rgba(), var("atlas"))
            .uniform("cavernTex", TyB::tex_rgba(), var("cavern_map"))

            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .vertex("blockPos", u8().vec(3), var("geom").field("block_pos"))
            .vertex("side", u8(), var("geom").field("side"))
            .vertex("tileCoord", u8().vec(2), var("geom").field("tile_coord"))

            .out_color(var("world"))
            .out_depth(var("world_depth"))

            .depth_test(DepthTest::GEqual)
        )
}

fn pass_structures(pb: &mut PassB) -> &mut PassB {
    pass_structures_common(pb, false)
}

fn pass_structure_shadows(pb: &mut PassB) -> &mut PassB {
    pass_structures_common(pb, true)
}

fn pass_structures_common(pb: &mut PassB, shadow: bool) -> &mut PassB {
    let (vert, frag) =
        if !shadow { ("structure2.vert", "structure2.frag") }
        else { ("structure2_shadow.vert", "structure2_shadow.frag") };

    if !shadow {
        pb.input("clip_enabled", u8());
    }

    pb
        .input("camera_pos", f32().vec(2))
        .input("slice_center", f32().vec(2))
        .input("slice_z", f32())
        .input("anim_now", f32())

        .input("atlas", TyB::tex_rgba())
        .input("cavern_map", TyB::tex_rgba())

        .input("geom", TyB::alias("structure_geom"))

        .output("world", TyB::tex_rgba())
        .output("world_depth", TyB::tex_depth())

        .local("camera_size", f32().vec(2), |e| e
            .init_arg(var("world").field("size"))
        )

        .render(vert, frag, |rs| rs
            .uniform("camera_pos", f32().vec(2), var("camera_pos"))
            .uniform("camera_size", f32().vec(2), var("camera_size"))
            .uniform("sliceCenter", f32().vec(2), var("slice_center"))
            .uniform("sliceZ", f32(), var("slice_z"))
            .uniform("now", f32(), var("anim_now"))
            .uniform("clip_enabled", i32(),
                if !shadow { var("clip_enabled") }
                else { u8_val(0) })
            .uniform("sheet_tex", TyB::tex_rgba(), var("atlas"))
            .uniform("cavernTex", TyB::tex_rgba(), var("cavern_map"))

            .vertex("vert_offset", i16().vec(3), var("geom").field("vert_offset"))
            .vertex("src_pos", i16().vec(2), var("geom").field("src_pos"))
            .vertex("block_pos", u8().vec(3), var("geom").field("block_pos"))
            .vertex("block_size", u8().vec(3), var("geom").field("block_size"))
            .vertex("anim_length", i8(), var("geom").field("anim_length"))
            .vertex("anim_rate", u8(), var("geom").field("anim_rate"))
            .vertex("anim_oneshot_start", u16(), var("geom").field("anim_oneshot_start"))
            .vertex("anim_step", u16(), var("geom").field("anim_step"))

            .out_color(var("world"))
            .out_depth(var("world_depth"))

            .depth_test(DepthTest::Always)
        )
}

fn pass_entities(pb: &mut PassB) -> &mut PassB {
    pb
        .input("camera_pos", f32().vec(2))
        .input("slice_center", f32().vec(2))
        .input("slice_z", f32())
        .input("anim_now", f32())

        .input("atlas", TyB::tex_rgba())
        .input("world_depth", TyB::tex_depth())
        .input("cavern_map", TyB::tex_rgba())
        .input("ui_atlas", TyB::tex_rgba())

        .input("geom", TyB::alias("entity_geom"))

        .output("world", TyB::tex_rgba())
        .output("world_entity_depth", TyB::tex_depth())

        .local("camera_size", f32().vec(2), |e| e
            .init_arg(var("world").field("size"))
        )

        .render("entity2.vert", "entity2.frag", |rs| rs
            .uniform("camera_pos", f32().vec(2), var("camera_pos"))
            .uniform("camera_size", f32().vec(2), var("camera_size"))
            .uniform("sliceCenter", f32().vec(2), var("slice_center"))
            .uniform("sliceZ", f32(), var("slice_z"))
            .uniform("now", f32(), var("anim_now"))

            .uniform("sheet_tex", TyB::tex_rgba(), var("atlas"))
            .uniform("depth_tex", TyB::tex_depth(), var("world_depth"))
            .uniform("cavernTex", TyB::tex_rgba(), var("cavern_map"))
            .uniform("ui_tex", TyB::tex_rgba(), var("ui_atlas"))

            .vertex("dest_pos", u16().vec(2), var("geom").field("dest_pos"))
            .vertex("src_pos", u16().vec(2), var("geom").field("src_pos"))
            .vertex("sheet", i8(), var("geom").field("sheet"))
            // TODO: normalize, and remove adjustment from entity2.vert
            .vertex("color", u8().vec(3), var("geom").field("color"))
            .vertex("ref_pos_size", u16().vec(4), var("geom").field("ref_pos_size"))
            .vertex("anim_info", u16().vec(4), var("geom").field("anim_info"))

            .out_color(var("world"))
            .out_depth(var("world_entity_depth"))

            .depth_test(DepthTest::Always)
        )
}

fn pass_entities_ui(pb: &mut PassB) -> &mut PassB {
    pb
        .input("atlas", TyB::tex_rgba())
        .input("ui_atlas", TyB::tex_rgba())

        .input("geom", TyB::alias("entity_geom"))

        .output("out", TyB::tex_rgba())

        .local("camera_size", f32().vec(2), |e| e
            .init_arg(var("out").field("size"))
        )

        .render("entity2_ui.vert", "entity2_ui.frag", |rs| rs
            .uniform("camera_size", f32().vec(2), var("camera_size"))
            .uniform("now", f32(), f32_val(0.0))

            .uniform("sheet_tex", TyB::tex_rgba(), var("atlas"))
            .uniform("ui_tex", TyB::tex_rgba(), var("ui_atlas"))

            .vertex("dest_pos", u16().vec(2), var("geom").field("dest_pos"))
            .vertex("src_pos", u16().vec(2), var("geom").field("src_pos"))
            .vertex("sheet", i8(), var("geom").field("sheet"))
            // TODO: normalize, and remove adjustment from entity2.vert
            .vertex("color", u8().vec(3), var("geom").field("color"))
            .vertex("ref_pos_size", u16().vec(4), var("geom").field("ref_pos_size"))
            .vertex("anim_info", u16().vec(4), var("geom").field("anim_info"))

            .out_color(var("out"))
        )
}

fn pass_lights(pb: &mut PassB) -> &mut PassB {
    pb
        .input("camera_pos", f32().vec(2))

        .input("world_depth", TyB::tex_depth())
        .input("world_entity_depth", TyB::tex_depth())

        .input("geom", TyB::alias("light_geom"))

        .output("light", TyB::tex_rgba())

        .local("camera_size", f32().vec(2), |e| e
            .init_arg(var("light").field("size"))
        )

        .render("light2.vert", "light2.frag", |rs| rs
            .uniform("cameraPos", f32().vec(2), var("camera_pos"))
            .uniform("cameraSize", f32().vec(2), var("camera_size"))

            .uniform("depthTex", TyB::tex_depth(), var("world_depth"))
            .uniform("entityDepthTex", TyB::tex_depth(), var("world_entity_depth"))

            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .vertex("center", u16().vec(3), var("geom").field("center"))
            // TODO: normalize, and remove adjustment from light2.vert
            .vertex("colorIn", u8().vec(3), var("geom").field("color"))
            .vertex("radiusIn", u16(), var("geom").field("radius"))

            .out_color(var("light"))

            .blend_mode(BlendMode::MultiplyInv)
        )
}

fn pass_selection(pb: &mut PassB) -> &mut PassB {
    pb
        .input("src", TyB::tex_rgba())
        .input("geom", TyB::alias("square01_geom"))
        .input("src_region", i32().vec(4))
        .input("dest_pos", i32().vec(2))
        .output("dest", TyB::tex_rgba())

        .render("blit_region.vert", "blit_output.frag", |rs| rs
            .uniform("image_tex", TyB::tex_rgba(), var("src"))
            .uniform("region_pos", f32().vec(2), var("src_region").field("01"))
            .uniform("region_size", f32().vec(2), var("src_region").field("23"))
            .uniform("tex_size", f32().vec(2), var("src").field("size"))
            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .out_color(var("dest"))
            .out_viewport(ExprB::ctor(i32().vec(4), |e| e
                .init_arg(var("dest_pos"))
                .init_arg(var("src_region").field("23"))
            ))
            .blend_mode(BlendMode::Alpha)
        )
}

fn pass_postprocess(pb: &mut PassB) -> &mut PassB {
    pb
        .input("world", TyB::tex_rgba())
        .input("light", TyB::tex_rgba())

        .input("geom", TyB::alias("square01_geom"))

        .output("final", TyB::tex_rgba())

        .local("camera_size", f32().vec(2), |e| e
            .init_arg(var("final").field("size"))
        )

        .render("blit_fullscreen.vert", "blit_post.frag", |rs| rs
            .uniform("screen_size", f32().vec(2), var("camera_size"))
            .uniform("color_tex", TyB::tex_rgba(), var("world"))
            .uniform("light_tex", TyB::tex_rgba(), var("light"))
            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .out_color(var("final"))
        )
}

fn pass_ui(pb: &mut PassB) -> &mut PassB {
    pb
        .input("atlas", TyB::tex_rgba())
        .input("items", TyB::tex_rgba())
        .input("scratch", TyB::tex_rgba())

        .input("geom", TyB::alias("ui_geom"))

        .output("dest", TyB::tex_rgba())

        .render("ui_blit2.vert", "ui_blit2.frag", |rs| rs
            .uniform("screenSize", f32().vec(2), var("dest").field("size"))
            .uniform("sheetSize0", f32().vec(2), var("items").field("size"))
            .uniform("sheetSize1", f32().vec(2), var("atlas").field("size"))
            .uniform("sheetSize2", f32().vec(2), var("scratch").field("size"))

            .uniform("sheet0", TyB::tex_rgba(), var("items"))
            .uniform("sheet1", TyB::tex_rgba(), var("atlas"))
            .uniform("sheet2", TyB::tex_rgba(), var("scratch"))

            .vertex("srcPos", u16().vec(2), var("geom").field("src_pos"))
            .vertex("srcSize", u8().vec(2), var("geom").field("src_size"))
            .vertex("sheetAttr", u8(), var("geom").field("sheet"))
            .vertex("dest", i16().vec(2), var("geom").field("dest"))
            .vertex("offset_", u16().vec(2), var("geom").field("offset"))

            .out_color(var("dest"))

            .blend_mode(BlendMode::Alpha)
        )

}

fn pass_blit_full(pb: &mut PassB) -> &mut PassB {
    pb
        .input("src", TyB::tex_rgba())
        .input("geom", TyB::alias("square01_geom"))
        .output("dest", TyB::tex_rgba())

        .render("blit_fullscreen.vert", "blit_output.frag", |rs| rs
            .uniform("image_tex", TyB::tex_rgba(), var("src"))
            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .out_color(var("dest"))
        )
}

fn pass_blit_full_alpha(pb: &mut PassB) -> &mut PassB {
    pb
        .input("src", TyB::tex_rgba())
        .input("geom", TyB::alias("square01_geom"))
        .output("dest", TyB::tex_rgba())

        .render("blit_fullscreen.vert", "blit_output.frag", |rs| rs
            .uniform("image_tex", TyB::tex_rgba(), var("src"))
            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .out_color(var("dest"))
            .blend_mode(BlendMode::Alpha)
        )
}

fn pass_blit_outline(pb: &mut PassB) -> &mut PassB {
    pb
        .input("src", TyB::tex_rgba())
        .input("color", f32().vec(4))
        .input("dest_region", i32().vec(4))
        .input("geom", TyB::alias("square01_geom"))
        .output("dest", TyB::tex_rgba())

        .render("blit_fullscreen.vert", "blit_outline.frag", |rs| rs
            .uniform("screen_size", f32().vec(2), var("dest_region").field("23"))
            .uniform("output_color", f32().vec(4), var("color"))
            .uniform("color_tex", TyB::tex_rgba(), var("src"))
            .vertex("corner", u8().vec(2), var("geom").field("corner"))
            .out_color(var("dest"))
            .out_viewport(var("dest_region"))
        )
}


fn pass_special_pony(pb: &mut PassB) -> &mut PassB {
    pb
        .input("out_region", i32().vec(4))

        .input("entity_atlas", TyB::tex_rgba())
        .input("ui_atlas", TyB::tex_rgba())

        .input("entity_geom", TyB::alias("entity_geom"))

        .output("scratch", TyB::tex_rgba())

        .clear(|cs| cs
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .out_color(var("scratch"))
            .out_viewport(var("out_region"))
        )

        .call("entities_ui", |cs| cs
            .input("atlas", var("entity_atlas"))
            .input("ui_atlas", var("ui_atlas"))

            .input("geom", var("entity_geom"))

            .output("out", var("scratch"))
        )
}

fn pass_special_color_plot_xy(pb: &mut PassB) -> &mut PassB {
    pb
        .input("out_region", i32().vec(4))
        .input("target", f32().vec(2))
        .input("cur_color", f32().vec(3))
        .input("square01_geom", TyB::alias("square01_geom"))

        .output("scratch", TyB::tex_rgba())

        .render("blit_fullscreen.vert", "color_plot_xy.frag", |rs| rs
            .uniform("target", f32().vec(2), var("target"))
            .uniform("z", f32(), var("cur_color").field("z"))
            .vertex("corner", u8().vec(2), var("square01_geom").field("corner"))
            .out_color(var("scratch"))
            .out_viewport(var("out_region"))
        )
}

fn pass_special_color_plot_z(pb: &mut PassB) -> &mut PassB {
    pb
        .input("out_region", i32().vec(4))
        .input("target", f32().vec(2))
        .input("cur_color", f32().vec(3))
        .input("square01_geom", TyB::alias("square01_geom"))

        .output("scratch", TyB::tex_rgba())

        .render("blit_fullscreen.vert", "color_plot_z.frag", |rs| rs
            .uniform("target", f32(), var("target").field("y"))
            .uniform("center_x", f32(), var("target").field("x"))
            .uniform("xy", f32().vec(2), var("cur_color").field("xy"))
            .vertex("corner", u8().vec(2), var("square01_geom").field("corner"))
            .out_color(var("scratch"))
            .out_viewport(var("out_region"))
        )
}

fn pass_special_color_slider(pb: &mut PassB) -> &mut PassB {
    pb
        .input("out_region", i32().vec(4))
        .input("target", i32())
        .input("channel", u8())
        .input("cur_color", f32().vec(3))
        .input("square01_geom", TyB::alias("square01_geom"))

        .output("scratch", TyB::tex_rgba())

        .render("blit_fullscreen.vert", "color_slider.frag", |rs| rs
            .uniform("target", f32(), var("target"))
            .uniform("channel", f32(), var("channel"))
            .uniform("color", f32().vec(3), var("cur_color"))
            .vertex("corner", u8().vec(2), var("square01_geom").field("corner"))
            .out_color(var("scratch"))
            .out_viewport(var("out_region"))
        )
}

fn pass_special_selection_cell(pb: &mut PassB) -> &mut PassB {
    let tile_ext = TILE_SIZE + 2;

    pb
        // `out_region` should always be tile_ext x tile_ext in size, but we don't check this
        // currently.
        .input("out_region", i32().vec(4))
        .input("square01_geom", TyB::alias("square01_geom"))
        .output("scratch", TyB::tex_rgba())

        .local("temp", TyB::tex_rgba(), |e| e
            .init_arg(i32_vec(&[tile_ext, tile_ext])))

        .clear(|cs| cs
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .out_color(var("temp"))
        )

        .render("ground_tile.vert", "solid_color.frag", |rs| rs
            .uniform("camera_size", f32().vec(2), var("temp").field("size"))
            // Draw tile at 1x1, so it's centered in the tile_ext x tile_ext buffer.
            .uniform("tile_offset", f32().vec(2), i32_vec(&[1, 1]))
            .uniform("output_color", f32().vec(4), f32_vec(&[0.0, 0.8, 1.0, 1.0]))
            .vertex("corner", u8().vec(2), var("square01_geom").field("corner"))
            .out_color(var("temp"))
        )

        .call("blit_outline", |cs| cs
            .input("src", var("temp"))
            .input("color", f32_vec(&[0.0, 0.8, 1.0, 1.0]))
            .input("geom", var("square01_geom"))
            .input("dest_region", var("out_region"))
            .output("dest", var("scratch"))
        )
}

fn pass_special_selection_structure(pb: &mut PassB) -> &mut PassB {
    pb
        .input("out_region", i32().vec(4))
        .input("anim_now", f32())

        .input("atlas", TyB::tex_rgba())

        .input("structure_geom", TyB::alias("structure_geom"))
        .input("square01_geom", TyB::alias("square01_geom"))

        .output("scratch", TyB::tex_rgba())

        .local("out_size", i32().vec(2), |e| e.init_arg(var("out_region").field("23")))

        .local("temp", TyB::tex_rgba(), |e| e.init_arg(var("out_size")))

        .clear(|cs| cs
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .out_color(var("temp"))
        )

        .render("structure2.vert", "solid_color_masked.frag", |rs| rs
            .uniform("camera_pos", f32().vec(2), f32_vec(&[0., 0.]))
            .uniform("camera_size", f32().vec(2), var("out_size"))
            .uniform("sliceCenter", f32().vec(2), f32_vec(&[0., 0.]))
            .uniform("sliceZ", f32(), f32_val(0.))
            .uniform("now", f32(), var("anim_now"))
            .uniform("output_color", f32().vec(4), f32_vec(&[0.0, 0.8, 1.0, 1.0]))
            .uniform("sheet_tex", TyB::tex_rgba(), var("atlas"))

            .vertex("vert_offset", i16().vec(3), var("structure_geom").field("vert_offset"))
            .vertex("src_pos", i16().vec(2), var("structure_geom").field("src_pos"))
            .vertex("block_pos", u8().vec(3), var("structure_geom").field("block_pos"))
            .vertex("block_size", u8().vec(3), var("structure_geom").field("block_size"))
            .vertex("anim_length", i8(), var("structure_geom").field("anim_length"))
            .vertex("anim_rate", u8(), var("structure_geom").field("anim_rate"))
            .vertex("anim_oneshot_start", u16(), var("structure_geom").field("anim_oneshot_start"))
            .vertex("anim_step", u16(), var("structure_geom").field("anim_step"))

            .out_color(var("temp"))
            .out_viewport(var("out_region"))
        )

        .call("blit_outline", |cs| cs
            .input("src", var("temp"))
            .input("color", f32_vec(&[0.0, 0.8, 1.0, 1.0]))
            .input("geom", var("square01_geom"))
            .input("dest_region", var("out_region"))
            .output("dest", var("scratch"))
        )
}

fn pass_special_selection_entity(pb: &mut PassB) -> &mut PassB {
    pb
        .input("out_region", i32().vec(4))
        .input("anim_now", f32())

        .input("entity_atlas", TyB::tex_rgba())
        .input("ui_atlas", TyB::tex_rgba())

        .input("entity_geom", TyB::alias("entity_geom"))
        .input("square01_geom", TyB::alias("square01_geom"))

        .output("scratch", TyB::tex_rgba())

        .local("out_size", i32().vec(2), |e| e.init_arg(var("out_region").field("23")))

        .local("temp", TyB::tex_rgba(), |e| e.init_arg(var("out_size")))

        .clear(|cs| cs
            .color(f32_vec(&[0.0, 0.0, 0.0, 0.0]))
            .out_color(var("temp"))
        )

        .render("entity2.vert", "solid_color_masked.frag", |rs| rs
            .uniform("camera_pos", f32().vec(2), f32_vec(&[0., 0.]))
            .uniform("camera_size", f32().vec(2), var("out_size"))
            .uniform("now", f32(), var("anim_now"))
            .uniform("output_color", f32().vec(4), f32_vec(&[0.0, 0.8, 1.0, 1.0]))

            .vertex("dest_pos", u16().vec(2), var("entity_geom").field("dest_pos"))
            .vertex("src_pos", u16().vec(2), var("entity_geom").field("src_pos"))
            .vertex("sheet", i8(), var("entity_geom").field("sheet"))
            // TODO: normalize, and remove adjustment from entity2.vert
            .vertex("color", u8().vec(3), var("entity_geom").field("color"))
            .vertex("ref_pos_size", u16().vec(4), var("entity_geom").field("ref_pos_size"))
            .vertex("anim_info", u16().vec(4), var("entity_geom").field("anim_info"))

            .uniform("sheet_tex", TyB::tex_rgba(), var("entity_atlas"))
            .uniform("ui_tex", TyB::tex_rgba(), var("ui_atlas"))

            .out_color(var("temp"))
            .out_viewport(var("out_region"))
        )

        .call("blit_outline", |cs| cs
            .input("src", var("temp"))
            .input("color", f32_vec(&[0.0, 0.8, 1.0, 1.0]))
            .input("geom", var("square01_geom"))
            .input("dest_region", var("out_region"))
            .output("dest", var("scratch"))
        )
}
