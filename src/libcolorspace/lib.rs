#![no_std]

use core::f32::consts::PI;
use core::hash::{Hash, Hasher};
use core::mem;


// These math functions call into libc, so we can't use the handy libstd wrappers for them.

macro_rules! math_funcs {
    ($(fn $name:ident($($arg:ident : $ArgTy:ty),*) -> $RetTy:ty;)*) => {
        $(
            fn $name($($arg: $ArgTy),*) -> $RetTy {
                extern {
                    fn $name($($arg: $ArgTy),*) -> $RetTy;
                }
                unsafe { $name($($arg),*) }
            }
        )*
    };
}

math_funcs! {
    fn powf(b: f32, e: f32) -> f32;
    fn sinf(x: f32) -> f32;
    fn cosf(x: f32) -> f32;
    fn atan2f(y: f32, x: f32) -> f32;
    fn sqrtf(x: f32) -> f32;
}


// All color components are scaled such that reasonable values lie in the range 0..1, or -1..1 for
// components that can be negative.

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Srgb {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct LinearRgb {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Xyz {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Lab {
    pub l: f32,
    pub a: f32,
    pub b: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Luv {
    pub l: f32,
    pub u: f32,
    pub v: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct LchAb {
    pub l: f32,
    pub c: f32,
    pub h: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct LchUv {
    pub l: f32,
    pub c: f32,
    pub h: f32,
}


// `f32` doesn't implement `Hash` or `Eq`, so we have to provide manual impls for each struct.
macro_rules! struct_impls {
    ($($Struct:ident ($($field:ident),*);)*) => {
        $(
            impl Hash for $Struct {
                fn hash<H: Hasher>(&self, state: &mut H) {
                    unsafe {
                        $(mem::transmute::<f32, u32>(self.$field).hash(state);)*
                    }
                }
            }

            impl Eq for $Struct {}
        )*
    };
}

struct_impls! {
    Srgb(r, g, b);
    LinearRgb(r, g, b);
    Xyz(x, y, z);
    Lab(l, a, b);
    Luv(l, u, v);
    LchAb(l, c, h);
    LchUv(l, c, h);
}


// Constants and helper functions

const SRGB_A: f32 = 0.055;

fn srgb_component_to_linear(c: f32) -> f32 {
    if c < 0.04045 {
        c / 12.92
    } else {
        powf((c + SRGB_A) / (1. + SRGB_A), 2.4)
    }
}

fn linear_rgb_component_to_srgb(c: f32) -> f32 {
    if c < 0.003130804 {
        c * 12.92
    } else {
        (1. + SRGB_A) * powf(c, 1. / 2.4) - SRGB_A
    }
}

const LAB_DELTA: f32 = 6. / 29.;
const LAB_DELTA2: f32 = LAB_DELTA * LAB_DELTA;
const LAB_DELTA3: f32 = LAB_DELTA2 * LAB_DELTA;

fn xyz_lab_f(c: f32) -> f32 {
    if c > LAB_DELTA3 {
        powf(c, 1. / 3.)
    } else {
        (c / (3. * LAB_DELTA2)) + (4. / 29.)
    }
}

fn lab_xyz_f_inv(c: f32) -> f32 {
    if c > LAB_DELTA {
        powf(c, 3.)
    } else {
        3. * LAB_DELTA2 * (c - 4. / 29.)
    }
}

const LAB_XN: f32 = 0.95047;
const LAB_YN: f32 = 1.00000;
const LAB_ZN: f32 = 1.08883;

// These are u'_n, v'_n for CIE Illuminant D65, which has (x,y) = (0.3128, 0.3290) (Note that these
// are lowercase x,y, not the uppercase X,Y used in XYZ.)
const LUV_U_PRIME_N: f32 = 0.1979;
const LUV_V_PRIME_N: f32 = 0.4683;


// Inherent impls

impl Srgb {
    pub fn to_linear_rgb(self) -> LinearRgb {
        LinearRgb {
            r: srgb_component_to_linear(self.r),
            g: srgb_component_to_linear(self.g),
            b: srgb_component_to_linear(self.b),
        }
    }

    pub fn to_xyz(self) -> Xyz {
        self.to_linear_rgb().to_xyz()
    }
}

impl LinearRgb {
    pub fn to_srgb(self) -> Srgb {
        Srgb {
            r: linear_rgb_component_to_srgb(self.r),
            g: linear_rgb_component_to_srgb(self.g),
            b: linear_rgb_component_to_srgb(self.b),
        }
    }

    pub fn to_xyz(self) -> Xyz {
        Xyz {
            x:  0.4124 * self.r +  0.3576 * self.g +  0.1805 * self.b,
            y:  0.2126 * self.r +  0.7152 * self.g +  0.0722 * self.b,
            z:  0.0193 * self.r +  0.1192 * self.g +  0.9505 * self.b,
        }
    }
}


impl Xyz {
    pub fn to_linear_rgb(self) -> LinearRgb {
        LinearRgb {
            r:  3.24062548 * self.x + -1.53720797 * self.y + -0.49862860 * self.z,
            g: -0.96893071 * self.x +  1.87575606 * self.y +  0.04151752 * self.z,
            b:  0.05571012 * self.x + -0.20402105 * self.y +  1.05699594 * self.z,
        }
    }

    pub fn to_srgb(self) -> Srgb {
        self.to_linear_rgb().to_srgb()
    }

    pub fn to_lab(self) -> Lab {
        let fx = xyz_lab_f(self.x / LAB_XN);
        let fy = xyz_lab_f(self.y / LAB_YN);
        let fz = xyz_lab_f(self.z / LAB_ZN);

        Lab {
            l: 1.16 * fy - 0.16,
            a: 5.00 * (fx - fy),
            b: 2.00 * (fy - fz),
        }
    }

    pub fn to_lch_ab(self) -> LchAb {
        self.to_lab().to_lch_ab()
    }

    pub fn to_luv(self) -> Luv {
        let denom = self.x + 15. * self.y + 3. * self.z;
        let u_prime = (4. * self.x) / denom; 
        let v_prime = (9. * self.y) / denom; 

        let l_star = 
            if self.y <= powf(6. / 29., 3.) {
                self.y * powf(29. / 3., 3.) / 100.
            } else {
                1.16 * powf(self.y, 1. / 3.) - 0.16
            };

        // The formulas on wikipedia mention a Yn, but it's exactly 1, so it's omitted here.
        Luv {
            l: l_star,
            u: 13. * l_star * (u_prime - LUV_U_PRIME_N),
            v: 13. * l_star * (v_prime - LUV_V_PRIME_N),
        }
    }

    pub fn to_lch_uv(self) -> LchUv {
        self.to_luv().to_lch_uv()
    }
}


impl Lab {
    pub fn to_xyz(self) -> Xyz {
        let l_adj = (self.l + 0.16) / 1.16;
        Xyz {
            x: LAB_XN * lab_xyz_f_inv(l_adj + self.a / 5.00),
            y: LAB_YN * lab_xyz_f_inv(l_adj),
            z: LAB_ZN * lab_xyz_f_inv(l_adj - self.b / 2.00),
        }
    }

    pub fn to_lch_ab(self) -> LchAb {
        let theta = atan2f(self.b, self.a);
        let theta = if theta < 0. { theta + 2. * PI } else { theta };

        LchAb {
            l: self.l,
            c: sqrtf(self.a * self.a + self.b * self.b),
            h: theta / (2. * PI),
        }
    }
}

impl LchAb {
    pub fn to_lab(self) -> Lab {
        let theta = self.h * 2. * PI;
        Lab {
            l: self.l,
            a: cosf(theta) * self.c,
            b: sinf(theta) * self.c,
        }
    }

    pub fn to_xyz(self) -> Xyz {
        self.to_lab().to_xyz()
    }
}


impl Luv {
    pub fn to_xyz(self) -> Xyz {
        let u_prime = self.u / (13. * self.l) + LUV_U_PRIME_N;
        let v_prime = self.v / (13. * self.l) + LUV_V_PRIME_N;

        let y =
            if self.l <= 0.08 {
                self.l * powf(3. / 29., 3.) * 100.
            } else {
                powf((self.l + 0.16) / 1.16, 3.)
            };

        Xyz{ 
            x: y * (9. * u_prime) / (4. * v_prime),
            y: y,
            z: y * (12. - 3. * u_prime - 20. * v_prime) / (4. * v_prime),
        }
    }

    pub fn to_lch_uv(self) -> LchUv {
        let theta = atan2f(self.v, self.u);
        let theta = if theta < 0. { theta + 2. * PI } else { theta };

        LchUv {
            l: self.l,
            c: sqrtf(self.u * self.u + self.v * self.v),
            h: theta / (2. * PI),
        }
    }
}

impl LchUv {
    pub fn to_luv(self) -> Luv {
        let theta = self.h * 2. * PI;
        Luv {
            l: self.l,
            u: cosf(theta) * self.c,
            v: sinf(theta) * self.c,
        }
    }

    pub fn to_xyz(self) -> Xyz {
        self.to_luv().to_xyz()
    }
}
