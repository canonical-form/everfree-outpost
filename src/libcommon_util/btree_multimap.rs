use std::collections::btree_map::{self, BTreeMap};
use std::collections::btree_set::BTreeSet;
use std::collections::Bound::Included;
use std::iter;
use common_types::*;
use syntax_exts::for_each_obj_named;


pub trait Bounded {
    fn min_bound() -> Self;
    fn max_bound() -> Self;
}

for_each_obj_named! {
    { 
        i8; i16; i32; i64; i128; isize;
        u8; u16; u32; u64; u128; usize;
    }
    impl Bounded for $obj {
        fn min_bound() -> Self { $obj::min_value() }
        fn max_bound() -> Self { $obj::max_value() }
    }
}

for_each_obj_named! {
    { client; entity; inventory; plane; terrain_chunk; structure; }
    impl Bounded for $ObjId {
        fn min_bound() -> Self { $ObjId(Bounded::min_bound()) }
        fn max_bound() -> Self { $ObjId(Bounded::max_bound()) }
    }
}

macro_rules! impl_bounded_for_tuple {
    ($($A:ident),*) => {
        impl<$($A: Bounded,)*> Bounded for ($($A,)*) {
            fn min_bound() -> Self { ($(<$A as Bounded>::min_bound(),)*) }
            fn max_bound() -> Self { ($(<$A as Bounded>::max_bound(),)*) }
        }
    };
}

impl_bounded_for_tuple!();
impl_bounded_for_tuple!(A);
impl_bounded_for_tuple!(A, B);
impl_bounded_for_tuple!(A, B, C);
impl_bounded_for_tuple!(A, B, C, D);
impl_bounded_for_tuple!(A, B, C, D, E);
impl_bounded_for_tuple!(A, B, C, D, E, F);
impl_bounded_for_tuple!(A, B, C, D, E, F, G);
impl_bounded_for_tuple!(A, B, C, D, E, F, G, H);
impl_bounded_for_tuple!(A, B, C, D, E, F, G, H, I);
impl_bounded_for_tuple!(A, B, C, D, E, F, G, H, I, J);


/// A B-tree-based multimap, where each key can be associated with multiple values.
#[derive(Clone, Debug)]
pub struct BTreeMultiMap<K, V> {
    map: BTreeMap<(K, V), usize>,
    total_len: usize,
}

#[allow(dead_code)]
impl<K: Ord+Clone, V: Ord+Bounded> BTreeMultiMap<K, V> {
    pub fn new() -> BTreeMultiMap<K, V> {
        BTreeMultiMap {
            map: BTreeMap::new(),
            total_len: 0,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    pub fn len(&self) -> usize {
        self.total_len
    }

    pub fn clear(&mut self) {
        self.map.clear();
        self.total_len = 0;
    }

    /// Iterate over the key of each distinct key-value pair in the map.
    pub fn keys<'a>(&'a self) -> impl Iterator<Item=&'a K>+'a {
        self.map.keys().map(|&(ref k, _)| k)
    }

    /// Iterate over the value of each distinct key-value pair in the map.
    pub fn values<'a>(&'a self) -> impl Iterator<Item=&'a V>+'a {
        self.map.keys().map(|&(_, ref v)| v)
    }

    /// Iterate over the distinct key-value pairs in the map.
    pub fn iter<'a>(&'a self) -> impl Iterator<Item=(&'a K, &'a V)>+'a {
        self.map.keys().map(|&(ref k, ref v)| (k, v))
    }

    /// Insert a key-value pair into this map.  Returns `true` if this was the first insertion of
    /// this pair (that is, if this insertion increased the multiplicity from zero).
    pub fn insert(&mut self, key: K, value: V) -> bool {
        self.total_len += 1;
        match self.map.entry((key, value)) {
            btree_map::Entry::Vacant(e) => {
                e.insert(1);
                true
            },
            btree_map::Entry::Occupied(e) => {
                *e.into_mut() += 1;
                false
            },
        }
    }

    /// Remove a key-value pair from this map.  Returns `true` if this was the last removal of this
    /// pair (that is, if this removal reduced the multiplicity to zero).
    pub fn remove(&mut self, key: K, value: V) -> bool {
        match self.map.entry((key, value)) {
            btree_map::Entry::Vacant(_) => {
                false
            },
            btree_map::Entry::Occupied(e) => {
                self.total_len -= 1;
                if *e.get() == 1 {
                    e.remove();
                    true
                } else {
                    *e.into_mut() -= 1;
                    false
                }
            },
        }
    }

    /// Iterate over each value associated with `key`.  If the map contains multiple copies of the
    /// same key-value pair, the value will appear multiple times in the output.
    pub fn get<'a>(&'a self, key: K) -> impl Iterator<Item=&'a V> + 'a {
        self.map.range((Included(&(key.clone(), V::min_bound())),
                        Included(&(key, V::max_bound()))))
            .flat_map(|(&(_, ref v), &n)| iter::repeat(v).take(n))
    }

    /// Iterate over each distinct value associated with `key`.  Visits each value once, regardless
    /// of its multiplicity in the map.
    pub fn get_distinct<'a>(&'a self, key: K) -> impl Iterator<Item=&'a V> + 'a {
        self.map.range((Included(&(key.clone(), V::min_bound())),
                        Included(&(key, V::max_bound()))))
            .map(|(&(_, ref v), _)| v)
    }

    pub fn contains(&self, key: K, value: V) -> bool {
        self.map.contains_key(&(key, value))
    }
}


/// Similar to `BTreeMultiMap`, except the values associated with a given key are distinct.  That
/// means inserting the same `(k, v)` twice has the same effect as inserting it only once.  This
/// change allows for a more memory-efficient representation.
#[derive(Clone, Debug)]
pub struct BTreeMultiMap1<K, V> {
    set: BTreeSet<(K, V)>,
}

#[allow(dead_code)]
impl<K: Ord+Clone, V: Ord+Bounded> BTreeMultiMap1<K, V> {
    pub fn new() -> BTreeMultiMap1<K, V> {
        BTreeMultiMap1 {
            set: BTreeSet::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.set.is_empty()
    }

    pub fn len(&self) -> usize {
        self.set.len()
    }

    pub fn clear(&mut self) {
        self.set.clear();
    }

    /// Iterate over the key of each distinct key-value pair in the map.
    pub fn keys<'a>(&'a self) -> impl Iterator<Item=&'a K>+'a {
        self.set.iter().map(|&(ref k, _)| k)
    }

    /// Iterate over the value of each distinct key-value pair in the map.
    pub fn values<'a>(&'a self) -> impl Iterator<Item=&'a V>+'a {
        self.set.iter().map(|&(_, ref v)| v)
    }

    /// Iterate over the distinct key-value pairs in the map.
    pub fn iter<'a>(&'a self) -> impl Iterator<Item=(&'a K, &'a V)>+'a {
        self.set.iter().map(|&(ref k, ref v)| (k, v))
    }

    /// Insert a key-value pair into this map.  Returns `true` if the pair was not yet present in
    /// the map.
    pub fn insert(&mut self, key: K, value: V) -> bool {
        self.set.insert((key, value))
    }

    /// Remove a key-value pair from this map.  Returns `true` if the pair was present in the map.
    pub fn remove(&mut self, key: K, value: V) -> bool {
        self.set.remove(&(key, value))
    }

    /// Iterate over the values associated with `key`.
    pub fn get<'a>(&'a self, key: K) -> impl Iterator<Item=&'a V> + 'a {
        self.set.range((Included(&(key.clone(), V::min_bound())),
                        Included(&(key, V::max_bound()))))
            .map(|&(_, ref v)| v)
    }

    pub fn contains(&self, key: K, value: V) -> bool {
        self.set.contains(&(key, value))
    }
}
