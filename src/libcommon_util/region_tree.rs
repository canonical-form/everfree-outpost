use std::cmp;
use std::fmt::Debug;
use std::i32;
use std::iter;
use std::mem;

use common_types::{V2, Region};

pub struct RegionTree<V> {
    root: Box<Node<V>>,
}

impl<V: Clone> RegionTree<V> {
    pub fn new() -> RegionTree<V> {
        RegionTree {
            root: Box::new(Node::Inner {
                level: 32,
                base: i32::MIN.into(),
                children: [None, None, None, None],
            }),
        }
    }

    pub fn insert(&mut self, region: Region<V2>, value: V) {
        Node::insert_inner(&mut self.root, region, value);
    }

    pub fn remove(&mut self, region: Region<V2>) {
        Node::remove_inner(&mut self.root, region);
    }

    pub fn get(&self, p: V2) -> Option<&V> {
        Node::get_no_opt(&self.root, p)
    }

    pub fn iter(&self) -> Iter<V> {
        Iter {
            c: Cursor::new(&self.root),
            next_quad: 0,
        }
    }

    pub fn iter_region(&self, bounds: Region<V2>) -> RegionIter<V> {
        RegionIter {
            c: Cursor::new(&self.root),
            bounds: bounds,
            next_quad: 0,
        }
    }

    #[allow(dead_code)]
    fn dump(&self)
            where V: Debug {
        Node::dump_no_opt(&self.root, 0);
    }
}


enum Node<V> {
    Inner { level: u8, base: V2, children: [Option<Box<Node<V>>>; 4] },
    Leaf { level: u8, bounds: Region<V2>, value: V },
}

impl<V: Clone> Node<V> {
    fn level(&self) -> u8 {
        match *self {
            Node::Inner { level, .. } => level,
            Node::Leaf { level, .. } => level,
        }
    }

    fn size(&self) -> i32 {
        1 << self.level()
    }

    fn base(&self) -> V2 {
        match *self {
            Node::Inner { base, .. } => base,
            Node::Leaf { level, bounds, .. } => bounds.min & (!((1 << level) - 1)),
        }
    }

    fn bounds(&self) -> Region<V2> {
        Region::sized(self.size()) + self.base()
    }

    fn inner_bounds(&self) -> Region<V2> {
        match *self {
            Node::Inner { .. } => self.bounds(),
            Node::Leaf { bounds, .. } => bounds,
        }
    }


    fn leaf(region: Region<V2>, value: V) -> Node<V> {
        Node::Leaf {
            level: region_level(region),
            bounds: region,
            value: value,
        }
    }

    fn insert(node: &mut Option<Box<Node<V>>>, region: Region<V2>, value: V) {
        debug_assert!(region.size().x > 0 &&
                      region.size().y > 0);
        if node.is_none() {
            *node = Some(Box::new(Node::leaf(region, value)));
            return;
        }

        // Keep the `Box<_>` part so we can move the entire node by pointer if we have to.
        let node: &mut Box<Node<V>> = node.as_mut().unwrap();

        if region.contains_region(node.inner_bounds()) {
            // The entire current node is covered by the new region.  Replace the whole thing.
            *node = Box::new(Node::leaf(region, value));
            return;
        }

        if !node.bounds().contains_region(region) {
            // Create an intermediate node to contain the current node and the new leaf.
            let joined = node.bounds().join(region);
            let level = region_level(joined);
            debug_assert!(level > node.level());
            let base = joined.min & (!((1 << level) - 1));

            let old_node: Box<Node<V>> = mem::replace(node, Box::new(Node::Inner {
                level: level,
                base: base,
                children: [None, None, None, None],
            }));

            let q = quadrant_index(old_node.base(), level);
            match **node {
                Node::Inner { ref mut children, .. } => children[q] = Some(old_node),
                _ => unreachable!(),
            }

            // Now continue with the insertion
        }
        debug_assert!(node.bounds().contains_region(region));

        Node::promote_to_inner(node);
        Node::insert_inner(node, region, value);
    }

    fn insert_inner(node: &mut Node<V>, region: Region<V2>, value: V) {
        let (level, base, children) = match *node {
            Node::Inner { level, base, ref mut children } => (level, base, children),
            _ => panic!("bad node type - expected Inner"),
        };

        debug_assert!(level > 0);
        // Explicitly use wrapping add here.  When `base` is i32::MIN and `level` is 32, we want
        // `mid` to overflow to zero.
        let step = 1 << (level - 1);
        let mid = V2::new(base.x.wrapping_add(step),
                          base.y.wrapping_add(step));

        let Region { min, max } = region;
        if min.y < mid.y {
            if min.x < mid.x {
                let region = Region::new(V2::new(min.x,
                                                 min.y),
                                         V2::new(cmp::min(max.x, mid.x),
                                                 cmp::min(max.y, mid.y)));
                Node::insert(&mut children[0], region, value.clone());
            }
            if max.x > mid.x {
                let region = Region::new(V2::new(cmp::max(min.x, mid.x),
                                                 min.y),
                                         V2::new(max.x,
                                                 cmp::min(max.y, mid.y)));
                Node::insert(&mut children[1], region, value.clone());
            }
        }
        if max.y > mid.y {
            if min.x < mid.x {
                let region = Region::new(V2::new(min.x,
                                                 cmp::max(min.y, mid.y)),
                                         V2::new(cmp::min(max.x, mid.x),
                                                 max.y));
                Node::insert(&mut children[2], region, value.clone());
            }
            if max.x > mid.x {
                let region = Region::new(V2::new(cmp::max(min.x, mid.x),
                                                 cmp::max(min.y, mid.y)),
                                         V2::new(max.x,
                                                 max.y));
                Node::insert(&mut children[3], region, value.clone());
            }
        }
    }

    fn promote_to_inner(node: &mut Node<V>) {
        let base = node.base();
        let (level, bounds, value) = match *node {
            Node::Leaf { level, bounds, ref value } => (level, bounds, value.clone()),
            _ => return,
        };

        *node = Node::Inner {
            level: level,
            base: base,
            children: [None, None, None, None],
        };
        Node::insert_inner(node, bounds, value)
    }


    fn remove(node: &mut Option<Box<Node<V>>>, region: Region<V2>) {
        debug_assert!(region.size().x > 0 &&
                      region.size().y > 0);
        if node.is_none() {
            // Nothing to remove.
            return;
        }

        if region.contains_region(node.as_ref().unwrap().inner_bounds()) {
            // The entire current node is covered by the new region.  Replace the whole thing.
            *node = None;
            return;
        }

        // Keep the `Box<_>` part so we can move the entire node by pointer if we have to.
        let node: &mut Box<Node<V>> = node.as_mut().unwrap();

        // Clip the removal region to the node region, since anything outside that is already gone.
        let region = region.intersect(node.bounds());
        debug_assert!(node.bounds().contains_region(region));

        // At this point we know only part of the node is covered.  If it's a Leaf, we need to
        // split it up so we can erase only the parts under `region`.
        Node::promote_to_inner(node);
        Node::remove_inner(node, region);
        Node::collapse_inner(node);
    }

    fn remove_inner(node: &mut Node<V>, region: Region<V2>) {
        let (level, base, children) = match *node {
            Node::Inner { level, base, ref mut children } => (level, base, children),
            _ => panic!("bad node type - expected Inner"),
        };

        debug_assert!(level > 0);
        // Explicitly use wrapping add here.  When `base` is i32::MIN and `level` is 32, we want
        // `mid` to overflow to zero.
        let step = 1 << (level - 1);
        let mid = V2::new(base.x.wrapping_add(step),
                          base.y.wrapping_add(step));

        let Region { min, max } = region;
        if min.y < mid.y {
            if min.x < mid.x {
                let region = Region::new(V2::new(min.x,
                                                 min.y),
                                         V2::new(cmp::min(max.x, mid.x),
                                                 cmp::min(max.y, mid.y)));
                Node::remove(&mut children[0], region);
            }
            if max.x > mid.x {
                let region = Region::new(V2::new(cmp::max(min.x, mid.x),
                                                 min.y),
                                         V2::new(max.x,
                                                 cmp::min(max.y, mid.y)));
                Node::remove(&mut children[1], region);
            }
        }
        if max.y > mid.y {
            if min.x < mid.x {
                let region = Region::new(V2::new(min.x,
                                                 cmp::max(min.y, mid.y)),
                                         V2::new(cmp::min(max.x, mid.x),
                                                 max.y));
                Node::remove(&mut children[2], region);
            }
            if max.x > mid.x {
                let region = Region::new(V2::new(cmp::max(min.x, mid.x),
                                                 cmp::max(min.y, mid.y)),
                                         V2::new(max.x,
                                                 max.y));
                Node::remove(&mut children[3], region);
            }
        }
    }

    /// If `node` is an Inner node with only one child, replace it with the child.
    fn collapse_inner(node: &mut Box<Node<V>>) {
        let opt_child = match **node {
            Node::Inner { ref mut children, .. } => {
                let num_nonempty = children.iter().filter(|c| c.is_some()).count();
                if num_nonempty == 1 {
                    let mut child = None;
                    for c in children {
                        if c.is_some() {
                            child = c.take();
                            break;
                        }
                    }
                    child
                } else {
                    None
                }

            },
            _ => panic!("bad node type - expected Inner"),
        };

        if let Some(child) = opt_child {
            *node = child;
        }
    }


    fn get(node: &Option<Box<Node<V>>>, p: V2) -> Option<&V> {
        match *node {
            Some(ref node) => Node::get_no_opt(node, p),
            None => None,
        }
    }

    fn get_no_opt(node: &Node<V>, p: V2) -> Option<&V> {
        match *node {
            Node::Inner { level, base, ref children } => {
                let qx = p.x.wrapping_sub(base.x) as u32 >> (level - 1);
                let qy = p.y.wrapping_sub(base.y) as u32 >> (level - 1);
                if qx >= 2 || qy >= 2 {
                    return None;
                }
                let q = (qy << 1) | qx;
                Node::get(&children[q as usize], p)
            },
            Node::Leaf { bounds, ref value, .. } => {
                if bounds.contains(p) {
                    Some(value)
                } else {
                    None
                }
            },
        }
    }

    #[allow(dead_code)]
    fn dump(node: &Option<Box<Node<V>>>, depth: usize)
            where V: Debug {
        let prefix = iter::repeat(' ').take(depth * 2).collect::<String>();
        match *node {
            Some(ref node) => Node::dump_no_opt(node, depth),
            None => println!("{}nil", prefix),
        }
    }

    #[allow(dead_code)]
    fn dump_no_opt(node: &Node<V>, depth: usize)
            where V: Debug {
        let prefix = iter::repeat(' ').take(depth * 2).collect::<String>();
        let node_bounds = 
            if node.level() < 32 { node.bounds() }
            else { Region::empty() };
        match *node {
            Node::Inner { level, base, ref children } => {
                println!("{}inner @{}: base={:?}, bounds={:?}..{:?}",
                         prefix, level, base, node_bounds.min, node_bounds.max);
                for c in children {
                    Node::dump(c, depth + 1);
                }
            },
            Node::Leaf { level, bounds, ref value } => {
                println!("{}leaf @{}: bounds={:?}..{:?}, inner={:?}..{:?}, value={:?}",
                         prefix, level, node_bounds.min, node_bounds.max,
                         bounds.min, bounds.max, value);
            },
        }
    }
}

fn region_level(region: Region<V2>) -> u8 {
    let x_diff = region.min.x ^ (region.max.x - 1);
    let y_diff = region.min.y ^ (region.max.y - 1);
    32 - (x_diff | y_diff).leading_zeros() as u8
}

fn quadrant_index(p: V2, level: u8) -> usize {
    debug_assert!(level > 0);
    let x = (p.x >> (level - 1)) & 1;
    let y = (p.y >> (level - 1)) & 1;
    ((y << 1) | x) as usize
}



struct Cursor<'a, V: 'a> {
    stack: Vec<&'a Node<V>>,
    steps: Vec<u8>,
}

impl<'a, V: Clone> Cursor<'a, V> {
    fn new(root: &'a Node<V>) -> Cursor<'a, V> {
        let mut stack = Vec::new();
        stack.push(root);

        Cursor {
            stack: stack,
            steps: Vec::new(),
        }
    }

    fn get(&self) -> &'a Node<V> {
        *self.stack.last().unwrap()
    }

    fn up(&mut self) -> Option<u8> {
        self.stack.pop();
        self.steps.pop()
    }

    /// Try to descend into the `quad` quadrant of the current node.  Returns `false` on failure
    /// (when the current node has no child in that position).
    fn down(&mut self, quad: u8) -> bool {
        let n = match *self.get() {
            Node::Inner { ref children, .. } =>
                match children[quad as usize] {
                    Some(ref n) => n,
                    None => return false,
                },
            _ => return false,
        };

        self.stack.push(n);
        self.steps.push(quad);
        true
    }

    fn down_bounded(&mut self, quad: u8, bounds: Region<V2>) -> bool {
        if !self.down(quad) {
            return false;
        }
        if !self.get().bounds().overlaps(bounds) {
            self.up();
            return false;
        }
        true
    }

    fn find_next_leaf(&mut self, mut quad: u8) -> bool {
        'top: loop {
            if self.empty() {
                return false;
            }

            match *self.get() {
                Node::Leaf { .. } => return true,
                _ => {},
            }

            while quad < 4 {
                if self.down(quad) {
                    // Successfully descended.  Begin searching from quadrant zero.
                    quad = 0;
                    continue 'top;
                } else {
                    quad += 1;
                }
            }

            // Done checking all quadrants here.  Proceed to the next quadrant of the level above.
            quad = self.up().unwrap_or(0) + 1;
        }
    }

    fn find_next_leaf_bounded(&mut self, mut quad: u8, bounds: Region<V2>) -> bool {
        'top: loop {
            if self.empty() {
                return false;
            }

            match *self.get() {
                Node::Leaf { bounds: leaf_bounds, .. } => {
                    if leaf_bounds.overlaps(bounds) {
                        return true;
                    } else {
                        quad = self.up().unwrap_or(0) + 1;
                        continue 'top;
                    }
                },
                _ => {},
            }

            while quad < 4 {
                if self.down_bounded(quad, bounds) {
                    // Successfully descended.  Begin searching from quadrant zero.
                    quad = 0;
                    continue 'top;
                } else {
                    quad += 1;
                }
            }

            // Done checking all quadrants here.  Proceed to the next quadrant of the level above.
            quad = self.up().unwrap_or(0) + 1;
        }
    }

    fn empty(&self) -> bool {
        self.stack.len() == 0
    }
}

pub struct Iter<'a, V: 'a> {
    c: Cursor<'a, V>,
    next_quad: u8,
}

impl<'a, V: Clone> Iterator for Iter<'a, V> {
    type Item = (Region<V2>, &'a V);

    fn next(&mut self) -> Option<(Region<V2>, &'a V)> {
        if !self.c.find_next_leaf(self.next_quad) {
            return None;
        }

        let result = match *self.c.get() {
            Node::Leaf { bounds, ref value, .. } => (bounds, value),
            _ => unreachable!(),
        };

        self.next_quad = self.c.up().unwrap_or(0) + 1;

        Some(result)
    }
}

pub struct RegionIter<'a, V: 'a> {
    c: Cursor<'a, V>,
    bounds: Region<V2>,
    next_quad: u8,
}

impl<'a, V: Clone> Iterator for RegionIter<'a, V> {
    type Item = (Region<V2>, &'a V);

    fn next(&mut self) -> Option<(Region<V2>, &'a V)> {
        if !self.c.find_next_leaf_bounded(self.next_quad, self.bounds) {
            return None;
        }

        let result = match *self.c.get() {
            Node::Leaf { bounds, ref value, .. } => (bounds.intersect(self.bounds), value),
            _ => unreachable!(),
        };

        self.next_quad = self.c.up().unwrap_or(0) + 1;

        Some(result)
    }
}



#[test]
fn test_insert_1x1() {
    let mut node = None;
    Node::insert(&mut node, Region::new(V2::new(0, 0), V2::new(1, 1)), 1234);
    assert_eq!(Node::get(&node, V2::new(0, 0)), Some(&1234));
    assert_eq!(Node::get(&node, V2::new(1, 0)), None);
    assert_eq!(Node::get(&node, V2::new(-1, 0)), None);
    assert_eq!(Node::get(&node, V2::new(0, 1)), None);
    assert_eq!(Node::get(&node, V2::new(0, -1)), None);
}

#[test]
fn test_insert_1x1_multi() {
    let mut node = None;

    for &y in &[0, 7] {
        for &x in &[0, 7] {
            let min = V2::new(x, y);
            let max = min + 1;
            Node::insert(&mut node, Region::new(min, max), x * 10 + y);
        }
    }

    for &y in &[0, 7] {
        for &x in &[0, 7] {
            assert_eq!(Node::get(&node, V2::new(x, y)), Some(&(x * 10 + y)));
        }
    }

    for &y in &[1, 6] {
        for &x in &[1, 6] {
            assert_eq!(Node::get(&node, V2::new(x, y)), None);
        }
    }
}

#[test]
fn test_insert_1x1_overlap() {
    let mut node = None;
    Node::insert(&mut node, Region::new(V2::new(0, 0), V2::new(1, 1)), 1234);
    Node::insert(&mut node, Region::new(V2::new(1, 1), V2::new(2, 2)), 1234);

    Node::insert(&mut node, Region::new(V2::new(0, 0), V2::new(1, 1)), 5678);

    assert_eq!(Node::get(&node, V2::new(0, 0)), Some(&5678));
    assert_eq!(Node::get(&node, V2::new(1, 1)), Some(&1234));
}

#[test]
fn test_insert_region() {
    let mut node = None;
    let r = Region::new(V2::new(1, 1), V2::new(5, 4));
    Node::insert(&mut node, r, 1234);

    for p in Region::sized(10).points() {
        if r.contains(p) {
            assert_eq!(Node::get(&node, p), Some(&1234));
        } else {
            assert_eq!(Node::get(&node, p), None);
        }
    }
}

#[test]
fn test_insert_region_multi() {
    let mut node = None;
    let r1 = Region::new(V2::new(0, 0), V2::new(7, 3));
    Node::insert(&mut node, r1, 1);
    let r2 = Region::new(V2::new(7, 1), V2::new(8, 8));
    Node::insert(&mut node, r2, 2);

    for p in Region::sized(10).points() {
        if r1.contains(p) {
            assert_eq!(Node::get(&node, p), Some(&1));
        } else if r2.contains(p) {
            assert_eq!(Node::get(&node, p), Some(&2));
        } else {
            assert_eq!(Node::get(&node, p), None);
        }
    }
}

#[test]
fn test_insert_region_overlap() {
    let mut node = None;
    let r1 = Region::new(V2::new(0, 3), V2::new(8, 5));
    Node::insert(&mut node, r1, 1);
    let r2 = Region::new(V2::new(3, 0), V2::new(5, 8));
    Node::insert(&mut node, r2, 2);

    for p in Region::sized(10).points() {
        // Check r2 first, since it was drawn over r1
        if r2.contains(p) {
            assert_eq!(Node::get(&node, p), Some(&2));
        } else if r1.contains(p) {
            assert_eq!(Node::get(&node, p), Some(&1));
        } else {
            assert_eq!(Node::get(&node, p), None);
        }
    }
}



#[test]
fn test_remove_1x1() {
    let mut node = None;
    Node::insert(&mut node, Region::sized(10), 1234);
    Node::remove(&mut node, Region::new(V2::new(0, 0), V2::new(1, 1)));
    assert_eq!(Node::get(&node, V2::new(0, 0)), None);
    assert_eq!(Node::get(&node, V2::new(1, 0)), Some(&1234));
    assert_eq!(Node::get(&node, V2::new(1, 1)), Some(&1234));
    assert_eq!(Node::get(&node, V2::new(0, 1)), Some(&1234));
}

#[test]
fn test_remove_1x1_multi() {
    let mut node = None;
    Node::insert(&mut node, Region::sized(10), 1234);

    for &y in &[0, 7] {
        for &x in &[0, 7] {
            let min = V2::new(x, y);
            let max = min + 1;
            Node::remove(&mut node, Region::new(min, max));
        }
    }

    for &y in &[0, 7] {
        for &x in &[0, 7] {
            assert_eq!(Node::get(&node, V2::new(x, y)), None);
        }
    }

    for &y in &[1, 6] {
        for &x in &[1, 6] {
            assert_eq!(Node::get(&node, V2::new(x, y)), Some(&1234));
        }
    }
}

#[test]
fn test_remove_region() {
    let mut node = None;
    Node::insert(&mut node, Region::sized(10), 1234);

    let r = Region::new(V2::new(1, 1), V2::new(5, 4));
    Node::remove(&mut node, r);

    for p in Region::sized(10).points() {
        if r.contains(p) {
            assert_eq!(Node::get(&node, p), None);
        } else {
            assert_eq!(Node::get(&node, p), Some(&1234));
        }
    }
}

#[test]
fn test_remove_region_multi() {
    let mut node = None;
    Node::insert(&mut node, Region::sized(10), 1234);
    let r1 = Region::new(V2::new(0, 0), V2::new(7, 3));
    Node::remove(&mut node, r1);
    let r2 = Region::new(V2::new(7, 1), V2::new(8, 8));
    Node::remove(&mut node, r2);

    for p in Region::sized(10).points() {
        if r1.contains(p) || r2.contains(p) {
            assert_eq!(Node::get(&node, p), None);
        } else {
            assert_eq!(Node::get(&node, p), Some(&1234));
        }
    }
}

#[test]
fn test_remove_region_overlap() {
    let mut node = None;
    let r1 = Region::new(V2::new(0, 3), V2::new(8, 5));
    Node::insert(&mut node, r1, 1);
    let r2 = Region::new(V2::new(3, 0), V2::new(5, 8));
    Node::remove(&mut node, r2);

    for p in Region::sized(10).points() {
        // Check r2 first, since it was drawn over r1
        if r2.contains(p) {
            assert_eq!(Node::get(&node, p), None);
        } else if r1.contains(p) {
            assert_eq!(Node::get(&node, p), Some(&1));
        } else {
            assert_eq!(Node::get(&node, p), None);
        }
    }
}

#[test]
fn test_remove_collapse() {
    let mut node = None;
    Node::insert(&mut node, Region::sized(2), 1234);
    Node::remove(&mut node, Region::sized(1));
    Node::remove(&mut node, Region::new(V2::new(0, 1), V2::new(2, 2)));

    // If collapse_inner works, `node` should be a single Leaf, with no intermediate Inner.
    let is_leaf = match **node.as_ref().unwrap() {
        Node::Leaf { .. } => true,
        _ => false,
    };
    assert!(is_leaf);
}



#[test]
fn test_tree_insert() {
    let mut tree = RegionTree::new();
    let r1 = Region::new(V2::new(0, 0), V2::new(1, 1));
    tree.insert(r1, 1234);
    let r2 = Region::new(V2::new(7, 0), V2::new(8, 8));
    tree.insert(r2, 1234);

    for p in Region::sized(10).points() {
        // Check r2 first, since it was drawn over r1
        if r1.contains(p) || r2.contains(p) {
            assert_eq!(tree.get(p), Some(&1234));
        } else {
            assert_eq!(tree.get(p), None);
        }
    }
}

#[test]
fn test_tree_insert_negative() {
    let mut tree = RegionTree::new();
    let r1 = Region::new(V2::new(0, 0), V2::new(1, 1)) - 5;
    tree.insert(r1, 1234);
    let r2 = Region::new(V2::new(7, 0), V2::new(8, 8)) - 5;
    tree.insert(r2, 1234);

    for p in (Region::sized(10) - 5).points() {
        // Check r2 first, since it was drawn over r1
        if r1.contains(p) || r2.contains(p) {
            assert_eq!(tree.get(p), Some(&1234));
        } else {
            assert_eq!(tree.get(p), None);
        }
    }
}

#[test]
fn test_tree_remove() {
    let mut tree = RegionTree::new();
    tree.insert(Region::sized(10), 1234);
    let r1 = Region::new(V2::new(0, 0), V2::new(1, 1));
    tree.remove(r1);
    let r2 = Region::new(V2::new(7, 0), V2::new(8, 8));
    tree.remove(r2);

    for p in Region::sized(10).points() {
        // Check r2 first, since it was drawn over r1
        if r1.contains(p) || r2.contains(p) {
            assert_eq!(tree.get(p), None);
        } else {
            assert_eq!(tree.get(p), Some(&1234));
        }
    }
}

#[test]
fn test_tree_remove_negative() {
    let mut tree = RegionTree::new();
    tree.insert(Region::sized(10) - 5, 1234);
    let r1 = Region::new(V2::new(0, 0), V2::new(1, 1)) - 5;
    tree.remove(r1);
    let r2 = Region::new(V2::new(7, 0), V2::new(8, 8)) - 5;
    tree.remove(r2);

    for p in (Region::sized(10) - 5).points() {
        // Check r2 first, since it was drawn over r1
        if r1.contains(p) || r2.contains(p) {
            assert_eq!(tree.get(p), None);
        } else {
            assert_eq!(tree.get(p), Some(&1234));
        }
    }
}

#[test]
fn test_iter_region() {
    let mut tree = RegionTree::new();
    let r1 = Region::new(V2::new(0, 0), V2::new(7, 3));
    tree.insert(r1, 1);
    let r2 = Region::new(V2::new(7, 0), V2::new(8, 8));
    tree.insert(r2, 2);

    let mut regions = Vec::new();
    let r = Region::new(V2::new(1, 1), V2::new(5, 5));
    for (bounds, &value) in tree.iter_region(r) {
        regions.push(bounds);
    }
    assert_eq!(&regions,
               &[
                   Region::new(V2::new(1, 1), V2::new(4, 3)),
                   Region::new(V2::new(4, 1), V2::new(5, 2)),
                   Region::new(V2::new(4, 2), V2::new(5, 3)),
               ]);
}
