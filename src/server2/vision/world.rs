use server_types::*;
use std::collections::HashMap;

use engine::component::camera::CameraPos;
use engine::component::char_invs::CharInvs;
use engine::component::crafting::{CraftingState, StationId};
use engine::component::dialog::Dialog;
use engine::update::{flags, Delta};
use syntax_exts::{define_expand_objs_list, for_each_obj_named};
use world::objects::{Activity, Item, Appearance};
use world::objects::InventoryFlags;

use super::{Location, loc};


pub struct Client {
    pub name: String,
    pub pawn: Option<EntityId>,

    pub dialog: Option<Box<Dialog>>,
    pub camera_pos: Option<CameraPos>,
}

pub struct Entity {
    pub plane: PlaneId,
    pub activity: Activity,
    pub act_start: Time,
    pub appearance: Appearance,

    pub name: String,

    pub char_invs: Option<CharInvs>,
}

impl Entity {
    fn view_pos(&self) -> V3 {
        activity_start_pos(&self.activity)
    }

    fn cpos(&self) -> V2 {
        self.view_pos().px_to_cpos()
    }
}

fn activity_start_pos(act: &Activity) -> V3 {
    match *act {
        Activity::Stand { pos, .. } => pos,
        Activity::Walk { pos, .. } => pos,
    }
}

pub struct Inventory {
    pub contents: Box<[Item]>,
    pub flags: InventoryFlags,
}

pub struct TerrainChunk {
    pub plane: PlaneId,
    pub cpos: V2,
    pub blocks: Box<BlockChunk>,
    pub compressed_blocks: Vec<u16>,
}

pub struct Structure {
    pub plane: PlaneId,
    /// Position, in tile coordinates.
    pub pos: V3,
    pub template: TemplateId,
}

impl Structure {
    fn cpos(&self) -> V2 {
        self.pos.tile_to_cpos()
    }
}

pub struct World {
    pub now: Time,

    pub clients: HashMap<ClientId, Client>,
    pub entities: HashMap<EntityId, Entity>,
    pub terrain_chunks: HashMap<TerrainChunkId, TerrainChunk>,
    pub structures: HashMap<StructureId, Structure>,
    pub inventories: HashMap<InventoryId, Inventory>,

    pub crafting_state: HashMap<StationId, CraftingState>,
}

impl World {
    pub fn new() -> World {
        World {
            now: 0,

            clients: HashMap::new(),
            entities: HashMap::new(),
            terrain_chunks: HashMap::new(),
            structures: HashMap::new(),
            inventories: HashMap::new(),

            crafting_state: HashMap::new(),
        }
    }
}



define_expand_objs_list! {
    any_world_objects = {
        client, clients;
        entity, entities;
        terrain_chunk, terrain_chunks;
        structure, structures;
    }
}

/// A reference to a (partial) World state.  This contains only the info needed by `SubChange`.
/// Its purpose is to abstract over `World` and `World + Delta`.
pub trait AnyWorld {
    type Client: AnyClient;
    type Entity: AnyEntity;
    type TerrainChunk: AnyTerrainChunk;
    type Structure: AnyStructure;

    fn get_client(&self, id: ClientId) -> Option<Self::Client>;
    fn get_entity(&self, id: EntityId) -> Option<Self::Entity>;
    fn get_terrain_chunk(&self, id: TerrainChunkId) -> Option<Self::TerrainChunk>;
    fn get_structure(&self, id: StructureId) -> Option<Self::Structure>;
}

pub trait AnyClient {
    fn pawn(&self) -> Option<EntityId>;
    fn dialog(&self) -> Option<&Dialog>;
    fn camera_pos(&self) -> Option<CameraPos>;
}

pub trait AnyEntity {
    fn char_invs(&self) -> Option<CharInvs>;
    fn view_loc(&self) -> Location;
}

pub trait AnyTerrainChunk {
    fn view_loc(&self) -> Location;
}

pub trait AnyStructure {
    fn view_loc(&self) -> Location;
}


impl<'a> AnyWorld for &'a World {
    for_each_obj_named! {
        [[any_world_objects]]
        type $Obj = &'a $Obj;
        fn $get_obj(&self, id: $ObjId) -> Option<&'a $Obj> {
            self.$objs.get(&id)
        }
    }
}

impl<'a> AnyClient for &'a Client {
    fn pawn(&self) -> Option<EntityId> {
        self.pawn
    }

    fn dialog(&self) -> Option<&Dialog> {
        self.dialog.as_ref().map(|x| x as &Dialog)
    }

    fn camera_pos(&self) -> Option<CameraPos> {
        self.camera_pos
    }
}

impl<'a> AnyEntity for &'a Entity {
    fn char_invs(&self) -> Option<CharInvs> {
        self.char_invs
    }

    fn view_loc(&self) -> Location {
        loc(self.plane, self.cpos())
    }
}

impl<'a> AnyTerrainChunk for &'a TerrainChunk {
    fn view_loc(&self) -> Location {
        loc(self.plane, self.cpos)
    }
}

impl<'a> AnyStructure for &'a Structure {
    fn view_loc(&self) -> Location {
        loc(self.plane, self.cpos())
    }
}


#[derive(Clone, Copy)]
pub struct NewWorld<'a> {
    world: &'a World,
    delta: &'a Delta,
}

impl<'a> NewWorld<'a> {
    pub fn new(world: &'a World, delta: &'a Delta) -> NewWorld<'a> {
        NewWorld { world, delta }
    }
}

for_each_obj_named! {
    [[any_world_objects]]

    #[derive(Clone, Copy)]
    pub struct $NewObj<'a> {
        delta: &'a Delta,
        id: $ObjId,
        old: Option<&'a $Obj>,
        flags: flags::$ObjUpdate,
    }

    impl<'a> $NewObj<'a> {
        fn old(&self) -> &'a $Obj {
            self.old.unwrap_or_else(|| panic!(
                    "missing old data for {:?}", self.id))
        }
    }
}

impl<'a> AnyWorld for NewWorld<'a> {
    for_each_obj_named! {
        [[any_world_objects]]
        type $Obj = $NewObj<'a>;

        fn $get_obj(&self, id: $ObjId) -> Option<$NewObj<'a>> {
            let flags = self.delta.$obj_flags(id);
            if flags.contains(flags::$O_DESTROYED) {
                return None;
            }
            Some($NewObj {
                delta: self.delta,
                id: id,
                old: self.world.$objs.get(&id),
                flags: flags,
            })
        }
    }
}

impl<'a> AnyClient for NewClient<'a> {
    fn pawn(&self) -> Option<EntityId> {
        if self.flags.intersects(flags::C_CREATED | flags::C_PAWN) {
            self.delta.new_client_pawn(self.id)
        } else {
            self.old().pawn
        }
    }

    fn dialog(&self) -> Option<&Dialog> {
        if self.flags.intersects(flags::C_CREATED | flags::C_DIALOG) {
            self.delta.new_client_dialog(self.id)
        } else {
            self.old().dialog.as_ref().map(|x| x as &Dialog)
        }
    }

    fn camera_pos(&self) -> Option<CameraPos> {
        if self.flags.intersects(flags::C_CREATED | flags::C_CAMERA_POS) {
            self.delta.new_client_camera_pos(self.id)
        } else {
            self.old().camera_pos
        }
    }
}

impl<'a> AnyEntity for NewEntity<'a> {
    fn char_invs(&self) -> Option<CharInvs> {
        if self.flags.intersects(flags::E_CREATED | flags::E_CHAR_INVS) {
            self.delta.new_entity_char_invs(self.id)
        } else {
            self.old().char_invs
        }
    }

    fn view_loc(&self) -> Location {
        let plane = if self.flags.intersects(flags::E_CREATED | flags::E_PLANE) {
            self.delta.new_entity_plane(self.id)
        } else {
            self.old().plane
        };

        let cpos = if self.flags.intersects(flags::E_CREATED | flags::E_ACTIVITY) {
            let (act, _start) = self.delta.new_entity_activity(self.id);
            activity_start_pos(act).px_to_cpos()
        } else {
            self.old().cpos()
        };

        loc(plane, cpos)
    }
}

impl<'a> AnyTerrainChunk for NewTerrainChunk<'a> {
    fn view_loc(&self) -> Location {
        if self.flags.intersects(flags::TC_CREATED) {
            let tc = self.delta.new_terrain_chunk(self.id);
            loc(tc.plane, tc.cpos)
        } else {
            loc(self.old().plane, self.old().cpos)
        }
    }
}

impl<'a> AnyStructure for NewStructure<'a> {
    fn view_loc(&self) -> Location {
        if self.flags.intersects(flags::S_CREATED) {
            let s = self.delta.new_structure(self.id);
            loc(s.plane, s.pos.tile_to_cpos())
        } else {
            loc(self.old().plane, self.old().cpos())
        }
    }
}
