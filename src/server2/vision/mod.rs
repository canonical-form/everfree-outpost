use server_types::*;
use std::collections::HashSet;
use std::collections::hash_map::{HashMap, Entry};
use std::hash::Hash;
use std::sync::Arc;
use std::sync::mpsc::{Sender, Receiver};

use common::util::ZOrdered;
use common::util::btree_multimap::Bounded;
use common::util::pubsub::{PubSub, DirectSub};
use engine::chat;
use engine::component::crafting::{CraftingState, StationId};
use engine::component::dialog::SpecialSub;
use engine::update::{flags, Delta, UpdateItem};
use world::objects::Activity;
use world::objects::{InventoryFlags, I_PUBLIC};

use conn::ToConn;
use messages::ClientResponse;
use util;

use self::subscription::*;
use self::world::*;


mod subscription;
mod world;


pub enum ToVision {
    // State update
    Delta(Time, Arc<Delta>),

    SubscribeWire(WireId, ClientId),
    UnsubscribeWire(WireId),

    // Transient messages
    ChatMessage(Box<ChatMessage>),

    // Misc

    /// Wrapper for messages to be forwarded to the connection handling thread.  This is sometimes
    /// needed to ensure proper ordering of `ToConn` messages.
    #[allow(dead_code)]
    ConnMessage(ToConn),

    /// Relay a message directly to a client's wire, with no additional processing.
    RelayResp(ClientId, ClientResponse),
}

pub struct ChatMessage {
    pub channel: chat::Channel,
    pub name: String,
    pub text: String,
}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Location {
    plane: PlaneId,
    cpos: ZOrdered<V2>,
}

fn loc(plane: PlaneId, cpos: V2) -> Location {
    Location {
        plane,
        cpos: ZOrdered(cpos),
    }
}

impl Location {
    fn plane(&self) -> PlaneId {
        self.plane
    }

    fn cpos(&self) -> V2 {
        self.cpos.0
    }

    fn vision_region(&self) -> impl Iterator<Item=Location> {
        let plane = self.plane();
        vision_region(self.cpos()).points()
            .map(move |cpos| loc(plane, cpos))
    }
}

fn vision_region(center: V2) -> Region<V2> {
    // 5x5 region centered on `center`
    Region::new(center - 2, center + 3)
}

impl Bounded for Location {
    fn min_bound() -> Location { 
        Location {
            plane: PlaneId::min_bound(),
            cpos: ZOrdered::<V2>::min_bound(),
        }
    }

    fn max_bound() -> Location { 
        Location {
            plane: PlaneId::max_bound(),
            cpos: ZOrdered::<V2>::max_bound(),
        }
    }
}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum ViewableId {
    // Ordinary location-based subscription targets
    Entity(EntityId),
    TerrainChunk(TerrainChunkId),
    Structure(StructureId),

    // Ordinary direct subscription targets
    Inventory(InventoryId),
    /// Additional data for a particular dialog.
    SpecialSub(SpecialSub),

    // Special targets
    /// A wire can only be bound to one client.
    Client(ClientId),
    /// Internal data about an entity, visible only to its controller.
    PawnEntity(EntityId),
}

impl Bounded for ViewableId {
    fn min_bound() -> ViewableId { ViewableId::Entity(Bounded::min_bound()) }
    fn max_bound() -> ViewableId { ViewableId::PawnEntity(Bounded::max_bound()) }
}


pub struct SendWrapper(Sender<ToConn>);

impl SendWrapper {
    fn send_raw(&mut self, msg: ToConn) {
        self.0.send(msg).unwrap();
    }

    fn send(&mut self,
            wire_id: WireId,
            msg: ClientResponse) {
        self.send_raw(ToConn::WireResp(wire_id, msg));
    }
}


pub struct AppearGone {
    appear: HashSet<(WireId, ViewableId)>,
    gone: HashSet<(WireId, ViewableId)>,
}

impl AppearGone {
    pub fn new() -> AppearGone {
        AppearGone {
            appear: HashSet::new(),
            gone: HashSet::new(),
        }
    }

    pub fn appear(&mut self, wire_id: WireId, viewable_id: ViewableId) {
        if !self.gone.remove(&(wire_id, viewable_id)) {
            self.appear.insert((wire_id, viewable_id));
        }
    }

    pub fn gone(&mut self, wire_id: WireId, viewable_id: ViewableId) {
        if !self.appear.remove(&(wire_id, viewable_id)) {
            self.gone.insert((wire_id, viewable_id));
        }
    }
}


fn adjust_activity_start(now: Time, act: Activity, start: Time) -> (Activity, Time) {
    if start >= now - 1000 {
        return (act, start);
    }

    // If the start time is too far in the past, the client will mistakenly think it's in the
    // future.  To avoid this, adjust the start_time forward to a time just prior to `now`.
    //
    // We step by a whole number of seconds so that the corresponding position offset will be a
    // whole number of pixels, regardless of velocity.
    let adj_s = (now - start) / 1000;

    let mut act = act;
    match act {
        Activity::Stand { .. } => {},
        Activity::Walk { ref mut pos, velocity, .. } => {
            *pos = *pos + velocity * adj_s as i32;
        },
    }

    (act, start + 1000 * adj_s)
}


const I_FLAG_MASK: InventoryFlags = I_PUBLIC;


/// `Vision` is split into `World` (the current game state) and `Subscriptions` (everything needed
/// to send messages to the right wires).  This makes it easy to send messages about a `World`
/// object while holding a reference to the object itself.
struct Vision {
    w: World,
    sub: Subscriptions,
}

impl Vision {
    pub fn new(to_conn: Sender<ToConn>) -> Vision {
        Vision {
            w: World::new(),
            sub: Subscriptions {
                ps: PubSub::new(),
                direct_ps: DirectSub::new(),

                wire_client: HashMap::new(),

                to_conn: SendWrapper(to_conn),
            }
        }
    }

    /// Update the subscription channels for all wires.  Appear/gone notifications go into `ag`
    /// instead of being sent immediately.  Also returns a set of wires whose subscriptions were
    /// (possibly) changed.
    fn update_subscriptions(&mut self, delta: &Delta, ag: &mut AppearGone) -> HashSet<WireId> {
        // We first make a list of all clients whose view positions could have changed, judging by
        // client and entity flags.  Then we actually compute the old and new view position for
        // each of those clients.
        let mut view_change = HashSet::new();
        for it in delta.items() {
            match it {
                UpdateItem::Client(id, f) => {
                    // Check the flags corresponding to the fields examined by `SubChange`.
                    if f.intersects(flags::C_CREATED | flags::C_DESTROYED |
                                    flags::C_PAWN | flags::C_DIALOG | flags::C_CAMERA_POS) {
                        self.sub.for_client_wires(id, |wire_id| {
                            view_change.insert(wire_id);
                        });
                    }
                },

                UpdateItem::Entity(id, f) => {
                    if f.intersects(flags::E_CREATED | flags::E_DESTROYED |
                                    flags::E_PLANE | flags::E_ACTIVITY | flags::E_CHAR_INVS) {
                        self.sub.for_pawn_entity_wires(id, |wire_id| {
                            view_change.insert(wire_id);
                        });
                    }
                },

                _ => {},
            }
        }

        for &wire_id in &view_change {
            self.sub.update_view(ag, &self.w, delta, wire_id);
        }

        view_change
    }

    /// Update the publication channels for all viewables.  Appear/gone notifications go into `ag`
    /// instead of being sent immediately.
    fn update_publications(&mut self, delta: &Delta, ag: &mut AppearGone) {
        for it in delta.items() {
            match it {
                UpdateItem::Entity(id, f) => {
                    if f.intersects(flags::E_CREATED | flags::E_DESTROYED |
                                     flags::E_PLANE | flags::E_ACTIVITY) {
                        self.sub.update_entity(ag, &self.w, delta, id)
                    }
                },

                UpdateItem::TerrainChunk(id, f) => {
                    if f.intersects(flags::TC_CREATED | flags::TC_DESTROYED) {
                        self.sub.update_terrain_chunk(ag, &self.w, delta, id)
                    }
                },

                UpdateItem::Structure(id, f) => {
                    if f.intersects(flags::S_CREATED | flags::S_DESTROYED) {
                        self.sub.update_structure(ag, &self.w, delta, id)
                    }
                },

                _ => {},
            }
        }
    }

    /// Apply a delta to the current world state (`self.w`).  This also sends update messages to
    /// subscribers, except for objects that have just appeared (those are handled by
    /// `send_appear`).
    fn update_world_state(&mut self, delta: &Delta, ag: &AppearGone) {
        // (client id, pawn id) for each pawn that needs its `name` field updated.
        let mut update_pawn_names = Vec::new();

        for it in delta.items() {
            // HACK: Workaround for a compiler bug (yes, really).  As of 7a31b4c, with Rust
            // 1.33.0 (built via Nix using upstream LLVM), and building with `--opt-level=3 -C
            // lto`, when an insert into `self.w.terrain_chunks` bumps existing entries into new
            // slots, it overwrites the `cpos.x` of the moved entries with the `cpos.x` of the
            // newly inserted one.  This can only be observed using GDB, since all sorts of minor
            // perturbations to the code cause the bug to magically disappear, including adding
            // logging.  The user-visible symptom of this issue is "thread 'vision' panicked at
            // 'remove: key-value pair not found'" when unloading chunks: the `unpublish` for the
            // unloaded chunk fails, because its `cpos` has changed, and so the `Location` passed
            // to `unpublish` doesn't match the one passed to `publish`.  Our current choice of
            // workaround is to block inlining of the `insert` call so that LLVM can't optimize
            // across that boundary.
            #[inline(never)]
            fn do_insert<K: Eq+Hash, V>(m: &mut HashMap<K, V>, k: K, v: V) {
                m.insert(k, v);
            }

            match it {
                UpdateItem::Client(id, f) => {
                    if f.contains(flags::C_CREATED | flags::C_DESTROYED) {
                        continue;

                    } else if f.contains(flags::C_CREATED) {
                        assert!(!self.w.clients.contains_key(&id));
                        let c = delta.new_client(id);
                        let dialog = delta.new_client_dialog(id).map(|d| Box::new(d.clone()));
                        let camera_pos = delta.new_client_camera_pos(id);

                        if let Some(pawn_id) = c.pawn {
                            update_pawn_names.push((id, pawn_id));
                        }

                        do_insert(&mut self.w.clients, id, Client {
                            name: c.name.clone(),
                            pawn: c.pawn,

                            dialog, camera_pos,
                        });
                        continue;

                    } else if f.contains(flags::C_DESTROYED) {
                        self.w.clients.remove(&id)
                            .unwrap_or_else(|| panic!("destroyed {:?} not in world", id));
                        continue;
                    }

                    let c = self.w.clients.get_mut(&id)
                        .unwrap_or_else(|| panic!("delta update for nonexistent {:?}", id));
                    if f.contains(flags::C_PAWN) {
                        c.pawn = delta.new_client_pawn(id);
                        if let Some(pawn_id) = c.pawn {
                            update_pawn_names.push((id, pawn_id));
                        }
                        // Camera update is sent below
                    }
                    if f.contains(flags::C_DIALOG) {
                        c.dialog = delta.new_client_dialog(id).cloned().map(Box::new);
                        if let Some(ref d) = c.dialog {
                            self.sub.send_client_no_appear(ag, id,
                                || ClientResponse::DialogOpen(d.clone()));
                        } else {
                            self.sub.send_client_no_appear(ag, id,
                                || ClientResponse::DialogClose);
                        }
                    }
                    if f.contains(flags::C_CAMERA_POS) {
                        c.camera_pos = delta.new_client_camera_pos(id);
                        // Camera update is sent below
                    }

                    if f.intersects(flags::C_PAWN | flags::C_CAMERA_POS) {
                        self.sub.send_client_camera_setting_no_appear(ag, id, c);
                    }
                },

                UpdateItem::Entity(id, f) => {
                    if f.contains(flags::E_CREATED | flags::E_DESTROYED) {
                        continue;

                    } else if f.contains(flags::E_CREATED) {
                        assert!(!self.w.entities.contains_key(&id));
                        let e = delta.new_entity(id);

                        let char_invs = delta.new_entity_char_invs(id);

                        do_insert(&mut self.w.entities, id, Entity {
                            plane: e.plane,
                            activity: e.activity.clone(),
                            act_start: e.activity_start,
                            appearance: e.appearance.clone(),

                            // Set during update_pawn_names handling
                            name: String::new(),

                            char_invs,
                        });
                        continue;

                    } else if f.contains(flags::E_DESTROYED) {
                        self.w.entities.remove(&id)
                            .unwrap_or_else(|| panic!("destroyed {:?} not in world", id));
                        continue;
                    }

                    let e = self.w.entities.get_mut(&id)
                        .unwrap_or_else(|| panic!("delta update for nonexistent {:?}", id));
                    if f.contains(flags::E_ACTIVITY) {
                        let (act, start) = delta.new_entity_activity(id);
                        e.activity = act.clone();
                        e.act_start = start;
                        self.sub.send_entity_no_appear(ag, id,
                            || ClientResponse::EntityActivity(
                                id, start, Box::new(e.activity.clone())));
                        // Associated subscription changes are handled in a previous pass
                    }
                    if f.contains(flags::E_PLANE) {
                        e.plane = delta.new_entity_plane(id);
                        // Associated subscription changes are handled in a previous pass
                    }
                    if f.contains(flags::E_APPEARANCE) {
                        e.appearance = delta.new_entity_appearance(id).clone();
                        self.sub.send_entity_no_appear(ag, id,
                            || ClientResponse::EntityChange(
                                id,
                                Box::new(e.appearance.clone()),
                                e.name.clone().into_boxed_str()));
                    }
                    if f.contains(flags::E_CHAR_INVS) {
                        e.char_invs = delta.new_entity_char_invs(id);
                        if let Some(char_invs) = e.char_invs {
                            self.sub.send_pawn_no_appear(ag, id,
                                || ClientResponse::CharInvs(char_invs));
                        }
                    }
                },

                UpdateItem::Inventory(id, f) => {
                    if f.contains(flags::I_CREATED | flags::I_DESTROYED) {
                        continue;

                    } else if f.contains(flags::I_CREATED) {
                        assert!(!self.w.inventories.contains_key(&id));
                        let i = delta.new_inventory(id);

                        do_insert(&mut self.w.inventories, id, Inventory {
                            contents: i.contents.clone(),
                            flags: i.flags & I_FLAG_MASK,
                        });
                        continue;

                    } else if f.contains(flags::I_DESTROYED) {
                        self.w.inventories.remove(&id)
                            .unwrap_or_else(|| panic!("destroyed {:?} not in world", id));
                        continue;
                    }

                    let i = self.w.inventories.get_mut(&id)
                        .unwrap_or_else(|| panic!("delta update for nonexistent {:?}", id));
                    if f.contains(flags::I_FLAGS) {
                        i.flags = delta.new_inventory_world_flags(id) & I_FLAG_MASK;
                        self.sub.send_inventory_no_appear(ag, id,
                            || ClientResponse::InventoryFlags(id, i.flags));
                    }
                    if f.contains(flags::I_CONTENTS) {
                        let contents = delta.new_inventory_contents(id);
                        i.contents = contents.to_owned().into_boxed_slice();
                        self.sub.send_inventory_no_appear(ag, id,
                            || ClientResponse::InventoryContents(
                                id, i.contents.clone()));
                        // Associated subscription changes are handled in a previous pass
                    }
                },

                UpdateItem::Plane(_id, _f) => {
                    // TODO: plane flags
                },

                UpdateItem::TerrainChunk(id, f) => {
                    if f.contains(flags::TC_CREATED | flags::TC_DESTROYED) {
                        continue;

                    } else if f.contains(flags::TC_CREATED) {
                        assert!(!self.w.terrain_chunks.contains_key(&id));
                        let tc = delta.new_terrain_chunk(id);

                        let compressed = util::encode_rle16(tc.blocks.iter().cloned());

                        do_insert(&mut self.w.terrain_chunks, id, TerrainChunk {
                            plane: tc.plane,
                            cpos: tc.cpos,
                            blocks: Box::new(*tc.blocks),
                            compressed_blocks: compressed,
                        });
                        continue;

                    } else if f.contains(flags::TC_DESTROYED) {
                        self.w.terrain_chunks.remove(&id)
                            .unwrap_or_else(|| panic!("destroyed {:?} not in world", id));
                        continue;
                    }

                    let tc = self.w.terrain_chunks.get_mut(&id)
                        .unwrap_or_else(|| panic!("delta update for nonexistent {:?}", id));
                    if f.contains(flags::TC_BLOCKS) {
                        let blocks = delta.new_terrain_chunk_blocks(id);
                        let compressed = util::encode_rle16(blocks.iter().cloned());
                        tc.blocks = Box::new(*blocks);
                        tc.compressed_blocks = compressed;
                        self.sub.send_terrain_chunk_no_appear(ag, id,
                            || ClientResponse::TerrainChunk(
                                tc.cpos, tc.compressed_blocks.clone()));
                    }
                },

                UpdateItem::Structure(id, f) => {
                    if f.contains(flags::S_CREATED | flags::S_DESTROYED) {
                        continue;

                    } else if f.contains(flags::S_CREATED) {
                        assert!(!self.w.structures.contains_key(&id));
                        let s = delta.new_structure(id);

                        do_insert(&mut self.w.structures, id, Structure {
                            plane: s.plane,
                            pos: s.pos,
                            template: s.template,
                        });
                        continue;

                    } else if f.contains(flags::S_DESTROYED) {
                        self.w.structures.remove(&id)
                            .unwrap_or_else(|| panic!("destroyed {:?} not in world", id));
                        continue;
                    }

                    let s = self.w.structures.get_mut(&id)
                        .unwrap_or_else(|| panic!("delta update for nonexistent {:?}", id));
                    if f.contains(flags::S_TEMPLATE) {
                        s.template = delta.new_structure_template(id);
                        self.sub.send_structure_no_appear(ag, id,
                            || ClientResponse::StructureReplace(
                                id, s.template));
                    }
                    if f.contains(flags::S_CRAFTING) {
                        let station_id = StationId::Structure(id);
                        // TODO: factor this out when adding a second crafting station kind
                        let send_state: Option<&CraftingState>;
                        if let Some((state, _inv)) = delta.new_structure_crafting(id) {
                            match self.w.crafting_state.entry(station_id) {
                                Entry::Vacant(e) => {
                                    send_state = Some(e.insert(state.clone()));
                                },
                                Entry::Occupied(mut e) => {
                                    *e.get_mut() = state.clone();
                                    send_state = Some(e.into_mut());
                                },
                            }
                        } else {
                            self.w.crafting_state.remove(&station_id);
                            send_state = None;
                        }
                        let sub_id = SpecialSub::CraftingState(station_id);
                        self.sub.send_special_sub_no_appear(ag, sub_id,
                            || ClientResponse::CraftingState(
                                id, send_state.map(|s| Box::new(s.clone()))));
                    }
                },

                UpdateItem::World(_f) => {
                    // Do nothing
                },
            }
        }

        for (cid, pawn_id) in update_pawn_names {
            let c = unwrap_or!(self.w.clients.get(&cid), {
                error!("update_pawn_names: unknown {:?}", cid);
                continue;
            });
            let e = unwrap_or!(self.w.entities.get_mut(&pawn_id), {
                error!("update_pawn_names: unknown {:?}", pawn_id);
                continue;
            });
            e.name = c.name.clone();
            self.sub.send_entity_no_appear(ag, pawn_id,
                || ClientResponse::EntityChange(
                    pawn_id, Box::new(e.appearance.clone()), e.name.clone().into_boxed_str()));
        }
    }

    /// Send "appear" messages based on the contentns of `ag`.
    fn send_appear(&mut self, ag: &AppearGone) {
        let mut appear = ag.appear.iter().cloned().collect::<Vec<_>>();
        appear.sort();
        for (wire_id, vid) in appear {
            match vid {
                ViewableId::Entity(eid) => {
                    let e = &self.w.entities[&eid];
                    let (act, start) = adjust_activity_start(
                        self.w.now, e.activity.clone(), e.act_start);
                    self.sub.to_conn.send(wire_id, ClientResponse::EntityAppear(
                            eid,
                            Box::new(e.appearance.clone()),
                            e.name.clone().into_boxed_str()));
                    self.sub.to_conn.send(wire_id, ClientResponse::EntityActivity(
                            eid, start, Box::new(act)));
                },

                ViewableId::TerrainChunk(tcid) => {
                    let tc = &self.w.terrain_chunks[&tcid];
                    self.sub.to_conn.send(wire_id, ClientResponse::TerrainChunk(
                            tc.cpos, tc.compressed_blocks.clone()));
                },

                ViewableId::Structure(sid) => {
                    let s = &self.w.structures[&sid];
                    self.sub.to_conn.send(wire_id, ClientResponse::StructureAppear(
                            sid, s.pos, s.template));
                },

                ViewableId::Inventory(iid) => {
                    let i = &self.w.inventories[&iid];
                    self.sub.to_conn.send(wire_id, ClientResponse::InventoryAppear(
                            iid, i.contents.clone(), i.flags));
                },

                ViewableId::SpecialSub(SpecialSub::CraftingState(station_id)) => {
                    let sid = match station_id {
                        StationId::Structure(sid) => sid,
                        _ => {
                            error!("unexpected station id type {:?}", station_id);
                            continue;
                        },
                    };
                    // For SpecialSub::CraftingState, `AppearGone` tracks the state of the
                    // subscription only, not the presence/absence of a `crafting_state` entry.
                    // (See comment on `Subscriptions`.)  We have to handle the `None` case here.
                    let state = self.w.crafting_state.get(&station_id)
                        .map(|s| Box::new(s.clone()));
                    self.sub.to_conn.send(wire_id, ClientResponse::CraftingState(
                            sid, state));
                },

                ViewableId::Client(cid) => {
                    let c = &self.w.clients[&cid];
                    if let Some(ref d) = c.dialog {
                        self.sub.to_conn.send(wire_id, ClientResponse::DialogOpen(d.clone()));
                    } else {
                        self.sub.to_conn.send(wire_id, ClientResponse::DialogClose);
                    }
                    if let Some((_plane, pos)) = c.camera_pos {
                        self.sub.to_conn.send(wire_id, ClientResponse::CameraFixed(pos));
                    } else if let Some(pawn_id) = c.pawn {
                        self.sub.to_conn.send(wire_id, ClientResponse::CameraPawn(pawn_id));
                    } else {
                        // No camera - set it to a reasonable default.
                        self.sub.to_conn.send(wire_id, ClientResponse::CameraFixed(scalar(0)));
                    }
                },

                ViewableId::PawnEntity(eid) => {
                    let e = &self.w.entities[&eid];
                    if let Some(char_invs) = e.char_invs {
                        self.sub.to_conn.send(wire_id, ClientResponse::CharInvs(
                                char_invs));
                    }
                },
            }
        }
    }

    /// Send "gone" messages based on the contentns of `ag`.
    fn send_gone(&mut self, ag: &AppearGone) {
        let mut gone = ag.gone.iter().cloned().collect::<Vec<_>>();
        gone.sort();
        for (wire_id, vid) in gone {
            match vid {
                ViewableId::Entity(eid) => {
                    self.sub.to_conn.send(wire_id, ClientResponse::EntityGone(eid));
                },

                ViewableId::TerrainChunk(_tcid) => {
                    // Do nothing.  Can't send `UnloadChunk` without the cpos, and that message is
                    // not that useful anyway.
                },

                ViewableId::Structure(sid) => {
                    self.sub.to_conn.send(wire_id, ClientResponse::StructureGone(sid));
                },

                ViewableId::Inventory(iid) => {
                    self.sub.to_conn.send(wire_id, ClientResponse::InventoryGone(iid));
                },

                ViewableId::SpecialSub(SpecialSub::CraftingState(station_id)) => {
                    let sid = match station_id {
                        StationId::Structure(sid) => sid,
                        _ => {
                            error!("unexpected station id type {:?}", station_id);
                            continue;
                        },
                    };
                    self.sub.to_conn.send(wire_id, ClientResponse::CraftingState(sid, None));
                },

                ViewableId::Client(_cid) => {
                    self.sub.to_conn.send(wire_id, ClientResponse::DialogClose);
                    // Reset camera to a reasonable default.
                    self.sub.to_conn.send(wire_id, ClientResponse::CameraFixed(scalar(0)));
                },

                ViewableId::PawnEntity(_eid) => {
                    // Do nothing.  There's no mechanism for unsetting `CharInvs`.
                },
            }
        }
    }

    pub fn apply_delta(&mut self, now: Time, delta: &Delta) {
        let mut ag = AppearGone::new();

        let view_changed = self.update_subscriptions(delta, &mut ag);
        self.update_publications(delta, &mut ag);
        // At this point all pub/sub entries are updated to reflect the new world state.

        let old_view_bases = self.collect_old_view_bases(view_changed);

        self.update_world_state(delta, &ag);
        self.w.now = now;

        self.send_new_view_bases(old_view_bases);

        self.send_appear(&ag);
        self.send_gone(&ag);

        self.sub.to_conn.send_raw(ToConn::EngineTime(now));
    }


    fn wire_view_base(&self, wire: WireId) -> Option<V2> {
        let cid = *self.sub.wire_client.get(&wire)?;
        let c = self.w.clients.get(&cid)?;
        // TODO: should camera take precedence over pawn, or the other way around?
        if let Some((_, pos)) = c.camera_pos {
            return Some(pos.px_to_cpos());
        }

        let eid = c.pawn?;
        let e = self.w.entities.get(&eid)?;
        let pos = e.activity.start_pos();
        return Some(pos.px_to_cpos());
    }

    fn collect_old_view_bases(&self, wires: impl IntoIterator<Item=WireId>)
                              -> HashMap<WireId, Option<V2>> {
        wires.into_iter()
            .map(|wire| (wire, self.wire_view_base(wire)))
            .collect()
    }

    fn send_new_view_bases(&mut self, old: HashMap<WireId, Option<V2>>) {
        for (wire, old_base) in old {
            let new_base = self.wire_view_base(wire);
            if new_base != old_base {
                if let Some(cpos) = new_base {
                    self.sub.to_conn.send_raw(ToConn::WireViewBase(wire, cpos));
                }
            }
        }
    }


    pub fn subscribe_wire(&mut self, wire_id: WireId, cid: ClientId) {
        debug!("subscribe_wire: {:?} -> {:?}", wire_id, cid);

        let mut ag = AppearGone::new();
        self.sub.subscribe_wire(&mut ag, &self.w, wire_id, cid);

        if let Some(cpos) = self.wire_view_base(wire_id) {
            self.sub.to_conn.send_raw(ToConn::WireViewBase(wire_id, cpos));
        }

        // Notify that the subscribe succeeded before sending update/appear messages
        self.sub.to_conn.send_raw(ToConn::WireSubscribed(wire_id));

        self.send_appear(&ag);
        if !ag.gone.is_empty() {
            error!("subscribe_wire ({:?} -> {:?}) produced Gone messages?", wire_id, cid);
        }
    }

    pub fn unsubscribe_wire(&mut self, wire_id: WireId) {
        debug!("unsubscribe_wire: {:?}", wire_id);

        let mut ag = AppearGone::new();
        self.sub.unsubscribe_wire(&mut ag, &self.w, wire_id);
        self.send_gone(&ag);
        if !ag.appear.is_empty() {
            error!("unsubscribe_wire ({:?}) produced Appear messages?", wire_id);
        }

        self.sub.to_conn.send_raw(ToConn::WireUnsubscribed(wire_id));
    }

    pub fn chat_message(&mut self, msg: &ChatMessage) {
        match msg.channel {
            chat::Channel::System => {
                let line = format!("&s\t{}\t{}", msg.name, msg.text);
                for &wire_id in self.sub.wire_client.keys() {
                    self.sub.to_conn.send(wire_id, ClientResponse::ChatUpdate(line.clone()));
                }
            },

            chat::Channel::Log(cid) => {
                let line = format!("&s\t{}\t{}", msg.name, msg.text);
                let to_conn = &mut self.sub.to_conn;
                self.sub.direct_ps.message(&ViewableId::Client(cid), |_, &wire_id| {
                    to_conn.send(wire_id, ClientResponse::ChatUpdate(line.clone()));
                });
            },

            chat::Channel::Global => {
                let line = format!("&g\t<{}>\t{}", msg.name, msg.text);
                for &wire_id in self.sub.wire_client.keys() {
                    self.sub.to_conn.send(wire_id, ClientResponse::ChatUpdate(line.clone()));
                }
            },

            chat::Channel::Local(plane, cpos) => {
                let line = format!("&l\t<{}>\t{}", msg.name, msg.text);
                let to_conn = &mut self.sub.to_conn;
                self.sub.ps.channel_message(&loc(plane, cpos), |_, &wire_id| {
                    to_conn.send(wire_id, ClientResponse::ChatUpdate(line.clone()));
                });
            },
        }
    }


    pub fn relay_response(&mut self, cid: ClientId, msg: ClientResponse) {
        let to_conn = &mut self.sub.to_conn;
        self.sub.direct_ps.message(&ViewableId::Client(cid), |_, &wire_id| {
            to_conn.send(wire_id, msg.clone());
        });
    }
}


pub fn run_vision(recv: Receiver<ToVision>,
                  to_conn: Sender<ToConn>) {
    use self::ToVision::*;

    let mut v = Vision::new(to_conn);

    for msg in recv.iter() {
        match msg {
            Delta(now, delta) =>
                v.apply_delta(now, &delta),

            SubscribeWire(wire_id, cid) =>
                v.subscribe_wire(wire_id, cid),
            UnsubscribeWire(wire_id) =>
                v.unsubscribe_wire(wire_id),

            ChatMessage(msg) =>
                v.chat_message(&msg),

            ConnMessage(msg) =>
                v.sub.to_conn.send_raw(msg),

            RelayResp(cid, msg) =>
                v.relay_response(cid, msg),

        }
    }

    info!("input channel disconnected");
}

pub fn start_vision(recv: Receiver<ToVision>,
                    to_conn: Sender<ToConn>) {
    util::spawn_named("vision", move || {
        run_vision(recv, to_conn);
    });
}
