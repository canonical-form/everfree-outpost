use std::io::{self, Read, Write, Cursor};
use std::iter;
use std::sync::mpsc::{Sender, Receiver};
use std::u16;

use common::proto::{game, control};
use common::proto::wire::{ReadFrom, WriteTo, Size};
use common::util::bytes::{ReadBytes, WriteBytes};
use server_types::WireId;

use conn::ToConn;
use util;


#[derive(Debug)]
pub enum Input {
    Control(control::Request),
    Game(WireId, game::Request),
}

fn read_one<R: Read>(r: &mut R) -> io::Result<Input> {
    let (raw_id, len) = try!(r.read_bytes::<(u16, u16)>());
    let mut buf = iter::repeat(0).take(len as usize).collect::<Vec<_>>();
    try!(r.read_exact(&mut buf));

    if raw_id == 0 {
        let msg = try!(control::Request::read_from(&mut Cursor::new(&buf)));
        Ok(Input::Control(msg))
    } else {
        let msg = try!(game::Request::read_from(&mut Cursor::new(&buf)));
        Ok(Input::Game(WireId(raw_id), msg))
    }
}

pub fn run_input<R: Read>(mut r: R, send: Sender<ToConn>) {
    loop {
        match read_one(&mut r) {
            Ok(input) => {
                trace!("input: {:?}", input);
                match send.send(ToConn::Input(input)) {
                    Ok(()) => (),
                    Err(_) => {
                        error!("error passing Input to channel: disconnected");
                        return;
                    },
                }
            }

            Err(e) => {
                use std::io::ErrorKind::*;
                error!("error reading Input from stream: {}", e);
                match e.kind() {
                    NotFound |
                    PermissionDenied |
                    ConnectionRefused |
                    ConnectionReset |
                    ConnectionAborted |
                    NotConnected |
                    BrokenPipe => return,
                    _ => {},
                }
            }
        }
    }
}

pub fn start_input<R: Read+Send+'static>(reader: R, send: Sender<ToConn>) {
    util::spawn_named("io-in", move || {
        run_input(reader, send);
    });
}


#[derive(Debug)]
pub enum Output {
    Control(control::Response),
    Game(WireId, game::Response),
}

fn write_one<W: Write>(w: &mut W, output: Output) -> io::Result<()> {
    match output {
        Output::Control(msg) => {
            let size = msg.size();
            assert!(size <= u16::MAX as usize);

            try!(w.write_bytes((0_u16, size as u16)));
            let mut buf = Vec::with_capacity(size);
            try!(msg.write_to(&mut buf));
            try!(w.write_all(&buf));
        },

        Output::Game(wire_id, msg) => {
            let size = msg.size();
            assert!(size <= u16::MAX as usize);

            try!(w.write_bytes((wire_id.unwrap(), size as u16)));
            let mut buf = Vec::with_capacity(size);
            try!(msg.write_to(&mut buf));
            try!(w.write_all(&buf));
        },
    }
    try!(w.flush());
    Ok(())
}

pub fn run_output<W: Write>(mut w: W, recv: Receiver<Output>) {
    loop {
        let output = match recv.recv() {
            Ok(x) => x,
            Err(_) => {
                error!("error taking Output from channel: disconnected");
                return;
            },
        };
        trace!("output: {:?}", output);
        match write_one(&mut w, output) {
            Ok(()) => (),
            Err(e) => {
                error!("error writing Output to stream: {}", e);
                return;
            },
        }
    }
}

pub fn start_output<W: Write+Send+'static>(writer: W, recv: Receiver<Output>) {
    util::spawn_named("io-out", move || {
        run_output(writer, recv);
    });
}
