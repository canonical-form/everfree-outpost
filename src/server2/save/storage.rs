use server_types::*;
use std::fs::{self, File};
use std::io::{self, Read};
use std::path::{Path, PathBuf};

use server_bundle::flat::{Flat, FlatView};
use server_bundle::types::Bundle;

use save::fs::{Filesystem, RealFs, UnionFs};
use save::memory::BundleKey;

/// Storage management for `Bundle` files.
///
/// This object manages a save-game directory.  At a logical level, the storage manager keeps a
/// mapping from `BundleKey`s to `Bundle`s, which client code can read and modify.
///
/// The storage manager also provides a "commit" operation.  Performing a commit indicates to the
/// storage manager that the current bundle map is fully consistent (there are no related changes
/// still buffered in memory) and is suitable as a rollback point in case of a future server crash.
/// The storage manager maintains a snapshot of the bundle map as of the most recent commit, in
/// order to support such rollbacks.
///
/// Internally, the storage manager maintains several directories:
///
///  * `base`: The most recent committed snapshot.
///  * `new`: The delta between `base` and the current bundle map.
///
///
/// # Snapshot process
///
/// The process of committing a snapshot goes like this:
///
///  1. Create a file called `snapshot.lock`.
///  2. Merge files from `new` into `base`.
///  3. Delete `snapshot.lock`.
///
///
/// # Consistency
///
/// The rules are simple:
///
///  * If `snapshot.lock` exists, then `base` may be inconsistent, but the combination of `base`
///    and `new` is consistent.
///  * Otherwise, `base` is consistent, but `base + new` may be inconsistent.
///
/// At startup, the storage manager needs to get into a consistent state.  If `snapshot.lock`
/// exists, then it does this by completing the snapshotting process: it merges `new` into `base`,
/// then removes `snapshot.lock`.  If `snapshot.lock` does not exist, it removes `new` (if it
/// exists).
///
///
/// # Directory layout
///
/// Each directory managed by `SaveStorage` has a similar format.
///
///  * `world.dat`: The top-level `World` bundle.
///  * `clients/aa.client`: Contains the `Client` bundle for the account whose user ID is `aa`.
///  * `planes/aa.plane`: Contains the `Plane` bundle for the plane whose stable ID is `aa`.
///  * `terrain_chunks/aa/bb,cc/dd,ee.terrain_chunk`: Contains the terrain chunk bundle at plane
///    `aa`, chunk position `dd,ee`.  The intermediate level (directory `bb,cc`) divides the plane
///    into 64x64-chunk regions, to avoid massive file counts for large planes.  The coordinates
///    `bb,cc` are the chunk position `dd,ee` rounded down to the nearest multiple of the storage
///    region size.
///
pub struct SaveStorage {
    path: PathBuf,
    fs: UnionFs<RealFs, RealFs>,
}

impl SaveStorage {
    pub fn new<P: AsRef<Path>>(path: P) -> io::Result<SaveStorage> {
        let path = path.as_ref().to_owned();

        let new_path = path.join("new");
        let lock_path = path.join("commit.lock");

        let base = RealFs::new(path.join("base"));
        let new = RealFs::new(new_path.clone());

        let fs = UnionFs::new(base, new);

        if lock_path.exists() {
            info!("found {} - finishing old commit", lock_path.display());
            fs.commit()?;
            fs::remove_file(&lock_path)?;
        } else if new_path.exists() && fs.upper.iter()?.next().is_some() {
            let msg = format!("{} is nonempty - save contains inconsistent delta",
                              new_path.display());
            return Err(io::Error::new(io::ErrorKind::Other, msg));
        }

        Ok(SaveStorage { path, fs })
    }

    fn bundle_path(&self, bk: BundleKey) -> PathBuf {
        let mut path = PathBuf::new();
        match bk {
            BundleKey::World => {
                path.push("world.dat");
            },

            BundleKey::Client(stable_id) => {
                let uid = stable_id.unwrap() as u32;
                path.push("clients");
                path.push(format!("{}.client", uid));
            },

            BundleKey::Plane(stable_id) => {
                path.push("planes");
                path.push(format!("{}.plane", stable_id.unwrap()));
            },

            BundleKey::TerrainChunk(stable_plane, cpos) => {
                const REGION_SIZE: i32 = 64;
                // Center a region on 0,0 so small dungeons use only a single directory
                let region_pos = (cpos + REGION_SIZE / 2)
                    .div_floor(REGION_SIZE) * REGION_SIZE;
                path.push("terrain_chunks");
                path.push(format!("{}:{},{}", stable_plane.unwrap(), region_pos.x, region_pos.y));
                path.push(format!("{},{}.terrain_chunk", cpos.x, cpos.y));
            },
        }
        path
    }

    /// Create a bundle file.  This always places the file into the `new` directory, at a relative
    /// path computed from its `BundleKey`.
    fn create_bundle_file(&self, bk: BundleKey) -> io::Result<File> {
        self.fs.create(&self.bundle_path(bk))
    }

    /// Load a bundle file.  This retrieves the file from `new`, if present, or from `base`
    /// otherwise.  If `new` contains a whiteout file, returns `Err(NotFound)`.
    fn open_bundle_file(&self, bk: BundleKey) -> io::Result<File> {
        self.fs.open(&self.bundle_path(bk))
    }

    /// Remove a bundle file.  If the file doesn't exist in `base`, removes it from `new`.  If it
    /// does exist in `base`, removes it from `new` and adds a `.whiteout` file.
    fn remove_bundle_file(&self, bk: BundleKey) -> io::Result<()> {
        self.fs.remove(&self.bundle_path(bk))
    }

    pub fn save(&mut self, bk: BundleKey, b: &Bundle) -> io::Result<()> {
        trace!("save {:?}", bk);
        let mut f = self.create_bundle_file(bk)?;
        let mut flat = Flat::new();
        flat.flatten_bundle(&b);
        flat.write(&mut f)?;
        Ok(())
    }

    pub fn load(&mut self, bk: BundleKey) -> io::Result<Bundle> {
        trace!("load {:?}", bk);
        let mut f = self.open_bundle_file(bk)?;
        let mut buf = Vec::with_capacity(4096);
        f.read_to_end(&mut buf)?;

        let flat = FlatView::from_bytes(&buf)?;
        let bundle = flat.unflatten_bundle();
        Ok(bundle)
    }

    #[allow(dead_code)]
    pub fn delete(&mut self, bk: BundleKey) -> io::Result<()> {
        trace!("delete {:?}", bk);
        self.remove_bundle_file(bk)
    }

    pub fn contains(&mut self, bk: BundleKey) -> bool {
        self.fs.exists(&self.bundle_path(bk))
    }

    pub fn commit(&mut self) -> io::Result<()> {
        let lock_path = self.path.join("commit.lock");
        File::create(&lock_path)?;
        self.fs.commit()?;
        fs::remove_file(&lock_path)?;
        Ok(())
    }
}
