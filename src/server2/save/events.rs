use server_types::*;

use engine::update::{Delta, flags};
use engine::update::event::*;
use server_extra::{Extra, AnyValue, View, HashViewMut, ArrayViewMut};


// Helper functions, used by both `events` and the main `Component` impl for ward_map.

pub fn add_ward<'a>(e_wm: ArrayViewMut<'a>,
                    owner: Stable<ClientId>,
                    owner_name: &str,
                    region: Region<V2>) -> HashViewMut<'a> {
    let mut e_ward = e_wm.push_hash();
    e_ward.borrow().set("owner", owner.into_value());
    e_ward.borrow().set("owner_name", owner_name.to_owned().into_value());
    e_ward.borrow().set("region", region.into_value());
    e_ward
}

pub fn remove_ward(mut e_wm: ArrayViewMut,
                   owner: Stable<ClientId>) {
    let mut i = 0;
    while i < e_wm.borrow().len() {
        let owner_id = {
            let e_ward = e_wm.borrow().get(i).unwrap_hash();
            e_ward.get("owner")
                  .and_then(View::as_value)
                  .and_then(Stable::<ClientId>::from_value)
                  .expect("bad owner in ward extra")
        };
        if owner_id == owner {
            e_wm.borrow().swap_remove(i);
        } else {
            i += 1;
        }
    }
}

pub fn add_permit<'a>(e_wp: HashViewMut<'a>,
                      owner: Stable<ClientId>,
                      user: &str) {
    e_wp.get_or_set_hash(&owner.unwrap().to_string())
        .set(user, true.into_value());
}

pub fn remove_permit<'a>(e_wp: HashViewMut<'a>,
                         owner: Stable<ClientId>,
                         user: &str) {
    e_wp.get_or_set_hash(&owner.unwrap().to_string())
        .remove(user);
}


pub fn event_ward_add(evt: &WardAdd, extra: &mut Extra) {
    let e_wm = extra.get_mut("ward_map")
        .expect("missing ward_map in plane extra")
        .unwrap_array();
    add_ward(e_wm, evt.owner, &evt.owner_name, evt.region);
}

pub fn event_ward_remove(evt: &WardRemove, extra: &mut Extra) {
    let e_wm = extra.get_mut("ward_map")
        .expect("missing ward_map in plane extra")
        .unwrap_array();
    remove_ward(e_wm, evt.owner);
}

pub fn event_permit_add(evt: &PermitAdd, extra: &mut Extra) {
    let e_wp = extra.get_or_set_hash("ward_permits");
    add_permit(e_wp, evt.owner, &evt.user);
}

pub fn event_permit_remove(evt: &PermitRemove, extra: &mut Extra) {
    let e_wp = extra.get_or_set_hash("ward_permits");
    remove_permit(e_wp, evt.owner, &evt.user);
}

// If any of these flags is set, then the final ward map (after applying all update events) is
// stored in the delta.
pub fn ward_map_overridden(delta: &Delta, id: PlaneId) -> bool {
    delta.plane_flags(id).intersects(
        flags::P_CREATED | flags::P_DESTROYED | flags::P_WARD_MAP)
}


pub fn event_key_value_set(evt: &KeyValueSet, extra: &mut Extra) {
    extra.get_or_set_hash("key_value").set(&evt.key, evt.value.clone());
}

pub fn event_key_value_clear(evt: &KeyValueClear, extra: &mut Extra) {
    extra.get_or_set_hash("key_value").remove(&evt.key);
}

pub fn key_value_overridden(delta: &Delta, id: AnyId) -> bool {
    expand_objs! {
        match id {
            $( AnyId::$Obj(id) => delta.$obj_flags(id).intersects(
                    flags::$O_CREATED | flags::$O_DESTROYED | flags::$O_KEY_VALUE), )*
        }
    }
}


pub fn looted_vaults_add(evt: &LootedVaultsAdd, extra: &mut Extra) {
    extra.get_or_set_array("looted_vaults")
        .push(evt.vault.into_value());
}

pub fn looted_vaults_overridden(delta: &Delta, id: PlaneId) -> bool {
    delta.plane_flags(id).intersects(
        flags::P_CREATED | flags::P_DESTROYED | flags::P_LOOTED_VAULTS)
}
