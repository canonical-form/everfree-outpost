use server_types::*;
use std::collections::hash_map::{HashMap, Entry, OccupiedEntry};
use std::hash::Hash;
use std::mem;
use std::sync::mpsc::{Sender, Receiver};

use common::proto::{control, game};
use common::proto::types::{LocalPos, LocalOffset, LocalTime};
use engine::component::crafting::{CraftingState, StationId};
use engine::component::dialog::{self, Dialog, ClientDialog};
use world::objects::{Activity, Item, Appearance};

use engine_thread::ToEngine;
use io::{Input, Output};
use messages::{ClientRequest, ClientResponse};
use util;
use vision::ToVision;

mod multistructure;


#[derive(Debug)]
pub enum ToConn {
    /// The engine finished loading the client object for user ID `.0`, and `.1` is its new
    /// (transient) ClientId.
    ClientLoaded(u32, ClientId),
    ClientUnloaded(u32),
    /// The engine wishes to kick the user with user ID `.0`, after sending the message `.1`.
    KickUser(u32, String),
    WireSubscribed(WireId),
    /// Vision acknowledges that it has unsubscribed wire `.0` from its previous client.
    WireUnsubscribed(WireId),

    WireResp(WireId, ClientResponse),
    #[allow(dead_code)]
    WireViewBase(WireId, V2),

    EngineTime(Time),
    Input(Input),
}

/// Maximum number of ticks that we can skip.  Sending `Begin`/`EndTick` periodically even when
/// there are no messages will help the client keep in sync with the server.
///
/// The current setting, 16, corresponds to sending a packet roughly every half-second.
const MAX_SKIPPED_TICKS: u8 = 16;


/// Tracks the state of a boolean variable in another module, including in-flight state changes.
///
/// Notify the `Toggle` each time you send a state-change request to the other module, and each
/// time you receive a change acknowledgement.  You can then query the `Toggle` for the current
/// requested state, the current acknowledged state, and whether any unacknowledged requests are
/// currently in flight.
struct Toggle {
    /// Number of requests sent.  Odd-numbered requests change the variable to `true`,
    /// even-numbered ones change it to `false`.
    sent: u32,
    /// Number of requests received.
    received: u32,
}

impl Toggle {
    pub fn new(init_value: bool) -> Toggle {
        if init_value {
            Toggle { sent: 1, received: 1 }
        } else {
            Toggle { sent: 0, received: 0 }
        }
    }

    pub fn requested_value(&self) -> bool {
        self.sent & 1 != 0
    }

    pub fn acked_value(&self) -> bool {
        self.received & 1 != 0
    }

    /// Returns the current requested and acknowledged value of the variable, or `None` if there
    /// are change requests in flight.
    pub fn value(&self) -> Option<bool> {
        if self.in_flight() {
            None
        } else {
            Some(self.requested_value())
        }
    }

    /// Check if there are any unacknowledged state change requests for this variable.
    pub fn in_flight(&self) -> bool {
        self.sent != self.received
    }

    /// Record a state-change request for this variable.
    ///
    /// Panics if the newly-requested state is identical to the previously-requested state.
    pub fn request(&mut self, state: bool) {
        assert!(state != self.requested_value());
        self.sent += 1;
    }

    /// Record a state-change acknowledgement for this variable.
    ///
    /// Panics if the newly-acknowledge state is identical to the previously-acknowledge state.
    pub fn ack(&mut self, state: bool) {
        assert!(state != self.acked_value());
        self.received += 1;
    }
}


/// Connection data.
///
/// A fully-established connection has an associated Client object in the engine, a subscription in
/// the vision module to receive updates on that Client, and a notification from the connected game
/// client that it is Ready.  The Connection object keeps track of which of these pieces are in
/// place.  Getting everything set up takes several steps (events), as does tearing it down again
/// at disconnect time.
///
/// There are two general kinds of state updates on Connection objects.
///
///  * Target change: This is when we change the "goal state" of the connection.  We send requests
///    to other modules that will bring about that goal state, if it is different from the current
///    state.  (When one Connection inherits IDs and connection data from another, it's possible
///    that the new Connection's goal is already partly achieved.)  These target changes usually
///    look like:
///
///         if conn.has_foo.requested_value() != X {
///             // Send a request to create/destroy `foo`
///             conn.has_foo.request(X);
///         }
///
///  * Progress update: This is when an external module makes progress toward the Gonnection's
///    goal.  We record the acknowledgement, then check if the connection has reached the goal.
///    These progress updates usually look like:
///
///         conn.has_foo.ack(X);
///         if conn.should_do_thing() {
///             // Goal reached - now we can do the thing
///         }
///
///
/// # `Toggle`s
///
/// The `has_foo` fields are `Toggle`s, which track not just the current state (do we have a "foo"
/// or not) but also any messages in flight that could change the state.  Having explicit knowledge
/// of the messages in flight is helpful when changing goals "on the fly".  By separating "where we
/// are" (i.e., what was the last requested state) from "where we're going" (the goal state), we
/// make it easy to determine at every point what messages should be sent.  (Namely: at points
/// where the goal changes, send messages to transition to the goal state, but only if it's
/// different from the last requested state.)  This is especially useful in handling complex (and
/// hopefully hypothetical) scenarios where connections bounce back and forth on the path between
/// "connected" and "disconnected" without ever fully achieving either goal.
struct Connection {
    user_id: u32,

    has_client: Toggle,
    client_id: ClientId,
    has_vision: Toggle,
    wire_ready: bool,
    closing: bool,

    /// This connection has been superseded by a newer one with the same `user_id`.  So there's no
    /// need to clear `uid_wire` when removing this connection.
    superseded: bool,

    /// `cpos` of the center of the client's current view of the world.
    view_base: V2,

    /// Did we send this client a `TickBegin` for the current tick?
    sent_begin: bool,
    /// Number of consecutive ticks for which no messages were sent.
    skipped_ticks: u8,

    /// Buffer for packing multiple `StructureAppear` events into a single `StructureAppearMulti`
    /// message.
    structure_buf: multistructure::Buffer,
}

impl Connection {
    fn new(uid: u32) -> Connection {
        Connection {
            user_id: uid,

            has_client: Toggle::new(false),
            client_id: ClientId(-1_i16 as u16),
            has_vision: Toggle::new(false),
            wire_ready: false,
            closing: false,
            superseded: false,

            view_base: scalar(0),
            sent_begin: false,
            skipped_ticks: 0,
            structure_buf: multistructure::Buffer::new(),
        }
    }

    fn should_vision_subscribe(&self) -> bool {
        !self.closing &&
        self.has_client.value() == Some(true) &&
        self.wire_ready &&
        !self.has_vision.requested_value()
    }

    fn should_send_conn_ready(&self) -> bool {
        !self.closing &&
        self.has_client.value() == Some(true) &&
        self.wire_ready &&
        self.has_vision.value() == Some(true)
    }

    fn should_delete(&self) -> bool {
        self.closing &&
        self.has_client.value() == Some(false) &&
        self.has_vision.value() == Some(false)
    }

    fn translate_request(&mut self, msg: game::Request) -> Option<ClientRequest> {
        use common::proto::game::Request::*;
        Some(match msg {
            InputStart(delay, dir, speed) =>
                ClientRequest::InputStart(delay, dir, speed),
            InputChange(rel_time, dir, speed) =>
                ClientRequest::InputChange(rel_time, dir, speed),

            PointInteract(pos) => ClientRequest::Interact(self.get_global_tile(pos)),
            PointDestroy(pos) => ClientRequest::Destroy(self.get_global_tile(pos)),
            PointUseItem(pos, item) => ClientRequest::UseItem(self.get_global_tile(pos), item),
            // TODO: distinguish targeted vs. untargeted item use
            UseItem(_when, item) => ClientRequest::UseItem(scalar(0), item),

            Chat(msg) => ClientRequest::Chat(msg),

            OpenInventory(()) => ClientRequest::DialogOpen(ClientDialog::Inventory),
            OpenAbilities(()) => ClientRequest::DialogOpen(ClientDialog::Abilities),
            OpenEquipment(()) => ClientRequest::DialogOpen(ClientDialog::Equipment),

            CloseDialog(()) => ClientRequest::DialogCancel,

            CreateCharacter(app_bytes) => {
                let appearance = match Appearance::from_bytes(&app_bytes) {
                    Ok(x) => x,
                    Err(e) => {
                        error!("invalid appearance bytes: {}", e);
                        return None;
                    },
                };
                ClientRequest::DialogAction(Box::new(
                        dialog::Action::CreateCharacter(appearance)))
            },
            MoveItem(src_inv, src_slot, dest_inv, dest_slot, count) =>
                ClientRequest::DialogAction(Box::new(dialog::Action::MoveItem {
                    src_inv: src_inv,
                    src_slot: src_slot,
                    dest_inv: dest_inv,
                    dest_slot: dest_slot,
                    count: count,
                })),
            CraftingStart(recipe_id, count) =>
                ClientRequest::DialogAction(Box::new(dialog::Action::CraftingStart {
                    recipe_id: recipe_id,
                    count: count,
                })),
            CraftingStop(_) =>
                ClientRequest::DialogAction(Box::new(dialog::Action::CraftingStop)),

            _ => return None,
        })
    }

    fn flush_buffers<F: FnMut(game::Response)>(&mut self, mut callback: F) {
        self.structure_buf.flush_gone(|base_id, rel_ids| {
            trace!("flush {} gone", rel_ids.len());
            if rel_ids.len() >= 2 {
                callback(game::Response::StructureGoneMulti(base_id, rel_ids));
            } else {
                let mut prev_id = base_id;
                for rel_id in rel_ids {
                    let id = StructureId(prev_id.unwrap() + rel_id as u32);
                    callback(game::Response::StructureGone(id));
                    prev_id = id;
                }
            }
        });

        self.structure_buf.flush_appear(|base_id, template, group| {
            trace!("flush {} appear", group.len());
            if group.len() >= 2 {
                callback(game::Response::StructureAppearMulti(base_id, template, group));
            } else {
                let mut prev_id = base_id;
                for s in group {
                    let id = StructureId(prev_id.unwrap() + s.rel_id as u32);
                    callback(game::Response::StructureAppear(id, template, s.pos));
                    prev_id = id;
                }
            }
        });
    }

    /// Translate a `WireResponse` into a low-level `game::Response`.  This involves converting
    /// position and timestamp fields, as well as batching up structure messages.  A call to method
    /// can produce any number of `game::Response`s (due to structure batching), so the caller must
    /// provide a callback instead of receiving a single return value.
    fn translate_response<F: FnMut(game::Response)>(&mut self,
                                                    msg: ClientResponse,
                                                    mut callback: F) {
        use common::proto::game::Response::*;

        match msg {
            ClientResponse::StructureAppear(_, _, _) |
            ClientResponse::StructureGone(_) => {},
            _ => self.flush_buffers(|resp| callback(resp)),
        }

        callback(match msg {
            ClientResponse::WorldInfo(now, day_night_cycle_ms) => {
                let local_now = LocalTime::from_global_64(now);
                let cycle_base = (now % day_night_cycle_ms as Time) as u32;
                Init(local_now, cycle_base, day_night_cycle_ms)
            },

            ClientResponse::EntityAppear(eid, appearance, name) =>
                EntityAppear(eid, appearance.to_bytes().into_vec(), name.into_string()),
            ClientResponse::EntityChange(eid, appearance, name) =>
                EntityChange(eid, appearance.to_bytes().into_vec(), name.into_string()),
            ClientResponse::EntityActivity(eid, time, act) => match *act {
                Activity::Stand { pos, dir } => {
                    let time = LocalTime::from_global_64(time);
                    let pos = LocalPos::from_global(pos);
                    EntityActStand(eid, time, pos, dir)
                },
                Activity::Walk { pos, velocity, dir } => {
                    let time = LocalTime::from_global_64(time);
                    let pos = LocalPos::from_global(pos);
                    let velocity = LocalOffset::from_global(velocity);
                    EntityActWalk(eid, time, pos, velocity, dir)
                },
                /*
                Activity::Busy { pos, anim, icon } => {
                    let pos = LocalPos::from_global(pos);
                    let time = LocalTime::from_global_64(time);
                    EntityActBusy(eid, time, pos, anim, icon)
                },
                */
            },
            ClientResponse::EntityGone(eid) =>
                // NB: client currently does not use the `time` field
                EntityGone(eid, LocalTime(0)),

            ClientResponse::TerrainChunk(cpos, blocks) =>
                TerrainChunk(self.cpos_to_index(cpos), blocks),
            ClientResponse::UnloadChunk(cpos) =>
                UnloadChunk(self.cpos_to_index(cpos)),

            ClientResponse::StructureAppear(id, pos, template) => {
                let px_pos = LocalPos::from_global(pos * TILE_SIZE);
                let pos = ((px_pos.x / TILE_SIZE as u16) as u8,
                           (px_pos.y / TILE_SIZE as u16) as u8,
                           (px_pos.z / TILE_SIZE as u16) as u8);
                self.structure_buf.appear(id, template, pos);
                return;
            },
            ClientResponse::StructureReplace(_id, _template) => {
                unimplemented!();   // FIXME
            },
            ClientResponse::StructureGone(id) => {
                self.structure_buf.gone(id);
                return;
            },

            ClientResponse::InventoryAppear(id, items, flags) => {
                let items = items.iter().map(|&i| encode_item(i)).collect();
                InventoryAppear(id, items, flags.bits())
            },
            ClientResponse::InventoryContents(id, items) => {
                let items = items.iter().map(|&i| encode_item(i)).collect();
                InventoryContents(id, items)
            },
            ClientResponse::InventoryFlags(id, flags) =>
                InventoryFlags(id, flags.bits()),
            ClientResponse::InventoryGone(id) =>
                InventoryGone(id),

            ClientResponse::ChatUpdate(msg) =>
                ChatUpdate(msg),
            ClientResponse::SyncStatus(status) =>
                SyncStatus(status as u8),
            ClientResponse::KickReason(msg) =>
                KickReason(msg),

            ClientResponse::AckRequest(t) =>
                AckRequest(LocalTime::from_global_64(t)),

            ClientResponse::NakRequest =>
                NakRequest(()),

            ClientResponse::CharInvs(invs) =>
                CharacterInventories(invs.main, invs.ability, invs.equip),

            ClientResponse::DialogOpen(d) => match *d {
                Dialog::PonyEdit(d) => OpenPonyEdit(d.name),
                Dialog::Inventory(d) => OpenInventory(d.id),
                Dialog::Abilities(d) => OpenAbilities(d.id),
                Dialog::Equipment(d) => OpenEquipment(d.inv_id, d.equip_id),
                Dialog::Container(d) => OpenContainer(d.id0, d.id1),
                Dialog::Crafting(d) => match d.station {
                    StationId::Structure(sid) =>
                        OpenCrafting(sid, d.class_mask, d.contents, d.char_main, d.char_ability),
                    _ => panic!("{:?} station type NYI", d.station),
                },
            },
            ClientResponse::DialogClose =>
                CancelDialog(()),

            ClientResponse::CraftingState(sid, cs) => match cs.as_ref().map(|x| &**x) {
                None => CraftingIdle(sid),
                Some(&CraftingState::Paused { recipe, count, saved_progress, reason: _ }) =>
                    CraftingPaused(sid, recipe, count, saved_progress),
                Some(&CraftingState::Active { recipe, count, start_progress, start_time }) => {
                    let start_time = LocalTime::from_global_64(start_time);
                    CraftingActive(sid, recipe, count, start_progress, start_time)
                },
            },

            ClientResponse::CameraFixed(pos) =>
                CameraFixed(LocalPos::from_global(pos)),

            ClientResponse::CameraPawn(pawn) =>
                CameraPawn(pawn),
        });
    }


    fn cpos_to_index(&self, cpos: V2) -> u16 {
        let x = cpos.x & LOCAL_MASK;
        let y = cpos.y & LOCAL_MASK;
        let idx = y * LOCAL_SIZE + x;
        idx as u16
    }

    /// Convert a `LocalPos` measured in tiles to global coordinates.
    fn get_global_tile(&self, local: LocalPos) -> V3 {
        let center = self.view_base.extend(0) * CHUNK_SIZE;
        local.to_global_bits(center, CHUNK_BITS + LOCAL_BITS)
    }
}

struct Channels {
    outputs: Sender<Output>,
    engine: Sender<ToEngine>,
    vision: Sender<ToVision>,
}

impl Channels {
    fn send_control(&mut self, resp: control::Response) {
        let output = Output::Control(resp);
        self.outputs.send(output).unwrap();
    }

    fn send_game(&mut self, wire_id: WireId, resp: game::Response) {
        let output = Output::Game(wire_id, resp);
        self.outputs.send(output).unwrap();
    }

    fn send_engine(&mut self, msg: ToEngine) {
        self.engine.send(msg).unwrap();
    }

    fn send_vision(&mut self, msg: ToVision) {
        self.vision.send(msg).unwrap();
    }

    fn send_req(&mut self, cid: ClientId, msg: ClientRequest) {
        self.send_engine(ToEngine::ClientReq(cid, msg));
    }
}

pub struct ConnState {
    channels: Channels,

    conns: HashMap<WireId, Connection>,
    /// Mapping from u32 user ID to WireId.  All connections have entires in this map.
    uid_wire: HashMap<u32, WireId>,

    /// Most recent time received from the engine.  This is used for `TickBegin`, `TickEnd`, and
    /// `Pong` messages.
    now: Time,
}

impl ConnState {
    pub fn new(outputs: Sender<Output>,
               to_engine: Sender<ToEngine>,
               to_vision: Sender<ToVision>) -> ConnState {
        ConnState {
            channels: Channels {
                outputs: outputs,
                engine: to_engine,
                vision: to_vision,
            },

            conns: HashMap::new(),
            uid_wire: HashMap::new(),

            now: 0,
        }
    }

    fn handle_msg(&mut self, msg: ToConn) {
        match msg {
            ToConn::ClientLoaded(uid, client_id) => {
                self.handle_client_loaded(uid, client_id);
            },

            ToConn::ClientUnloaded(uid) => {
                self.handle_client_unloaded(uid);
            },

            ToConn::KickUser(uid, msg) => {
                self.handle_kick_user(uid, msg);
            },

            ToConn::WireSubscribed(wire_id) => {
                self.handle_wire_subscribed(wire_id);
            },

            ToConn::WireUnsubscribed(wire_id) => {
                self.handle_wire_unsubscribed(wire_id);
            },

            ToConn::WireResp(wire_id, resp) => {
                self.handle_wire_resp(wire_id, resp);
            },

            ToConn::WireViewBase(wire_id, view_base) => {
                self.handle_wire_view_base(wire_id, view_base);
            },

            ToConn::EngineTime(now) => {
                self.handle_engine_time(now);
            },

            ToConn::Input(Input::Control(req)) => {
                self.handle_input_control(req);
            },

            ToConn::Input(Input::Game(wire, req)) => {
                self.handle_input_game(wire, req);
            },
        }
    }

    fn handle_input_control(&mut self, req: control::Request) {
        match req {
            control::Request::AddClient(wire_id, uid, name) => {
                let new_wire_id = WireId(wire_id);
                let mut new_conn = Connection::new(uid);

                if let Some(old_wire_id) = self.uid_wire.remove(&uid) {
                    debug!("superseding connection {:?} (uid {:?}, {:?})", old_wire_id, uid, name);
                    let mut old_conn = get_entry(&mut self.conns, old_wire_id)
                        .unwrap_or_else(|| panic!("no conn for uid_wire {:?} -> {:?}",
                                                  uid, wire_id));

                    self.channels.send_game(old_wire_id, game::Response::KickReason(
                            "logged in from another location".to_owned()));

                    // Steal the ClientId from the old connection, if it has one.
                    new_conn.has_client = mem::replace(
                        &mut old_conn.get_mut().has_client, Toggle::new(false));
                    new_conn.client_id = mem::replace(
                        &mut old_conn.get_mut().client_id, ClientId(-1_i16 as u16));

                    old_conn.get_mut().closing = true;
                    old_conn.get_mut().superseded = true;

                    Self::maybe_vision_unsubscribe(
                        old_conn.get_mut(), old_wire_id, &mut self.channels);
                    Self::maybe_delete(old_conn, &mut self.uid_wire, &mut self.channels);
                }

                debug!("adding connection for {:?}, uid {}", wire_id, uid);

                Self::maybe_user_connect(&mut new_conn, name, &mut self.channels);

                // We know new_conn.wire_ready is false, so we have to stay in the `opening` state
                // until that changes.

                assert!(!self.conns.contains_key(&new_wire_id));
                self.conns.insert(new_wire_id, new_conn);
                assert!(!self.uid_wire.contains_key(&uid));
                self.uid_wire.insert(uid, new_wire_id);
            },

            control::Request::RemoveClient(wire_id) => {
                let wire_id = WireId(wire_id);
                let mut conn = unwrap_or_error!(get_entry(&mut self.conns, wire_id),
                        "RemoveClient: unknown {:?}", wire_id);

                if conn.get().closing {
                    info!("RemoveClient: already closing {:?}", wire_id);
                    return;
                }

                debug!("removing connection {:?} (uid {:?})", wire_id, conn.get().user_id);


                conn.get_mut().closing = true;

                Self::maybe_user_disconnect(conn.get_mut(), &mut self.channels);
                Self::maybe_vision_unsubscribe(conn.get_mut(), wire_id, &mut self.channels);
                Self::maybe_delete(conn, &mut self.uid_wire, &mut self.channels);
            },

            _ => {
                warn!("unsupported control request: {:?}", req);
            },
        }
    }

    fn handle_input_game(&mut self, wire_id: WireId, req: game::Request) {
        // There is no acknowledgement for engine-side connection closes, so we may occasionally
        // get messages from wrapper about a closed connection.
        let conn = unwrap_or_warn!(self.conns.get_mut(&wire_id),
                                   "game request for unknown {:?}", wire_id);
        if conn.closing {
            info!("game request: already closing {:?}", wire_id);
        }

        match req {
            game::Request::Ping(cookie) => {
                let now = LocalTime::from_global_64(self.now);
                self.channels.send_game(wire_id, game::Response::Pong(cookie, now));
            },

            game::Request::Ready(()) => {
                debug!("connection {:?} (uid {}) got Ready", wire_id, conn.user_id);
                if conn.wire_ready {
                    // This is user input, so we shouldn't panic even if it's bad.
                    warn!("got Ready for already wire-ready connection {:?}", wire_id);
                    return;
                }

                conn.wire_ready = true;

                Self::maybe_vision_subscribe(conn, wire_id, &mut self.channels);
                Self::maybe_send_conn_ready(conn, &mut self.channels);

            },

            _ => {
                if !conn.has_client.acked_value() {
                    warn!("client message for {:?}, which has no ClientId: {:?}", wire_id, req);
                    return;
                }
                if conn.closing {
                    return;
                }

                if let Some(client_req) = conn.translate_request(req) {
                    self.channels.send_req(conn.client_id, client_req);
                } else {
                    warn!("bad request from {:?}", conn.client_id);
                    // TODO: do something about it
                }
            },
        }
    }

    fn handle_client_loaded(&mut self, uid: u32, client_id: ClientId) {
        let wire_id = *unwrap_or_error!(self.uid_wire.get(&uid),
                                        "ClientLoaded: unknown uid {:?}", uid);
        let conn = self.conns.get_mut(&wire_id)
            .unwrap_or_else(|| panic!("missing conn for {:?} (uid {})", wire_id, uid));
        debug!("connection {:?} (uid {}) got client {:?}", wire_id, uid, client_id);

        conn.has_client.ack(true);
        conn.client_id = client_id;

        Self::maybe_vision_subscribe(conn, wire_id, &mut self.channels);
        Self::maybe_send_conn_ready(conn, &mut self.channels);
    }

    fn handle_client_unloaded(&mut self, uid: u32) {
        let wire_id = *unwrap_or_error!(self.uid_wire.get(&uid),
                                        "ClientUnloaded: unknown uid {:?}", uid);
        let mut conn = get_entry(&mut self.conns, wire_id)
            .unwrap_or_else(|| panic!("missing conn for {:?} (uid {})", wire_id, uid));
        debug!("connection {:?} (uid {}) client destroyed", wire_id, uid);

        conn.get_mut().has_client.ack(false);
        conn.get_mut().client_id = ClientId(-1_i16 as u16);

        Self::maybe_delete(conn, &mut self.uid_wire, &mut self.channels);
    }

    fn handle_kick_user(&mut self, uid: u32, msg: String) {
        let wire_id = unwrap_or_error!(self.uid_wire.remove(&uid),
                                       "KickUser for unknown uid {:?}", uid);
        let mut conn = get_entry(&mut self.conns, wire_id)
            .unwrap_or_else(|| panic!("missing conn for {:?} (uid {})", wire_id, uid));

        if conn.get().closing {
            info!("KickUser: already closing {:?}", wire_id);
            return;
        }

        debug!("connection {:?} (uid {}) kick: {:?}", wire_id, uid, msg);

        conn.get_mut().closing = true;

        Self::maybe_user_disconnect(conn.get_mut(), &mut self.channels);
        Self::maybe_vision_unsubscribe(conn.get_mut(), wire_id, &mut self.channels);
        Self::maybe_delete(conn, &mut self.uid_wire, &mut self.channels);
    }

    fn handle_wire_subscribed(&mut self, wire_id: WireId) {
        let conn = unwrap_or_error!(self.conns.get_mut(&wire_id),
                                    "WireSubscribed: unknown {:?}", wire_id);
        debug!("connection {:?} (uid {}) got vision subscription", wire_id, conn.user_id);

        conn.has_vision.ack(true);

        Self::maybe_send_conn_ready(conn, &mut self.channels);
    }

    fn handle_wire_unsubscribed(&mut self, wire_id: WireId) {
        let mut conn = unwrap_or_error!(get_entry(&mut self.conns, wire_id),
                                        "WireUnsbscribed: unknown {:?}", wire_id);
        debug!("connection {:?} (uid {}) vision subscription destroyed",
               wire_id, conn.get().user_id);

        conn.get_mut().has_vision.ack(false);

        Self::maybe_delete(conn, &mut self.uid_wire, &mut self.channels);
    }

    fn handle_wire_resp(&mut self, wire_id: WireId, msg: ClientResponse) {
        let conn = unwrap_or_error!(self.conns.get_mut(&wire_id),
                                    "WireResp: unknown {:?}", wire_id);

        if conn.closing {
            return;
        }

        // Check if we need to send a `TickBegin` message for this client.
        if !conn.sent_begin {
            let time = LocalTime::from_global_64(self.now);
            self.channels.send_game(wire_id, game::Response::TickBegin(time));
            conn.sent_begin = true;
        }

        let channels = &mut self.channels;
        conn.translate_response(msg, |resp| {
            channels.send_game(wire_id, resp);
        });
    }

    fn handle_wire_view_base(&mut self, wire_id: WireId, view_base: V2) {
        let conn = unwrap_or_error!(self.conns.get_mut(&wire_id),
                                    "WireViewBase: unknown {:?}", wire_id);

        conn.view_base = view_base;
    }

    fn handle_engine_time(&mut self, now: Time) {
        // Send `TickEnd` with the previous timestamp to any clients that need it.
        let time = LocalTime::from_global_64(self.now);
        for (&wire_id, conn) in self.conns.iter_mut() {
            let should_send =
                if conn.sent_begin {
                    true
                } else {
                    conn.skipped_ticks += 1;
                    conn.skipped_ticks >= MAX_SKIPPED_TICKS
                };

            if should_send {
                let mut channels = &mut self.channels;
                conn.flush_buffers(|resp| {
                    channels.send_game(wire_id, resp);
                });
                channels.send_game(wire_id, game::Response::TickEnd(time));
                conn.sent_begin = false;
                conn.skipped_ticks = 0;
            }
        }

        self.now = now;
    }

    // Common state-change cases.  Note there are other state changes that don't go through one of
    // these functions - mainly in the AddClient handler.

    fn maybe_user_connect(conn: &mut Connection,
                          name: String,
                          channels: &mut Channels) {
        if !conn.has_client.requested_value() {
            trace!("  send: userconnect");
            channels.send_engine(ToEngine::UserConnect(conn.user_id, name));
            conn.has_client.request(true);
        }
    }

    fn maybe_user_disconnect(conn: &mut Connection,
                             channels: &mut Channels) {
        if conn.has_client.requested_value() {
            trace!("  send: disconnect");
            channels.send_engine(ToEngine::UserDisconnect(conn.user_id));
            conn.has_client.request(false);
        }
    }

    fn maybe_vision_unsubscribe(conn: &mut Connection,
                                wire_id: WireId,
                                channels: &mut Channels) {
        if conn.has_vision.requested_value() {
            trace!("  send: unsubscribe");
            channels.send_vision(ToVision::UnsubscribeWire(wire_id));
            conn.has_vision.request(false);
        }
    }

    fn maybe_vision_subscribe(conn: &mut Connection,
                              wire_id: WireId,
                              channels: &mut Channels) {
        if conn.should_vision_subscribe() {
            trace!("  send: subscribe");
            channels.send_vision(ToVision::SubscribeWire(wire_id, conn.client_id));
            conn.has_vision.request(true);
        }
    }

    fn maybe_send_conn_ready(conn: &mut Connection,
                             channels: &mut Channels) {
        if conn.should_send_conn_ready() {
            trace!("  send: connready");
            channels.send_req(conn.client_id, ClientRequest::ConnReady);
        }
    }

    fn maybe_delete(conn: OccupiedEntry<WireId, Connection>,
                    uid_wire: &mut HashMap<u32, WireId>,
                    channels: &mut Channels) {
        if conn.get().should_delete() {
            Self::do_delete(conn, uid_wire, channels);
        }
    }

    fn do_delete(conn: OccupiedEntry<WireId, Connection>,
                 uid_wire: &mut HashMap<u32, WireId>,
                 channels: &mut Channels) {
        trace!("  delete conn");
        if !conn.get().superseded {
            uid_wire.remove(&conn.get().user_id);
        }
        let wire_id = *conn.key();
        conn.remove();
        channels.send_control(
            control::Response::ClientRemoved(wire_id.unwrap()));
    }


    fn run(&mut self, recv: Receiver<ToConn>) {
        for msg in recv {
            self.handle_msg(msg);
        }
        info!("input channel disconnected");
    }
}


pub fn run_conn(recv: Receiver<ToConn>,
                outputs: Sender<Output>,
                to_engine: Sender<ToEngine>,
                to_vision: Sender<ToVision>) {
    let mut state = ConnState::new(outputs, to_engine, to_vision);
    state.run(recv);
}

pub fn start_conn(recv: Receiver<ToConn>,
                  outputs: Sender<Output>,
                  to_engine: Sender<ToEngine>,
                  to_vision: Sender<ToVision>) {
    util::spawn_named("conn", move || {
        run_conn(recv, outputs, to_engine, to_vision);
    });
}


fn encode_item(i: Item) -> (u8, u8, ItemId) {
    // TODO: change protocol encoding of items
    const TAG_EMPTY: u8 = 0;
    const TAG_BULK: u8 = 1;

    if i.is_none() {
        (TAG_EMPTY, 0, 0)
    } else {
        (TAG_BULK, i.count, i.id)
    }
}

fn get_entry<'a, K, V>(map: &'a mut HashMap<K, V>, key: K) -> Option<OccupiedEntry<'a, K, V>>
        where K: Hash+Eq+'a, V: 'a {
    match map.entry(key) {
        Entry::Occupied(e) => Some(e),
        Entry::Vacant(_) => None,
    }
}
