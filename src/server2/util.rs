use std::fs;
use std::io;
use std::path::Path;
use std::os::unix::fs::FileTypeExt;
use std::os::unix::net::UnixListener;
use std::thread::{self, JoinHandle};

pub fn encode_rle16<I: Iterator<Item=u16>>(iter: I) -> Vec<u16> {
    let mut result = Vec::new();

    let mut iter = iter.peekable();
    while !iter.peek().is_none() {
        let cur = iter.next().unwrap();

        let mut count = 1u16;
        while count < 0x0fff && iter.peek().map(|&x| x) == Some(cur) {
            iter.next();
            count += 1;
        }
        if count > 1 {
            result.push(0xf000 | count);
        }
        result.push(cur);
    }

    result
}

/// Like `UnixListener::bind`, but tries to remove the old socket if `AddrInUse`.
pub fn unix_bind(path: &Path) -> io::Result<UnixListener> {
    match UnixListener::bind(path) {
        Ok(s) => return Ok(s),
        Err(e) =>
            if e.kind() == io::ErrorKind::AddrInUse {}  // keep going
            else { return Err(e) },
    }

    // This check is racy, but that's okay.  It's only for debugging purposes.
    let metadata = try!(fs::metadata(path));
    assert!(metadata.file_type().is_socket(),
            "tried to bind unix socket to non-socket path {:?}", path);

    try!(fs::remove_file(path));
    UnixListener::bind(path)
}

pub fn spawn_named<F, T>(name: &str, f: F) -> JoinHandle<T>
        where F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
    thread::Builder::new()
        .name(name.to_owned())
        .spawn(f)
        .unwrap()
}

