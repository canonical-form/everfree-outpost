#![crate_name = "multipython"]

#![feature(
    optin_builtin_traits,
    proc_macro_hygiene,
    raw,
)]

extern crate core;
extern crate libc;
pub extern crate python3_sys;
extern crate syntax_exts;

#[macro_use] pub mod macros;
pub mod reexports;

#[macro_use] pub mod module;
#[macro_use] pub mod class;

pub mod api;
pub mod builtin;
#[macro_use] pub mod conv;
pub mod exc;
pub mod interp;
pub mod marker;
pub mod ptr;
pub mod util;

#[cfg(test)] mod test;


pub use self::interp::Interp;

pub use exc::PyResult;
pub use ptr::{PyBox, PyRef};
