use python3_sys::*;

pub use exc::{PyExc, PyResult};
pub use ptr::{PyBox, PyRef};


pub mod object {
    use std::ffi::{CString, CStr};
    use std::ptr;
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn call(callable: PyRef, args: PyRef, kw: Option<PyRef>) -> PyResult<PyBox> {
        unsafe {
            let kw_ptr = match kw {
                None => ptr::null_mut(),
                Some(kw) => kw.as_ptr(),
            };
            PyBox::new(PyObject_Call(callable.as_ptr(), args.as_ptr(), kw_ptr))
        }
    }

    pub fn is_instance(obj: PyRef, cls: PyRef) -> bool {
        unsafe { PyObject_IsInstance(obj.as_ptr(), cls.as_ptr()) != 0 }
    }

    pub fn type_(obj: PyRef) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyObject_Type(obj.as_ptr())) }
    }


    pub fn get_attr_cstr(dct: PyRef, attr_name: &CStr) -> PyResult<PyBox> {
        unsafe {
            PyBox::new(PyObject_GetAttrString(dct.as_ptr(), attr_name.as_ptr()))
        }
    }

    pub fn get_attr_str(dct: PyRef, attr_name: &str) -> PyResult<PyBox> {
        get_attr_cstr(dct, &CString::new(attr_name).unwrap())
    }


    pub fn set_attr_cstr(dct: PyRef, attr_name: &CStr, val: PyRef) -> PyResult<()> {
        unsafe {
            let ret = PyObject_SetAttrString(dct.as_ptr(), attr_name.as_ptr(), val.as_ptr());
            pycheck!(ret == 0);
            Ok(())
        }
    }

    pub fn set_attr_str(dct: PyRef, attr_name: &str, val: PyRef) -> PyResult<()> {
        set_attr_cstr(dct, &CString::new(attr_name).unwrap(), val)
    }

    pub fn repr(obj: PyRef) -> PyResult<String> {
        let s = try!(unsafe { PyBox::new(PyObject_Repr(obj.as_ptr())) });
        super::unicode::as_string(s.borrow())
    }
}

pub mod bytes {
    use std::ptr;
    use std::slice;
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyBytes_Check(obj.as_ptr()) != 0 }
    }

    pub fn from_bytes(b: &[u8]) -> PyResult<PyBox> {
        pyassert!(b.len() as u64 <= PY_SSIZE_T_MAX as u64);
        unsafe {
            let ptr = PyBytes_FromStringAndSize(b.as_ptr() as *const i8,
                                                b.len() as Py_ssize_t);
            PyBox::new(ptr)
        }
    }

    pub fn as_bytes(obj: PyRef) -> PyResult<Vec<u8>> {
        unsafe {
            let mut ptr = ptr::null_mut();
            let mut len = 0;
            let ret = PyBytes_AsStringAndSize(obj.as_ptr(), &mut ptr, &mut len);
            pycheck!(ret == 0);
            let bytes = slice::from_raw_parts(ptr as *const u8, len as usize);
            Ok(bytes.to_owned())
        }
    }
}

pub mod unicode {
    use libc::c_char;
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyUnicode_Check(obj.as_ptr()) != 0 }
    }

    pub fn from_str(s: &str) -> PyResult<PyBox> {
        pyassert!(s.len() as u64 <= PY_SSIZE_T_MAX as u64);
        unsafe {
            let ptr = PyUnicode_FromStringAndSize(s.as_ptr() as *const i8,
                                                  s.len() as Py_ssize_t);
            PyBox::new(ptr)
        }
    }

    pub fn encode_utf8(obj: PyRef) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyUnicode_AsUTF8String(obj.as_ptr())) }
    }

    pub fn as_string(obj: PyRef) -> PyResult<String> {
        let py_bytes = try!(encode_utf8(obj));
        let bytes = try!(super::bytes::as_bytes(py_bytes.borrow()));
        unsafe {
            Ok(String::from_utf8_unchecked(bytes))
        }
    }

    /// Compare a Python string object to an ASCII `&str`.  If `s` contains non-ASCII characters,
    /// the return value is undefined.
    pub fn eq_ascii(obj: PyRef, s: &str) -> bool {
        assert!(s.ends_with("\0"), "expected a null-terminated `str`");
        unsafe {
            PyUnicode_CompareWithASCIIString(obj.as_ptr(),
                                             s.as_ptr() as *const c_char) == 0
        }
    }
}

pub mod int {
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyLong_Check(obj.as_ptr()) != 0 }
    }

    pub fn from_u64(val: u64) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyLong_FromUnsignedLongLong(val)) }
    }

    pub fn from_i64(val: i64) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyLong_FromLongLong(val)) }
    }

    pub fn as_u64(obj: PyRef) -> PyResult<u64> {
        let val = unsafe { PyLong_AsUnsignedLongLong(obj.as_ptr()) };
        pycheck!();
        Ok(val)
    }

    pub fn as_i64(obj: PyRef) -> PyResult<i64> {
        let val = unsafe { PyLong_AsLongLong(obj.as_ptr()) };
        pycheck!();
        Ok(val)
    }
}

pub mod eval {
    use python3_sys::*;
    use super::{PyBox, PyRef};

    pub fn get_builtins() -> PyBox {
        unsafe {
            let ptr = PyEval_GetBuiltins();
            PyRef::new_non_null(ptr).to_box()
        }
    }
}

pub mod bool {
    use python3_sys::*;
    use super::{PyRef, PyResult};

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyBool_Check(obj.as_ptr()) != 0 }
    }

    pub fn false_() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(Py_False()) }
    }

    pub fn true_() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(Py_True()) }
    }

    pub fn from_bool(b: bool) -> PyRef<'static> {
        if b { true_() } else { false_() }
    }

    pub fn as_bool(obj: PyRef) -> PyResult<bool> {
        if obj == true_() {
            Ok(true)
        } else if obj == false_() {
            Ok(false)
        } else {
            pyraise!(type_error, "expected bool");
        }
    }
}

pub mod float {
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyFloat_Check(obj.as_ptr()) != 0 }
    }

    pub fn from_f64(val: f64) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyFloat_FromDouble(val)) }
    }

    pub fn as_f64(obj: PyRef) -> PyResult<f64> {
        let val = unsafe { PyFloat_AsDouble(obj.as_ptr()) };
        pycheck!();
        Ok(val)
    }
}

pub mod dict {
    use std::ffi::{CString, CStr};
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn new() -> PyResult<PyBox> {
        unsafe {
            PyBox::new(PyDict_New())
        }
    }

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyDict_Check(obj.as_ptr()) != 0 }
    }

    pub fn get_item_cstr<'a>(dct: PyRef<'a>, key: &CStr) -> PyResult<Option<PyRef<'a>>> {
        let val = unsafe {
            PyRef::new_opt(PyDict_GetItemString(dct.as_ptr(), key.as_ptr()))
        };
        pycheck!();
        Ok(val)
    }

    pub fn get_item_str<'a>(dct: PyRef<'a>, key: &str) -> PyResult<Option<PyRef<'a>>> {
        get_item_cstr(dct, &CString::new(key).unwrap())
    }

    pub fn get_item<'a>(dct: PyRef<'a>, key: PyRef) -> PyResult<Option<PyRef<'a>>> {
        let val = unsafe {
            PyRef::new_opt(PyDict_GetItem(dct.as_ptr(), key.as_ptr()))
        };
        pycheck!();
        Ok(val)
    }

    pub fn set_item_cstr(dct: PyRef, key: &CStr, val: PyRef) -> PyResult<()> {
        unsafe {
            let ret = PyDict_SetItemString(dct.as_ptr(), key.as_ptr(), val.as_ptr());
            pycheck!(ret == 0);
            Ok(())
        }
    }

    pub fn set_item_str(dct: PyRef, key: &str, val: PyRef) -> PyResult<()> {
        set_item_cstr(dct, &CString::new(key).unwrap(), val)
    }

    pub fn set_item(dct: PyRef, key: PyRef, val: PyRef) -> PyResult<()> {
        unsafe {
            let ret = PyDict_SetItem(dct.as_ptr(), key.as_ptr(), val.as_ptr());
            pycheck!(ret == 0);
            Ok(())
        }
    }

    pub fn del_item_cstr(dct: PyRef, key: &CStr) -> PyResult<()> {
        unsafe {
            let ret = PyDict_DelItemString(dct.as_ptr(), key.as_ptr());
            pycheck!(ret == 0);
            Ok(())
        }
    }

    pub fn del_item_str(dct: PyRef, key: &str) -> PyResult<()> {
        del_item_cstr(dct, &CString::new(key).unwrap())
    }

    pub fn del_item(dct: PyRef, key: PyRef) -> PyResult<()> {
        unsafe {
            let ret = PyDict_DelItem(dct.as_ptr(), key.as_ptr());
            pycheck!(ret == 0);
            Ok(())
        }
    }

    pub fn clear(dct: PyRef) {
        unsafe { PyDict_Clear(dct.as_ptr()) };
    }

    pub fn contains(dct: PyRef, key: PyRef) -> PyResult<bool> {
        unsafe {
            let ret = PyDict_Contains(dct.as_ptr(), key.as_ptr());
            pycheck!(ret >= 0);
            Ok(ret != 0)
        }
    }

    /// Return a `list` object containing all the items from the dictionary.
    pub fn items(dct: PyRef) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyDict_Items(dct.as_ptr())) }
    }
}

pub mod list {
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn new() -> PyResult<PyBox> {
        unsafe { PyBox::new(PyList_New(0)) }
    }

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyList_Check(obj.as_ptr()) != 0 }
    }

    pub fn size(t: PyRef) -> PyResult<usize> {
        let val = unsafe { PyList_Size(t.as_ptr()) as usize };
        pycheck!();
        Ok(val)
    }

    pub fn get_item<'a>(t: PyRef<'a>, pos: usize) -> PyResult<PyRef<'a>> {
        unsafe { PyRef::new(PyList_GetItem(t.as_ptr(), pos as Py_ssize_t)) }
    }

    pub fn append(l: PyRef, item: PyRef) -> PyResult<()> {
        unsafe {
            let ret = PyList_Append(l.as_ptr(), item.as_ptr());
            pycheck!(ret == 0);
            Ok(())
        }
    }
}

pub mod tuple {
    use std::collections::Bound;
    use std::ops::RangeBounds;
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn new(len: usize) -> PyResult<PyBox> {
        pyassert!(len as u64 <= PY_SSIZE_T_MAX as u64);
        unsafe { PyBox::new(PyTuple_New(len as Py_ssize_t)) }
    }

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyTuple_Check(obj.as_ptr()) != 0 }
    }

    pub fn size(t: PyRef) -> PyResult<usize> {
        let val = unsafe { PyTuple_Size(t.as_ptr()) as usize };
        pycheck!();
        Ok(val)
    }

    pub fn get_item<'a>(t: PyRef<'a>, pos: usize) -> PyResult<PyRef<'a>> {
        unsafe { PyRef::new(PyTuple_GetItem(t.as_ptr(), pos as Py_ssize_t)) }
    }

    pub unsafe fn set_item(t: PyRef, pos: usize, val: PyBox) -> PyResult<()> {
        let ret = PyTuple_SetItem(t.as_ptr(), pos as Py_ssize_t, val.unwrap());
        pycheck!(ret == 0);
        Ok(())
    }

    pub fn slice<R: RangeBounds<usize>>(t: PyRef, range: R) -> PyResult<PyBox> {
        let low = match range.start_bound() {
            Bound::Included(&x) => x,
            Bound::Excluded(&x) => x + 1,
            Bound::Unbounded => 0,
        };
        let high = match range.end_bound() {
            Bound::Included(&x) => x + 1,
            Bound::Excluded(&x) => x,
            Bound::Unbounded => size(t)?,
        };

        pyassert!(low as u64 <= PY_SSIZE_T_MAX as u64);
        pyassert!(high as u64 <= PY_SSIZE_T_MAX as u64);

        unsafe { PyBox::new(PyTuple_GetSlice(t.as_ptr(),
                                             low as Py_ssize_t,
                                             high as Py_ssize_t)) }
    }


    macro_rules! tuple_pack {
        ($count:expr, $pack:ident: ($($arg:ident $idx:expr),*)) => {
            pub fn $pack($($arg: PyBox),*) -> PyResult<PyBox> {
                #![allow(unused_unsafe)]    // when $count == 0
                unsafe {
                    let t = try!(new($count));
                    $(
                        try!(set_item(t.borrow(), $idx, $arg));
                    )*
                    Ok(t)
                }
            }
        };
    }

    tuple_pack!(0, pack0: ());
    tuple_pack!(1, pack1: (a 0));
    tuple_pack!(2, pack2: (a 0, b 1));
    tuple_pack!(3, pack3: (a 0, b 1, c 2));
    tuple_pack!(4, pack4: (a 0, b 1, c 2, d 3));
    tuple_pack!(5, pack5: (a 0, b 1, c 2, d 3, e 4));
    tuple_pack!(6, pack6: (a 0, b 1, c 2, d 3, e 4, f 5));
}

pub mod iter {
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn next(obj: PyRef) -> PyResult<Option<PyBox>> {
        let result = unsafe { PyBox::new_opt(PyIter_Next(obj.as_ptr())) };
        // PyIter_Next returns NULL both for "end of iteration" and "error during iteration".  The
        // two can be distinguished by checking for a pending exception.
        if result.is_none() {
            pycheck!();
        }
        Ok(result)
    }
}

pub mod module {
    use std::ffi::{CString, CStr};
    use libc::c_void;
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn new_cstr(name: &CStr) -> PyResult<PyBox> {
        unsafe { PyBox::new(PyModule_New(name.as_ptr())) }
    }

    pub fn new(name: &str) -> PyResult<PyBox> {
        new_cstr(&CString::new(name).unwrap())
    }

    pub unsafe fn create(mod_def: *mut PyModuleDef) -> PyResult<PyBox> {
        PyBox::new(PyModule_Create(mod_def))
    }

    pub fn find(mod_def: *mut PyModuleDef) -> PyResult<PyBox> {
        let r = try!(unsafe { PyRef::new(PyState_FindModule(mod_def)) });
        Ok(r.to_box())
    }

    pub fn state(module: PyRef) -> *mut c_void {
        unsafe { PyModule_GetState(module.as_ptr()) }
    }

    pub fn register(module: PyRef) -> PyResult<()> {
        use api as py;

        let modules = try!(py::sys::get_object_str("modules"));

        let name = try!(py::object::get_attr_str(module, "__name__"));

        let present = try!(py::dict::contains(modules.borrow(), name.borrow()));
        pyassert!(!present, system_error, "module is already present");

        try!(py::dict::set_item(modules.borrow(), name.borrow(), module));

        Ok(())
    }
}

pub mod type_ {
    use std::mem;
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub unsafe fn from_spec(spec: *mut PyType_Spec) -> PyResult<PyBox> {
        PyBox::new(PyType_FromSpec(spec))
    }

    pub fn check(obj: PyRef) -> bool {
        unsafe { PyType_Check(obj.as_ptr()) != 0 }
    }

    /// Allocate a new instance of a type.
    ///
    /// This function is unsafe because it creates an object that passes `is_instance` checks, but
    /// is not a fully initialized instance of the type.
    pub unsafe fn instantiate(type_: PyRef) -> PyResult<PyBox> {
        pyassert!(check(type_));
        let alloc = PyType_GetSlot(type_.as_ptr() as *mut PyTypeObject, Py_tp_alloc);
        let alloc: Option<allocfunc> = mem::transmute(alloc);
        let alloc = alloc.unwrap_or(PyType_GenericAlloc);
        PyBox::new(alloc(type_.as_ptr() as *mut PyTypeObject, 0))
    }

    pub unsafe fn instantiate_var(type_: PyRef, len: usize) -> PyResult<PyBox> {
        pyassert!(check(type_));
        pyassert!(len as u64 <= PY_SSIZE_T_MAX as u64);
        let alloc = PyType_GetSlot(type_.as_ptr() as *mut PyTypeObject, Py_tp_alloc);
        let alloc: Option<allocfunc> = mem::transmute(alloc);
        let alloc = alloc.unwrap_or(PyType_GenericAlloc);
        PyBox::new(alloc(type_.as_ptr() as *mut PyTypeObject, 
                         len as Py_ssize_t))
    }
}

pub mod cfunc {
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    /// This function is unsafe because the caller must arrange for `def` to outlive the returned
    /// `PyBox`.
    pub unsafe fn new(def: *mut PyMethodDef,
                      slf: PyRef,
                      module: PyRef) -> PyResult<PyBox> {
        PyBox::new(PyCFunction_NewEx(def, slf.as_ptr(), module.as_ptr()))
    }
}

pub mod err {
    use std::ptr;
    use python3_sys::*;
    use super::{PyBox, PyExc};

    pub fn print() {
        unsafe { PyErr_Print() };

        // Call sys.stderr.flush().  This ensures we see the Python stack trace before the Rust one
        // (if any).
        let _ = (|| -> super::PyResult<()> {
            use api::{object, tuple};
            use util::import;

            let sys = try!(import("sys"));
            let stderr = try!(object::get_attr_str(sys.borrow(), "stderr"));
            let flush = try!(object::get_attr_str(stderr.borrow(), "flush"));
            try!(object::call(flush.borrow(), try!(tuple::pack0()).borrow(), None));
            Ok(())
        })();
    }

    pub fn normalize_exception(type_: &mut Option<PyBox>,
                               value: &mut Option<PyBox>,
                               traceback: &mut Option<PyBox>) {
        unsafe {
            let mut raw_type = type_.take().map_or(ptr::null_mut(), |b| b.unwrap());
            let mut raw_value = value.take().map_or(ptr::null_mut(), |b| b.unwrap());
            let mut raw_traceback = traceback.take().map_or(ptr::null_mut(), |b| b.unwrap());
            PyErr_NormalizeException(&mut raw_type, &mut raw_value, &mut raw_traceback);
            *type_ = PyBox::new_opt(raw_type);
            *value = PyBox::new_opt(raw_value);
            *traceback = PyBox::new_opt(raw_traceback);
        }
    }

    pub fn fetch() -> PyExc {
        let mut raw_type = ptr::null_mut();
        let mut raw_value = ptr::null_mut();
        let mut raw_traceback = ptr::null_mut();
        unsafe { 
            PyErr_Fetch(&mut raw_type, &mut raw_value, &mut raw_traceback);
            if raw_type.is_null() {
                raw_type = super::exc::system_error().to_box().unwrap();
            }
            let exc = PyExc::from_python(PyBox::new_opt(raw_type),
                                         PyBox::new_opt(raw_value),
                                         PyBox::new_opt(raw_traceback));

            // TODO: only do this for exceptions not caught (i.e., those that flow into an unwrap()
            // or warn_on_err!()).
            //warn!("caught python exception: {}", exc.get_rust().msg);
            if let Some(ref tb) = exc.get_python().traceback {
                super::traceback::print(tb.borrow());
            }

            exc
        }
    }

    pub fn occurred() -> bool {
        unsafe { !PyErr_Occurred().is_null() }
    }

    pub fn raise(exc: PyExc) {
        let exc = exc.unwrap_python_raw();
        unsafe {
            let raw_type = exc.type_.map_or(ptr::null_mut(), |b| b.unwrap());
            let raw_value = exc.value.map_or(ptr::null_mut(), |b| b.unwrap());
            let raw_traceback = exc.traceback.map_or(ptr::null_mut(), |b| b.unwrap());
            PyErr_Restore(raw_type, raw_value, raw_traceback);
        }
    }
}

pub mod traceback {
    use python3_sys::*;
    use super::{PyRef, PyResult};

    fn print_(tb: PyRef) -> PyResult<()> {
        use api::{object, tuple};
        use util::import;

        let sys = try!(import("sys"));
        let stderr = try!(object::get_attr_str(sys.borrow(), "stderr"));
        let ret = unsafe { PyTraceBack_Print(tb.as_ptr(), stderr.as_ptr()) };
        pycheck!(ret == 0);

        let flush = try!(object::get_attr_str(stderr.borrow(), "flush"));
        let unit = try!(tuple::pack0());
        try!(object::call(flush.borrow(), unit.borrow(), None));

        Ok(())
    }

    pub fn print(tb: PyRef) {
        let _ = print_(tb); // TODO
    }
}

pub mod exc {
    use python3_sys::*;
    use super::PyRef;

    pub fn system_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_SystemError) }
    }

    pub fn runtime_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_RuntimeError) }
    }

    pub fn type_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_TypeError) }
    }

    pub fn key_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_KeyError) }
    }

    pub fn value_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_ValueError) }
    }

    pub fn index_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_IndexError) }
    }

    pub fn attribute_error() -> PyRef<'static> {
        unsafe { PyRef::new_non_null(PyExc_AttributeError) }
    }
}

pub mod sys {
    use std::ffi::{CString, CStr};
    use python3_sys::*;
    use super::{PyBox, PyRef, PyResult};

    pub fn get_object_cstr(name: &CStr) -> PyResult<PyBox> {
        let r = unsafe { try!(PyRef::new(PySys_GetObject(name.as_ptr()))) };
        Ok(r.to_box())
    }

    pub fn get_object_str(name: &str) -> PyResult<PyBox> {
        get_object_cstr(&CString::new(name).unwrap())
    }
}


pub fn initialize() {
    unsafe { Py_InitializeEx(0) };
}

pub fn is_initialized() -> bool {
    unsafe { Py_IsInitialized() != 0 }
}

pub unsafe fn finalize() {
    Py_Finalize();
}

pub fn none() -> PyRef<'static> {
    unsafe { PyRef::new_non_null(Py_None()) }
}

pub fn not_implemented() -> PyRef<'static> {
    unsafe { PyRef::new_non_null(Py_NotImplemented()) }
}

