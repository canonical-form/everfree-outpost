use interp::{Interp, StateGuard};
use ptr::PyBox;

pub mod class_method;
pub mod module;
pub mod module_func;
pub mod property;


pub struct BuiltinTypes {
    pub class_method: PyBox,
    pub module: PyBox,
    pub module_func: PyBox,
    pub property: PyBox,
}

impl BuiltinTypes {
    pub fn new() -> BuiltinTypes {
        BuiltinTypes {
            // FIXME: handle errors properly
            class_method: class_method::register().unwrap(),
            module: module::register().unwrap(),
            module_func: module_func::register().unwrap(),
            property: property::register().unwrap(),
        }
    }

    pub fn get() -> StateGuard<Self> {
        Interp::get_state().unwrap()
    }
}
