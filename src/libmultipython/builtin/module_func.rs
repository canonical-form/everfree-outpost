//! "Module function" type, replacing `PyCFunction`.  The function is bound to a specific module,
//! allowing the Rust implementation of the function to rely on having certain module data
//! available.

use std::mem;
use std::ptr;
use libc::{c_char, c_int, c_uint, c_void};
use python3_sys::*;

use api as py;
use builtin::BuiltinTypes;
use exc::{self, PyResult};
use ptr::{PyBox, PyRef};


//
// Public API
//

/// Construct a new `ModuleFunc` object.  Unsafe because `func` must be safe to call when the first
/// argument is `module`.
pub unsafe fn new(func: unsafe fn(PyRef, PyRef) -> PyResult<PyBox>,
                  module: PyBox,
                  name: &'static str) -> PyResult<PyBox> {
    let bt = BuiltinTypes::get();
    new_typed(bt.module_func.borrow(), func, module, name)
}

/// Construct a new `ModuleFunc` object.  The `ty` argument must be the result of a previous
/// `func::register()` call.
///
/// This function is unsafe because passing the wrong type can cause undefined behavior, and for
/// the same reasons as `new`.
pub unsafe fn new_typed(ty: PyRef,
                        func: unsafe fn(PyRef, PyRef) -> PyResult<PyBox>,
                        module: PyBox,
                        name: &'static str) -> PyResult<PyBox> {
    let obj = py::type_::instantiate(ty)?;
    {
        let mf = &mut *(obj.as_ptr() as *mut ModuleFunc);
        ptr::write(&mut (*mf).func, func);
        ptr::write(&mut (*mf).module, module);
        ptr::write(&mut (*mf).name, name);
    }
    Ok(obj)
}



//
// Implementation
//

#[repr(C)]
struct ModuleFunc {
    #[allow(dead_code)]
    base: PyObject,
    func: unsafe fn(PyRef, PyRef) -> PyResult<PyBox>,
    module: PyBox,
    name: &'static str,
}

unsafe extern "C" fn dealloc(obj: *mut PyObject) {
    let mf = &mut *(obj as *mut ModuleFunc);
    ptr::drop_in_place(&mut (*mf).func);
    ptr::drop_in_place(&mut (*mf).module);
    ptr::drop_in_place(&mut (*mf).name);

    let ty = Py_TYPE(obj);
    let free: freefunc = mem::transmute(PyType_GetSlot(ty, Py_tp_free));
    free(obj as *mut c_void);
}

unsafe extern "C" fn repr(obj: *mut PyObject) -> *mut PyObject {
    let mf = &*(obj as *mut ModuleFunc);
    let r = py::unicode::from_str(&format!("<built-in function {}>", mf.name));
    exc::return_result(r)
}

unsafe extern "C" fn call(obj: *mut PyObject,
                          arg: *mut PyObject,
                          kw: *mut PyObject) -> *mut PyObject {
    let mf = &*(obj as *mut ModuleFunc);
    let r = call_inner(mf, arg, kw);
    exc::return_result(r)
}

unsafe fn call_inner(mf: &ModuleFunc,
                     arg: *mut PyObject,
                     kw: *mut PyObject) -> PyResult<PyBox> {
    let arg = PyRef::new_non_null(arg);
    pyassert!(kw.is_null(),
              type_error, "{}() does not support keyword arguments", mf.name);

    (mf.func)(mf.module.borrow(), arg)
}

pub fn register() -> PyResult<PyBox> {
    unsafe {
        macro_rules! slot {
            ($slot:expr, $val:expr) => {
                PyType_Slot { slot: $slot, pfunc: mem::transmute($val) }
            };
        }

        let mut slots = [
            slot!(Py_tp_dealloc, dealloc as destructor),
            slot!(Py_tp_repr, repr as reprfunc),
            slot!(Py_tp_call, call as ternaryfunc),
            slot!(0, 0_u64),
        ];

        let mut spec = PyType_Spec {
            name: "module_func\0".as_ptr() as *const c_char,
            basicsize: mem::size_of::<ModuleFunc>() as c_int,
            itemsize: 0,
            flags: Py_TPFLAGS_DEFAULT as c_uint,
            slots: slots.as_mut_ptr(),
        };

        let raw = PyType_FromSpec(&mut spec);
        PyBox::new(raw)
    }
}


#[cfg(test)]
mod test {
    use super::*;
    use builtin::module;
    use interp::Interp;

    #[test]
    fn basic() {
        fn func(_m: PyRef, _args: PyRef) -> PyResult<PyBox> {
            Ok(py::none().to_box())
        }

        let mut i = Interp::new();
        let s = i.enter(|| {
            let m = module::new("m", ()).unwrap();
            let f = unsafe { new(func, m, "f") }.unwrap();
            let r = py::object::call(f.borrow(),
                                     py::tuple::pack0().unwrap().borrow(),
                                     None).unwrap();
            py::object::repr(r.borrow()).unwrap()
        });
        assert_eq!(&s, "None")
    }
}
