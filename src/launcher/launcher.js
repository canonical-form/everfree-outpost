function unpack(arr) {
    return String.fromCharCode.apply(null, arr);
}

function pack(s) {
    var arr = new Uint8Array(s.length);
    packInto(s, arr);
    return arr;
}

function packInto(s, arr) {
    for (var i = 0; i < s.length; ++i) {
        arr[i] = s.charCodeAt(i);
    }
}

function base64(s) {
    return btoa(s).replace(/\+/g, '-').replace(/\//g, '_');
}

function unbase64(s) {
    return atob(s.replace(/-/g, '+').replace(/_/g, '/'));
}

function fromUTF8(s) {
    return decodeURIComponent(escape(s));
}

if (Promise.prototype.finally == null) {
    Promise.prototype.finally = function(f) {
        return this.then(function(x) {
            f();
            return x;
        }, function(e) {
            f();
            return Promise.reject(e);
        });
    };
}



var OP_AUTH_RESPONSE =      0x0014;
var OP_KICK_REASON =        0x8006;
var OP_AUTH_RESULT =        0x8022;
var OP_TICK_END =           0x802f;

var MODE_SSO =      0;
var MODE_LOCAL =    1;



function runXHR(url, type, args) {
    args = args || {};
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open(args.post != null ? 'POST' : 'GET', url.href, true);
        xhr.responseType = type;
        xhr.withCredentials = !!args.with_creds;
        xhr.timeout = args.timeout || 30000;

        if (args.onprogress != null) {
            xhr.onprogress = args.onprogress;
        }
        xhr.onload = function(evt) {
            if (xhr.status == 200) {
                resolve(evt);
            } else {
                reject(evt);
            }
        };
        xhr.onerror = function(evt) {
            reject(evt);
        };

        if (args.post != null) {
            xhr.send(args.post);
        } else {
            xhr.send();
        }
    });
}

function convert(buf, type) {
    if (type == 'document') {
        let text = new TextDecoder('utf-8').decode(buf);
        return new DOMParser().parseFromString(text, 'text/html');
    } else if (type == 'json') {
        let text = new TextDecoder('utf-8').decode(buf);
        return JSON.parse(text);
    } else if (type == 'text') {
        let text = new TextDecoder('utf-8').decode(buf);
        return text;
    } else if (type == 'image') {
        return new Promise(function(resolve, reject) {
            let blob = new Blob([buf], {'type': 'image/png'});
            let blob_href = URL.createObjectURL(blob);
            let img = new Image();
            img.onload = function(evt) {
                resolve(img);
            };
            img.onerror = function(evt) {
                reject('failed to load image');
            };
            img.src = blob_href;
        });
    } else if (type == 'blob') {
        return new Blob([buf]);
    } else if (type == 'arraybuffer') {
        return buf;
    } else {
        throw 'unsupported file type ' + type;
    }
}

function load(url, onprogress) {
    return runXHR(url, 'arraybuffer', { onprogress: onprogress }).then(function(evt) {
        if (onprogress != null) {
            onprogress(evt);
        }
        return evt.target.response;
    }, function(evt) {
        let basename = url.pathname.split('/').pop();
        return (evt.target.statusText || 'Connection error') +
            ' (while loading ' + basename + ')';
    });
}



/** @constructor */
function Socket() {
    this.ws = null;

    // If there is a pending Promise for this socket, `_reject` will be set to
    // the promise's `reject` function.  Otherwise, it's null.  `_handleError`
    // tries to reject the pending promise, if there is one, so that errors
    // during an operation will show up immediately.
    this._reject = null;
    this._error = null;

    this._msgs = [];
    this._msgFilter = null;
}

Socket.prototype._handleError = function(err) {
    if (this._reject != null) {
        this._reject(err);
        this._reject = null;
    } else {
        this._error = err;
    }
};

Socket.prototype.checkError = function() {
    if (this._error != null) {
        return Promise.reject(this._error);
    } else {
        return Promise.resolve(null);
    }
};

Socket.prototype._handleMessage = function(evt) {
    if (evt.data.byteLength < 2) {
        this._handleError('received message is too short (' +
            evt.data.byteLength + ' bytes)');
        return;
    }

    let view = new DataView(evt.data);
    let opcode = view.getUint16(0, true);

    // If the current message filter consumes the incoming message, then we're
    // done.
    if (this._msgFilter != null && this._msgFilter(opcode, evt.data)) {
        return;
    }

    // Try to handle some known opcodes.

    if (evt.data.byteLength >= 4 && opcode == OP_KICK_REASON) {
        var len = view.getUint16(2, true);
        var msg_utf8 = new Uint8Array(evt.data).subarray(4, 4 + len);
        this._handleError(fromUTF8(unpack(msg_utf8)));
        return;
    } else if (opcode == OP_TICK_END) {
        // Ignore.  The server sends these periodically to all clients, but
        // their contents are only important once the client has sent OP_READY,
        // and that happens only after loading has completed.
        return;
    }

    // Other messages get queued for the client to handle once loading is done
    this._msgs.push(evt.data);
    if (this._msgs.length >= 4) {
        // Too many queued messages
        var opcodes = [];
        for (var i = 0; i < this._msgs.length; ++i) {
            let view = new DataView(this._msgs[i]);
            let opcode = view.getUint16(0, true);
            opcodes.push(opcode.toString(16));
        }

        this._handleError('Unexpected message from server (opcodes: ' + opcodes.join(', ') + ')');
    }
};

Socket.prototype.connect = function(url) {
    if (this.ws != null) {
        throw 'this socket is already connected!';
    }

    return new Promise((resolve, reject) => {
        let ws = new OutpostEncryptedSocket(url);
        //ws.binaryType = 'arraybuffer';

        this._reject = reject;
        ws.onopen = (evt) => {
            resolve(evt);
            this._reject = null;
            ws.onopen = null;
        };
        ws.onmessage = (evt) => {
            this._handleMessage(evt);
        };
        ws.onerror = (evt) => {
            this._handleError('Error connecting to game server');
        };
        ws.onclose = (evt) => {
            this._handleError('Game server disconnected unexpectedly');
        };

        this.ws = ws;
    });
};

// Return a promise that resolves to the next message matching `filt`.
Socket.prototype._getNextMessage = function(filt, timeout) {
    if (timeout == null) {
        timeout = 30000;
    }

    // First, check if we have a queued message matching the filter.
    for (let i = 0; i < this._msgs.length; ++i) {
        let data = this._msgs[i];
        let view = new DataView(data);
        let opcode = view.getUint16(0, true);
        if (filt(opcode, data)) {
            return Promise.resolve(data);
        }
    }

    let timer = null;

    // No queued messages.  Wait up to `timeout` ms for an incoming one.
    return new Promise((resolve, reject) => {
        timer = setTimeout(() => {
            timer = null;
            reject(null);
        }, timeout);
        this._msgFilter = (opcode, data) => {
            if (filt(opcode, data)) {
                resolve(data);
                return true;
            } else {
                return false;
            }
        };
    }).finally(() => {
        if (timer != null) {
            clearTimeout(timer);
            timer = null;
        }
        this._msgFilter = null;
    });
};

Socket.prototype.getAuthChallenge = function() {
    return Promise.resolve({ mode: MODE_SSO, nonce: this.ws.cb_token });
};

Socket.prototype.sendAuthResponse = function(response) {
    let msg = new Uint8Array(2 + response.length);
    new DataView(msg.buffer).setUint16(0, OP_AUTH_RESPONSE, true);
    packInto(response, msg.subarray(2));
    this.ws.send(msg);
};

Socket.prototype.getAuthResult = function() {
    return this.checkError().then(() => {
        return this._getNextMessage((opcode, data) => opcode == OP_AUTH_RESULT);
    }).then((buf) => {
        let flags = new DataView(buf).getUint16(2, true);
        let data = new Uint8Array(buf).subarray(4);

        var s = fromUTF8(unpack(data));
        if ((flags & 1) == 0) {
            return Promise.reject(s);
        }

        return { name: s };
    });
};

function signAuthChallenge(mode, nonce) {
    if (mode == MODE_SSO) {
        let post = JSON.stringify({
            'challenge': base64(unpack(nonce)),
        });
        let url = new URL('sign_challenge', window.SSO_ENDPOINT);
        return runXHR(url, 'json', { post: post, with_creds: true }).then(function(evt) {
            return evt.target.response;
        }, function(evt) {
            return (evt.target.statusText || 'Connection error') +
                ' (while logging in)';
        }).then(function(json) {
            if (json['status'] != 'ok') {
                let reason = json['reason'];
                // Handle standardized error messages
                if (reason == 'not_logged_in') {
                    reason = 'You are not logged in.';
                }
                return Promise.reject(reason);
            }

            var response = unbase64(json['response']);
            return response;
        });
    } else {
        return Promise.reject('Auth mode ' + mode + ' is not supported');
    }
}



/** @constructor */
function ModuleLoader() {
    // Table of loaded modules.  Keyed on url.href, so we can easily handle
    // relative `require()` calls.
    this.modules = {};
    this.cur_module = null;
    // Base path for absolute imports.
    this.base_path = null;
}

ModuleLoader.prototype.setBasePath = function(url) {
    if (this.base_path != null && this.base_path != url) {
        throw 'tried to change base_path from ' + this.base_path + ' to ' + url;
    }
    this.base_path = url;
};

ModuleLoader.prototype.getModule = function(url) {
    return this.modules[url.href];
};

ModuleLoader.prototype.require = function(path) {
    if (this.cur_module == null) {
        throw 'require() called with no current module!';
    }

    if (!path.endsWith('.js')) {
        path = path + '.js';
    }

    let target;
    if (path.startsWith('./') || path.startsWith('../')) {
        // Relative import.
        target = new URL(path, this.cur_module);
    } else {
        // Old-style absolute import.  Requires `base_path` to be set.
        if (this.base_path == null) {
            throw 'absolute import of ' + path + ' is not supported in this mode';
        }
        target = new URL(path, this.base_path);
    }

    if (this.modules[target.href] == null) {
        throw 'module ' + target.href + ' is not available';
    }
    return this.modules[target.href];
};

function runScript(url, src) {
    let old_onerror;

    return new Promise((resolve, reject) => {
        let blob = new Blob(['(function() {', src, '})()\n',
            '//# sourceURL=' + url.href + '\n']);
        let blob_href = URL.createObjectURL(blob);

        let tag = document.createElement('script');
        tag.onload = (evt) => {
            resolve();
        };
        tag.onerror = (evt) => {
            reject(evt.error);
        };

        old_onerror = window.onerror;
        window.onerror = function(msg, url, line, col, err) {
            reject(err);
        };

        tag.src = blob_href;
        document.head.appendChild(tag);
    }).finally(() => {
        window.onerror = old_onerror;
    });
}

ModuleLoader.prototype.runModule = function(url, src) {
    if (this.cur_module != null) {
        throw 'already loading a module!';
    }
    if (this.modules[url.href] != null) {
        throw 'already loaded this module!';
    }

    // Set some global state, to be cleared when the module finishes
    // running.
    let old_exports = window.exports;
    let old_require = window.require;
    let old_module = this.cur_module;

    window.exports = {};
    window.require = (path) => this.require(path);
    this.cur_module = url;

    return runScript(url, src).then(() => {
        // Only save the module exports on success.
        this.modules[this.cur_module.href] = window.exports;
    }).finally(() => {
        window.exports = old_exports;
        window.require = old_require;
        this.cur_module = old_module;
    });
};



function openDB(name, version, onupgradeneeded) {
    return new Promise(function(resolve, reject) {
        let req = window.indexedDB.open(name, version);
        req.onsuccess = function(evt) {
            resolve(req.result);
        };
        req.onerror = function(evt) {
            reject(req.error);
        };
        req.onupgradeneeded = onupgradeneeded;
    });
}

function readDB(db, store, key) {
    return new Promise(function(resolve, reject) {
        let tx = db.transaction([store], 'readonly');
        let s = tx.objectStore(store);
        let req = s.get(key);

        let val = null;

        tx.oncomplete = function(evt) {
            resolve(val);
        };
        tx.onerror = function(evt) {
            reject(tx.error);
        };

        req.onsuccess = function(evt) {
            val = req.result;
        };
        req.onerror = function(evt) {
            reject(req.error);
        };
    });
}

function writeDB(db, store, key, value) {
    return new Promise(function(resolve, reject) {
        let tx = db.transaction([store], 'readwrite');
        let s = tx.objectStore(store);
        let req = s.put(value, key);

        tx.oncomplete = function(evt) {
            resolve(null);
        };
        tx.onerror = function(evt) {
            reject(tx.error);
        };

        // No `req.onsuccess` handler needed.  We resolve only upon completion
        // of the transaction.
        req.onerror = function(evt) {
            reject(req.error);
        };
    });
}

function FileCache() {
    this.db = null;
}

FileCache.prototype.init = function() {
    return openDB('outpost-launcher-cache', 1, (evt) => {
        console.log('upgrade', evt);
        let db = evt.target.result;
        if (evt.oldVersion < 1) {
            db.createObjectStore('files');
        }
    }).then((db) => {
        this.db = db;
    }).catch((e) => {
        console.warn('error opening cache database', e);
        this.db = null;
    });
}

// Get a file from the cache, if it is present.
FileCache.prototype.get = function(hash) {
    if (this.db == null) {
        return Promise.resolve(null);
    }

    return readDB(this.db, 'files', hash).catch((err) => {
        console.error('error reading file (' + hash + ') from cache', err);
        return null;
    });
};

// Load a file, either from the cache or from the network.
FileCache.prototype.load = function(url, hash, onprogress) {
    return this.get(hash).then((buf) => {
        if (buf != null) {
            return Promise.resolve(buf);
        } else {
            return this.populate(url, hash, onprogress);
        }
    });
};

FileCache.prototype.populate = function(url, hash, onprogress) {
    let buf = null;
    return load(url, onprogress).then((buf_) => {
        buf = buf_;
        return window.crypto.subtle.digest('SHA-256', buf);
    }).then((dl_hash_raw) => {
        // We specifically want normal base64, not web-safe.
        let dl_hash = btoa(unpack(new Uint8Array(dl_hash_raw)));
        if (dl_hash != hash) {
            let basename = url.pathname.split('/').pop();
            throw 'hash check failed for ' + basename;
        }

        if (this.db != null) {
            return writeDB(this.db, 'files', hash, buf);
        } else {
            return Promise.resolve();
        }
    }).catch((e) => {
        console.warn('error storing ' + url + ' (' + hash + ') in cache database', e);
    }).then(() => {
        return buf;
    });
};



function Launcher() {
    this.stat = 'Loading...';
    this.err = null;

    this.mod_loader = new ModuleLoader();
    this.file_cache = new FileCache();

    this.server_url = null;
    this.server_info = null;
    this.client_url = null;
    this.client_info = null;
    this.socket = null;
    this.login_name = null;
    this.files = null;
    this.data_blob = null;
    this.wasm_modules = {};
    this.client = null;

    // Downloaded and total size of the current file
    this.dl_cur_progress = 0;
    this.dl_cur_size = 0;
    // Bytes in all completed files
    this.dl_complete = 0;
    // Bytes in all files
    this.dl_total = 0;
    // `true` if the download stage is complete, even if dl_total is zero.
    this.after_dl = false;
}

Launcher.prototype.launch = function(server_url, redir) {
    return Promise.resolve(null).then(() => {
        return this.file_cache.init();
    }).then(() => {
        this.server_url = server_url;

        this.setMessage('Loading launcher...');
        return this.loadLauncher();
    }).then(() => {
        this.setMessage('Loading server info...');
        let url = new URL('server.json', this.server_url);
        return load(url).then((buf) => convert(buf, 'json'));
    }).then((info) => {
        console.log('server info', info);
        this.server_info = info;
        this.client_url = new URL(info['version'] + '/', window.VERSIONS_BASE);

        this.setMessage('Loading client info...');
        let url = new URL('manifest.json', this.client_url);
        return load(url).then((buf) => convert(buf, 'json'));
    }).then((info) => {
        console.log('client info', info);
        this.client_info = info;

        if (redir != null) {
            return this.redirect(redir);
        }

        if (info['module_base'] != null) {
            this.mod_loader.setBasePath(new URL(info['module_base'] + '/', this.client_url));
        }

        this.setMessage('Connecting to server...');
        this.socket = new Socket();
        return this.socket.connect(this.server_info.url);
    }).then(() => {
        return this.socket.getAuthChallenge();
    }).then((challenge) => {
        this.setMessage('Logging in...');
        return signAuthChallenge(challenge.mode, challenge.nonce);
    }).then((response) => {
        this.socket.sendAuthResponse(response);
        return this.socket.getAuthResult();
    }).then((result) => {
        this.login_name = result.name;
        console.log('logged in as ' + this.login_name);

        this.setMessage('Checking for updates...');
        return this.collectFiles();
    }).then(() => {
        return this.downloadFiles();
    }).then(() => {
        this.after_dl = true;
        this.setMessage('Loading files...');
        return this.processFiles();

    }).then(() => {
        this.client = new window.OutpostClient();

        this.setMessage('Initializing...');
        return this.client.initData(this.data_blob);
    }).then(() => {
        return this.client.initAsm(this.wasm_modules);
    }).then(() => {
        this.setMessage('Ready!');
        return this.client;

    }).catch((err) => {
        console.error(err);
        this.setMessage('Error: ' + err);
        return Promise.reject(err);
    });
};

Launcher.prototype.redirect = function(redir) {
    let path = this.client_info['redirects'][redir];
    if (path == null) {
        return Promise.reject('unknown redirect ' + redir);
    }
    let url = new URL(path, this.client_url);
    window.location.replace(url);
    return Promise.reject('Redirecting...');
};

Launcher.prototype.handleClientFile = function(path, buf) {
    if (path.endsWith('.js')) {
        let src = convert(buf, 'text');
        let url = new URL(path, this.client_url);
        return this.mod_loader.runModule(url, src);

    } else if (path.endsWith('.html')) {
        let fragment = convert(buf, 'document');
        while (fragment.body.firstChild) {
            document.body.appendChild(fragment.body.firstChild);
        }

    } else if (path.endsWith('.wasm')) {
        return WebAssembly.compile(buf).then((m) => {
            let basename = path.split('/').pop();
            this.wasm_modules[basename] = m;
        });

    } else {
        throw 'unknown file type for ' + path;
    }
};

Launcher.prototype.loadLauncher = function() {
    let crypto_url = new URL('crypto.wasm', window.location);
    let crypto_hash = window.LAUNCHER_HASHES['crypto.wasm']
    let encsocket_url = new URL('encsocket.js', window.location);
    let encsocket_hash = window.LAUNCHER_HASHES['encsocket.js']

    return this.file_cache.load(crypto_url, crypto_hash).then((buf) => {
        return WebAssembly.compile(buf);
    }).then((m) => {
        // Pass the crypto module to encsocket.js using a global variable.
        window.OUTPOST_CRYPTO_WASM = m;

        return this.file_cache.load(encsocket_url, encsocket_hash);
    }).then((buf) => {
        let src = convert(buf, 'text');
        return runScript(encsocket_url, src);
    }).then(() => {
        delete window.OUTPOST_CRYPTO_WASM;
    });
};

// Build a list of required files, and load any that are already available from
// the cache.
Launcher.prototype.collectFiles = function() {
    this.files = [];

    for (let f of this.client_info['files']) {
        let path = f[0];
        this.files.push({
            url: new URL(f[0], this.client_url),
            size: f[1],
            hash: f[2],
            buf: null,
            handler: (buf) => this.handleClientFile(path, buf),
        });
    }

    let pack = this.server_info['pack'];
    this.files.push({
        url: new URL(pack[0], this.server_url),
        size: pack[1],
        hash: pack[2],
        buf: null,
        handler: (buf) => {
            this.data_blob = new Blob([buf]);
        },
    });

    return this.files.reduce((p, f) => p.then(() => {
        return this.file_cache.get(f.hash).then((buf) => {
            f.buf = buf;
        });
    }), Promise.resolve());
}

// Download any files that were not found in the cache.
Launcher.prototype.downloadFiles = function() {
    this.dl_total = 0;
    for (let f of this.files) {
        if (f.buf == null) {
            this.dl_total += f.size;
        }
    }
    this.dl_complete = 0;

    return this.files.reduce((p, f) => p.then(() => {
        if (f.buf != null) {
            return Promise.resolve();
        }

        this.dl_cur_progress = 0;
        this.dl_cur_size = f.size;
        let onprogress = (evt) => this._handleProgress(evt);
        return this.file_cache.load(f.url, f.hash, onprogress).then((buf) => {
            f.buf = buf;
            this.dl_complete += this.dl_cur_size;
            this.dl_cur_progress = 0;
            this.dl_cur_size = 0;
        });
    }), Promise.resolve());
};

// Run the handler for each loaded file.
Launcher.prototype.processFiles = function() {
    let idx = 0;
    let len = this.files.length;

    return this.files.reduce((p, f) => p.then(() => {
        this.setMessage('Loading files: ' + (idx + 1) + ' / ' + len);
        ++idx;
        return f.handler(f.buf);
    }), Promise.resolve());
};

function kb(b) {
    return (b / 1024)|0;
}

function byteProgress(cur, total) {
    var pct = (cur / total * 100)|0;
    return kb(cur) + 'k/' + kb(total) + 'k bytes (' + pct + '%)';
}

Launcher.prototype._handleProgress = function(evt) {
    this.dl_cur_progress = evt.loaded;
    let cur = this.dl_complete + Math.min(this.dl_cur_progress, this.dl_cur_size);
    this.setMessage('Downloading update: ' + byteProgress(cur, this.dl_total));
};

// Load a file, updating the download progress indicator.
Launcher.prototype.loadFile = function(file, base_url, type) {
    let url = new URL(file[0], base_url);
    let hash = file[2];

    this.dl_cur_progress = 0;
    this.dl_cur_size = file[1];
    let onprogress = (evt) => this._handleProgress(evt);
    return this.file_cache.load(url, hash, type, onprogress).then((x) => {
        this.dl_complete += this.dl_cur_size;
        this.dl_cur_progress = 0;
        this.dl_cur_size = 0;
        return x;
    });
};

Launcher.prototype.loadClientFile = function(file) {
    if (file[0].endsWith('.js')) {
        return this.loadFile(file, this.client_url, 'text').then((src) => {
            let url = new URL(file[0], this.client_url);
            return this.mod_loader.runModule(url, src);
        });
    } else if (file[0].endsWith('.html')) {
        return this.loadFile(file, this.client_url, 'document').then((fragment) => {
            while (fragment.body.firstChild) {
                document.body.appendChild(fragment.body.firstChild);
            }
            return null;
        });
    } else {
        return Promise.reject("don't know how to handle " + file[0]);
    }
};

Launcher.prototype.loadClientFilesIndexed = function(index) {
    let files = this.client_info['files'];
    if (index >= files.length) {
        return Promise.resolve(null);
    }

    let file = files[index];
    return this.loadClientFile(file).then(() => {
        return this.loadClientFilesIndexed(index + 1);
    });
};

Launcher.prototype.loadClientFiles = function() {
    return this.loadClientFilesIndexed(0);
};

Launcher.prototype.getProgress = function() {
    if (this.after_dl) {
        return 1;
    } else if (this.dl_total > 0) {
        let cur = this.dl_complete + this.dl_cur_progress;
        return cur / this.dl_total;
    } else {
        return 0;
    }
};

Launcher.prototype.setMessage = function(msg) {
    console.log('launcher status:', msg);
    this.stat = msg;
}

Launcher.prototype.getMessage = function() {
    return this.stat;
};

Launcher.prototype.getInfo = function() {
    return {
        'server_url': this.server_url,
        'server_info': this.server_info,
        'client_url': this.client_url,
        'client_info': this.client_info,
    };
};



/** @constructor */
function LauncherUI(launcher) {
    this.launcher = launcher;

    this.img_banner = null;
    this.img_font = null;
    this.metrics = null;
    this.pending = 0;

    this.canvas = null;
    this.ctx = null;
    this.start_time = null;

    this.canvas_promise = null;
}

function autoResize(canvas) {
    function handleResize() {
        if (canvas.parentNode == null) {
            window.removeEventListener('resize', handleResize);
            return;
        }
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    window.addEventListener('resize', handleResize);
    handleResize();
}

LauncherUI.prototype.init = function() {
    this.canvas_promise = Promise.resolve(null).then(() => {
        return load(new URL(window.LOGO_BAR_URL)).then((buf) => convert(buf, 'image'));
    }).then((img) => {
        this.img_banner = img;

        return load(new URL('font.png', window.location)).then((buf) => convert(buf, 'image'));
    }).then((img) => {
        this.img_font = img;

        return load(new URL('font_metrics.json', window.location))
            .then((buf) => convert(buf, 'json'));
    }).then((json) => {
        this.metrics = json;

        this.initCanvas();
        return this.canvas;
    });
};

LauncherUI.prototype.initCanvas = function() {
    this.canvas = document.createElement('canvas');
    this.canvas.style.position = 'absolute';
    this.canvas.style.top = '0px';
    this.canvas.style.left = '0px';
    document.body.insertBefore(this.canvas, document.body.firstChild);
    autoResize(this.canvas);

    var i = document.getElementById('i');
    i.parentNode.removeChild(i);

    this.ctx = this.canvas.getContext('2d');
    this.ctx.imageSmoothingEnabled = false;
    this.ctx.mozImageSmoothingEnabled = false;
    this.ctx.webkitImageSmoothingEnabled = false;

    this.start_time = Date.now();
    // Render a frame immediately, to replace the previous image.
    this.renderFrame();
};

LauncherUI.prototype.getCanvas = function() {
    return this.canvas_promise;
};

LauncherUI.prototype.stop = function() {
    this.ctx = null;
    // renderFrame will now cancel its loop
};

function calcBannerPos() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    var px = Math.max(w, h);
    var scale = Math.max(1, Math.round(px / 1024));
    var x = Math.floor(w / 2 / scale) - 80;
    var y = Math.floor(h / 2 / scale) - 60;
    // NB: x and y are pre-scaling coordinates
    return {x: x, y: y, scale: scale};
}

function findGlyph(fm, gm, c) {
    if (gm[c] == null) {
        var code = c.charCodeAt(0);
        var glyph = -1;
        for (var j = 0; j < fm['spans'].length; ++j) {
            var span = fm['spans'][j];
            if (span[0] <= code && code < span[1]) {
                glyph = span[2] + (code - span[0]);
                break;
            }
        }
        gm[c] = glyph;
    }

    return gm[c];
}

function measureString(fm, s, gm) {
    var glyph_map = gm || {};
    var x = 0;
    for (var i = 0; i < s.length; ++i) {
        var c = s.charAt(i);

        var glyph = findGlyph(fm, glyph_map, c);
        if (glyph == -1) {
            x += fm['space_width'];
        } else {
            x += fm['widths2'][glyph] + (i > 0 ? fm['spacing'] : 0);
        }
    }

    return x;
}

function drawString(ctx, fm, img, s, base_x, base_y, gm) {
    var glyph_map = gm || {};
    var dest_x = 0;
    var height = fm['height'];

    for (var i = 0; i < s.length; ++i) {
        var c = s.charAt(i);

        var glyph = findGlyph(fm, glyph_map, c);
        if (glyph == -1) {
            dest_x += fm['space_width'];
        } else {
            var src_x = fm['xs2'][glyph];
            var width = fm['widths2'][glyph];

            ctx.drawImage(img,
                    src_x, 0, width, height,
                    dest_x + base_x, base_y, width, height);

            dest_x += width + (i > 0 ? fm['spacing'] : 0);
        }
    }
}

LauncherUI.prototype.renderFrame = function() {
    if (this.ctx == null) {
        // The canvas has been handed off to the OutpostClient.
        return;
    }

    var this_ = this;
    window.requestAnimationFrame(function() { this_.renderFrame(); });

    var ctx = this.ctx;

    var banner = calcBannerPos();
    ctx.save();
    ctx.scale(banner.scale, banner.scale);
    ctx.translate(banner.x, banner.y);
    ctx.clearRect(0, 0, 160, 120);
    ctx.drawImage(this.img_banner, 0, 0);

    var bar_x0 = 19;
    var bar_x1 = 141;
    var bar_y = 105;

    var frac = this.launcher.getProgress();
    var width = ((bar_x1 - bar_x0) * frac)|0;
    ctx.fillStyle = '#dad45e';
    ctx.fillRect(bar_x0, bar_y, width, 2);
    ctx.fillStyle = '#6daa2c';
    ctx.fillRect(bar_x0, bar_y + 2, width, 2);

    var gm = {};
    var msg = this.launcher.getMessage();
    var width = measureString(this.metrics, msg, gm);
    var msg_y = 118;
    ctx.clearRect(-160, msg_y, 480, this.metrics['height']);
    drawString(ctx, this.metrics, this.img_font,
            msg, 80 - (width / 2)|0, msg_y, gm);

    ctx.restore();
};



function parseHash(h) {
    var dct = {};
    if (h.startsWith('#')) {
        h = h.substring(1);
    }
    var parts = h.split(';');
    console.log('split', h, 'as', parts);

    for (var i = 0; i < parts.length; ++i) {
        var p = parts[i];
        var idx = p.indexOf('=');
        var k = p.substring(0, idx);
        var v = decodeURIComponent(p.substring(idx + 1));
        console.log('parsed', parts[i], 'as', k, v);
        dct[k] = v;
    }

    return dct;
}

function main() {
    var ARGS = parseHash(location.hash);

    var server = ARGS['s'];
    if (server == null) {
        location.replace('serverlist.html');
    }
    if (!server.endsWith('/')) {
        server = server + '/';
    }

    var redir = ARGS['r'];

    let launcher = new Launcher();
    window.OUTPOST_LAUNCHER = launcher;
    let ui = new LauncherUI(launcher);

    ui.init();

    launcher.launch(server, redir).then(function() {
        return ui.getCanvas();
    }).then(function(canvas) {
        console.log(launcher.socket);
        ui.stop();
        launcher.client.handoff(canvas, launcher.socket.ws, launcher.socket._msgs);
        window.OUTPOST_LAUNCHER_INFO = launcher.getInfo();
        window.OUTPOST_LAUNCHER = null;
    });
}
document.addEventListener('DOMContentLoaded', main);
