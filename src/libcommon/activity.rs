use types::*;
use common_types::v3::{V3, scalar};


#[derive(Clone, PartialEq, Eq, Debug, Hash)]
pub enum Activity {
    Stand { pos: V3, dir: u8 },
    // Note: `dir` is independent of `velocity` because the entity may be facing one direction
    // while stopped or sliding in a different direction.
    Walk { pos: V3, velocity: V3, dir: u8 },
    // TODO: Emote { pos: V3, emote: Emote },
    // TODO: Busy { pos: V3, anim: BusyAnim, icon: BusyIcon },
}

impl Activity {
    pub fn default() -> Activity {
        Activity::Stand {
            pos: scalar(0),
            dir: 0,
        }
    }

    pub fn is_movement(&self) -> bool {
        match *self {
            Activity::Stand { .. } |
            Activity::Walk { .. } => true,
            //_ => false,
        }
    }

    pub fn is_walk(&self) -> bool {
        match *self {
            Activity::Walk { .. } => true,
            _ => false,
        }
    }

    pub fn start_pos(&self) -> V3 {
        match *self {
            Activity::Stand { pos, .. } => pos,
            Activity::Walk { pos, .. } => pos,
        }
    }

    pub fn pos(&self, start: Time, now: Time) -> V3 {
        match *self {
            Activity::Stand { pos, .. } => pos,
            Activity::Walk { pos, velocity, .. } => {
                // We previously used a simpler computation for `step`:
                //
                //      let step = velocity * (now - start) as i32 / 1000;
                //
                // This has unfortunate rounding behavior when computing the distance traveled over
                // some duration (i.e., `pos(now2) - pos(now1)`): if the distance is not a whole
                // number, whether it gets rounded up or down depends on the value of `now1`
                // relative to `start`.  Two entities with the same speed can move different
                // distances in the same engine tick, depending only on when each one started
                // moving.  Worse, for a single entity, the number of 1-px steps it takes in an
                // engine tick can depend on whether it changes direction during that tick (for
                // example, by hitting a wall and beginning to slide).
                //
                // This new formula makes the distance traveled independent of `start`.  (In
                // `pos(now2) - pos(now1)`, the `start_offset`s cancel out.)  The rounding behavior
                // now depends only on the value of `now1 % 1000`.  This makes it possible to count
                // 1-px steps given only the entity's movement speed and the interval's start and
                // end times, which is useful for movement updates.  It also means that two
                // entities moving at the same speed will remain at fixed offsets from one another,
                // taking rounded-up and rounded-down steps at exactly the same times, which
                // eliminates the jitter previously seen when following another player.

                // Furthermore, to avoid overflow, we need to cap the value of `t` in the
                // computation `velocity * t / 1000`.  Thus, we divide each `t` into two portions,
                // `whole = t / 1000` and `part = t % 1000`, and compute `velocity * whole +
                // velocity * part / 1000`.  This gets us the right rounding behavior, without
                // ever multiplying `velocity` by the full value of `t`.

                let start_whole = start / 1000;
                let start_part = start % 1000;
                let now_whole = now / 1000;
                let now_part = now % 1000;

                let off_start = velocity * start_part as i32 / 1000;
                let off_now = velocity * now_part as i32 / 1000;
                // `off_whole` is `offset(now_whole) - offset(start_whole)`.
                let off_whole = velocity * (now_whole - start_whole) as i32;

                let step = off_whole + off_now - off_start;

                //trace!("activity pos: {} .. {} = {}s + {}ms - {}ms, step = {:?}",
                //       start, now, now_whole - start_whole, now_part, start_part, step);

                pos + step
            },
            //Activity::Busy { pos, .. } => pos,
        }
    }

    pub fn dir(&self) -> Option<u8> {
        match *self {
            Activity::Stand { dir, .. } => Some(dir),
            Activity::Walk { dir, .. } => Some(dir),
            //_ => None,
        }
    }

    pub fn speed(&self) -> u8 {
        match *self {
            Activity::Stand { .. } => 0,
            Activity::Walk { .. } => 1,
            //_ => 0,
        }
    }

    pub fn velocity(&self) -> V3 {
        match *self {
            Activity::Stand { .. } => scalar(0),
            Activity::Walk { velocity, .. } => velocity,
            //Activity::Busy { .. } => scalar(0),
        }
    }

    pub fn interruptible(&self) -> bool {
        match *self {
            Activity::Stand { .. } |
            Activity::Walk { .. } => true,
            //Activity::Busy { .. } => false,
        }
    }
}


