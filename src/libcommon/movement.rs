use types::*;
use std::cmp;
use activity::Activity;
use physics::collide_2d::{self, MoveResult};

pub use physics::collide_2d::ShapeSource;


// TODO: remove (currently used by physics::floodfill)
pub trait OldShapeSource {
    fn get_shape(&self, pos: V3) -> Shape;

    fn get_shape_below(&self, mut pos: V3) -> (Shape, i32) {
        while pos.z >= 0 {
            let s = self.get_shape(pos);
            if !s.is_empty() {
                return (s, pos.z);
            }
            pos.z -= 1;
        }
        (Shape::Empty, 0)
    }
}


bitflags! {
    pub flags InputBits: u16 {
        const INPUT_LEFT =      0x0001,
        const INPUT_RIGHT =     0x0002,
        const INPUT_UP =        0x0004,
        const INPUT_DOWN =      0x0008,
        const INPUT_RUN =       0x0010,
        const INPUT_HOLD =      0x0020,

        const INPUT_DIR_MASK =  INPUT_LEFT.bits |
                                INPUT_RIGHT.bits |
                                INPUT_UP.bits |
                                INPUT_DOWN.bits,
    }
}

static DIR_INDEX_TABLE: [i8; 16] = [
    /* ---- */ -1,
    /* L--- */ 4,
    /* -R-- */ 0,
    /* LR-- */ -1,
    /* --U- */ 6,
    /* L-U- */ 5,
    /* -RU- */ 7,
    /* LRU- */ 6,
    /* ---D */ 2,
    /* L--D */ 3,
    /* -R-D */ 1,
    /* LR-D */ 2,
    /* --UD */ -1,
    /* L-UD */ 4,
    /* -RUD */ 0,
    /* LRUD */ -1,
];

impl InputBits {
    pub fn to_direction(&self) -> Option<V3> {
        if (*self & INPUT_DIR_MASK) == InputBits::empty() {
            return None
        }

        let x =
            if self.contains(INPUT_LEFT) { -1 } else { 0 } +
            if self.contains(INPUT_RIGHT) { 1 } else { 0 };
        let y =
            if self.contains(INPUT_UP) { -1 } else { 0 } +
            if self.contains(INPUT_DOWN) { 1 } else { 0 };
        Some(V3::new(x, y, 0))
    }

    pub fn to_direction_index(&self) -> Option<u8> {
        let dir_bits = (*self & INPUT_DIR_MASK).bits();
        let idx = DIR_INDEX_TABLE[dir_bits as usize];
        if 0 <= idx && idx < 8 { Some(idx as u8) } else { None }
    }

    pub fn to_speed(&self) -> u8 {
        if self.contains(INPUT_HOLD) ||
           (*self & INPUT_DIR_MASK) == InputBits::empty() { 0 }
        else if self.contains(INPUT_RUN) { 3 }
        else { 1 }
    }
}


const ENTITY_SIZE: V3 = V3 { x: 16, y: 16, z: 24 };

/// Advance the simulation of an entity from `start` to `end`.  Applies `input` over the entity's
/// current activity `act`, and returns the new activity.  Returns `None` if the current activity
/// continues unchanged.
pub fn update_movement(shape: &mut impl ShapeSource,
                       act: Activity,
                       act_start: Time,
                       dir: u8,
                       speed: i32,
                       start: Time,
                       end: Time) -> Option<(Activity, Time)> {
    if !act.interruptible() {
        return None;
    }

    let mut pos = act.pos(act_start, start);

    // (1) Special case for empty input: change activity to `Stand` instead of `Walk`.

    if speed == 0 {
        // Don't interrupt emotes or existing `Stand` on empty input.
        if !act.is_walk() {
            return None;
        }

        //let dir = act.dir().unwrap_or(0);
        return Some((Activity::Stand { pos, dir }, start));
    }

    // (2) Handle direction change (for nonempty input).

    let target_step = DIR_VEC[dir as usize].reduce();

    if act.dir() != Some(dir) {
        let mr = collide_2d::try_move(pos.reduce(), ENTITY_SIZE.reduce(), target_step, shape);
        let (step, count) = match mr {
            MoveResult::Blocked => (0.into(), 0),
            MoveResult::Steps(step, count) => (step.extend(0), count as i32),
            MoveResult::PartialStep(part_step) => (part_step.extend(0), 1),
        };
        let velocity = if count == 0 { 0.into() } else { step * speed };

        // Apply the input change as of time `start`.
        return Some((Activity::Walk { pos, velocity, dir }, start));
    }

    // (3) Handle velocity change.

    // Compute the number of 1-px steps the entity will take during `start .. end`.  This should
    // match the formula for `step` in `Activity::pos`.  If there's a mismatch, we might generate
    // lots of spurious activity changes, since the final position computed here won't match the
    // result of `act.pos` at `end`.
    let rel_start = start % 1000;
    let rel_end = end - start + rel_start;
    let start_offset = speed * rel_start as i32 / 1000;
    let end_offset = speed * rel_end as i32 / 1000;
    let num_steps = end_offset - start_offset;

    trace!("running {} steps ({} .. {}), pos {:?}, target_step {:?}",
           num_steps, start, end, pos, target_step);

    let mut steps_taken = 0;
    while steps_taken < num_steps {
        let mr = collide_2d::try_move(pos.reduce(), ENTITY_SIZE.reduce(), target_step, shape);
        let (step, count) = match mr {
            MoveResult::Blocked => (0.into(), 0),
            MoveResult::Steps(step, count) => (step.extend(0), count as i32),
            MoveResult::PartialStep(part_step) => (part_step.extend(0), 1),
        };
        let velocity = if count == 0 { 0.into() } else { step * speed };
        trace!("  at {:?}, take {} steps in direction {:?} (velocity = {:?})",
               pos, count, step, velocity);

        if act.velocity() != velocity {
            // `change` time is immediately after the last step was taken.
            let change_offset = start_offset + steps_taken;
            let rel_change = (change_offset * 1000 + speed - 1) / speed;
            // Sanity check - make sure we really got the first millisecond with the correct value.
            // The +1000 offset prevents the numerator from going negative when `rel_change == 0`.
            debug_assert!(speed * (1000 + rel_change) / 1000 - start_offset ==
                          speed + steps_taken);
            debug_assert!(speed * (1000 + rel_change - 1) / 1000 - start_offset ==
                          speed + steps_taken - 1);
            // Now clamp - avoid setting a new start time that's less than `start`.
            let rel_change = cmp::max(rel_change as Time, rel_start);
            let change = rel_change - rel_start + start;
            trace!("  calc change time: offset {} .. {} .. {}",
                   start_offset, change_offset, end_offset);
            trace!("  calc change time: rel {} .. {} .. {}",
                   rel_start, rel_change, rel_end);
            trace!("  calc change time: abs {} .. {} .. {}",
                   start, change, end);
            return Some((Activity::Walk { pos, velocity, dir }, change));
        }

        if velocity == 0 {
            // Previous cases passed, so `act` has the right direction and velocity.  The entity
            // isn't actually moving, so we don't need to run any further collision checks.
            break;
        }

        steps_taken += count;
        pos = pos + step * count;
    }

    // (4) Sim finished with no changes.

    None
}
