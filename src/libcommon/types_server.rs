use std::i64;

pub use common_types::*;

pub type Time = i64;
pub const TIME_MIN: Time = i64::MIN;
pub const TIME_MAX: Time = i64::MAX;
pub const DAY_NIGHT_CYCLE_MS: Time = 24 * 60 * 1000;

// Reexports of commonly used macros.  These shouldn't really be here, but this module is basically
// the prelude for most server-side crates.
pub use common_util::{fail, unwrap, unwrap_or, unwrap_or_warn, unwrap_or_error};
