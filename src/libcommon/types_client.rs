use std::i32;

pub use common_types::*;

pub type Time = i32;
pub const TIME_MIN: Time = i32::MIN;
pub const TIME_MAX: Time = i32::MAX;

// Reexports of commonly used macros.  These shouldn't really be here, but this module is basically
// the prelude for most client-side crates.
pub use common_util::{fail, unwrap, unwrap_or, unwrap_or_warn, unwrap_or_error};
