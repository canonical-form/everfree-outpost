use proc_macro::TokenStream;
use proc_macro2;
use syn::{Ident, Result};
use syn::ext::IdentExt;
use syn::parse::{Parse, ParseStream};

struct Args {
    id1: Ident,
    eq: bool,
    id2: Ident,
    rest: TokenStream,
}

impl Parse for Args {
    fn parse(ps: ParseStream) -> Result<Args> {
        let id1 = ps.call(Ident::parse_any)?;
        let eq =
            if let Ok(_) = ps.parse::<Token![==]>() { true }
            else if let Ok(_) = ps.parse::<Token![!=]>() { false }
            else { return Err(ps.error("expected == or !=")); };
        let id2 = ps.call(Ident::parse_any)?;
        ps.parse::<Token![:]>()?;
        let rest = ps.parse::<proc_macro2::TokenStream>()?.into();

        Ok(Args { id1, eq, id2, rest })
    }
}

pub fn if_ident(input: TokenStream) -> TokenStream {
    let args = parse_macro_input!(input as Args);

    if (args.id1.to_string() == args.id2.to_string()) == args.eq {
        args.rest
    } else {
        TokenStream::new()
    }
}
