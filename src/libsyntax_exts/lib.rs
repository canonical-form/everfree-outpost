#![crate_name = "syntax_exts"]
#![feature(
    proc_macro_diagnostic,
    proc_macro_span,
)]
extern crate proc_macro;
extern crate proc_macro2;
#[macro_use] extern crate syn;
#[macro_use] extern crate quote;

use proc_macro::TokenStream;

mod util;

mod ident_str;
mod if_ident;
mod expand_objs;
mod multipython;


#[proc_macro]
pub fn ident_str(input: TokenStream) -> TokenStream {
    ident_str::ident_str(input)
}

#[proc_macro]
pub fn if_ident(input: TokenStream) -> TokenStream {
    if_ident::if_ident(input)
}


#[proc_macro]
pub fn expand_objs_named(input: TokenStream) -> TokenStream {
    let output = expand_objs::expand_objs_named(input, '$');
    output
}

#[proc_macro]
pub fn expand_objs_named_macro_safe(input: TokenStream) -> TokenStream {
    expand_objs::expand_objs_named(input, '#')
}

#[proc_macro]
pub fn for_each_obj_named(input: TokenStream) -> TokenStream {
    expand_objs::for_each_obj_named(input, '$')
}

#[proc_macro]
pub fn for_each_obj_named_macro_safe(input: TokenStream) -> TokenStream {
    expand_objs::for_each_obj_named(input, '#')
}

#[proc_macro]
pub fn define_expand_objs_list(input: TokenStream) -> TokenStream {
    expand_objs::define_expand_objs_list(input)
}


#[proc_macro]
pub fn python_class(input: TokenStream) -> TokenStream {
    multipython::python_module(input, multipython::OutputRules {
        .. multipython::OutputRules::named("python_class_impl")
    })
}

#[proc_macro]
pub fn python_class_builder(input: TokenStream) -> TokenStream {
    multipython::python_module(input, multipython::OutputRules {
        builder_name: true,
        .. multipython::OutputRules::named("python_class_builder_impl")
    })
}

#[proc_macro]
pub fn python_module(input: TokenStream) -> TokenStream {
    multipython::python_module(input, multipython::OutputRules {
        data_expr: true,
        generics: true,
        .. multipython::OutputRules::named("python_module_impl")
    })
}

#[proc_macro]
pub fn python_module_builder(input: TokenStream) -> TokenStream {
    multipython::python_module(input, multipython::OutputRules {
        builder_name: true,
        generics: true,
        .. multipython::OutputRules::named("python_module_builder_impl")
    })
}
