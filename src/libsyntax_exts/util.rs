use std::fmt::Display;

use proc_macro::{TokenStream, Span, Diagnostic};
use proc_macro::{TokenTree, Group, Ident, Punct, Literal, Delimiter, Spacing};


// Input parsing helpers

pub struct TtBuf {
    tts: Vec<TokenTree>,
    parent_span: Span,
}

#[derive(Clone, Copy, Debug)]
pub struct TtCurs<'a> {
    tts: &'a [TokenTree],
    parent_span: Span,
}

pub type TtResult<T> = Result<T, Diagnostic>;

impl TtBuf {
    pub fn new(ts: TokenStream, span: Span) -> TtBuf {
        fn add(out: &mut Vec<TokenTree>, tt: TokenTree) {
            if let TokenTree::Group(ref g) = tt {
                if g.delimiter() == Delimiter::None {
                    for tt in g.stream() {
                        add(out, tt);
                    }
                    return;
                }
            }

            out.push(tt);
        }

        let mut tts = Vec::new();
        for tt in ts {
            add(&mut tts, tt);
        }

        TtBuf { tts, parent_span: span }
    }

    pub fn curs(&self) -> TtCurs {
        TtCurs {
            tts: &self.tts,
            parent_span: self.parent_span,
        }
    }
}

#[allow(dead_code)]
impl<'a> TtCurs<'a> {
    fn tt_span(self) -> Option<Span> {
        self.tts.first().map(|tt| tt.span())
    }

    fn span(self) -> Span {
        self.tt_span().unwrap_or(self.parent_span)
    }

    pub fn skip(self, n: usize) -> Self {
        if n < self.tts.len() {
            Self {
                tts: &self.tts[n..],
                .. self
            }
        } else {
            Self {
                tts: &[],
                .. self
            }
        }
    }

    pub fn peek(self) -> Option<&'a TokenTree> {
        self.tts.first()
    }

    pub fn peek_n(self, n: usize) -> Option<&'a TokenTree> {
        self.tts.get(n)
    }

    pub fn is_eof(self) -> bool {
        self.tts.len() == 0
    }

    pub fn eof(self) -> TtResult<()> {
        if self.is_eof() {
            Ok(())
        } else {
            Err(self.span().error("expected end of input"))
        }
    }

    pub fn token_tree(self) -> TtResult<(&'a TokenTree, Self)> {
        match self.peek() {
            Some(tt) => Ok((tt, self.skip(1))),
            None => Err(self.parent_span.error("expected tokentree (at end of input)")),
        }
    }

    pub fn ident(self) -> TtResult<(&'a Ident, Self)> {
        match self.peek() {
            Some(&TokenTree::Ident(ref i)) => Ok((i, self.skip(1))),
            Some(tt) => Err(tt.span().error("expected ident")),
            None => Err(self.parent_span.error("expected ident (at end of input)")),
        }
    }

    pub fn exact_ident(self, s: &str) -> TtResult<Self> {
        match self.peek() {
            Some(&TokenTree::Ident(ref i)) if i.to_string() == s => Ok(self.skip(1)),
            Some(tt) => Err(tt.span().error(format!("expected {:?}", s))),
            None => Err(self.parent_span.error(format!("expected {:?} (at end of input)", s))),
        }
    }

    pub fn punct(self) -> TtResult<(&'a Punct, Self)> {
        match self.peek() {
            Some(&TokenTree::Punct(ref p)) => Ok((p, self.skip(1))),
            Some(tt) => Err(tt.span().error("expected ident")),
            None => Err(self.parent_span.error("expected ident (at end of input)")),
        }
    }

    pub fn exact_punct(self, ch: char) -> TtResult<Self> {
        match self.peek() {
            Some(&TokenTree::Punct(ref p)) if p.as_char() == ch =>
                Ok(self.skip(1)),
            Some(tt) => Err(tt.span().error(format!("expected {:?}", ch))),
            None => Err(self.parent_span.error(format!("expected {:?} (at end of input)", ch))),
        }
    }

    pub fn exact_punct_joint(self, ch: char) -> TtResult<Self> {
        match self.peek() {
            Some(&TokenTree::Punct(ref p))
                if p.as_char() == ch && p.spacing() == Spacing::Joint =>
                Ok(self.skip(1)),
            Some(tt) => Err(tt.span().error(format!("expected {:?}", ch))),
            None => Err(self.parent_span.error(format!("expected {:?} (at end of input)", ch))),
        }
    }

    pub fn exact_punct_span(self, ch: char) -> TtResult<(Span, Self)> {
        match self.peek() {
            Some(&TokenTree::Punct(ref p)) if p.as_char() == ch =>
                Ok((p.span(), self.skip(1))),
            Some(tt) => Err(tt.span().error(format!("expected {:?}", ch))),
            None => Err(self.parent_span.error(format!("expected {:?} (at end of input)", ch))),
        }
    }

    pub fn exact_punct_multi(self, s: &str) -> TtResult<Self> {
        let mut c = self;
        let char_len = s.chars().count();
        for (i, ch) in s.chars().enumerate() {
            let result =
                if i + 1 < char_len { c.exact_punct_joint(ch) }
                else { c.exact_punct(ch) };
            match result {
                Ok(new_c) => { c = new_c; },
                Err(_) => {
                    return Err(self.error(format!("expected {:?}", s)));
                },
            }
        }
        Ok(c)
    }

    pub fn group(self) -> TtResult<(&'a Group, Self)> {
        match self.peek() {
            Some(&TokenTree::Group(ref g)) => Ok((g, self.skip(1))),
            Some(tt) => Err(tt.span().error("expected ident")),
            None => Err(self.parent_span.error("expected ident (at end of input)")),
        }
    }

    pub fn delim_group(self, delim: Delimiter) -> TtResult<(&'a Group, Self)> {
        match self.peek() {
            Some(&TokenTree::Group(ref g)) if g.delimiter() == delim => Ok((g, self.skip(1))),
            Some(tt) => Err(tt.span().error(format!("expected {:?} group", delim))),
            None => Err(self.parent_span.error(
                    format!("expected {:?} group (at end of input)", delim))),
        }
    }

    pub fn delimited(self, delim: Delimiter) -> TtResult<(TtBuf, Self)> {
        match self.peek() {
            Some(&TokenTree::Group(ref g)) if g.delimiter() == delim =>
                Ok((TtBuf::new(g.stream(), g.span()), self.skip(1))),
            Some(tt) => Err(tt.span().error(format!("expected {:?} group", delim))),
            None => Err(self.parent_span.error(
                    format!("expected {:?} group (at end of input)", delim))),
        }
    }

    pub fn literal(self) -> TtResult<(&'a Literal, Self)> {
        match self.peek() {
            Some(&TokenTree::Literal(ref l)) => Ok((l, self.skip(1))),
            Some(tt) => Err(tt.span().error("expected ident")),
            None => Err(self.parent_span.error("expected ident (at end of input)")),
        }
    }


    pub fn optional<F, R>(self, f: F) -> (Option<R>, Self)
            where F: FnOnce(Self) -> TtResult<(R, Self)> {
        match f(self) {
            Ok((x, c)) => (Some(x), c),
            Err(_) => (None, self),
        }
    }

    pub fn until_eof<F, R>(mut self, mut f: F) -> TtResult<(Vec<R>, Self)>
            where F: FnMut(Self) -> TtResult<(R, Self)> {
        let mut rs = Vec::new();
        while !self.is_eof() {
            let (r, c) = f(self)?;
            rs.push(r);
            self = c;
        }
        Ok((rs, self))
    }

    pub fn error<S: Display>(self, msg: S) -> Diagnostic {
        if self.is_eof() {
            return self.parent_span.error(format!("{} (at end of input)", msg));
        } else {
            return self.span().error(msg.to_string());
        }
    }
}


