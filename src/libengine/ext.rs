use server_types::*;
use std::rc::Rc;
use world::{self, World};

use chat;
use component::Components;
use update::{Update, UpdateBuilder};

pub type FullWorld = World<WorldExt>;
pub type BaseWorld = FullWorld;


pub trait Ext {
    fn handle_update(&mut self, w: &FullWorld, u: Update);

    fn request_client_bundle(&mut self, uid: u32, name: &str);
    fn request_plane_bundle(&mut self, stable_id: Stable<PlaneId>);
    fn request_terrain_chunk_bundle(&mut self, plane: Stable<PlaneId>, cpos: V2);

    // This should get turned into Update entries at some point
    fn client_world_info(&mut self,
                         id: ClientId,
                         now: Time,
                         day_night_cycle_ms: u32);
    // TODO: Remove (currently does nothing)
    fn client_readiness(&mut self, id: ClientId, readiness: bool);

    fn client_loaded(&mut self, uid: u32, cid: ClientId);
    fn client_unloaded(&mut self, uid: u32);

    fn kick_user(&mut self, uid: u32, reason: &str);

    fn chat_message(&mut self, channel: chat::Channel, name: &str, text: &str);

    fn ack_request(&mut self, cid: ClientId, apply_time: Time);
    fn nak_request(&mut self, cid: ClientId);
}


pub struct WorldExt;

impl world::ext::Ext for WorldExt {
    type Components = Components;
    type Update = Rc<UpdateBuilder>;
}
