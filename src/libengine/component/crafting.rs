//! Low-level crafting code.  This module just handles the mechanics of the crafting process
//! itself: starting/stopping, pausing/resuming, and running the recipe on a periodic timer.
//! Higher-level logic regarding conditions for starting crafting (supported classes / required
//! abilities) is located elsewhere.
use server_types::*;
use std::cmp;
use std::collections::hash_map::{HashMap, Entry};
use std::{i32, u16, u32};
use common::util::btree_multimap::Bounded;
use server_config::data;
use server_config::data::RecipeItem;
use world;
use world::objects::*;
use world::object_ref::ObjectRef;

use component::inv_update::{InvUpdates, Behavior};
use engine::Engine;
use ext::Ext;
use logic;
use timer::{TimerId, Timers};
use update::UpdateBuilder;


// TODO - refactor crafting handling.
// Pass in all info from external callers.  Use the StructureId only as a key for looking up
// station state.  Later we can add support for other kinds of keys, like EntityIds.  This will
// allow for crafting from items/abilities/admin commands, with no actual structure.  Can also
// support multi-user "astral workbench" type structures.
//
// - Add a "filter check" function, to check class + ability requirements
// - Modify "start" to remove checks, and take contents InvId as an argument 
// - Remove `StationState::Idle`.  Instead, create & destroy state entries as needed
// - Add some persistence flags, for crafting that should continue running while unloaded etc.
// - Remove StationId from crafting dialog.  Client tracks only one crafting state at a time.


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum CraftingState {
    /// Crafting is paused, but there is saved progress and/or remaining crafting counts for some
    /// recipe.
    Paused {
        recipe: RecipeId,
        count: u8,
        saved_progress: u32,
        reason: PauseReason,
    },

    /// Actively crafting the given recipe.
    Active {
        recipe: RecipeId,
        count: u8,
        start_progress: u32,
        start_time: Time,
    },
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum PauseReason {
    /// The player clicked deliberately clicked the pause button or closed the dialog.  Crafting
    /// won't resume until the user explicitly starts it.
    User,

    /// Further progress is currently blocked due to the state of the station's inventory.
    /// Crafting will resume automatically once the necessary items are added/removed.
    Inventory,
}

impl CraftingState {
    pub fn paused(recipe: RecipeId,
                  count: u8,
                  saved_progress: u32,
                  reason: PauseReason) -> CraftingState {
        CraftingState::Paused {
            recipe: recipe,
            count: count,
            saved_progress: saved_progress,
            reason: reason,
        }
    }

    pub fn active(recipe: RecipeId,
                  count: u8,
                  start_progress: u32,
                  start_time: Time) -> CraftingState {
        CraftingState::Active {
            recipe: recipe,
            count: count,
            start_progress: start_progress,
            start_time: start_time,
        }
    }

    pub fn recipe(&self) -> RecipeId {
        match *self {
            CraftingState::Paused { recipe, .. } => recipe,
            CraftingState::Active { recipe, .. } => recipe,
        }
    }

    pub fn count(&self) -> u8 {
        match *self {
            CraftingState::Paused { count, .. } => count,
            CraftingState::Active { count, .. } => count,
        }
    }

    pub fn saved_progress(&self, now: Time) -> u32 {
        match *self {
            CraftingState::Paused { saved_progress, .. } => saved_progress,
            CraftingState::Active { start_progress, start_time, .. } => {
                let delta = now - start_time;
                if delta < 0 {
                    start_progress
                } else if delta >= (i32::MAX - start_progress as i32) as Time {
                    // Cap at i32::MAX instead of u32::MAX because Time might be i32.
                    i32::MAX as u32
                } else {
                    start_progress + delta as u32
                }
            },
        }
    }

    fn needs_update_on_inv_change(&self) -> bool {
        match *self {
            CraftingState::Active { .. } => true,
            CraftingState::Paused { reason: PauseReason::Inventory, .. } => true,
            _ => false,
        }
    }

    fn user_pause(&mut self, now: Time) {
        match *self {
            CraftingState::Active { recipe, count, .. } |
            CraftingState::Paused { recipe, count, reason: PauseReason::Inventory, .. } => {
                let progress = self.saved_progress(now);
                *self = CraftingState::paused(recipe, count, progress, PauseReason::User);
            },
            CraftingState::Paused { reason: PauseReason::User, .. } => {},
        }
    }
}


struct StationState {
    pub state: CraftingState,

    /// The inventory used as the source for materials and the destination for crafted items.
    pub inv: InventoryId,

    /// The ID of the timer for the next crafting cycle, if crafting is active.
    timer: Option<TimerId>,

    /// Whether updates to `inv` are currently tracked with `Behavior::UpdateCrafting`.
    tracking_inv: bool,
}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub enum StationId {
    Structure(StructureId),
    // Make existing pattern matches non exhaustive.  Otherwise we get errors about "irrefutable
    // if-let patterns". TODO - remove this once more variants exist
    Dummy,
}

impl Bounded for StationId {
    fn min_bound() -> Self {
        StationId::Structure(Bounded::min_bound())
    }

    fn max_bound() -> Self {
        StationId::Structure(Bounded::max_bound())
    }
}


enum Cancel {
    Timer(TimerId),
    InvUpdate(InventoryId, StationId),
}

pub struct CraftingStations {
    map: HashMap<StationId, StationState>,
    pending_cancel: Vec<Cancel>,
}

impl CraftingStations {
    pub fn new() -> CraftingStations {
        CraftingStations {
            map: HashMap::new(),
            pending_cancel: Vec::new(),
        }
    }


    /// Non-engine crafting cancellation.  This does the same thing as `crafting::cancel`, but
    /// defers cancellation of any outstanding timer or inventory subscription, so it doesn't
    /// require access to the full `Engine`.
    fn cancel_inner(&mut self, id: StationId) -> Option<StationState> {
        let station = unwrap_or!(self.map.remove(&id), return None);

        // It's okay if these cancellations happen a bit late.  `update_state` is a no-op if the
        // station doesn't exist.
        //
        // It's also okay if the station begins crafting again before we process the cancellations.
        // Timer IDs are almost never recycled, so the new crafting state will have a different
        // one.  And `inv_update` tracks multiplicity of subscriptions, so we won't accidentally
        // cancel the new crafting session's inventory subscription.
        if let Some(timer) = station.timer {
            self.pending_cancel.push(Cancel::Timer(timer));
        }
        if station.tracking_inv {
            self.pending_cancel.push(Cancel::InvUpdate(station.inv, id));
        }

        Some(station)
    }

    fn pause(&mut self,
             id: StationId,
             now: Time,
             ub: &UpdateBuilder) {
        let station = unwrap_or!(self.map.get_mut(&id));
        record_update(id, Some(station), ub);

        if let Some(timer) = station.timer.take() {
            self.pending_cancel.push(Cancel::Timer(timer));
        }
        if station.tracking_inv {
            self.pending_cancel.push(Cancel::InvUpdate(station.inv, id));
            station.tracking_inv = false;
        }

        station.state.user_pause(now);
        assert!(!station.state.needs_update_on_inv_change());
    }


    pub fn handle_destroy_structure(&mut self,
                                    sid: StructureId,
                                    ub: &UpdateBuilder) {
        let id = StationId::Structure(sid);
        if let Some(station) = self.cancel_inner(id) {
            record_destroyed(id, Some(station), ub);
        }
    }

    pub fn handle_close_dialog(&mut self,
                               id: StationId,
                               now: Time,
                               ub: &UpdateBuilder) {
        self.pause(id, now, ub);
    }


    pub fn has(&self, id: StationId) -> bool {
        self.map.contains_key(&id)
    }

    pub fn get(&self, id: StationId) -> Option<(&CraftingState, InventoryId)> {
        self.map.get(&id).map(|s| (&s.state, s.inv))
    }

    pub fn update<E>(&mut self,
                 timers: &mut Timers<E>,
                 inv_updates: &mut InvUpdates) {
        for cancel in self.pending_cancel.drain(..) {
            match cancel {
                Cancel::Timer(t) => {
                    timers.remove(t);
                },
                Cancel::InvUpdate(inv, id) => {
                    inv_updates.remove_behavior(inv, Behavior::UpdateCrafting(id));
                },
            }
        }
    }
}

fn record_update(id: StationId, station: Option<&StationState>, ub: &UpdateBuilder) {
    let old = station.map(|s| (&s.state, s.inv));
    match id {
        StationId::Structure(sid) => ub.structure_crafting(sid, old),
        StationId::Dummy => unimplemented!(),
    }
}

fn record_destroyed(id: StationId, station: Option<StationState>, ub: &UpdateBuilder) {
    let old = station.map(|s| (s.state, s.inv));
    match id {
        StationId::Structure(sid) => ub.structure_crafting_destroyed(sid, old),
        StationId::Dummy => unimplemented!(),
    }
}

/// Start a new crafting run, consisting of `count` iterations of `recipe_id`.
pub fn start<E: Ext>(eng: &mut Engine<E>,
                     id: StationId,
                     inv: InventoryId,
                     recipe_id: RecipeId,
                     count: u8) {
    let _ = unwrap_or_warn!(data().get_recipe(recipe_id),
                            "invalid recipe {:?}", recipe_id);
    let now = eng.w.now;


    // Cancel old crafting for this station, if it's not compatible with the new one.
    let should_cancel = eng.crafting.map.get(&id).map_or(false, |s| {
        s.inv != inv || s.state.recipe() != recipe_id
    });
    if should_cancel {
        cancel(eng, id);
    }

    // Now there are two cases.  Either there is no crafting for station `id`, or there is and it
    // uses the same inventory and recipe.
    match eng.w.crafting.map.entry(id) {
        Entry::Vacant(e) => {
            record_update(id, None, &eng.update);
            e.insert(StationState {
                // The initial state doesn't matter too much, since the upcoming `update_state`
                // will immediately switch to the correct state for this crafting session.  The
                // only requirement is it can't be `Paused(User)`, since then `update_state` will
                // do nothing.
                state: CraftingState::paused(recipe_id, count, 0, PauseReason::Inventory),
                inv: inv,
                timer: None,
                tracking_inv: false,
            });
        },

        Entry::Occupied(mut e) => {
            record_update(id, Some(e.get()), &eng.update);
            assert!(e.get().inv == inv);
            assert!(e.get().state.recipe() == recipe_id);

            let progress = e.get().state.saved_progress(now);
            e.get_mut().state = CraftingState::paused(recipe_id,
                                                      count,
                                                      progress,
                                                      PauseReason::Inventory);
        },
    }

    update_state(eng, id);
}

pub fn pause<E: Ext>(eng: &mut Engine<E>,
                     id: StationId) {
    let now = eng.now;
    let station = unwrap_or!(eng.w.crafting.map.get_mut(&id));
    record_update(id, Some(station), &eng.update);

    if let Some(timer) = station.timer.take() {
        eng.timers.remove(timer);
    }
    if station.tracking_inv {
        eng.inv_updates.remove_behavior(station.inv, Behavior::UpdateCrafting(id));
        station.tracking_inv = false;
    }

    station.state.user_pause(now);
    assert!(!station.state.needs_update_on_inv_change());
}

pub fn cancel<E: Ext>(eng: &mut Engine<E>,
                      id: StationId) {
    // `clear_state` `error!`s if the station doesn't have a crafting session, so check first.
    if eng.crafting.has(id) {
        clear_state(eng, id);
    }
}

/// Check the status of a crafting station, and update its state based on progress accumulation or
/// inventory changes.
pub fn update_state<E: Ext>(eng: &mut Engine<E>,
                            id: StationId) {
    let now = eng.w.now;
    trace!("update_state: station {:?}, time {}", id, now);

    let progress;
    let recipe_id;
    let recipe;
    let count;
    let iid;
    {
        // Return early if `id` doesn't have any crafting state.
        let station = unwrap_or!(eng.w.crafting.map.get(&id));
        match station.state {
            CraftingState::Paused { reason: PauseReason::User, .. } => {
                // No automatic updates should happen until the user explicitly unpauses.
                return;
            },
            _ => {},
        }

        progress = station.state.saved_progress(now);
        recipe_id = station.state.recipe();
        recipe = unwrap_or_error!(data().get_recipe(recipe_id),
                                  "invalid recipe ID {} for station {:?}", recipe_id, id);
        count = station.state.count();
        iid = station.inv;
        trace!("  update: init state {:?}, recipe {}", station.state, recipe.name());
    };

    let craft_time = recipe.time as u32;

    // Logic:
    //
    // The general idea is that each crafting station with a recipe selected has a `progress`
    // value, indicating how much progress has been made toward crafting the recipe.  Progress
    // starts at 0 and increases over time until it reaches the `craft_time` of the recipe.  At
    // that point, the recipe is actually run (inputs are taken from the station inventory and
    // replaced with outputs), and progress resets to 0.
    //
    // There are a few special conditions:
    //
    // - Progress can occur only if all the recipe's inputs are present in the station inventory.
    //   If some inputs are missing, progress is always 0.  Removing inputs while crafting is in
    //   progress will cause progress to reset to 0, and stay there until new inputs are provided.
    //
    // - The recipe can run only if there is room for its outputs.  If there isn't room, progress
    //   will reach the cap and stall there, until some items are removed to make space.  (The
    //   recipe also can't run if inputs are missing, but in that case progress is always 0, so it
    //   can never reach `craft_time`.)
    //
    // - Progress only increases when the station's state is `Active`.  The user can pause
    //   crafting, in which case progress is saved until they switch recipes or remove input items.

    // Implementation:
    //
    // The implementation handles arbitrarily many iterations of the above logic.  That is, given
    // enough inputs and enough time to make progress, one call to this function can potentially
    // run the recipe multiple times.  This is useful for letting crafting continue in unloaded
    // chunks, or to deal with extremely fast recipes (craft_time < TICK_MS).
    //
    // (1) We run the recipe (converting inputs to outputs, and decrementing `progress` by
    //     `craft_time` and `count` by 1) as long as four conditions are met:
    //   - `count > 0`
    //   - `progress >= craft_time`
    //   - At least one full set of inputs is available in the inventory
    //   - The inventory has space to add at least one full set of outputs
    //
    // (2) Afterward, we update the station state based on which of the four conditions stopped
    //     holding first.  (In case of a tie, take the first action in this list.)
    //   - `count == 0`: This crafting run has finished.  Set state to `Idle`.
    //   - No inputs: Progress can't occur.  Set progress to 0 and state to `Paused`.
    //   - `progress < craft_time`: Need to accumulate progress.  Set state to `Active`.
    //   - No space for outputs: Can't run the recipe until some are removed.  Cap progress at
    //     `craft_time` and set state to `Paused`.

    // In reality, we compute up front how many times we'll be able to craft the recipe, and which
    // of the three conditions forces us to stop.  Then (1) craft the recipe that many times, and
    // (2) branch on the stop reason to decide on the next state.


    // Limit checks:

    let count_limit = count as u16;

    // Number of full `craft_time`s of available progress
    let progress_limit = cmp::min(progress / craft_time, u16::MAX as u32) as u16;

    // Number of full sets of input items available in the inventory.
    let input_limit;
    // Number of full sets of output items that can be placed into the inventory.
    let output_limit;
    {
        let i = unwrap_or_error!(eng.w.get_inventory(iid),
                                 "invalid contents {:?} for {:?}", iid, id);
        input_limit = recipe.inputs().iter()
            .map(|ri| logic::inventory::count(i, ri.item) / ri.quantity as u16)
            .min().unwrap_or(u16::MAX);
        output_limit = count_output_space(i, recipe.outputs(), Some(input_limit));
    }

    trace!("  update: craft limits: count = {}, progress = {}, input = {}, output = {}",
           count_limit, progress_limit, input_limit, output_limit);


    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    enum StopReason {
        Count,
        Input,
        Progress,
        Output,
    }

    let (limit, stop_reason) = {
        // Figure out which of the conditions hits its limit first.  We use `<=` here so that if
        // several hit their limits at the same time, `stop_reason` follows the priority described
        // above.
        if count_limit <= progress_limit &&
           count_limit <= input_limit &&
           count_limit <= output_limit {
            (count_limit, StopReason::Count)
        } else if input_limit <= progress_limit &&
                  input_limit <= output_limit {
            (input_limit, StopReason::Input)
        } else if progress_limit <= output_limit {
            (progress_limit, StopReason::Progress)
        } else {
            (output_limit, StopReason::Output)
        }
    };

    trace!("  update: total limit = {}, reason = {:?}", limit, stop_reason);


    // Actual crafting & update:

    run_recipe(eng, iid, recipe_id, limit);


    let new_count = count - limit as u8;
    match stop_reason {
        StopReason::Count => {
            clear_state(eng, id);
        },
        StopReason::Input => {
            let s = CraftingState::paused(recipe_id, new_count, 0, PauseReason::Inventory);
            set_state(eng, id, s);
        },
        StopReason::Progress => {
            let new_progress = progress % craft_time;
            let s = CraftingState::active(recipe_id, new_count, new_progress, now);
            let delay = craft_time - new_progress;
            set_state_with_timer(eng, id, s, now + delay as Time);
        },
        StopReason::Output => {
            let s = CraftingState::paused(recipe_id,
                                          new_count,
                                          craft_time,
                                          PauseReason::Inventory);
            set_state(eng, id, s);
        },
    }
}

fn clear_state<E: Ext>(eng: &mut Engine<E>, id: StationId) {
    let s = unwrap_or_error!(eng.crafting.map.remove(&id),
                             "clear_state: {:?} has no crafting state", id);
    record_update(id, Some(&s), &eng.update);
    if let Some(timer) = s.timer {
        eng.timers.remove(timer);
    }
    if s.tracking_inv {
        eng.inv_updates.remove_behavior(s.inv, Behavior::UpdateCrafting(id));
    }

    match id {
        StationId::Structure(sid) => {
            eng.update.structure_crafting(sid, None);
        },
        StationId::Dummy => {},
    }
}

fn set_state_common<E: Ext>(eng: &mut Engine<E>,
                            id: StationId,
                            state: CraftingState,
                            wake: Option<Time>) {
    let s = unwrap_or_error!(eng.w.components.crafting.map.get_mut(&id),
                             "set_state_common: {:?} has no crafting state", id);
    record_update(id, Some(s), &eng.update);

    if let Some(timer) = s.timer {
        eng.timers.remove(timer);
        s.timer = None;
    }
    if let Some(wake) = wake {
        let timer = eng.timers.insert(wake, move |eng| update_state(eng, id));
        s.timer = Some(timer);
    }

    let needs_tracking = state.needs_update_on_inv_change();
    if s.tracking_inv != needs_tracking {
        if needs_tracking {
            eng.inv_updates.add_behavior(s.inv, Behavior::UpdateCrafting(id));
        } else {
            eng.inv_updates.remove_behavior(s.inv, Behavior::UpdateCrafting(id));
        }
        s.tracking_inv = needs_tracking;
    }

    s.state = state;
}

fn set_state<E: Ext>(eng: &mut Engine<E>,
                     id: StationId,
                     state: CraftingState) {
    set_state_common(eng, id, state, None);
}

fn set_state_with_timer<E: Ext>(eng: &mut Engine<E>,
                                id: StationId,
                                state: CraftingState,
                                wake: Time) {
    set_state_common(eng, id, state, Some(wake));
}

fn run_recipe<E: Ext>(eng: &mut Engine<E>,
                      iid: InventoryId,
                      recipe_id: RecipeId,
                      count: u16) {
    if count == 0 {
        // Avoid generating a spurious `inventory_change_contents` event.
        return;
    }

    let recipe = data().recipe(recipe_id);
    trace!("craft recipe {:?} x{} into {:?}", recipe.name(), count, iid);
    eng.w.inventory_mut(iid).modify_contents(|contents| {
        for ri in recipe.inputs() {
            logic::inventory::remove_contents(contents, ri.item, ri.quantity * count);
        }
        for ri in recipe.outputs() {
            logic::inventory::add_contents(contents, ri.item, ri.quantity * count);
        }
    });
}

/// Count how many sets of output items we can place into `i`.  This function accounts for the fact
/// that placing an item into an empty slot makes the slot unavailable for other items.
///
/// If `limit` is `Some`, stop after finding space for at least `limit` sets of items.  This avoids
/// a bit of unnecessary search if an upper bound is already known.
fn count_output_space<E: world::Ext>(i: ObjectRef<E, Inventory>,
                                     items: &[RecipeItem],
                                     limit: Option<u16>) -> u16 {
    let limit = limit.unwrap_or(u16::MAX);
    if items.len() == 0 {
        return limit;
    } else if items.len() == 1 {
        let ri = &items[0];
        let space = logic::inventory::count_space(i, ri.item);
        return cmp::min(limit, space / ri.quantity as u16);
    }

    let max_stack = 255;

    // How many empty slots remain in `i`?
    let mut num_empty = i.contents().iter().filter(|slot| slot.id == NO_ITEM).count();
    // For each item, how much space is available across all slots currently allocated to that
    // item?
    let mut space = items.iter()
        .map(|ri| logic::inventory::count_space_non_empty(i, ri.item))
        .collect::<Vec<_>>();

    loop {
        // Find which item is currently the limiting factor (`space / quantity` is minimum), and
        // assign the next empty slot to that item.  This increases the space available for that
        // item, so the next iteration will have a higher limit and (possibly) a different limiting
        // item.

        let mut min_idx = 0;
        let mut min_count = u16::MAX;
        for (i, (ri, s)) in items.iter().zip(space.iter()).enumerate() {
            let count = s / ri.quantity;
            if count < min_count {
                min_count = count;
                min_idx = i;
            }
        }

        // Stop if we've found enough space for `limit` sets of items.
        if min_count >= limit {
            return limit;
        }

        // Stop if we've run out of empty slots to assign.
        if num_empty == 0 {
            return min_count;
        }

        // Assign the next empty slot.
        num_empty -= 1;
        space[min_idx] += max_stack;
    }
}


/// Set the component value without recording an update.  Call this only when applying a `Delta`.
pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            id: StationId,
                            new: Option<(CraftingState, InventoryId)>) {
    if eng.crafting.has(id) {
        clear_state(eng, id);
    }

    if let Some((state, inv)) = new {
        unwrap_or_error!(eng.w.get_inventory(inv), "apply: nonexistent {:?}", inv);
        unwrap_or_error!(data().get_recipe(state.recipe()),
                         "apply: nonexistent recipe {}", state.recipe());
        // Old state at this point is always `None`, thanks to the call to `clear_state`.
        record_update(id, None, &eng.update);

        eng.crafting.map.insert(id, StationState {
            state, inv,
            timer: None,
            tracking_inv: false,
        });
        // `update_state` records a change only via `set_state`, and it only calls `set_state` if
        // something actually changes (for example, if some crafting happened while the station was
        // unloaded).
        update_state(eng, id);
    }
}
