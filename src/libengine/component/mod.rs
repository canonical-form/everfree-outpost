use server_types::*;
use std::rc::Rc;
use world;

use update::UpdateBuilder;

use self::bitmap::Bitmaps;
use self::camera::ClientCameras;
use self::char_invs::CharInvMap;
use self::compass_targets::CompassData;
use self::contents::Contents;
use self::crafting::CraftingStations;
use self::dialog::{Dialogs, Dialog};
use self::key_value::{KeyValueStore};
use self::ward_map::WardMaps;
use self::ward_permits::WardPermits;


pub mod ext;

pub mod bitmap;
pub mod compass_targets;
pub mod crafting;
pub mod char_invs;
pub mod contents;
pub mod dialog;
pub mod key_value;
pub mod ward_map;
pub mod ward_permits;

// TODO: move these out of `component` at some point
pub mod camera;
pub mod inv_update;


#[repr(C)]
pub struct Components {
    pub now: Time,

    pub bitmaps: Bitmaps,
    pub char_invs: CharInvMap,
    pub client_cameras: ClientCameras,
    pub compass_data: CompassData,
    pub contents: Contents,
    pub crafting: CraftingStations,
    pub dialogs: Dialogs,
    pub key_value: KeyValueStore,
    pub ward_maps: WardMaps,
    pub ward_permits: WardPermits,
}

impl Components {
    pub fn new(now: Time) -> Components {
        Components {
            now: now,

            bitmaps: Bitmaps::new(),
            char_invs: CharInvMap::new(),
            client_cameras: ClientCameras::new(),
            compass_data: CompassData::new(),
            contents: Contents::new(),
            crafting: CraftingStations::new(),
            dialogs: Dialogs::new(),
            key_value: KeyValueStore::new(),
            ward_maps: WardMaps::new(),
            ward_permits: WardPermits::new(),
        }
    }
}

impl world::ext::Components<Rc<UpdateBuilder>> for Components {
    fn destroy_client(&mut self, id: ClientId, update: &Rc<UpdateBuilder>) {
        let Components { now, ref mut dialogs, ref mut crafting,
                         ref mut client_cameras, ref mut key_value, .. } = *self;
        client_cameras.handle_destroy_client(id, update);
        dialogs.handle_destroy_client(id, update, |d| {
            if let &Dialog::Crafting(ref d) = d {
                crafting.handle_close_dialog(d.station, now, update);
            }
        });
        key_value.handle_destroy(AnyId::Client(id), update);
    }

    fn destroy_entity(&mut self, id: EntityId, update: &Rc<UpdateBuilder>) {
        let Components { ref mut char_invs, ref mut key_value, .. } = *self;
        // Note that these functions don't modify `update`.  Clients are expected to know that
        // destroying an object destroys all of its components.  This is certainly the effect that
        // `update::apply` has - destroying the object comes through this code path to clean up all
        // components.
        char_invs.handle_destroy_entity(id, update);
        key_value.handle_destroy(AnyId::Entity(id), update);
    }

    fn destroy_inventory(&mut self, id: InventoryId, update: &Rc<UpdateBuilder>) {
        let Components { now, ref mut dialogs, ref mut crafting,
                         ref mut key_value, .. } = *self;
        dialogs.handle_destroy_inventory(id, update, |d| {
            if let &Dialog::Crafting(ref d) = d {
                crafting.handle_close_dialog(d.station, now, update);
            }
        });
        key_value.handle_destroy(AnyId::Inventory(id), update);
    }

    fn destroy_plane(&mut self, id: PlaneId, update: &Rc<UpdateBuilder>) {
        let Components { ref mut ward_maps, ref mut key_value, ref mut compass_data, .. } = *self;
        ward_maps.handle_destroy_plane(id, update);
        key_value.handle_destroy(AnyId::Plane(id), update);
        compass_data.handle_destroy_plane(id, update);
    }

    fn destroy_terrain_chunk(&mut self, id: TerrainChunkId, update: &Rc<UpdateBuilder>) {
        let Components { ref mut key_value, ref mut compass_data, .. } = *self;
        key_value.handle_destroy(AnyId::TerrainChunk(id), update);
        compass_data.handle_destroy_terrain_chunk(id, update);
    }

    fn destroy_structure(&mut self, id: StructureId, update: &Rc<UpdateBuilder>) {
        let Components { now, ref mut bitmaps, ref mut contents, ref mut crafting,
                         ref mut dialogs, ref mut key_value, ref mut compass_data, .. } = *self;
        bitmaps.handle_destroy_structure(id, update);
        contents.handle_destroy_structure(id, update);
        crafting.handle_destroy_structure(id, update);
        dialogs.handle_destroy_structure(id, update, |d| {
            if let &Dialog::Crafting(ref d) = d {
                crafting.handle_close_dialog(d.station, now, update);
            }
        });
        key_value.handle_destroy(AnyId::Structure(id), update);
        compass_data.handle_destroy_structure(id, update);
    }
}
