use server_types::*;

use component::dialog;
use component::dialog::{Dialog, DialogImpl, Action};
use engine::Engine;
use ext::Ext;
use logic;


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct PonyEdit {
    // TODO: doesn't make sense to have this here, but it simplifies the dialog handling in server2
    pub name: String,
}

pub fn new(name: String) -> Dialog {
    Dialog::PonyEdit(PonyEdit {
        name: name,
    })
}

impl DialogImpl for PonyEdit {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
        match action {
            Action::CreateCharacter(appearance) => {
                if logic::client::create_pawn(eng, id, appearance) {
                    dialog::close(eng, id);
                }
            },
            _ => {
                error!("{:?} sent invalid action for PonyEdit dialog: {:?}", id, action);
            },
        }
    }

    fn handle_cancel<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId) {
        // The client isn't allowed to cancel this dialog.  If they try, reopen it.
        dialog::open(eng, id, new(self.name.clone()));
    }
}
