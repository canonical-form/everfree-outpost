use server_types::*;
use std::collections::HashMap;
use common::Appearance;
use common::util::SmallVec;
use common::util::btree_multimap::Bounded;
use common::util::pubsub::DirectSub;

use component::crafting::StationId;
use engine::Engine;
use ext::Ext;
use update::UpdateBuilder;


pub mod crafting;
pub mod inventory;
pub mod pony_edit;

use self::crafting::Crafting;
use self::inventory::{Inventory, Abilities, Equipment, Container};
use self::pony_edit::PonyEdit;


#[allow(unused_variables)]
pub trait DialogImpl {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {}

    /// Called when the client attempts to cancel the dialog.
    fn handle_cancel<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId) {}

    /// Called when the dialog is closed programmatically, from the server side.  This fires only
    /// when `dialog::close` is called, not when the dialog is closed automatically due to objects
    /// being destroyed.
    fn handle_close<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId) {}

    fn with_structures<F: FnMut(StructureId)>(&self, f: F) {}
    fn with_inventories<F: FnMut(InventoryId)>(&self, f: F) {}
    fn with_special_subs<F: FnMut(SpecialSub)>(&self, f: F) {}
}


macro_rules! define_types {
    ($($Variant:ident,)*) => {
        #[derive(Clone, Debug)]
        pub enum Dialog {
            $( $Variant($Variant), )*
        }

        impl DialogImpl for Dialog {
            fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
                match *self {
                    $( Dialog::$Variant(ref d) => {
                        d.handle_action(eng, id, action);
                    }, )*
                }
            }

            fn handle_cancel<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId) {
                match *self {
                    $( Dialog::$Variant(ref d) => d.handle_cancel(eng, id), )*
                }
            }

            fn handle_close<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId) {
                match *self {
                    $( Dialog::$Variant(ref d) => d.handle_close(eng, id), )*
                }
            }

            fn with_structures<F: FnMut(StructureId)>(&self, f: F) {
                match *self {
                    $( Dialog::$Variant(ref d) => d.with_structures(f), )*
                }
            }

            fn with_inventories<F: FnMut(InventoryId)>(&self, f: F) {
                match *self {
                    $( Dialog::$Variant(ref d) => d.with_inventories(f), )*
                }
            }

            fn with_special_subs<F: FnMut(SpecialSub)>(&self, f: F) {
                match *self {
                    $( Dialog::$Variant(ref d) => d.with_special_subs(f), )*
                }
            }

        }
    };
}

define_types! {
    PonyEdit,
    Inventory,
    Abilities,
    Equipment,
    Container,
    Crafting,
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Action {
    CreateCharacter(Appearance),

    MoveItem {
        src_inv: InventoryId,
        src_slot: u8,
        dest_inv: InventoryId,
        dest_slot: u8,
        count: u8,
    },

    CraftingStart {
        recipe_id: RecipeId,
        count: u8,
    },
    CraftingStop,
}

/// Special subscription identifiers.  A dialog can have a "special subscription", which indicates
/// that some additional server-side state should be replicated to the client as long as the dialog
/// is open.  The actual replication is handled outside the engine.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum SpecialSub {
    CraftingState(StationId),
}

impl Bounded for SpecialSub {
    fn min_bound() -> Self {
        SpecialSub::CraftingState(Bounded::min_bound())
    }

    fn max_bound() -> Self {
        SpecialSub::CraftingState(Bounded::max_bound())
    }
}


pub struct Dialogs {
    /// Holds info on dialogs that are currently open.  Every entry in this map should have
    /// corresponding subscriptions in `structure_sub` and `inventory_sub`, for all of the
    /// structures and inventories referenced by the dialog.
    current: HashMap<ClientId, Dialog>,

    structure_sub: DirectSub<StructureId, ClientId>,
    inventory_sub: DirectSub<InventoryId, ClientId>,
}

impl Dialogs {
    pub fn new() -> Dialogs {
        Dialogs {
            current: HashMap::new(),

            structure_sub: DirectSub::new(),
            inventory_sub: DirectSub::new(),
        }
    }

    /// Perform the internal parts of opening a dialog.  This adds the dialog to `current` and
    /// subscribes the client to its structures and inventories, but doesn't notify the client in
    /// any way.
    fn open_internal(&mut self, cid: ClientId, dialog: Dialog) {
        assert!(!self.current.contains_key(&cid));

        dialog.with_structures(|sid| {
            self.structure_sub.subscribe(cid, sid);
        });
        dialog.with_inventories(|iid| {
            self.inventory_sub.subscribe(cid, iid);
        });

        self.current.insert(cid, dialog);
    }

    /// Perform the internal parts of closing a dialog.  This removes the dialog from `current` and
    /// unsubscribes the client from its structures and inventories, but doesn't notify the client
    /// in any way.
    fn close_internal(&mut self, cid: ClientId) -> Dialog {
        let dialog = self.current.remove(&cid).unwrap();

        dialog.with_structures(|sid| {
            self.structure_sub.unsubscribe(cid, sid);
        });
        dialog.with_inventories(|iid| {
            self.inventory_sub.unsubscribe(cid, iid);
        });

        dialog
    }

    pub fn contains(&self, cid: ClientId) -> bool {
        self.current.contains_key(&cid)
    }

    pub fn get(&self, cid: ClientId) -> Option<&Dialog> {
        self.current.get(&cid)
    }

    pub fn handle_destroy_client<F>(&mut self,
                                    cid: ClientId,
                                    ub: &UpdateBuilder,
                                    mut callback: F)
            where F: FnMut(&Dialog) {
        if self.contains(cid) {
            let d = self.close_internal(cid);
            callback(&d);
            ub.client_dialog_destroyed(cid, Some(d));
        }
    }

    pub fn handle_destroy_structure<F>(&mut self,
                                       sid: StructureId,
                                       ub: &UpdateBuilder,
                                       mut callback: F)
            where F: FnMut(&Dialog) {
        let mut clients = SmallVec::new();
        self.structure_sub.message(&sid, |_, &cid| {
            clients.push(cid);
        });

        for &cid in clients.iter() {
            let d = self.close_internal(cid);
            callback(&d);
            ub.client_dialog(cid, Some(&d));
        }
    }

    pub fn handle_destroy_inventory<F>(&mut self,
                                       iid: InventoryId,
                                       ub: &UpdateBuilder,
                                       mut callback: F)
            where F: FnMut(&Dialog) {
        let mut clients = SmallVec::new();
        self.inventory_sub.message(&iid, |_, &cid| {
            clients.push(cid);
        });

        for &cid in clients.iter() {
            let d = self.close_internal(cid);
            callback(&d);
            ub.client_dialog(cid, Some(&d));
        }
    }
}


/// Open a dialog for a client.
pub fn open<E: Ext>(eng: &mut Engine<E>,
                    id: ClientId,
                    dialog: Dialog) {
    if eng.w.dialogs.contains(id) {
        return;
    }

    eng.w.dialogs.open_internal(id, dialog);
    // If `open_internal` succeeded, then the dialog changed from `None` to `Some`.
    eng.update.client_dialog(id, None);
}

/// Close a client's current dialog.
pub fn close<E: Ext>(eng: &mut Engine<E>,
                     id: ClientId) -> Option<Dialog> {
    if !eng.w.dialogs.contains(id) {
        return None;
    }

    let dialog = eng.w.dialogs.close_internal(id);
    eng.update.client_dialog(id, Some(&dialog));
    dialog.handle_close(eng, id);
    Some(dialog)
}


/// Names of dialogs that can be opened arbitrarily by the client.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum ClientDialog {
    Inventory,
    Abilities,
    Equipment,
}

pub fn on_request_open<E: Ext>(eng: &mut Engine<E>,
                               id: ClientId,
                               client_dialog: ClientDialog) {
    if eng.dialogs.contains(id) {
        warn!("on_open_dialog: {:?} already has a dialog open", id);
        return;
    }

    // Get the server-side dialog representation, or bail out.
    let dialog = {
        // All ClientDialogs currently require CharInvs.
        let c = unwrap_or_error!(eng.w.get_client(id),
                                 "on_open_dialog: nonexistent {:?}", id);
        let eid = unwrap_or_warn!(c.pawn_id(),
                                  "on_open_dialog: {:?} has no pawn", id);
        let invs = unwrap_or_warn!(eng.char_invs.get(eid),
                                   "on_open_dialog: {:?} pawn {:?} has no invs", id, eid);

        match client_dialog {
            ClientDialog::Inventory => inventory::inventory(invs.main),
            ClientDialog::Abilities => inventory::abilities(invs.ability),
            ClientDialog::Equipment => inventory::equipment(invs.main, invs.equip),
        }
    };

    open(eng, id, dialog);
}

pub fn on_action<E: Ext>(eng: &mut Engine<E>,
                         id: ClientId,
                         act: Action) {
    let d = unwrap_or_warn!(eng.dialogs.get(id),
                            "no dialog for {:?}", id).clone();
    trace!("action for {:?}: {:?}", id, act);
    d.handle_action(eng, id, act);
}

pub fn on_cancel<E: Ext>(eng: &mut Engine<E>,
                         id: ClientId) {
    let d = unwrap_or_warn!(close(eng, id),
                            "no dialog for {:?}", id);
    trace!("cancel for {:?}", id);
    d.handle_cancel(eng, id);
}

