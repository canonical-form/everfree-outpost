use server_types::*;
use std::collections::{HashMap, HashSet};

use engine::Engine;
use ext::Ext;
use update::UpdateBuilder;


/// For a given chunk, this component records the positions of vaults near the chunk.  The "chaotic
/// compass" item uses this information to compute which direction it should point.
///
/// This is initialized during terrain gen and never changes afterward.
#[derive(Clone, Debug, Default)]
pub struct CompassTargets {
    /// Positions (in tile coordinates) of vaults within compass radius of this chunk.
    pub points: Vec<V2>,
}

/// We identify vaults by their location as it appears in `CompassTargets`.  
pub type VaultId = V2;

/// For a given plane, this component records the set of vaults / compass targets that have been
/// "used up", meaning at least one of the vault's loot chests has been opened by a player.  Currently we store them
/// all in one giant hash set, instead of using a quadtree.
#[derive(Clone, Debug, Default)]
pub struct LootedVaults {
    pub set: HashSet<VaultId>,
}


#[derive(Default)]
pub struct CompassData {
    targets: HashMap<TerrainChunkId, CompassTargets>,
    looted: HashMap<PlaneId, LootedVaults>,
    parents: HashMap<StructureId, VaultId>,
}

impl CompassData {
    pub fn new() -> CompassData {
        Self::default()
    }

    /// Mark a vault as used.  Requires that the UsedCompassTargets component is already present on
    /// the Plane.  (This component should be created during terrain gen.)
    pub fn set_looted(&mut self, pid: PlaneId, vault: VaultId, ub: &UpdateBuilder) {
        let looted = unwrap_or_error!(self.looted.get_mut(&pid),
                                      "plane {:?} has no LootedVaults", pid);
        if looted.set.insert(vault) {
            ub.event_looted_vaults_add(pid, vault);
        }
    }

    pub fn looted(&self, pid: PlaneId, vault: VaultId) -> bool {
        let looted = unwrap_or!(self.looted.get(&pid), return false);
        looted.set.contains(&vault)
    }

    pub fn get_parent_vault(&self, sid: StructureId) -> Option<VaultId> {
        self.parents.get(&sid).cloned()
    }

    pub fn nearest_target(&self,
                          pid: PlaneId,
                          tcid: TerrainChunkId,
                          pos: V3) -> Option<(VaultId, V3)> {
        let targets = unwrap_or!(self.targets.get(&tcid), return None);
        let tile_pos = pos.px_to_tile().reduce();
        targets.points.iter()
            .filter(|&&p| !self.looted(pid, p))
            .min_by_key(|&&p| (p - tile_pos).mag2())
            .map(|&p| (p, p.extend(0).tile_to_px()))
    }

    pub fn handle_destroy_plane(&mut self, id: PlaneId, ub: &UpdateBuilder) {
        if let Some(old_looted) = self.looted.remove(&id) {
            ub.plane_looted_vaults_destroyed(id, Some(old_looted));
        }
    }

    pub fn handle_destroy_terrain_chunk(&mut self, id: TerrainChunkId, ub: &UpdateBuilder) {
        if let Some(old_targets) = self.targets.remove(&id) {
            ub.terrain_chunk_compass_targets_destroyed(id, Some(old_targets));
        }
    }

    pub fn handle_destroy_structure(&mut self, id: StructureId, ub: &UpdateBuilder) {
        if let Some(old_parent) = self.parents.remove(&id) {
            ub.structure_parent_vault_destroyed(id, Some(old_parent));
        }
    }

    pub fn get_compass_targets(&self, id: TerrainChunkId) -> Option<&CompassTargets> {
        self.targets.get(&id)
    }

    pub fn get_looted_vaults(&self, id: PlaneId) -> Option<&LootedVaults> {
        self.looted.get(&id)
    }


    pub fn apply_compass_targets_silent(&mut self,
                                        id: TerrainChunkId,
                                        targets: Option<CompassTargets>) {
        if let Some(targets) = targets {
            self.targets.insert(id, targets);
        } else {
            self.targets.remove(&id);
        }
    }

    pub fn apply_looted_vaults_silent(&mut self,
                                      id: PlaneId,
                                      looted: Option<LootedVaults>) {
        if let Some(looted) = looted {
            self.looted.insert(id, looted);
        } else {
            self.looted.remove(&id);
        }
    }

    pub fn apply_parent_vault_silent(&mut self,
                                     id: StructureId,
                                     parent: Option<VaultId>) {
        if let Some(parent) = parent {
            self.parents.insert(id, parent);
        } else {
            self.parents.remove(&id);
        }
    }
}

pub fn set_looted<E: Ext>(eng: &mut Engine<E>,
                          id: PlaneId,
                          vault_id: VaultId) {
    eng.w.compass_data.set_looted(id, vault_id, &eng.update);
}
