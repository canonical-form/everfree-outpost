use server_types::*;
use std::cmp;

use engine::Engine;
use ext::Ext;
use world::ext::Ext as WExt;
use world::objects::*;
use world::object_ref::{ObjectRef, ObjectMut};


/// Count the total number of `item` across all stacks.
pub fn count<E: WExt>(i: ObjectRef<E, Inventory>,
                      item: ItemId) -> u16 {
    i.contents().iter()
     .filter(|slot| slot.id == item)
     .map(|slot| slot.count as u16)
     .sum()
}

/// Count the total amount of space available for inserting more `item`.
pub fn count_space<E: WExt>(i: ObjectRef<E, Inventory>,
                            item: ItemId) -> u16 {
    let max_stack = 255;
    i.contents().iter()
     .filter(|slot| slot.id == item || slot.id == NO_ITEM)
     .map(|slot| max_stack - slot.count as u16)
     .sum()
}

/// Count the total amount of space available in existing stacks of `item`.  This is like
/// `count_space`, but ignores the possibility of placing items in currently-empty slots.
pub fn count_space_non_empty<E: WExt>(i: ObjectRef<E, Inventory>,
                                      item: ItemId) -> u16 {
    let max_stack = 255;
    i.contents().iter()
     .filter(|slot| slot.id == item)
     .map(|slot| max_stack - slot.count as u16)
     .sum()
}


/// Check if it's possible to add the indicated item to the inventory.
pub fn can_add<E: WExt>(i: ObjectRef<E, Inventory>,
                        item: ItemId,
                        count: u16) -> bool {
    if count == 0 {
        return true;
    }

    let max_stack = 255;

    let mut count = count;
    for slot in i.contents() {
        if slot.id != item && slot.id != NO_ITEM {
            continue;
        }

        let space = (max_stack - slot.count) as u16;
        let placed = cmp::min(count, space);
        count -= placed;
        if count == 0 {
            return true;
        }
    }

    // Got here without placing all `count` items.  So there's no space.
    false
}

/// Check if it's possible to add all the indicated items to the inventory.
///
/// Assumes (but does not check) that all item IDs in the list are distinct.  If this is not the
/// case, the function may wrongly return `true`.
pub fn can_add_multi<E: WExt>(i: ObjectRef<E, Inventory>,
                              items: &[(ItemId, u16)]) -> bool {
    if items.len() == 0 {
        return true;
    }

    let mut num_empty = i.contents().iter().filter(|slot| slot.id == NO_ITEM).count() as u16;

    for &(item, count) in items {
        let max_stack = 255;

        // First, try filling in existing stacks of the same item.
        let existing_space = count_space_non_empty(i, item);
        if existing_space >= count {
            // We placed all items of this type, in existing stacks.
            continue;
        }
        let count = count - existing_space;

        // Now try creating new stacks for the remaining items.
        let stacks = (count + max_stack - 1) / max_stack;
        if stacks > num_empty {
            // Some of the stacks don't fit, so we can't add all the items.
            return false;
        }
        // Some formerly empty slots are now unavailable for the remaining items.
        num_empty -= stacks;
    }

    // Placed all items successfully.
    true
}

/// Add the indicated item to an inventory's `contents` list.
///
/// Note that this does not report an error upon running out of space.  The extra items will simply
/// be dropped.  Use `can_add` to check if this function will succeed.
pub fn add_contents(contents: &mut [Item],
                    item: ItemId,
                    count: u16) {
    if count == 0 {
        return;
    }

    let max_stack = 255;

    let mut count = count;

    // Prefer to grow existing stacks of `item` first.  Only start a new stack in an empty slot if
    // absolutely necessary.
    for &stack_with_empty in &[false, true] {
        for slot in contents.iter_mut() {
            if !(slot.id == item || (stack_with_empty && slot.id == NO_ITEM)) {
                continue;
            }

            let space = (max_stack - slot.count) as u16;
            let placed = cmp::min(count, space);

            if slot.id == NO_ITEM {
                slot.id = item;
                slot.count = placed as u8;
            } else {
                slot.count += placed as u8;
            }

            count -= placed;
            if count == 0 {
                return;
            }
        }
    }
}

/// Add the indicated item to the inventory.
///
/// Note that this does not report an error upon running out of space.  The extra items will simply
/// be dropped.  Use `can_add` to check if this function will succeed.
pub fn add<E: WExt>(mut i: ObjectMut<E, Inventory>,
                    item: ItemId,
                    count: u16) {
    i.modify_contents(|contents| add_contents(contents, item, count));
}

/// Check if it's possible to remove the indicated item to the inventory.  Returns false if there
/// are fewer than `count` items of the specified type.
pub fn can_remove<E: WExt>(i: ObjectRef<E, Inventory>,
                           item: ItemId,
                           count: u16) -> bool {
    if count == 0 {
        return true;
    }

    let mut count = count;
    for slot in i.contents() {
        if slot.id != item {
            continue;
        }

        let avail = slot.count as u16;
        let taken = cmp::min(count, avail);
        count -= taken;
        if count == 0 {
            return true;
        }
    }

    false
}

/// Remove the indicated item from an inventory's `contents` list.
///
/// Note that this does not report an error upon running out of items.  If there are fewer than
/// `count` items of the specified type, all such items will be removed.  Use `can_remove` to check
/// if this function will succeed.
pub fn remove_contents(contents: &mut [Item],
                       item: ItemId,
                       count: u16) {
    if count == 0 {
        return;
    }

    let mut count = count;

    // Take from the highest-indexed slots first.
    for slot in contents.iter_mut().rev() {
        if slot.id != item && slot.id != NO_ITEM {
            continue;
        }

        let avail = slot.count as u16;
        let taken = cmp::min(count, avail);

        slot.count -= taken as u8;
        if slot.count == 0 {
            slot.id = NO_ITEM;
        }

        count -= taken;
        if count == 0 {
            break;
        }
    }
}

/// Remove the indicated item from the inventory.
///
/// Note that this does not report an error upon running out of items.  If there are fewer than
/// `count` items of the specified type, all such items will be removed.  Use `can_remove` to check
/// if this function will succeed.
pub fn remove<E: WExt>(mut i: ObjectMut<E, Inventory>,
                       item: ItemId,
                       count: u16) {
    i.modify_contents(|contents| remove_contents(contents, item, count));
}


pub fn transfer_slot<E: Ext>(eng: &mut Engine<E>,
                             src_iid: InventoryId,
                             src_slot: u8,
                             dest_iid: InventoryId,
                             dest_slot: u8,
                             count: u8) {
    if (src_iid, src_slot) == (dest_iid, dest_slot) {
        return;
    }

    // Check that the transfer is possible.
    let item;
    let move_count = {
        let src = unwrap_or!(eng.get_inventory(src_iid));
        let src_item = unwrap_or!(src.contents().get(src_slot as usize));
        item = src_item.id;
        if item == NO_ITEM {
            return;
        }
        let src_avail = src_item.count;

        let max_stack = 255;

        let dest = unwrap_or!(eng.get_inventory(dest_iid));
        let dest_item = unwrap_or!(dest.contents().get(dest_slot as usize));
        if dest_item.id != item && dest_item.id != NO_ITEM {
            return;
        }
        let dest_space = max_stack - dest_item.count;

        cmp::min(count, cmp::min(src_avail, dest_space))
    };

    info!("moving {}x {}", move_count, item);

    if move_count == 0 {
        return;
    }

    // `dest` side of the transfer
    {
        let mut dest = eng.inventory_mut(dest_iid);
        dest.modify_contents(|contents| {
            info!("adding {}x {} to {:?} #{}", move_count, item, dest_iid, dest_slot);
            let i = &mut contents[dest_slot as usize];
            i.count += move_count;
            i.id = item;
        });
    }

    // `src` side of the transfer
    {
        let mut src = eng.inventory_mut(src_iid);
        src.modify_contents(|contents| {
            info!("removing {}x {} from {:?} #{}", move_count, item, src_iid, src_slot);
            let i = &mut contents[src_slot as usize];
            i.count -= move_count;
            if i.count == 0 {
                i.id = NO_ITEM;
            }
        });
    }
}
