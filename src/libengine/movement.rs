//! This module handles walking and related entity movements.
//!
//! # Interaction with other activities
//!
//! Normally, this module receives movement inputs from the client, and applies them by changing
//! the relevant entity's `activity` field.  However, other modules and scripts may change entity
//! activities as well.  To support interoperability, the movement system has a notion of an entity
//! "trying to move".
//!
//! Each tick, for each entity that is "trying to move", the movement update function will try to
//! interrupt the current activity and replace it with the appropriate "walk" activity.  Entities
//! that are not trying to move will have their activities left unchanged.
//!
//! An entity is "trying to move" under two conditions.  First, if the most recent inputs for that
//! entity include a non-zero movement speed.  And second, if an input applied in the current tick
//! changed the movement speed to zero.  The second condition ensures that the entity stops moving
//! (switches to `Activity::Stand`) when the client sends the "key released" message.
//!
//! The implementation of "trying to move" has a few parts.  The `simulate` function, which runs
//! physics and applies activity changes, determines whether an entity is trying to move based on
//! the presence or absence of an entry in `Movement::cur`.  Usually, `cur` only contains entries
//! with non-zero movement speed, so this properly enforces the first condition.
//!
//! However, this is enforced only at the end of each `update` call.  During `update`, every input
//! event will be applied to `Movement::cur`, even if it has zero speed.  The subsequent `simulate`
//! will then see the entry and update the activity accordingly, causing the entity to stop moving.
//! This implements the second condition.  At the end of `update`, entries with zero speed are
//! removed from `cur`, so they will no longer interrupt emotes or other activities.

use server_types::*;
use std::collections::hash_map::{self, HashMap};
use std::collections::VecDeque;
use std::u16;
use common::movement::{self, ShapeSource};
use world::objects::Activity;

use cache::terrain::TerrainCache;
use engine::Engine;
use ext::{Ext, FullWorld};
use tick::TICK_MS;
use update::{Update, flags};



#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Input {
    dir: u8,
    speed: u8,
}

impl Input {
    pub fn new(dir: u8, speed: u8) -> Option<Input> {
        if dir >= 8 {
            error!("dir out of range: {}", dir);
            return None;
        }

        if speed == 2 || speed >= 4 {
            error!("speed out of range: {}", speed);
            return None;
        }

        Some(Input { dir: dir, speed: speed })
    }
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct EntityMovement {
    input_time: Time,
    input: Input,
}

impl EntityMovement {
    fn new(input_time: Time, input: Input) -> EntityMovement {
        EntityMovement {
            input_time: input_time,
            input: input,
        }
    }
}



const WHEEL_SIZE: usize = 32;

type WheelBucket = Vec<(Time, EntityId, Input)>;

pub struct Movement {
    wheel: VecDeque<WheelBucket>,
    cur: HashMap<EntityId, EntityMovement>,
    time_base: HashMap<EntityId, Time>,
}

impl Movement {
    pub fn new() -> Movement {
        let mut wheel = VecDeque::with_capacity(WHEEL_SIZE);
        for _ in 0 .. WHEEL_SIZE {
            wheel.push_back(Vec::new());
        }

        Movement {
            wheel: wheel,
            cur: HashMap::new(),
            time_base: HashMap::new(),
        }
    }

    /// Queue an input event, and return the time when it will be applied.  Returns `None` if the
    /// event won't be applied due to some kind of error.
    fn queue(&mut self, now: Time, time: Time, id: EntityId, input: Input) -> Option<Time> {
        let delta = time - now;
        let delta = 
            if delta < 0 {
                warn!("{:?}: time is in the past ({} ms)", id, delta);
                0
            } else {
                delta
            };

        // TODO: set `time` to `now` if `time` is in the past
        // TODO: don't allow time_base to move backward

        // Note that this code assumes `now` lies on a boundary between two buckets.
        let idx = (delta / TICK_MS) as usize;
        if idx >= WHEEL_SIZE {
            error!("{:?}: time is too far in the future ({} ticks)", id, idx);
            return None;
        }

        self.wheel[idx].push((time, id, input));
        self.time_base.insert(id, time);

        Some(time)
    }

    fn queue_start(&mut self,
                   now: Time,
                   delay: Time,
                   id: EntityId,
                   input: Input) -> Option<Time> {
        let time = now + delay;

        self.queue(now, time, id, input)
    }

    fn queue_change(&mut self,
                    now: Time,
                    rel_time: Time,
                    id: EntityId,
                    input: Input) -> Option<Time> {
        let time_base = *unwrap_or!(self.time_base.get(&id), {
            error!("{:?}: no time_base for entity", id);
            return None;
        });
        let time = time_base + rel_time;

        self.queue(now, time, id, input)
    }

    fn take_bucket(&mut self) -> WheelBucket {
        let b = self.wheel.pop_front().unwrap();
        self.wheel.push_back(Vec::new());
        b
    }

    fn remove_entity(&mut self, now: Time, id: EntityId) {
        self.cur.remove(&id);
        if let Some(time_base) = self.time_base.remove(&id) {
            // Check if we need to filter the wheel to remove future inputs of this entity.
            // Filtering the entire wheel is expensive, so we avoid it if possible by checking
            // `time_base`.  `time_base` gets set to the input time every time an input is queued,
            // so only buckets for timestamps between `now` and `time_base` may contain events for
            // the removed entity.

            if time_base >= now {
                let max_idx = ((time_base - now) / TICK_MS) as usize;
                for idx in 0 .. max_idx + 1 {
                    self.wheel[idx].retain(|&(_, input_id, _)| input_id != id);
                }
            }
        }
    }

    pub fn update(&mut self, w: &FullWorld, u: &Update) {
        for (id, f) in u.entities() {
            if f.contains(flags::E_DESTROYED) {
                self.remove_entity(w.now, id);
            }
        }
    }

    pub fn entity_ids(&self) -> MovingEntityIds {
        MovingEntityIds(self.cur.keys())
    }
}

pub struct MovingEntityIds<'a>(hash_map::Keys<'a, EntityId, EntityMovement>);

impl<'a> Iterator for MovingEntityIds<'a> {
    type Item = EntityId;

    fn next(&mut self) -> Option<EntityId> {
        self.0.next().map(|&id| id)
    }
}


/// Run physics for entity `id` over times `start .. end`.  Entity `id` must exist.
fn simulate<E: Ext>(eng: &mut Engine<E>,
                    id: EntityId,
                    start: Time,
                    end: Time) {
    let input = unwrap_or!(eng.movement.cur.get(&id)).input;

    let mut act: Activity;
    let mut act_start: Time;
    let pos: V3;
    let plane: PlaneId;
    {
        let e = eng.w.entity(id);
        act = e.activity().clone();
        act_start = e.activity_start();
        pos = e.pos(start);
        plane = e.plane_id();
    }

    let mut now = start;
    let mut changed = false;

    let mut shape = ChunksSource {
        cache: &eng.terrain_cache,
        z: pos.z >> TILE_BITS,
        plane,
    };

    while let Some((new_act, new_start)) = movement::update_movement(
            &mut shape, act.clone(), act_start,
            input.dir, input.speed as i32 * MOVE_SPEED,
            now, end) {
        act = new_act;
        act_start = new_start;
        now = new_start;
        changed = true;
        trace!("  update: {:?}, at {}", act, act_start);
    }

    if changed {
        trace!("set activity: {:?}, {:?}, at {}", id, act, act_start);
        let mut e = eng.w.entity_mut(id);
        e.set_activity(act, act_start);
    }
}

pub fn update<E: Ext>(eng: &mut Engine<E>) {
    let now = eng.w.now;
    let next = now + TICK_MS;

    // Process inputs in the order they were received.  For each input, simulate the entity up to
    // the time of the input, then apply the input.  At the end, simulate every entity up to time
    // `next`.
    //
    // Note that this may result in entity activities being changed out of order.  For example,
    // entity 1 may update at times 5 and 10, then entity 2 may update at times 3 and 7.  The
    // updates would occur with timestamps 5, 10, 3, 7.
    let mut entity_time = eng.movement.cur.keys()
                             .map(|&id| (id, now))
                             .collect::<HashMap<_, _>>();
    let inputs = eng.movement.take_bucket();

    for (time, id, input) in inputs {
        // Check that id is valid.  It's possible the entity was destroyed and we haven't been
        // updated yet.
        if eng.w.get_entity(id).is_none() {
            continue;
        }

        trace!("apply {:?} at {} for {:?}", input, time, id);

        // Simulate the entity up to `time`.
        let start_time = *entity_time.entry(id).or_insert(now);
        simulate(eng, id, start_time, time);
        entity_time.insert(id, time);

        // Set the current input based on the event.
        //
        // Note that this step may insert an input with zero speed.  This means the entity is
        // "trying to move", or really "trying to stop moving".  If the speed is still zero at the
        // end of `update`, the entry will be removed from cur so it doesn't interrupt emotes.
        eng.movement.cur.insert(id, EntityMovement::new(time, input));

        // Note that we don't simulate *after* setting the input.  That part will be handled by the
        // next iteration for this entity, at which point we know the end time for the simulation,
        // or by the final simulation step up to time `next` at the end of the function.
    }

    for (id, time) in entity_time {
        if eng.w.get_entity(id).is_none() {
            continue;
        }
        simulate(eng, id, time, next);
    }

    // TODO: filter zero-speed entries out of `eng.movement.cur`.
}


enum QueueError {
    NoSuchClient,
    Message(String),
}

impl From<String> for QueueError {
    fn from(x: String) -> QueueError {
        QueueError::Message(x)
    }
}

fn queue_input_result<E: Ext>(eng: &mut Engine<E>,
                              cid: ClientId,
                              start: bool,
                              time: Time,
                              dir: u8,
                              speed: u8) -> Result<Time, QueueError> {
    let now = eng.w.now;
    let eid = {
        let c = eng.w.get_client(cid)
            .ok_or(QueueError::NoSuchClient)?;
        let e = c.pawn()
            .ok_or_else(|| format!("no pawn for {:?}", cid))?;
        e.id()
    };
    trace!("queue_{} {:?} delay={} dir={} speed={}",
           if start { "start" } else { "change" }, eid, time, dir, speed);
    let input = Input::new(dir, speed)
        .ok_or_else(|| format!("invalid input"))?;
    let opt_time = if start {
        eng.movement.queue_start(now, time, eid, input)
    } else {
        eng.movement.queue_change(now, time, eid, input)
    };
    let time = opt_time.ok_or_else(|| format!("failed to queue input"))?;
    Ok(time)
}

fn queue_input<E: Ext>(eng: &mut Engine<E>,
                       cid: ClientId,
                       start: bool,
                       time: Time,
                       dir: u8,
                       speed: u8) {
    match queue_input_result(eng, cid, start, time, dir, speed) {
        Ok(queue_time) => {
            eng.ext.ack_request(cid, queue_time);
        },
        Err(QueueError::NoSuchClient) => {
            error!("error queueing input for {:?}: no such client", cid);
        },
        Err(QueueError::Message(e)) => {
            warn!("error queueing input for {:?}: {}", cid, e);
            eng.ext.nak_request(cid);
        },
    }
}


pub fn on_queue_start<E: Ext>(eng: &mut Engine<E>,
                              cid: ClientId,
                              delay: u16,
                              dir: u8,
                              speed: u8) {
    queue_input(eng, cid, true, delay as Time, dir, speed);
}



pub fn on_queue_change<E: Ext>(eng: &mut Engine<E>,
                               cid: ClientId,
                               rel_time: u16,
                               dir: u8,
                               speed: u8) {
    queue_input(eng, cid, false, rel_time as Time, dir, speed);
}


struct ChunksSource<'a> {
    cache: &'a TerrainCache,
    z: i32,
    plane: PlaneId,
}

impl<'a> ShapeSource for ChunksSource<'a> {
    fn can_pass(&mut self, _start: V2, end: V2) -> bool {
        let shape = self.cache.get(self.plane, end.extend(self.z)).shape();
        shape == Shape::Floor
    }
}
