use std::ops::{Deref, DerefMut};
use std::path::Path;
use std::rc::Rc;
use world::World;

use cache::structure::StructureCache;
use cache::terrain::TerrainCache;
use component::Components;
use component::inv_update::InvUpdates;
use lifecycle::conn_map::ConnMap;
use lifecycle::demand::Demand;
use lifecycle::import_fsm::ImportFsm;
use lifecycle::login_fsm::LoginFsm;
use movement::Movement;
use script::Scripting;
use timer::Timers;
use update::{self, UpdateBuilder, Delta};

use ext::{Ext, WorldExt};

pub struct Engine<E> {
    // Persistent game state
    pub w: World<WorldExt>,

    // Transient game state
    pub movement: Movement,

    // Lifecycle data
    pub conn_map: ConnMap,
    pub demand: Demand,
    pub import_fsm: ImportFsm,
    pub login_fsm: LoginFsm,

    // Components
    pub inv_updates: InvUpdates,

    // Caches
    pub structure_cache: StructureCache,
    pub terrain_cache: TerrainCache,

    // Misc
    pub scripting: Scripting<E>,
    pub timers: Timers<E>,

    pub update: Rc<UpdateBuilder>,
    pub ext: E,
}

impl<E: Ext> Engine<E> {
    pub fn new(boot_path: &Path,
               init_delta: Delta,
               ext: E) -> Engine<E> {
        let update = Rc::new(UpdateBuilder::new());

        let now = if init_delta.world_flags().contains(update::flags::W_TIME) {
            init_delta.new_world_time()
        } else {
            0
        };

        let components = Components::new(now);
        let w = World::<WorldExt>::new(components, update.clone());

        let mut eng = Engine {
            w: w,

            movement: Movement::new(),

            conn_map: ConnMap::new(),
            demand: Demand::new(),
            import_fsm: ImportFsm::new(),
            login_fsm: LoginFsm::new(),

            inv_updates: InvUpdates::new(),

            structure_cache: StructureCache::new(),
            terrain_cache: TerrainCache::new(),

            scripting: Scripting::new(boot_path),
            timers: Timers::new(now),

            update: update,
            ext: ext,
        };

        let mut init_delta = init_delta;
        update::apply::apply(&mut eng, &mut init_delta).unwrap();

        Scripting::handle(&mut eng, |h| h.on_engine_init());

        eng
    }
}

impl<E> Deref for Engine<E> {
    type Target = World<WorldExt>;

    fn deref(&self) -> &Self::Target {
        &self.w
    }
}

impl<E> DerefMut for Engine<E> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.w
    }
}
