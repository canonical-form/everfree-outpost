use server_types::*;
use world::objects::*;
use std::collections::HashMap;
use std::mem;
use server_extra::Value;
use syntax_exts::{
    if_ident,
    expand_objs_named_macro_safe,
    for_each_obj_named,
    for_each_obj_named_macro_safe,
};

use engine::Engine;
use ext::Ext;

use update::{UpdateKey, UpdateItem, FlagMap, flags};
use update::data_map::{DataMap, DataKey, ObjectType};
use update::event::{self, Event, EventRef, EventVec};
use update::traits::Component;


/// A summary of changes to the game state.
///
/// Conceptually, the `Delta` stores two values for each modified field: the "old" value as of the
/// start of tick or creation of the object, and the "new" value as of the end of tick or
/// destruction of the object.  Methods on `Delta` provide access to the old and new values for
/// modified objects.
///
/// Note that the `Delta` does not track old values for all fields, only for fields where the old
/// value is needed by some client code.  For other fields, the old value is not stored, and no
/// `old_obj_field` method is provided.  Similarly, the `Delta` omits new values for a few fields,
/// though nearly all fields and components are needed for either client or save-file updates.
///
/// # Implementation
///
/// Internally, things are a little more complicated, because we'd like to store an entire object
/// struct in one piece if we can.  Note we call the API-level "old" and "new" values "first" and
/// "last", to avoid confusion with the `old` and `new` fields.  The invariants we uphold are:
///
///  - The "first" value is `old(field)`, if present, or the "last" value otherwise.
///  - The "last" value is, in order of precedence, `new(struct).field`, `new(field)`, or the
///  current value from the `World`.
///  - If the object was created or destroyed in this tick, then `new(struct)` is present.
///    Otherwise, `new(field)`s are present, and `new(struct)` is not.
///  - In a complete `Delta` (post-`populate`), either `new(field)` or `new(struct)` is present for
///    every modified field.  (This implies there is no need to get values from the world.)
///
/// Here is the actual behavior on each kind of event:
///
///  - Object creation: store nothing in `old` or `new`.  ("First" and "last" are both drawn from
///    the world.)
///  - Object destruction: store the whole object and each of its components in `new`.  ("Last" is
///    drawn from `new`.  "First" is drawn from `old`, if there was a previous modify for this
///    field, and otherwise from `new`, since the "first" and "last" values are identical.)
///  - Field or component change: store the previous value in `old`, if an earlier value isn't
///    already present.  ("First" is now drawn from `old`.  Note that if the object was created,
///    then the pre-modify value of the first modification must be the value as of creation time.
///    "Last" is still drawn from the world, because `new` has no entry for this field.  Only the
///    destroy handler can add entries to `new` at this point, and destroyed objects can't be
///    modified.)
///
/// After all handlers have run, the "first" and "last" values are correct, but some values are
/// still drawn from the world.  The `populate` method fixes this, filling in `new` with enough
/// data that no created/modified/destroyed field values come from the world.  Specifically:
///
///  - If `X_DESTROYED` is set, do nothing (and return).  (`new` was fully populated by the destroy
///    handler, so "first" and "last" are drawn from there if nowhere else.)
///  - If `X_CREATED` is set, put the entire object and every component into `new`, as in the
///    destroy handler, and then return.  (The object wasn't destroyed, so it has no data in `new`.
///    That means all "last" values and some "first" values are still drawn from the world.)
///  - For each field flag that's set, put the current value of the field into `new`.  (Every
///    modified field now gets its "last" value from `new`.  Note that there are no structs in
///    `new`, because the object wasn't created or destroyed this tick, and only those cases add
///    structs.)
///
/// ## Example
///
/// Suppose an object is created in state A, its state is changed to B and later to C, and the
/// object is destroyed.
///
///  1. The creation handler runs, but does nothing.
///  2. The modify handler for B stores the current value, `A`, into `old`.
///  3. The modify handler for C stores nothing, because it's not the first modify of this field.
///  4. The destruction handler runs, storing the struct `{ state: C }` into `new`.
///  5. `populate` does nothing, because the `X_DESTROYED` flag is set.
///
/// "First" is `old(field)`, which is `A`.  "Last" is `new(struct).field` (present because the
/// object was created and destroyed in this tick), which is `C`.
///
/// Next, suppose an object is created in state A, then its state is changed to B.
///
///  1. The creation handler runs, but does nothing.
///  2. The modify handler for B stores the current value, `A`, into `old`.
///  3. `populate` sees `X_CREATED` is set, and stores the struct `{ state: B }` into `new`.
///
/// "First" is `old(field)`, which is `A`.  "Last" is `new(struct).field` (present because the
/// object was created in this tick), which is `B`.
///
/// Finally, suppose an object already exists in state A and its state is changed to B.
///
///  1. The modify handler for B stores the current value, `A`, into `old`.
///  2. `populate` sees the state field's flag is set, and stores the current value, `B`, into
///     `new`.
///
/// "First" is `old(field)`, which is `A`.  "Last" is `new(field)` (present because the object
/// was neither created nor destroyed in this tick), which is `B`.
#[derive(Debug)]
pub struct Delta {
    flags: FlagMap,
    old: DataMap,
    new: DataMap,
    events: EventVec,
}

// NB: If you set `store_new` to `true` for any of these, you must also update `populate`!


impl Delta {
    pub fn new() -> Delta {
        Delta {
            flags: FlagMap::new(),
            old: DataMap::new(),
            new: DataMap::new(),
            events: EventVec::new(),
        }
    }

    // These are convenience wrappers around methods of `flags`.  If the lifetimes are too strict
    // for you, use the methods on `flags` directly instead.

    pub fn keys<'a>(&'a self) -> impl Iterator<Item=UpdateKey> + 'a {
        self.flags.keys()
    }

    pub fn items<'a>(&'a self) -> impl Iterator<Item=UpdateItem> + 'a {
        self.flags.items()
    }


    pub fn world_flags(&self) -> flags::WorldUpdate {
        self.flags.world(())
    }

    for_each_obj! {
        pub fn $objs<'a>(&'a self) -> impl Iterator<Item=($ObjId, flags::$ObjUpdate)> + 'a {
            self.flags.$objs()
        }

        pub fn $obj_flags(&self, id: $ObjId) -> flags::$ObjUpdate {
            self.flags.$obj(id)
        }
    }


    pub fn new_plane_refs<'a>(&'a self) -> impl Iterator<Item=(PlaneId, Stable<PlaneId>)> + 'a {
        self.new.keys().filter_map(move |k| match k {
            DataKey::PlaneRef(id) => Some((id, *self.new.plane_ref(id))),
            _ => None,
        })
    }

    pub fn get_new_plane_ref(&self, id: PlaneId) -> Option<Stable<PlaneId>> {
        self.new.get_plane_ref(id).cloned()
    }


    // Update and accessor methods

    // World time
    pub fn old_world_time(&self) -> Time {
        *self.old.world_time(())
    }

    pub fn new_world_time(&self) -> Time {
        *self.new.world_time(())
    }

    // World stable ID counters
    pub fn old_world_next_stable_id(&self, ty: ObjectType) -> StableId {
        *self.old.world_next_stable_id(ty)
    }

    pub fn new_world_next_stable_id(&self, ty: ObjectType) -> StableId {
        *self.old.world_next_stable_id(ty)
    }

    // Whole objects
    for_each_obj! {
        pub fn $new_obj(&self, id: $ObjId) -> &$Obj {
            self.new.$obj(id)
        }
    }

    // Object core fields
    for_each_core_field! {
        if_ident! { $store_old == true:
            pub fn $old_obj(&self, id: $Id) -> ref_ty!($DM) {
                let flags = expand!($base => self.flags.#obj(id));
                if flags.contains(flags::$FLAG) {
                    owned_ref_to_ref!($DM, self.old.$obj(id))
                } else {
                    self.$new_obj(id)
                }
            }
        }

        if_ident! { $store_new == true:
            pub fn $new_obj(&self, id: $Id) -> ref_ty!($DM) {
                let flags = expand!($base => self.flags.#obj(id));
                if expand!($base => flags.contains(flags::#O_CREATED) ||
                                    flags.contains(flags::#O_DESTROYED)) {
                    let base = self.new.$base(id);
                    field_ref!($Tag, $Base, base)
                } else {
                    owned_ref_to_ref!($DM, self.new.$obj(id))
                }
            }
        }
    }

    // Components
    for_each_component! {
        if_ident! { $store_old == true:
            pub fn $old_obj(&self, id: $Id)
                            -> Option<ref_ty!($DM)> {
                let flags = expand!($base => self.flags.#obj(id));
                if flags.contains(flags::$FLAG) {
                    // Output of $get_obj is Option<&Option<T>>
                    self.old.$get_obj(id)
                        .and_then(|x| x.as_ref())
                        .map(|x| owned_ref_to_ref!($DM, x))
                } else {
                    self.$new_obj(id)
                }
            }
        }

        if_ident! { $store_new == true:
            pub fn $new_obj(&self, id: $Id)
                            -> Option<ref_ty!($DM)> {
                self.new.$get_obj(id)
                        .and_then(|x| x.as_ref())
                        .map(|x| owned_ref_to_ref!($DM, x))
            }
        }
    }


    // Events

    pub fn iter_events<'a>(&'a self) -> impl Iterator<Item=EventRef<'a>>+'a {
        self.events.iter()
    }


    // Incremental update handling

    pub fn merge(&mut self, other: Delta) {
        self.flags.merge(&other.flags);
        self.old.merge_under(other.old);
        self.new.merge_over(other.new);
    }


    // Internals

    pub fn as_internals_mut<F: FnOnce(&mut DeltaInternals) -> R, R>(&mut self, f: F) -> R {
        let mut di: DeltaInternals = mem::replace(self, Delta::new()).into();
        let r = f(&mut di);
        *self = di.into();
        r
    }
}


pub struct IncDelta {
    d: Delta,
}

impl IncDelta {
    pub fn new() -> IncDelta {
        IncDelta {
            d: Delta::new(),
        }
    }


    // World stable ID counters
    for_each_obj! {
        pub fn $next_obj_stable_id(&mut self, old: StableId) {
            self.d.flags.world_mut(()).insert(flags::$W_NEXT_OBJ);
            self.d.old.insert_world_next_stable_id(ObjectType::$Obj, old);
        }
    }


    // Whole objects
    for_each_obj! {
        pub fn $obj_created(&mut self, id: $ObjId) {
            self.d.flags.$obj_mut(id).insert(flags::$O_CREATED);
        }

        pub fn $obj_destroyed(&mut self, id: $ObjId, val: $Obj) {
            self.d.flags.$obj_mut(id).insert(flags::$O_DESTROYED);
            self.d.new.$insert_obj(id, val);
        }

        pub fn $obj_loaded(&mut self, id: $ObjId) {
            self.d.flags.$obj_mut(id).insert(flags::$O_LOADED);
        }

        pub fn $obj_unloaded(&mut self, id: $ObjId) {
            self.d.flags.$obj_mut(id).insert(flags::$O_UNLOADED);
        }
    }


    // Object core fields
    for_each_core_field! {
        #[allow(unused_variables)]  // `old` is used only when `$store_old == true`.
        pub fn $obj(&mut self,
                    id: $Id,
                    old: ref_ty!($DM)) {
            let flags = expand!($base => self.d.flags.#obj_mut(id));
            if !flags.contains(flags::$FLAG) {
                flags.insert(flags::$FLAG);
                if_ident! { $store_old == true:
                    self.d.old.$insert_obj(id, ref_to_owned!($DM, old)); }
            }
        }

        #[allow(unused_variables)]  // `old` is used only when `$store_old == true`.
        pub fn $obj_move(&mut self,
                         id: $Id,
                         old: owned_ty!($DM)) {
            let flags = expand!($base => self.d.flags.#obj_mut(id));
            if !flags.contains(flags::$FLAG) {
                flags.insert(flags::$FLAG);
                if_ident! { $store_old == true:
                    self.d.old.$insert_obj(id, old); }
            }
        }
    }


    // Components
    for_each_component! {
        /// Record the previous component value for an update.
        #[allow(unused_variables)]  // `old` is used only when `$store_old == true`.
        pub fn $obj(&mut self,
                    id: $Id,
                    old: Option<ref_ty!($DM)>) {
            let flags = expand!($base => self.d.flags.#obj_mut(id));
            if !flags.contains(flags::$FLAG) {
                flags.insert(flags::$FLAG);
                if_ident! { $store_old == true:
                    self.d.old.$insert_obj(id, old.map(|x| ref_to_owned!($DM, x))); }
            }
        }

        /// Record the previous component value for an update.
        #[allow(unused_variables)]  // `old` is used only when `$store_old == true`.
        pub fn $obj_move(&mut self,
                         id: $Id,
                         old: Option<owned_ty!($DM)>) {
            let flags = expand!($base => self.d.flags.#obj_mut(id));
            if !flags.contains(flags::$FLAG) {
                flags.insert(flags::$FLAG);
                if_ident! { $store_old == true:
                    self.d.old.$insert_obj(id, old.map(|x| x)); }
            }
        }

        /// Record the final component value prior to object destruction.
        #[allow(unused_variables)]  // `new` is used only when `$store_new == true`.
        pub fn $obj_destroyed(&mut self,
                              id: $Id,
                              new: Option<owned_ty!($DM)>) {
            // Don't bother inserting if `new` is `None`.  The `new_obj()` accessor will return
            // `None` whether the key is absent or present with a value of `None`.
            //
            // We could remove `Option` from the type of `new`, but it's convenient to let clients
            // call `obj_destroyed(self.map.remove(key))`.
            if new.is_some() {
                if_ident! { $store_new == true:
                    self.d.new.$insert_obj(id, new.map(|x| x)); }
            }
        }
    }


    // Special

    expand_objs! {
        pub fn object_key_value(&mut self,
                                id: AnyId,
                                old: Option<&HashMap<String, Value>>) {
            match id {
                $( AnyId::$Obj(id) => self.$obj_key_value(id, old), )*
            }
        }

        pub fn object_key_value_destroyed(&mut self,
                                          id: AnyId,
                                          new: Option<HashMap<String, Value>>) {
            match id {
                $( AnyId::$Obj(id) => self.$obj_key_value_destroyed(id, new), )*
            }
        }
    }


    // Events

    pub fn event<T: Event>(&mut self, evt: T) {
        self.d.events.push(evt);
    }

    for_each_obj_named! { [[update_events]]
        expand_objs_named_macro_safe! { $args
            pub fn $event_obj(&mut self, #(#obj: #Ty,)*) {
                self.event(event::$Obj { #(#obj,)* });
            }
        }
    }


    // Populate

    fn populate<E: Ext>(&mut self, eng: &Engine<E>) {
        self.populate_filtered(eng, |_| true);
    }

    fn populate_filtered<E: Ext, F>(&mut self, eng: &Engine<E>, mut filter: F)
            where F: FnMut(UpdateItem) -> bool {
        for it in self.d.flags.items() {
            if !filter(it) {
                continue;
            }

            // Handle objects and built-in fields.  Hopefully this match gets fused with the next.
            expand_objs! {
                match it {
                    $(
                        UpdateItem::$Obj(id, f) => {
                            if f.contains(flags::$O_DESTROYED) {
                                // The object was already inserted by `obj_destroyed`, and its
                                // components were already inserted by `obj_component_destroyed`.
                                continue;
                            }

                            if f.contains(flags::$O_CREATED) {
                                self.d.new.$insert_obj(id, eng.w.$obj(id).obj().clone());
                            } else {
                                for_each_obj_named_macro_safe! { [[$obj_fields]]
                                    if f.contains(flags::#FLAG) {
                                        let obj = eng.w.$obj(id).obj();
                                        let f_ref: ref_ty!(#DM) =
                                            field_ref!(#Tag, #Base, obj);
                                        let val = ref_to_owned!(#DM, f_ref);
                                        self.d.new.#insert_obj(id, val);
                                    }
                                }
                            }
                        },
                    )*
                    _ => {},
                }
            }

            // Handle components.
            expand_objs! {
                match it {
                    UpdateItem::World(f) => {
                        if f.contains(flags::W_TIME) {
                            self.d.new.insert_world_time((), eng.w.now);
                        }

                        $( if f.contains(flags::$W_NEXT_OBJ) {
                            self.d.new.insert_world_next_stable_id(ObjectType::$Obj,
                                                                   eng.w.$next_obj_stable_id());
                        } )*
                    },

                    $( UpdateItem::$Obj(id, f) => {
                        let created = f.contains(flags::$O_CREATED);
                        for_each_obj_named_macro_safe! { [[$obj_components]]
                            if f.contains(flags::#FLAG) || created {
                                let val = <#Tag as Component<$ObjId>>::get(eng, id);
                                self.d.new.#insert_obj(id, val);
                            }
                        }
                    } )*,
                }
            }
        }
    }

    pub fn into_delta<E: Ext>(mut self, eng: &Engine<E>) -> Delta {
        self.populate(eng);
        self.d
    }
}

impl From<IncDelta> for Delta {
    fn from(x: IncDelta) -> Delta {
        x.d
    }
}


/// Provides access to the internal representation of a `Delta`.  Only use this if you know what
/// you're doing.  This should eventually be phased out in favor of new types of delta builders.
pub struct DeltaInternals {
    pub flags: FlagMap,
    pub old: DataMap,
    pub new: DataMap,
    pub events: EventVec,
}

impl DeltaInternals {
    pub fn new() -> DeltaInternals {
        DeltaInternals {
            flags: FlagMap::new(),
            old: DataMap::new(),
            new: DataMap::new(),
            events: EventVec::new(),
        }
    }
}

impl From<Delta> for DeltaInternals {
    fn from(d: Delta) -> DeltaInternals {
        let Delta { flags, old, new, events } = d;
        DeltaInternals { flags, old, new, events }
    }
}

impl From<DeltaInternals> for Delta {
    fn from(d: DeltaInternals) -> Delta {
        let DeltaInternals { flags, old, new, events } = d;
        Delta { flags, old, new, events }
    }
}



