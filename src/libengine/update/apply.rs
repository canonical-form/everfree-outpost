use server_types::*;
use std::cmp::Ord;
use common::util::StringResult;
use syntax_exts::{if_ident, for_each_obj_named_macro_safe};
use world::import::IdRemapper;
use world::objects::*;

use engine::Engine;
use ext::Ext;
use update::{UpdateItem, flags};
use update::data_map::{DataKey, ObjectType};
use update::debug;
use update::delta::{Delta, DeltaInternals};
use update::traits::Component;


pub fn check_planes<E, F>(eng: &Engine<E>, d: &DeltaInternals, mut f: F) -> bool
        where E: Ext, F: FnMut(Stable<PlaneId>) {
    let mut ok = true;

    for k in d.new.keys() {
        match k {
            DataKey::PlaneRef(id) => {
                let stable_id = *d.new.plane_ref(id);
                if eng.w.transient_plane_id(stable_id).is_none() {
                    f(stable_id);
                    ok = false;
                }
            },
            _ => {},
        }
    }

    ok
}

/// Assign a phase number to each `UpdateItem`.  Phases are assigned such that inserting new
/// objects in phase order will ensure that the parent object (if any) exists before the child is
/// created.
fn update_item_phase(item: &UpdateItem) -> i32 {
    match *item {
        UpdateItem::World(..) => 0,
        UpdateItem::Client(..) => 0,
        UpdateItem::Plane(..) => 0,

        // TerrainChunk can be attached to Plane
        UpdateItem::TerrainChunk(..) => 1,

        // Entity can be attached to TerrainChunk or Client
        UpdateItem::Entity(..) => 2,
        // Structure can be attached to Plane or TerrainChunk
        UpdateItem::Structure(..) => 2,

        // Inventory can be attached to Client, Entity, or Structure
        UpdateItem::Inventory(..) => 3,
    }
}

struct ApplyDelta<'a, 'b, E: 'a> {
    mapper: IdRemapper,
    eng: &'a mut Engine<E>,
    delta: &'b mut DeltaInternals,
}

impl<'a, 'b, E: Ext> ApplyDelta<'a, 'b, E> {
    pub fn new(eng: &'a mut Engine<E>,
               delta: &'b mut DeltaInternals) -> ApplyDelta<'a, 'b, E> {
        ApplyDelta {
            mapper: IdRemapper::new(),
            eng,
            delta,
        }
    }

    /// Initialize ID mappings for importing the current delta.  Panics if any referenced planes
    /// are missing (run `check_planes` first).
    pub fn init_id_maps(&mut self) {
        let mapper = &mut self.mapper;
        let eng = &mut *self.eng;
        let d = &mut *self.delta;

        mapper.assign_ids(&mut eng.w, || d.flags.items().filter_map(|it| {
            if it.generic_flags().contains(flags::X_CREATED) {
                expand_objs! {
                    match it {
                        UpdateItem::World(_) => unreachable!(),
                        $( UpdateItem::$Obj(id, _) =>
                           Some(AnyId::$Obj(id)), )*
                    }
                }
            } else {
                None
            }
        }));

        for k in d.new.keys() {
            match k {
                DataKey::PlaneRef(d_id) => {
                    let stable_id = *d.new.plane_ref(d_id);
                    let w_id = eng.w.transient_plane_id(stable_id).unwrap();
                    mapper.insert_plane(d_id, w_id);
                },
                _ => {},
            }
        }
    }

    /// Phase 1 of `apply`: validate the `Delta` and rewrite object references inside.  This does
    /// not modify any game state.
    fn validate_and_rewrite(&mut self, items: &[UpdateItem]) -> StringResult<()> {
        // List of pawn checks to make.  When a client's `pawn` field is `Some`, we also need to
        // check that the pawn's attachment is to the client.  However, at the time we process the
        // client, the entity may or may not have had its fields rewritten already.  So we record
        // some information about the expected relationship, and check that it holds only once all
        // rewrites are finished.
        //
        // Specifically, `check_pawn` stores the pair `(old_cid, old_eid)`.  To check, we look up
        // the object data for `old_eid` (note that even after rewriting, objects are looked up by
        // old IDs), and compare its `attachment` field to the translation of `old_cid`.
        let mut check_pawn = Vec::new();

        for &it in items {
            expand_objs! {
                match it {
                    UpdateItem::World(f) => {
                        if f.contains(flags::W_TIME) {
                            if !self.delta.new.contains_world_time(()) {
                                fail!("missing world time");
                            }
                        }

                        $( if f.contains(flags::$W_NEXT_OBJ) {
                            if !self.delta.new.contains_world_next_stable_id(ObjectType::$Obj) {
                                fail!(concat!("missing next stable id for ", stringify!($Obj)));
                            }
                        } )*

                        self.rewrite_world_components(f)?;
                    },

                    $( UpdateItem::$Obj(id, f) => {
                        if f.contains(flags::$O_CREATED) {
                            {
                                // Since the object was just created, we know the entire object
                                // struct is present in `new`.
                                let obj = self.delta.new.$get_obj_mut(id)
                                    .ok_or_else(|| format!("missing object data for {:?}", id))?;
                                if self.mapper.$map_obj(id).is_none() {
                                    fail!("id mapping failed for {:?}", id);
                                }

                                if_ident!($obj == client: {
                                    if let Some(old_eid) = obj.pawn {
                                        check_pawn.push((id, old_eid));
                                    }
                                });

                                $rewrite_obj(&self.mapper, obj)?;
                            }
                            self.$rewrite_obj_components(id, f)?;
                        } else {
                            fail!("object modification is NYI");
                        }

                        if f.contains(flags::$O_DESTROYED) {
                            fail!("object removal is NYI");
                        }
                    }, )*
                }
            }
        }

        for (old_cid, old_eid) in check_pawn {
            let e = self.delta.new.get_entity(old_eid)
                .ok_or_else(|| format!("missing object data for {:?} (pawn check)", old_eid))?;

            let new_cid = self.mapper.map_client(old_cid)
                .expect("check_pawn entry failed to map id?");  // checked above

            if e.attachment != EntityAttachment::Client(new_cid) {
                fail!("{:?} is pawn of {:?} but is not attached to it", old_eid, old_cid);
            }
        }

        if self.delta.events.len() > 0 {
            fail!("applying events is not yet implemented");
        }

        Ok(())
    }

    /// Phase 2 of `apply`: copy data from `delta` into `eng`.  This method panics on any
    /// inconsistency in `delta`, since those should have been ruled out already by
    /// `validate_and_rewrite`
    fn copy_game_state(&mut self, items: &[UpdateItem]) {
        // Pawn handling for clients: `Client::pre_insert` clears `pawn`, since there's no
        // guarantee that it satisfies the right invariants.  We directly set `pawn` after insert
        // (using `obj_mut()` to bypass all checks and update tracking).  This is safe because
        // `validate_and_rewrite` ensures that everything lines up, and it avoids an unwanted
        // C_PAWN update on every import.
        //
        // Note that a crash between inserting the client and inserting its pawn can leave the
        // `World` in an invalid state.

        for &it in items {
            expand_objs! {
                match it {
                    UpdateItem::World(f) => {
                        if f.contains(flags::W_TIME) {
                            self.eng.w.now = *self.delta.new.world_time(());
                        }

                        $(
                            if f.contains(flags::$W_NEXT_OBJ) {
                                let next = *self.delta.new.world_next_stable_id(ObjectType::$Obj);
                                self.eng.w.$set_next_obj_stable_id(next);
                            }
                        )*

                        self.apply_world_components(f);
                    },

                    $( UpdateItem::$Obj(orig_id, f) => {
                        if f.contains(flags::$O_CREATED) {
                            let obj = self.delta.new.$remove_obj(orig_id);
                            let new_id = self.mapper.$map_obj(orig_id).unwrap();
                            trace!("copy {:?} => {:?}: {:?}", orig_id, new_id, f);
                            {
                                // Pawn handling happens here (see explanation above).
                                if_ident!($obj == client: let pawn = obj.pawn;);
                                #[allow(unused_variables, unused_mut)]
                                let mut w_obj = self.eng.w.$create_obj_as(new_id, obj);
                                if_ident!($obj == client: w_obj.obj_mut().pawn = pawn;);
                            }
                            self.$apply_obj_components(orig_id, new_id, f);
                            self.eng.update.$obj_loaded(new_id);
                        } else {
                            panic!("object modification is NYI");
                        }

                        if f.contains(flags::$O_DESTROYED) {
                            panic!("object removal is NYI");
                        }
                    }, )*
                }
            }
        }
    }

    /// Apply the `Delta` to the game state.
    ///
    /// On failure, `eng` remains unmodified, but `delta` may be arbitrarily modified.
    pub fn apply(&mut self) -> StringResult<()> {
        trace!("apply delta\n{}", debug::dump(self.delta).unwrap());

        let mut items = self.delta.flags.items().collect::<Vec<_>>();
        items.sort_by(|a, b| {
            update_item_phase(a).cmp(&update_item_phase(b))
                .then_with(|| a.key().cmp(&b.key()))
        });

        self.validate_and_rewrite(&items)?;
        self.copy_game_state(&items);
        Ok(())
    }


    fn rewrite_world_components(&mut self,
                                f: flags::WorldUpdate) -> StringResult<()> {
        for_each_obj_named_macro_safe! { [[world_components]]
            if f.contains(flags::#FLAG) {
                let opt_comp = self.delta.new.#get_obj_mut(())
                    .ok_or_else(|| format!("missing {}", stringify!(#obj)))?;
                if let Some(comp) = opt_comp.as_mut() {
                    <#Tag as Component<()>>::pre_apply(comp, &self.mapper)
                        .map_err(|e| format!("{}: {}", stringify!(#obj), e))?;
                }
            }
        }
        Ok(())
    }

    fn apply_world_components(&mut self,
                              f: flags::WorldUpdate) {
        for_each_obj_named_macro_safe! { [[world_components]]
            if f.contains(flags::#FLAG) {
                let opt_comp = self.delta.new.#remove_obj(());
                <#Tag as Component<()>>::apply(self.eng, (), opt_comp);
            }
        }
    }

    for_each_obj! {
        fn $rewrite_obj_components(&mut self,
                                   id: $ObjId,
                                   f: flags::$ObjUpdate) -> StringResult<()> {
            for_each_obj_named_macro_safe! { [[$obj_components]]
                if f.contains(flags::#FLAG) {
                    let opt_comp = self.delta.new.#get_obj_mut(id)
                        .ok_or_else(|| format!("{:?}: missing {}", id, stringify!(#obj)))?;
                    if let Some(comp) = opt_comp.as_mut() {
                        <#Tag as Component<$ObjId>>::pre_apply(comp, &self.mapper)
                            .map_err(|e| format!("{:?}: {}: {}", id, stringify!(#obj), e))?;
                    }
                }
            }
            Ok(())
        }

        fn $apply_obj_components(&mut self,
                                 old_id: $ObjId,
                                 new_id: $ObjId,
                                 f: flags::$ObjUpdate) {
            for_each_obj_named_macro_safe! { [[$obj_components]]
                if f.contains(flags::#FLAG) {
                    let opt_comp = self.delta.new.#remove_obj(old_id);
                    <#Tag as Component<$ObjId>>::apply(self.eng, new_id, opt_comp);
                }
            }
        }
    }
}


fn rewrite_client(mapper: &IdRemapper, c: &mut Client) -> StringResult<()> {
    if let Some(id) = c.pawn {
        c.pawn = Some(mapper.map_entity(id)
                      .ok_or_else(|| format!("id mapping failed for client pawn {:?}", id))?);
    }

    Ok(())
}

fn rewrite_entity(mapper: &IdRemapper, e: &mut Entity) -> StringResult<()> {
    e.plane = mapper.map_plane(e.plane)
        .ok_or_else(|| format!("id mapping failed for entity plane {:?}", e.plane))?;
    match e.attachment {
        EntityAttachment::World |
        EntityAttachment::Chunk => {},
        EntityAttachment::Client(ref mut id) => {
            *id = mapper.map_client(*id)
                .ok_or_else(|| format!("id mapping failed for entity parent {:?}", *id))?;
        },
    }

    Ok(())
}

fn rewrite_inventory(mapper: &IdRemapper, i: &mut Inventory) -> StringResult<()> {
    match i.attachment {
        InventoryAttachment::World => {},
        InventoryAttachment::Client(ref mut id) => {
            *id = mapper.map_client(*id)
                .ok_or_else(|| format!("id mapping failed for inventory parent {:?}", *id))?;
        },
        InventoryAttachment::Entity(ref mut id) => {
            *id = mapper.map_entity(*id)
                .ok_or_else(|| format!("id mapping failed for inventory parent {:?}", *id))?;
        },
        InventoryAttachment::Structure(ref mut id) => {
            *id = mapper.map_structure(*id)
                .ok_or_else(|| format!("id mapping failed for inventory parent {:?}", *id))?;
        },
    }

    Ok(())
}

fn rewrite_plane(_mapper: &IdRemapper, _p: &mut Plane) -> StringResult<()> {
    // Nothing to do

    Ok(())
}

fn rewrite_terrain_chunk(mapper: &IdRemapper, tc: &mut TerrainChunk) -> StringResult<()> {
    tc.plane = mapper.map_plane(tc.plane)
        .ok_or_else(|| format!("id mapping failed for terrain chunk plane {:?}", tc.plane))?;

    Ok(())
}

fn rewrite_structure(mapper: &IdRemapper, s: &mut Structure) -> StringResult<()> {
    s.plane = mapper.map_plane(s.plane)
        .ok_or_else(|| format!("id mapping failed for structure plane {:?}", s.plane))?;

    Ok(())
}


pub fn apply<E: Ext>(eng: &mut Engine<E>, d: &mut Delta) -> StringResult<IdRemapper> {
    d.as_internals_mut(|di| {
        let mut missing = Vec::new();
        if !check_planes(eng, di, |id| { missing.push(id); }) {
            fail!("missing planes: {:?}", missing);
        }

        let mut ad = ApplyDelta::new(eng, di);
        ad.init_id_maps();
        ad.apply()?;
        Ok(ad.mapper)
    })
}
