use server_types::*;
use std::collections::HashMap;

use component;
use engine::Engine;
use ext::Ext;
use lifecycle::demand::{Source, Target};
use logic;


#[derive(Debug)]
enum State {
    WaitClient,
    WaitCameraPlane,
    Done {
        has_camera_plane: bool,
    },
}

impl State {
    fn has_client_ref(&self) -> bool {
        true
    }

    fn has_camera_plane_ref(&self) -> bool {
        match *self {
            State::WaitCameraPlane => true,
            State::Done { has_camera_plane } => has_camera_plane,
            _ => false,
        }
    }

    /// Does the login FSM have ownership of the user's `conn_map` entry?  This entry is created on
    /// `start` and released (ownership transferred elsewhere) when the login process finishes.
    fn has_conn_map_entry(&self) -> bool {
        match *self {
            State::Done { .. } => false,
            _ => true,
        }
    }

    fn has_client_loaded(&self) -> bool {
        match *self {
            // In `WaitClient`, we haven't called `client_loaded` yet.
            State::WaitClient => false,
            State::WaitCameraPlane => true,
            // In `Done`, we've already released ownership of this property.
            State::Done { .. } => false,
        }
    }

    fn is_done(&self) -> bool {
        match *self {
            State::Done { .. } => true,
            _ => false,
        }
    }
}

pub struct LoginFsm {
    map: HashMap<u32, State>,
}

impl LoginFsm {
    pub fn new() -> LoginFsm {
        LoginFsm {
            map: HashMap::new(),
        }
    }

    pub fn contains(&self, uid: u32) -> bool {
        self.map.contains_key(&uid)
    }
}


pub fn start<E: Ext>(eng: &mut Engine<E>, uid: u32, name: String) {
    if eng.login_fsm.map.contains_key(&uid) {
        error!("duplicate connection for uid {} ({})", uid, name);
        return;
    }

    let name =
        if &name == "" { format!("Anon:{:04}", uid) }
        else { name };

    eng.conn_map.create_conn(uid, name);
    eng.login_fsm.map.insert(uid, State::WaitClient);
    eng.demand.inc_ref(Source::LoginFsm(uid), Target::Client(Stable::new(uid as u64)));
}

pub fn advance<E: Ext>(eng: &mut Engine<E>, uid: u32, target: Target) {
    let mut state = unwrap_or_error!(eng.login_fsm.map.remove(&uid),
                                     "no login_fsm for uid {}", uid);
    trace!("advance {} from {:?} via {:?}", uid, state, target);

    match (&state, target) {
        (&State::WaitClient, Target::Client(stable_id)) => {
            assert!(stable_id.unwrap() as u32 == uid);
            advance_wait_client(eng, uid, &mut state)
        },
        (&State::WaitCameraPlane, Target::Plane(stable_id)) => {
            assert!(stable_id == STABLE_PLANE_FOREST);
            advance_wait_camera_plane(eng, uid, &mut state)
        },
        _ => {
            error!("bad state/target combination ({:?}, {:?})", state, target);
            return;
        },
    };

    trace!("advanced {} to {:?} via {:?}", uid, state, target);

    if !state.is_done() {
        eng.login_fsm.map.insert(uid, state);
    } else {
        trace!("reached final state for {}", uid);
        finish(eng, uid, &state);
        cleanup(eng, uid, state);
    }
}

fn advance_wait_client<E: Ext>(eng: &mut Engine<E>, uid: u32, state: &mut State) {
    let stable_id = Stable::new(uid as u64);
    let cid = unwrap_or_error!(eng.w.transient_client_id(stable_id),
                               "client not found: {:?}", stable_id);

    eng.conn_map.create_client(uid, cid);
    eng.ext.client_loaded(uid, cid);

    eng.w.client_mut(cid).set_name(eng.conn_map.get_name(uid).unwrap().to_owned());

    if eng.w.client(cid).pawn_id().is_none() && !eng.w.client_cameras.contains(cid) {
        if eng.demand.inc_ref(Source::LoginFsm(uid), Target::Plane(STABLE_PLANE_FOREST)) {
            // Already done - advance through the next state.
            advance_wait_camera_plane(eng, uid, state);
        } else {
            *state = State::WaitCameraPlane;
        }
    } else {
        *state = State::Done { has_camera_plane: false };
    }
}

fn advance_wait_camera_plane<E: Ext>(eng: &mut Engine<E>, uid: u32, state: &mut State) {
    let stable_id = Stable::new(uid as u64);
    let cid = unwrap_or_error!(eng.w.transient_client_id(stable_id),
                               "client not found: {:?}", stable_id);
    let pid = unwrap_or_error!(eng.w.transient_plane_id(STABLE_PLANE_FOREST),
                               "plane not found: {:?}", STABLE_PLANE_FOREST);
    component::camera::set(eng, cid, pid, scalar(0));
    *state = State::Done { has_camera_plane: true };
}

fn finish<E: Ext>(eng: &mut Engine<E>, uid: u32, _state: &State) {
    // The login process is finished, so record the long-term `Conn -> Client` ref that will live
    // for the life of the connection.  We also release ownership of the `conn_map` entry at this
    // point (so it becomes the `lifecycle` module's responsibility to clean it up on disconnect).
    eng.demand.inc_ref(Source::Conn(uid), Target::Client(Stable::new(uid as u64)));
    let cid = eng.w.transient_client_id(Stable::new(uid as u64)).unwrap();
    logic::client::client_loaded(eng, cid);
}

/// Clean up refs held by this `LoginFsm`.
fn cleanup<E: Ext>(eng: &mut Engine<E>, uid: u32, state: State) {
    if state.has_client_ref() {
        eng.demand.dec_ref(Source::LoginFsm(uid), Target::Client(Stable::new(uid as u64)));
    }

    if state.has_camera_plane_ref() {
        eng.demand.dec_ref(Source::LoginFsm(uid), Target::Plane(STABLE_PLANE_FOREST));
    }

    if state.has_conn_map_entry() {
        eng.conn_map.destroy_conn(uid);
    }

    if state.has_client_loaded() {
        eng.ext.client_unloaded(uid);
    }
}

pub fn cancel<E: Ext>(eng: &mut Engine<E>, uid: u32) {
    let state = unwrap_or_error!(eng.login_fsm.map.remove(&uid),
                                 "no login_fsm for uid {}", uid);
    cleanup(eng, uid, state);
}
