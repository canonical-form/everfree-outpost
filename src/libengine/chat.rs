use server_types::*;
use std::cmp;

use engine::Engine;
use ext::Ext;
use script::Scripting;

enum Line<'a> {
    Chat(&'a str, Channel),
    Command(&'a str, &'a str),
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum Channel {
    /// Server-wide broadcast messages.
    System,
    /// Results of player actions for a given player.
    Log(ClientId),
    /// The server-wide player chat channel.
    Global,
    /// The local player chat channel for a given plane and chunk.
    Local(PlaneId, V2),
}

pub fn on_chat<E: Ext>(eng: &mut Engine<E>,
                       id: ClientId,
                       msg: &str) {
    // First, make sure the client ID is valid.
    if eng.w.get_client(id).is_none() {
        error!("on_chat: nonexistent {:?}", id);
        return;
    }

    let line =
        if msg.starts_with("/l ") {
            let (plane, cpos) = match eng.w.client(id).pawn() {
                Some(e) => (e.plane_id(), e.pos(eng.w.now).px_to_cpos()),
                None => {
                    warn!("on_chat: {:?} has no pawn; can't use local chat", id);
                    return;
                },
            };
            Line::Chat(&msg[3..], Channel::Local(plane, cpos))
        } else if msg.starts_with("/") {
            if let Some(idx) = msg.find(' ') {
                Line::Command(&msg[1..idx], &msg[idx + 1 ..])
            } else {
                Line::Command(&msg[1..], "")
            }
        } else {
            Line::Chat(&msg, Channel::Global)
        };

    match line {
        Line::Chat(msg, channel) => {
            // Truncate to 300 characters, without splitting a UTF-8 code point.
            let mut i = cmp::min(msg.len(), 300);
            while i > 0 && !msg.is_char_boundary(i) {
                i -= 1;
            }
            if i < msg.len() {
                info!("truncating long chat message: {} -> {}", msg.len(), i);
            }
            let msg = &msg[..i];

            // Now send the message.
            let name = eng.w.client(id).name().to_owned();
            eng.ext.chat_message(channel, &name, msg);
        },

        Line::Command(cmd, args) => {
            Scripting::handle(eng, |h| h.on_chat_command(id, cmd, args));
        },
    }
}

pub fn send_system<E: Ext>(eng: &mut Engine<E>,
                           msg: &str) {
    eng.ext.chat_message(Channel::System, "***", msg);
}

pub fn send_log<E: Ext>(eng: &mut Engine<E>,
                        id: ClientId,
                        msg: &str) {
    eng.ext.chat_message(Channel::Log(id), "---", msg);
}
