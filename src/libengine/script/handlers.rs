use server_types::*;
use multipython::{PyBox, PyRef, PyResult};
use multipython::api as py;
use server_python_conv::{Pack, Unpack};

macro_rules! define_handlers {
    ($( fn $name:ident ( $($arg:ident : $ty:ty),* ) -> $ret:ty; )*) => {
        pub struct Handlers {
            $($name: PyBox,)*
        }

        impl Handlers {
            pub fn new(module: PyRef) -> PyResult<Handlers> {
                Ok(Handlers {
                    $( $name: py::object::get_attr_str(module, stringify!($name))?, )*
                })
            }

            $(
                pub fn $name(&self, $($arg: $ty),* ) -> PyResult<$ret> {
                    // Note trailing comma in `$($arg,)*`.  This ensures we get a tuple even when
                    // there is only one arg.
                    let args = Pack::pack(($($arg,)*))?;
                    let result = py::object::call(
                        self.$name.borrow(), args.borrow(), None)?;
                    Unpack::unpack(result.borrow())
                }
            )*
        }
    };
}

define_handlers! {
    fn on_engine_init() -> ();
    fn on_client_join(id: ClientId) -> ();
    fn on_chat_command(id: ClientId, cmd: &str, args: &str) -> ();
    fn on_action_interact(cid: ClientId, sid: StructureId) -> ();
    fn on_action_destroy(cid: ClientId, sid: StructureId) -> ();
    fn on_action_use_item(id: ClientId, pos: V3, item: ItemId) -> ();
    fn on_inventory_change(id: InventoryId) -> ();

    fn on_ipython_request(req: String) -> String;
}
