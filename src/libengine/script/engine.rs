use server_types::*;
use std::ptr;
use common::Activity;
use libc::c_int;
use multipython::{self, PyBox, PyRef, PyResult};
use multipython::builtin;
use multipython::builtin::module::ModuleData;
use multipython::module::SelfArg;
use server_extra::Value;
use server_python_conv::Pack;
use syntax_exts::{ident_str, python_module};
use world::objects;

use chat;
use component::compass_targets::{self, VaultId};
use component::dialog;
use component::ext::*;
use component::key_value;
use component::ward_map;
use component::ward_permits;
use engine::Engine;
use ext::Ext;
use logic;


pub struct EngineRef {
    // Use `Engine<()>` to avoid taking any type parameters.  Otherwise `ModuleData` will demand an
    // `E: Ext+'static` bound, which we don't necessarily want to satisfy.
    ptr: *mut Engine<()>,
}

impl EngineRef {
    pub fn new() -> EngineRef {
        EngineRef {
            ptr: ptr::null_mut(),
        }
    }
}

impl ModuleData for EngineRef {
    fn traverse<F>(&self, _visit: F) -> Result<(), c_int>
            where F: Fn(PyRef) -> Result<(), c_int> {
        Ok(())
    }
}

impl<'a, E> SelfArg<'a, EngineRef> for &'a Engine<E> {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<&'a Engine<E>> {
        let r: &EngineRef = <&EngineRef as SelfArg<EngineRef>>::from_py_ref(slf)?;
        let ptr = r.ptr as *const Engine<E>;
        let eng = pyunwrap!(ptr.as_ref(),
                            runtime_error, "engine is not available");
        Ok(eng)
    }
}

impl<'a, E> SelfArg<'a, EngineRef> for &'a mut Engine<E> {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<&'a mut Engine<E>> {
        let r: &EngineRef = <&EngineRef as SelfArg<EngineRef>>::from_py_ref(slf)?;
        let ptr = r.ptr as *mut Engine<E>;
        let eng = pyunwrap!(ptr.as_mut(),
                            runtime_error, "engine is not available");
        Ok(eng)
    }
}



fn build_module<E: Ext>() -> PyResult<PyBox> {
    expand_objs! {
        python_module! {
            name = _outpost_engine;
            generics = <E: Ext>;
            data: EngineRef = EngineRef::new();
            conv_module = ::server_python_conv;
            self_type = Engine<E>;



            fn now(&self) -> Time {
                self.w.now
            }

            fn clients_len(&self) -> usize {
                self.conn_map.len()
            }


            $(
                fn $obj_stable_id(&mut self, id: $ObjId) -> PyResult<Stable<$ObjId>> {
                    let mut obj = pyunwrap!(
                        self.w.$get_obj_mut(id),
                        runtime_error, concat!("no ", ident_str!($obj), " with that ID"));
                    Ok(obj.pin())
                }

                fn $obj_transient_id(&self, stable_id: Stable<$ObjId>) -> Option<$ObjId> {
                    self.w.$transient_obj_id(stable_id)
                }
            )*


            // Client

            fn client_name(&self, cid: ClientId) -> PyResult<String> {
                let c = pyunwrap!(self.w.get_client(cid),
                                  runtime_error, "no client with that ID");
                Ok(c.name().to_owned())
            }

            fn client_pawn_id(&self, cid: ClientId) -> PyResult<Option<EntityId>> {
                let c = pyunwrap!(self.w.get_client(cid),
                                  runtime_error, "no client with that ID");
                Ok(c.pawn_id())
            }

            fn client_by_name(&self, name: String) -> Option<ClientId> {
                self.conn_map.get_by_name(&name)
            }

            fn client_kick(&mut self, cid: ClientId, msg: String) -> PyResult<()> {
                let _ = pyunwrap!(self.w.get_client(cid),
                                  runtime_error, "no client with that ID");
                logic::client::kick(self, cid, &msg);
                Ok(())
            }

            /* TODO
            fn client_send_chat_update(&mut self, cid: ClientId, msg: String) {
            }
            */

            /* TODO
            fn client_open_dialog(&mut self, cid: ClientId, dialog: Dialog) {
            }
            */

            fn client_create_child_inventory(&mut self,
                                             cid: ClientId,
                                             size: u8) -> PyResult<InventoryId> {
                pyunwrap!(self.w.get_client(cid),
                          runtime_error, "no client with that ID");
                let raw_i = objects::Inventory::new(size);
                let mut i = self.w.create_inventory(raw_i);
                i.set_attachment(objects::InventoryAttachment::Client(cid));
                Ok(i.id())
            }


            // Entity

            fn entity_pos(&self, eid: EntityId) -> PyResult<V3> {
                let e = pyunwrap!(self.w.get_entity(eid),
                                  runtime_error, "no entity with that ID");
                Ok(e.pos(self.w.now))
            }

            fn entity_plane_id(&self, eid: EntityId) -> PyResult<PlaneId> {
                let e = pyunwrap!(self.w.get_entity(eid),
                                  runtime_error, "no entity with that ID");
                Ok(e.plane_id())
            }

            fn entity_controller(&self, eid: EntityId) -> PyResult<Option<ClientId>> {
                let e = pyunwrap!(self.w.get_entity(eid),
                                  runtime_error, "no entity with that ID");
                Ok(e.controller().map(|c| c.id()))
            }

            fn entity_teleport(&mut self, eid: EntityId, pos: V3) -> PyResult<()> {
                let now = self.now;
                let mut e = pyunwrap!(self.w.get_entity_mut(eid),
                                      runtime_error, "no entity with that ID");
                let dir = e.activity().dir().unwrap_or(0);
                e.set_activity(Activity::Stand { pos, dir }, now);
                Ok(())
            }


            // Inventory

            fn inventory_create(&mut self, size: u8) -> InventoryId {
                let raw_i = objects::Inventory::new(size);
                let i = self.w.create_inventory(raw_i);
                i.id()
            }

            fn inventory_is_empty(&self, iid: InventoryId) -> PyResult<bool> {
                let i = pyunwrap!(self.w.get_inventory(iid),
                                  runtime_error, "no inventory with that ID");
                Ok(i.is_empty())
            }

            fn inventory_is_public(&self, iid: InventoryId) -> PyResult<bool> {
                let i = pyunwrap!(self.w.get_inventory(iid),
                                  runtime_error, "no inventory with that ID");
                Ok(i.flags().contains(objects::I_PUBLIC))
            }

            fn inventory_set_public(&mut self, iid: InventoryId, public: bool) -> PyResult<()> {
                let mut i = pyunwrap!(self.w.get_inventory_mut(iid),
                                  runtime_error, "no inventory with that ID");
                if i.flags().contains(objects::I_PUBLIC) != public {
                    i.modify_flags(|f| f.toggle(objects::I_PUBLIC));
                }
                Ok(())
            }


            // Structure

            fn structure_pos(&self, sid: StructureId) -> PyResult<V3> {
                let s = pyunwrap!(self.w.get_structure(sid),
                                  runtime_error, "no structure with that ID");
                Ok(s.pos())
            }

            fn structure_plane(&self, sid: StructureId) -> PyResult<PlaneId> {
                let s = pyunwrap!(self.w.get_structure(sid),
                                  runtime_error, "no structure with that ID");
                Ok(s.plane_id())
            }

            fn structure_template_id(&self, sid: StructureId) -> PyResult<TemplateId> {
                let s = pyunwrap!(self.w.get_structure(sid),
                                  runtime_error, "no structure with that ID");
                Ok(s.template())
            }

            fn structure_create(&mut self,
                                plane: PlaneId,
                                pos: V3,
                                template: TemplateId) -> PyResult<StructureId> {
                let s = objects::Structure::new(plane, pos, template);
                match self.w.try_create_structure(s) {
                    Ok(s) => Ok(s.id()),
                    Err(e) => pyraise!(runtime_error, "{}", e),
                }
            }

            fn structure_destroy(&mut self, sid: StructureId) -> PyResult<()> {
                // Check that the ID is valid
                pyunwrap!(self.w.get_structure(sid),
                          runtime_error, "no structure with that ID");
                self.w.destroy_structure(sid);
                Ok(())
            }

            fn structure_create_child_inventory(&mut self,
                                                sid: StructureId,
                                                size: u8) -> PyResult<InventoryId> {
                pyunwrap!(self.w.get_structure(sid),
                          runtime_error, "no structure with that ID");
                let raw_i = objects::Inventory::new(size);
                let mut i = self.w.create_inventory(raw_i);
                i.set_attachment(objects::InventoryAttachment::Structure(sid));
                Ok(i.id())
            }


            // Chat

            fn chat_send_system(&mut self, msg: String) {
                chat::send_system(self, &msg);
            }

            fn chat_send_log(&mut self, id: ClientId, msg: String) {
                chat::send_log(self, id, &msg);
            }


            // Character inventories

            fn char_inv_main(&self, id: EntityId) -> PyResult<InventoryId> {
                let e = pyunwrap!(self.w.get_entity(id),
                                  key_error, "no entity with that ID");
                let invs = pyunwrap!(e.get_char_invs(),
                                     key_error, "no character inventories for that entity");
                Ok(invs.main)
            }

            fn char_inv_ability(&self, id: EntityId) -> PyResult<InventoryId> {
                let e = pyunwrap!(self.w.get_entity(id),
                                  key_error, "no entity with that ID");
                let invs = pyunwrap!(e.get_char_invs(),
                                     key_error, "no character inventories for that entity");
                Ok(invs.ability)
            }

            fn char_inv_equip(&self, id: EntityId) -> PyResult<InventoryId> {
                let e = pyunwrap!(self.w.get_entity(id),
                                  key_error, "no entity with that ID");
                let invs = pyunwrap!(e.get_char_invs(),
                                     key_error, "no character inventories for that entity");
                Ok(invs.equip)
            }


            // Logic - Inventory

            fn logic_inventory_can_add(&self,
                                       id: InventoryId,
                                       item: ItemId,
                                       count: u16) -> PyResult<bool> {
                let i = pyunwrap!(self.w.get_inventory(id),
                                  runtime_error, "no inventory with that ID");
                Ok(logic::inventory::can_add(i, item, count))
            }

            fn logic_inventory_can_add_multi(&self,
                                             id: InventoryId,
                                             items: Vec<(ItemId, u16)>) -> PyResult<bool> {
                let i = pyunwrap!(self.w.get_inventory(id),
                                  runtime_error, "no inventory with that ID");
                Ok(logic::inventory::can_add_multi(i, &items))
            }

            fn logic_inventory_add(&mut self,
                                   id: InventoryId,
                                   item: ItemId,
                                   count: u16) -> PyResult<()> {
                let i = pyunwrap!(self.w.get_inventory_mut(id),
                                  runtime_error, "no inventory with that ID");
                Ok(logic::inventory::add(i, item, count))
            }

            fn logic_inventory_can_remove(&self,
                                          id: InventoryId,
                                          item: ItemId,
                                          count: u16) -> PyResult<bool> {
                let i = pyunwrap!(self.w.get_inventory(id),
                                  runtime_error, "no inventory with that ID");
                Ok(logic::inventory::can_remove(i, item, count))
            }

            fn logic_inventory_remove(&mut self,
                                      id: InventoryId,
                                      item: ItemId,
                                      count: u16) -> PyResult<()> {
                let i = pyunwrap!(self.w.get_inventory_mut(id),
                                  runtime_error, "no inventory with that ID");
                Ok(logic::inventory::remove(i, item, count))
            }


            // Logic - Structure

            fn logic_structure_can_place(&self,
                                         pid: PlaneId,
                                         pos: V3,
                                         template: TemplateId) -> PyResult<bool> {
                pyassert!(self.w.get_plane(pid).is_some(),
                          runtime_error, "no plane with that ID");
                Ok(logic::structure::can_place(self, pid, pos, template))
            }

            fn logic_structure_can_replace(&self,
                                           sid: StructureId,
                                           template: TemplateId) -> PyResult<bool> {
                pyassert!(self.w.get_structure(sid).is_some(),
                          runtime_error, "no structure with that ID");
                Ok(logic::structure::can_replace(self, sid, template))
            }


            // Logic - Permission

            fn logic_permission_get_blocker(&self,
                                            actor: Stable<ClientId>,
                                            actor_name: String,
                                            plane: PlaneId,
                                            region: Region<V2>) -> Option<String> {
                logic::permission::get_blocker(self, actor, actor_name, plane, region)
            }


            // Dialogs

            fn dialog_open_container(&mut self,
                                     cid: ClientId,
                                     iid1: InventoryId,
                                     iid2: InventoryId,
                                     sid: StructureId) {
                let d = dialog::inventory::container(iid1, iid2, sid);
                dialog::open(self, cid, d);
            }

            fn dialog_open_crafting(&mut self,
                                    cid: ClientId,
                                    sid: StructureId,
                                    class_mask: u32) -> PyResult<()> {
                let d = pyunwrap!(
                    logic::crafting::build_structure_dialog(self, cid, sid, class_mask),
                    runtime_error, "failed to build crafting dialog for {:?}, {:?}", cid, sid);
                dialog::open(self, cid, d);
                Ok(())
            }

            fn dialog_open_crafting_celestial(&mut self,
                                              cid: ClientId,
                                              sid: StructureId,
                                              iid: InventoryId,
                                              class_mask: u32) -> PyResult<()> {
                let d = pyunwrap!(
                    logic::crafting::build_celestial_dialog(self, cid, sid, iid, class_mask),
                    runtime_error,
                    "failed to build celestial crafting dialog for {:?}, {:?}, {:?}",
                    cid, sid, iid);
                dialog::open(self, cid, d);
                Ok(())
            }


            // Contents

            fn contents_set(&mut self, sid: StructureId, iid: InventoryId) -> PyResult<()> {
                pyunwrap!(self.w.get_inventory(iid),
                          runtime_error, "no inventory with that ID");
                let mut s = pyunwrap!(self.w.get_structure_mut(sid),
                                      runtime_error, "no structure with that ID");
                s.set_contents(iid);
                Ok(())
            }

            fn contents_clear(&mut self, sid: StructureId) -> PyResult<()> {
                let mut s = pyunwrap!(self.w.get_structure_mut(sid),
                                      runtime_error, "no structure with that ID");
                s.clear_contents();
                Ok(())
            }

            fn contents_get(&self, sid: StructureId) -> PyResult<Option<InventoryId>> {
                let s = pyunwrap!(self.w.get_structure(sid),
                                  runtime_error, "no structure with that ID");
                Ok(s.get_contents())
            }


            // Ward map

            fn ward_map_add_ward(&mut self,
                                 pid: PlaneId,
                                 region: Region<V2>,
                                 owner: Stable<ClientId>,
                                 owner_name: String) -> PyResult<()> {
                match ward_map::add_ward(self, pid, region, owner, owner_name) {
                    Ok(()) => Ok(()),
                    Err(ward_map::AddError::NoWardMap) =>
                        pyraise!(runtime_error, "no ward map for that plane"),
                    Err(ward_map::AddError::KeyPresent) =>
                        pyraise!(key_error, "that owner already has a ward"),
                }
            }

            fn ward_map_remove_ward(&mut self,
                                    pid: PlaneId,
                                    owner: Stable<ClientId>) {
                ward_map::remove_ward(self, pid, owner);
            }

            fn ward_map_contains(&self,
                                 pid: PlaneId) -> bool {
                self.w.ward_maps.contains(pid)
            }

            fn ward_map_has_ward(&self,
                                 pid: PlaneId,
                                 owner: Stable<ClientId>) -> PyResult<bool> {
                let wm = pyunwrap!(self.w.ward_maps.get(pid),
                                   runtime_error, "no ward map for that plane");
                Ok(wm.contains(owner))
            }

            fn ward_map_owner_name(&self,
                                   pid: PlaneId,
                                   owner: Stable<ClientId>) -> PyResult<String> {
                let wm = pyunwrap!(self.w.ward_maps.get(pid),
                                   runtime_error, "no ward map for that plane");
                let w = pyunwrap!(wm.get(owner),
                                  key_error, "that client has no ward");
                Ok(w.owner_name.clone())
            }

            fn ward_map_region(&self,
                               pid: PlaneId,
                               owner: Stable<ClientId>) -> PyResult<Region<V2>> {
                let wm = pyunwrap!(self.w.ward_maps.get(pid),
                                   runtime_error, "no ward map for that plane");
                let w = pyunwrap!(wm.get(owner),
                                  key_error, "that client has no ward");
                Ok(w.region)
            }


            // Ward permits

            fn ward_permit_add(&mut self,
                               owner: Stable<ClientId>,
                               user: String) -> bool {
                ward_permits::add_permit(self, owner, user)
            }

            fn ward_permit_remove(&mut self,
                                  owner: Stable<ClientId>,
                                  user: String) -> bool {
                ward_permits::remove_permit(self, owner, user)
            }

            fn ward_permit_list(&self,
                                owner: Stable<ClientId>) -> Vec<String> {
                self.w.ward_permits.get(owner).cloned().collect::<Vec<_>>()
            }

            fn ward_permit_granted(&self,
                                   owner: Stable<ClientId>,
                                   actor_name: String) -> bool {
                self.w.ward_permits.has_permit(owner, &actor_name)
            }


            // Key-value

            fn key_value_set(&mut self, id: AnyId, key: String, value: Value) -> PyResult<()> {
                check_any_id(self, id)?;
                key_value::set(self, id, key, value);
                Ok(())
            }

            fn key_value_get(&mut self, id: AnyId, key: String) -> PyResult<Value> {
                check_any_id(self, id)?;
                let v = pyunwrap!(self.w.key_value.get(id, &key),
                                  key_error, "no entry for that key");
                Ok(v.clone())
            }

            fn key_value_get_default(&mut self,
                                     id: AnyId,
                                     key: String,
                                     default: PyBox) -> PyResult<PyBox> {
                check_any_id(self, id)?;
                if let Some(v) = self.w.key_value.get(id, &key) {
                    v.clone().pack()
                } else {
                    Ok(default)
                }
            }

            fn key_value_clear(&mut self, id: AnyId, key: String) -> PyResult<()> {
                check_any_id(self, id)?;
                key_value::clear(self, id, key);
                Ok(())
            }

            fn key_value_contains(&mut self, id: AnyId, key: String) -> PyResult<bool> {
                check_any_id(self, id)?;
                Ok(self.w.key_value.contains(id, &key))
            }


            // Compass targets & looted vaults

            fn structure_parent_vault(&self, sid: StructureId) -> PyResult<Option<VaultId>> {
                pyunwrap!(self.w.get_structure(sid),
                          runtime_error, "no structure with that ID");
                Ok(self.w.compass_data.get_parent_vault(sid))
            }

            fn plane_set_looted_vault(&mut self, pid: PlaneId, vault_id: VaultId) -> PyResult<()> {
                pyunwrap!(self.w.get_plane(pid),
                          runtime_error, "no plane with that ID");
                compass_targets::set_looted(self, pid, vault_id);
                Ok(())
            }

            fn plane_nearest_compass_target(&self,
                                            pid: PlaneId,
                                            pos: V3) -> PyResult<Option<(VaultId, V3)>> {
                let p = pyunwrap!(self.w.get_plane(pid),
                                  runtime_error, "no plane with that ID");
                let cpos = pos.px_to_cpos();
                let tcid = pyunwrap!(p.chunk_id(cpos),
                                     runtime_error, "no terrain chunk at that position");
                Ok(self.w.compass_data.nearest_target(pid, tcid, pos))
            }
        }
    }
}

fn check_any_id<E>(eng: &Engine<E>, id: AnyId) -> PyResult<()> {
    expand_objs! {
        match id {
            $( AnyId::$Obj(id) => {
                pyunwrap!(eng.w.$get_obj(id),
                          runtime_error, concat!("no ", stringify!($obj), " with that ID"));
            }, )*
        }
    }

    Ok(())
}

pub fn register<E: Ext>() -> PyResult<PyBox> {
    let m = build_module::<E>()?;
    multipython::util::register_module(m.borrow())?;
    Ok(m)
}


pub struct RefGuard<'a> {
    m: PyRef<'a>,
}

impl<'a> Drop for RefGuard<'a> {
    fn drop(&mut self) {
        unsafe {
            if let Some(r) = builtin::module::get_mut_unchecked::<EngineRef>(self.m) {
                r.ptr = ptr::null_mut();
            }
        }
    }
}

pub unsafe fn set_ref<'a, E: Ext>(m: PyRef<'a>, ptr: *mut Engine<E>) -> RefGuard<'a> {
    if let Some(r) = builtin::module::get_mut_unchecked::<EngineRef>(m) {
        r.ptr = ptr as *mut Engine<()>;
    }
    RefGuard { m: m }
}
