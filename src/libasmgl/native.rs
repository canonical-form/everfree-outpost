use std::ffi::CString;
use std::fmt::Write as _;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::ptr;
use std::slice;
use std::str;
use std::u16;

use outpost_gl::*;
use outpost_gl::types::*;
use png_file;

use super::{Size16};

fn check_error(line: u32) {
    loop {
        let err = unsafe { GetError() };
        if err == NO_ERROR { break; }
        let name = match err {
            INVALID_ENUM => "INVALID_ENUM",
            INVALID_VALUE => "INVALID_VALUE",
            INVALID_OPERATION => "INVALID_OPERATION",
            INVALID_FRAMEBUFFER_OPERATION => "INVALID_FRAMEBUFFER_OPERATION",
            OUT_OF_MEMORY => "OUT_OF_MEMORY",
            STACK_UNDERFLOW => "STACK_UNDERFLOW",
            STACK_OVERFLOW => "STACK_OVERFLOW",
            _ => "unknown error",
        };
        error!("opengl error @ {}: {} ({:x})", line, name, err);
    }
}

macro_rules! check_error {
    () => { check_error(line!()) };
}


macro_rules! gltrace {
    ($s:expr) => { gltrace!($s,) };
    ($s:expr, $($rest:tt)*) => {
        if log_enabled!(::log::LogLevel::Trace) {
            let now = ::time::get_time();
            trace!(concat!("[{}.{:09}] ", $s), now.sec, now.nsec, $($rest)*);
        }
    };
}


// draw_buffers and depth_texture are both core since OpenGL 2.0
pub unsafe fn asmgl_has_draw_buffers() -> u8 { 1 }
pub unsafe fn asmgl_has_depth_texture() -> u8 { 1 }

pub unsafe fn asmgl_gen_buffer() -> u32 {
    let mut raw: GLuint = 0;
    GenBuffers(1, &mut raw);
    check_error!();
    assert!(raw != 0);
    gltrace!("gen_buffer() = {}", raw);
    raw as u32
}
pub unsafe fn asmgl_delete_buffer(name: u32) {
    gltrace!("delete_buffer({})", name);
    let raw = name as GLuint;
    DeleteBuffers(1, &raw);
    check_error!();
}
pub unsafe fn asmgl_bind_buffer(target: u8, name: u32) {
    gltrace!("bind_buffer({:?}, {})", target, name);
    BindBuffer(target_enum(target), name as GLuint);
    check_error!();
}
pub unsafe fn asmgl_buffer_data_alloc(target: u8, len: usize) {
    gltrace!("buffer_data_alloc({:?}, {})", target, len);
    BufferData(target_enum(target),
               len as GLsizeiptr,
               ptr::null(),
               STATIC_DRAW);
    check_error!();
}
pub unsafe fn asmgl_buffer_subdata(target: u8, offset: usize, ptr: *const u8, len: usize) {
    gltrace!("buffer_subdata({:?}, {}, <{} bytes>)", target, offset, len);
    BufferSubData(target_enum(target),
                  offset as GLintptr, 
                  len as GLsizeiptr,
                  ptr as *const GLvoid);
    check_error!();
}

pub unsafe fn asmgl_load_shader(vert_name_ptr: *const u8,
                                vert_name_len: usize,
                                frag_name_ptr: *const u8,
                                frag_name_len: usize,
                                defs_ptr: *const u8,
                                defs_len: usize) -> u32 {
    let vert_name = str::from_utf8_unchecked(slice::from_raw_parts(vert_name_ptr, vert_name_len));
    let frag_name = str::from_utf8_unchecked(slice::from_raw_parts(frag_name_ptr, frag_name_len));
    let defs = str::from_utf8_unchecked(slice::from_raw_parts(defs_ptr, defs_len));

    info!("loading shader: {} + {}", vert_name, frag_name);
    let vert_src = load_shader(vert_name, defs, "VERTEX");
    let frag_src = load_shader(frag_name, defs, "FRAGMENT");

    trace!("vertex source:\n{}", vert_src);
    trace!("fragment source:\n{}", frag_src);

    let vert = CreateShader(VERTEX_SHADER);
    ShaderSource(vert,
                 1,
                 &(vert_src.as_ptr() as *const GLchar),
                 &(vert_src.len() as GLint));
    CompileShader(vert);
    let status = get_status(|ptr| GetShaderiv(vert, COMPILE_STATUS, ptr));
    if status == 0 {
        let msg = get_info_log(|max, len, ptr| GetShaderInfoLog(vert, max, len, ptr));
        error!("shader compilation errors ({}):\n{}", vert_name, msg);
        panic!("shader failed to compile");
    }
    check_error!();

    let frag = CreateShader(FRAGMENT_SHADER);
    ShaderSource(frag,
                 1,
                 &(frag_src.as_ptr() as *const GLchar),
                 &(frag_src.len() as GLint));
    CompileShader(frag);
    let status = get_status(|ptr| GetShaderiv(frag, COMPILE_STATUS, ptr));
    if status == 0 {
        let msg = get_info_log(|max, len, ptr| GetShaderInfoLog(frag, max, len, ptr));
        error!("shader compilation errors ({}):\n{}", frag_name, msg);
        panic!("shader failed to compile");
    }
    check_error!();

    let prog = CreateProgram();
    AttachShader(prog, vert);
    AttachShader(prog, frag);
    LinkProgram(prog);
    let status = get_status(|ptr| GetProgramiv(prog, LINK_STATUS, ptr));
    if status == 0 {
        let msg = get_info_log(|max, len, ptr| GetProgramInfoLog(prog, max, len, ptr));
        info!("shader linking errors ({} + {}):\n{}", vert_name, frag_name, msg);
        panic!("shader failed to link");
    }
    check_error!();

    DetachShader(prog, vert);
    DetachShader(prog, frag);
    DeleteShader(vert);
    DeleteShader(frag);
    check_error!();

    gltrace!("load_shader({}, {}, <{} bytes>) = {}", vert_name, frag_name, defs, prog);
    prog
}
pub unsafe fn asmgl_delete_shader(name: u32) {
    gltrace!("delete_shader({})", name);
    DeleteProgram(name as GLuint);
    check_error!();
}
pub unsafe fn asmgl_bind_shader(name: u32) {
    gltrace!("bind_shader({})", name);
    UseProgram(name as GLuint);
    check_error!();
}
pub unsafe fn asmgl_get_uniform_location(shader_name: u32,
                                         name_ptr: *const u8,
                                         name_len: usize) -> i32 {
    let name = str::from_utf8_unchecked(slice::from_raw_parts(name_ptr, name_len));
    let cstr = CString::new(name).unwrap();
    let loc = GetUniformLocation(shader_name as GLuint,
                                 cstr.as_ptr()) as i32;
    gltrace!("get_uniform_location({}, {}) = {}", shader_name, name, loc);
    check_error!();
    loc
}
pub unsafe fn asmgl_get_attrib_location(shader_name: u32,
                                        name_ptr: *const u8,
                                        name_len: usize) -> i32 {
    let name = str::from_utf8_unchecked(slice::from_raw_parts(name_ptr, name_len));
    let cstr = CString::new(name).unwrap();
    let loc = GetAttribLocation(shader_name as GLuint,
                                cstr.as_ptr()) as i32;
    gltrace!("get_attrib_location({}, {}) = {}", shader_name, name, loc);
    check_error!();
    loc
}
pub unsafe fn asmgl_set_uniform_1i(location: i32, value: i32) {
    gltrace!("set_uniform_1i({}, {})", location, value);
    Uniform1i(location as GLint,
              value as GLint);
    check_error!();
}
pub unsafe fn asmgl_set_uniform_1f(location: i32, value: f32) {
    gltrace!("set_uniform_1f({}, {})", location, value);
    Uniform1f(location as GLint,
              value as GLfloat);
    check_error!();
}
pub unsafe fn asmgl_set_uniform_2f(location: i32, value: &[f32; 2]) {
    gltrace!("set_uniform_2f({}, {:?})", location, value);
    // Cast to GLfloat before .as_ptr() to ensure GLfloat = f32
    Uniform2fv(location as GLint,
               1,
               (value as &[GLfloat]).as_ptr());
    check_error!();
}
pub unsafe fn asmgl_set_uniform_3f(location: i32, value: &[f32; 3]) {
    gltrace!("set_uniform_3f({}, {:?})", location, value);
    Uniform3fv(location as GLint,
               1,
               (value as &[GLfloat]).as_ptr());
    check_error!();
}
pub unsafe fn asmgl_set_uniform_4f(location: i32, value: &[f32; 4]) {
    gltrace!("set_uniform_4f({}, {:?})", location, value);
    Uniform4fv(location as GLint,
               1,
               (value as &[GLfloat]).as_ptr());
    check_error!();
}

pub unsafe fn asmgl_load_texture(name_ptr: *const u8,
                                 name_len: usize,
                                 size_p: *mut Size16) -> u32 {
    let name = str::from_utf8_unchecked(slice::from_raw_parts(name_ptr, name_len));
    let path = format!("img/{}.png", name);
    info!("loading texture: {} ({})", name, path);

    let (info, buf) = png_file::read_png_file(Path::new(&path)).unwrap();

    assert!(info.width <= u16::MAX as _);
    assert!(info.height <= u16::MAX as _);
    let (width, height) = (info.width as u16, info.height as u16);

    let mut raw: GLuint = 0;
    GenTextures(1, &mut raw);
    BindTexture(TEXTURE_2D, raw);
    check_error!();
    TexParameteri(TEXTURE_2D, TEXTURE_MAG_FILTER, NEAREST as GLint);
    TexParameteri(TEXTURE_2D, TEXTURE_MIN_FILTER, NEAREST as GLint);
    TexParameteri(TEXTURE_2D, TEXTURE_WRAP_S, CLAMP_TO_EDGE as GLint);
    TexParameteri(TEXTURE_2D, TEXTURE_WRAP_T, CLAMP_TO_EDGE as GLint);
    check_error!();

    TexImage2D(TEXTURE_2D,
               0,
               RGBA as GLint,
               width as GLsizei,
               height as GLsizei,
               0,
               RGBA,
               UNSIGNED_BYTE,
               buf.as_ptr() as *const GLvoid);
    check_error!();
    gltrace!("load_texture({}) = {}, ({}x{})", name, raw, width, height);
    (*size_p).x = width;
    (*size_p).y = height;
    raw as u32
}
pub unsafe fn asmgl_gen_texture(width: u16, height: u16, kind: u8) -> u32 {
    let mut raw: GLuint = 0;
    let (internal, format, type_) = texture_kind_enums(kind);
    GenTextures(1, &mut raw);
    BindTexture(TEXTURE_2D, raw);
    TexParameteri(TEXTURE_2D, TEXTURE_MAG_FILTER, NEAREST as GLint);
    TexParameteri(TEXTURE_2D, TEXTURE_MIN_FILTER, NEAREST as GLint);
    TexParameteri(TEXTURE_2D, TEXTURE_WRAP_S, CLAMP_TO_EDGE as GLint);
    TexParameteri(TEXTURE_2D, TEXTURE_WRAP_T, CLAMP_TO_EDGE as GLint);
    check_error!();

    TexImage2D(TEXTURE_2D,
               0,
               internal as GLint,
               width as GLsizei,
               height as GLsizei,
               0,
               format,
               type_,
               ptr::null());
    check_error!();
    gltrace!("gen_texture({}, {}, {:?}) = {}", width, height, kind, raw);
    raw as u32
}
pub unsafe fn asmgl_delete_texture(name: u32) {
    gltrace!("delete_texture({})", name);
    DeleteTextures(1, &(name as GLuint));
    check_error!();
}
pub unsafe fn asmgl_active_texture(unit: usize) {
    gltrace!("active_texture({})", unit);
    ActiveTexture(TEXTURE0 + unit as GLenum);
    check_error!();
}
pub unsafe fn asmgl_bind_texture(name: u32) {
    gltrace!("bind_texture({})", name);
    BindTexture(TEXTURE_2D, name as GLuint);
    check_error!();
}
pub unsafe fn asmgl_texture_image(width: u16,
                                  height: u16,
                                  kind: u8,
                                  data_ptr: *const u8,
                                  data_len: usize) {
    let data = slice::from_raw_parts(data_ptr, data_len);
    gltrace!("texture_image({}, {}, {:?}, <{} bytes>)", width, height, kind, data.len());
    let (internal, format, type_) = texture_kind_enums(kind);
    TexImage2D(TEXTURE_2D,
               0,
               internal as GLint,
               width as GLsizei,
               height as GLsizei,
               0,
               format,
               type_,
               data.as_ptr() as *const GLvoid);
    check_error!();
}
pub unsafe fn asmgl_texture_subimage(x: u16,
                                     y: u16,
                                     width: u16,
                                     height: u16,
                                     kind: u8,
                                     data_ptr: *const u8,
                                     data_len: usize) {
    let data = slice::from_raw_parts(data_ptr, data_len);
    gltrace!("texture_subimage({}, {}, {}, {}, {:?}, <{} bytes>)",
           x, y, width, height, kind, data.len());
    let (_, format, type_) = texture_kind_enums(kind);
    TexSubImage2D(TEXTURE_2D,
                  0,
                  x as GLint,
                  y as GLint,
                  width as GLsizei,
                  height as GLsizei,
                  format,
                  type_,
                  data.as_ptr() as *const GLvoid);
    check_error!();
}

pub unsafe fn asmgl_gen_framebuffer() -> u32 {
    let mut raw: GLuint = 0;
    GenFramebuffers(1, &mut raw);
    check_error!();
    gltrace!("gen_framebuffer() = {}", raw);
    raw as u32
}
pub unsafe fn asmgl_delete_framebuffer(name: u32) {
    gltrace!("delete_framebuffer({})", name);
    DeleteFramebuffers(1, &(name as GLuint));
    check_error!();
}
pub unsafe fn asmgl_bind_framebuffer(name: u32) {
    gltrace!("bind_framebuffer({})", name);
    BindFramebuffer(FRAMEBUFFER, name as GLuint);
    check_error!();
}
//pub unsafe fn asmgl_gen_renderbuffer(width: u16, height: u16, is_depth: u8) -> u32;
//pub unsafe fn asmgl_delete_renderbuffer(name: u32);
// No use for bind_renderbuffer so far
// NB: The `attachment` args here should really be `i8`, but as of 1.24.1, rust-compiled
// wasm code uses i32 for all integer arguments, with sign-extension on the callee side.
// Since the callee here is Javascript, we adjust the signature so that the sign extension
// happens on the Rust side instead.
pub unsafe fn asmgl_framebuffer_texture(tex_name: u32,
                                        attachment: i32) {
    gltrace!("framebuffer_texture({}, {:?})", tex_name, attachment);
    FramebufferTexture2D(FRAMEBUFFER,
                         attachment_enum(attachment as i8),
                         TEXTURE_2D,
                         tex_name as GLuint,
                         0);
    check_error!();
}
//pub unsafe fn asmgl_framebuffer_renderbuffer(rb_name: u32,
//                                      attachment: i32);
//pub unsafe fn asmgl_check_framebuffer_status() -> u8;
pub unsafe fn asmgl_draw_buffers(num_attachments: u8) {
    gltrace!("draw_buffers({})", num_attachments);
    let mut arr = Vec::with_capacity(num_attachments as usize);
    for i in 0 .. num_attachments {
        arr.push(COLOR_ATTACHMENT0 + i as GLenum);
    }
    DrawBuffers(arr.len() as GLsizei, arr.as_ptr());
    check_error!();
}

pub unsafe fn asmgl_viewport(x: i32, y: i32, w: i32, h: i32) {
    gltrace!("viewport({}, {}, {}, {})", x, y, w, h);
    Viewport(x as GLint,
             y as GLint,
             w as GLsizei,
             h as GLsizei);
    check_error!();
}
pub unsafe fn asmgl_scissor(x: i32, y: i32, w: i32, h: i32) {
    gltrace!("scissor({}, {}, {}, {})", x, y, w, h);
    Enable(SCISSOR_TEST);
    Scissor(x as GLint,
            y as GLint,
            w as GLsizei,
            h as GLsizei);
    check_error!();
}
pub unsafe fn asmgl_scissor_disable() {
    gltrace!("scissor_disable()");
    Disable(SCISSOR_TEST);
    check_error!();
}
pub unsafe fn asmgl_clear_color(r: f32, g: f32, b: f32, a: f32) {
    gltrace!("clear_color({}, {}, {}, {})", r, g, b, a);
    ClearColor(r as GLfloat,
               g as GLfloat,
               b as GLfloat,
               a as GLfloat);
    check_error!();
}
pub unsafe fn asmgl_clear_depth(d: f32) {
    gltrace!("clear_depth({})", d);
    ClearDepth(d as GLdouble);
    check_error!();
}
pub unsafe fn asmgl_clear() {
    gltrace!("clear()");
    Clear(COLOR_BUFFER_BIT | DEPTH_BUFFER_BIT);
    check_error!();
}
pub unsafe fn asmgl_set_depth_test(mode: u8) {
    gltrace!("set_depth_test({:?})", mode);
    match mode {
        0 => {
            Disable(DEPTH_TEST);
        },
        1 => {
            Enable(DEPTH_TEST);
            DepthFunc(GEQUAL);
        },
        2 => {
            Enable(DEPTH_TEST);
            DepthFunc(EQUAL);
        },
        3 => {
            Enable(DEPTH_TEST);
            DepthFunc(ALWAYS);
        },
        _ => panic!("invalid enum"),
    }
    check_error!();
}
pub unsafe fn asmgl_set_blend_mode(mode: u8) {
    gltrace!("set_blend_mode({:?})", mode);
    match mode {
        0 => {
            Disable(BLEND);
        },
        1 => {
            Enable(BLEND);
            BlendFunc(SRC_ALPHA, ONE_MINUS_SRC_ALPHA);
        },
        2 => {
            Enable(BLEND);
            BlendFunc(ONE, ONE);
        },
        3 => {
            Enable(BLEND);
            BlendFunc(ONE_MINUS_DST_COLOR, ONE);
        },
        _ => panic!("invalid enum"),
    }
    check_error!();
}
pub unsafe fn asmgl_enable_vertex_attrib_array(loc: i32) {
    gltrace!("enable_vertex_attrib_array({})", loc);
    EnableVertexAttribArray(loc as GLuint);
    check_error!();
}
pub unsafe fn asmgl_disable_vertex_attrib_array(loc: i32) {
    gltrace!("disable_vertex_attrib_array({})", loc);
    DisableVertexAttribArray(loc as GLuint);
    check_error!();
}
pub unsafe fn asmgl_vertex_attrib_pointer(loc: i32,
                                          count: usize,
                                          ty: u8,
                                          normalize: u8,
                                          stride: usize,
                                          offset: usize) {
    gltrace!("vertex_attrib_pointer({}, {}, {:?}, {}, {}, {})",
           loc, count, ty, normalize, stride, offset);
    VertexAttribPointer(loc as GLuint,
                        count as GLint,
                        data_type_enum(ty),
                        normalize as GLboolean,
                        stride as GLsizei,
                        offset as *const GLvoid);
    check_error!();
}

pub unsafe fn asmgl_draw_arrays_triangles(start: usize, count: usize) {
    gltrace!("draw_arrays_triangles({}, {})", start, count);
    DrawArrays(TRIANGLES, start as GLint, count as GLsizei);
    check_error!();
}


fn data_type_enum(ty: u8) -> GLenum {
    match ty {
        0 => UNSIGNED_BYTE,
        1 => UNSIGNED_SHORT,
        2 => UNSIGNED_INT,
        3 => BYTE,
        4 => SHORT,
        5 => INT,
        6 => FLOAT,
        _ => panic!("invalid enum"),
    }
}

fn target_enum(target: u8) -> GLenum {
    match target {
        0 => ARRAY_BUFFER,
        1 => ELEMENT_ARRAY_BUFFER,
        _ => panic!("invalid enum"),
    }
}

fn texture_kind_enums(kind: u8) -> (GLenum, GLenum, GLenum) {
    match kind {
        0 => (RGBA, RGBA, UNSIGNED_BYTE),
        1 => (DEPTH_COMPONENT, DEPTH_COMPONENT, UNSIGNED_INT),
        2 => (LUMINANCE, LUMINANCE, UNSIGNED_BYTE),
        _ => panic!("invalid enum"),
    }
}

fn attachment_enum(attachment: i8) -> GLenum {
    if attachment == -1 {
        DEPTH_ATTACHMENT
    } else {
        COLOR_ATTACHMENT0 + attachment as GLenum
    }
}


fn load_shader_impl(name: &str, buf: &mut String) {
    let mut f = File::open(&format!("shaders/{}", name)).unwrap();
    let mut s = String::new();
    f.read_to_string(&mut s).unwrap();
    for line in s.lines() {
        if line.starts_with("#include") {
            let start = line.find('"').unwrap();
            let end = line.rfind('"').unwrap();
            load_shader_impl(&line[start + 1 .. end], buf);
        } else {
            buf.push_str(line);
            buf.push('\n');
        }
    }
}

fn load_shader(name: &str, defs: &str, kind: &str) -> String {
    let mut s = defs.to_owned();
    writeln!(s, "#define OUTPOST_{}_SHADER", kind).unwrap();
    load_shader_impl("prelude.inc", &mut s);
    load_shader_impl(name, &mut s);
    s
}

unsafe fn get_status<F: FnOnce(*mut GLint)>(f: F) -> GLint {
    let mut x = 0;
    f(&mut x);
    x
}

unsafe fn get_info_log<F: FnOnce(GLsizei, *mut GLsizei, *mut GLchar)>(f: F) -> String {
    let mut buf = Vec::with_capacity(4096);
    let mut len = 4096;
    f(4096, &mut len, buf.as_mut_ptr() as *mut GLchar);
    buf.set_len(len as usize);
    String::from_utf8_lossy(&buf).into_owned()
}

