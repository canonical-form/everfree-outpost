use super::{Size16};

extern "C" {
    pub fn asmgl_has_draw_buffers() -> u8;
    pub fn asmgl_has_depth_texture() -> u8;

    pub fn asmgl_gen_buffer() -> u32;
    pub fn asmgl_delete_buffer(name: u32);
    pub fn asmgl_bind_buffer(target: u8, name: u32);
    pub fn asmgl_buffer_data_alloc(target: u8, len: usize);
    pub fn asmgl_buffer_subdata(target: u8, offset: usize, ptr: *const u8, len: usize);

    pub fn asmgl_load_shader(vert_name_ptr: *const u8,
                             vert_name_len: usize,
                             frag_name_ptr: *const u8,
                             frag_name_len: usize,
                             defs_ptr: *const u8,
                             defs_len: usize) -> u32;
    pub fn asmgl_delete_shader(name: u32);
    pub fn asmgl_bind_shader(name: u32);
    pub fn asmgl_get_uniform_location(shader_name: u32,
                                      name_ptr: *const u8,
                                      name_len: usize) -> i32;
    pub fn asmgl_get_attrib_location(shader_name: u32,
                                     name_ptr: *const u8,
                                     name_len: usize) -> i32;
    pub fn asmgl_set_uniform_1i(location: i32, value: i32);
    pub fn asmgl_set_uniform_1f(location: i32, value: f32);
    pub fn asmgl_set_uniform_2f(location: i32, value: &[f32; 2]);
    pub fn asmgl_set_uniform_3f(location: i32, value: &[f32; 3]);
    pub fn asmgl_set_uniform_4f(location: i32, value: &[f32; 4]);

    pub fn asmgl_load_texture(name_ptr: *const u8,
                              name_len: usize,
                              size_p: *mut Size16) -> u32;
    pub fn asmgl_gen_texture(width: u16, height: u16, kind: u8) -> u32;
    pub fn asmgl_delete_texture(name: u32);
    pub fn asmgl_active_texture(unit: usize);
    pub fn asmgl_bind_texture(name: u32);
    pub fn asmgl_texture_image(width: u16,
                               height: u16,
                               kind: u8,
                               data_ptr: *const u8,
                               data_len: usize);
    pub fn asmgl_texture_subimage(x: u16,
                                  y: u16,
                                  width: u16,
                                  height: u16,
                                  kind: u8,
                                  data_ptr: *const u8,
                                  data_len: usize);

    pub fn asmgl_gen_framebuffer() -> u32;
    pub fn asmgl_delete_framebuffer(name: u32);
    pub fn asmgl_bind_framebuffer(name: u32);
    pub fn asmgl_gen_renderbuffer(width: u16, height: u16, is_depth: u8) -> u32;
    pub fn asmgl_delete_renderbuffer(name: u32);
    // No use for bind_renderbuffer so far
    // NB: The `attachment` args here should really be `i8`, but as of 1.24.1, rust-compiled
    // wasm code uses i32 for all integer arguments, with sign-extension on the callee side.
    // Since the callee here is Javascript, we adjust the signature so that the sign extension
    // happens on the Rust side instead.
    pub fn asmgl_framebuffer_texture(tex_name: u32,
                                     attachment: i32);
    pub fn asmgl_framebuffer_renderbuffer(rb_name: u32,
                                          attachment: i32);
    pub fn asmgl_check_framebuffer_status() -> u8;
    pub fn asmgl_draw_buffers(num_attachments: u8);

    pub fn asmgl_viewport(x: i32, y: i32, w: i32, h: i32);
    pub fn asmgl_scissor(x: i32, y: i32, w: i32, h: i32);
    pub fn asmgl_scissor_disable();
    pub fn asmgl_clear_color(r: f32, g: f32, b: f32, a: f32);
    pub fn asmgl_clear_depth(d: f32);
    pub fn asmgl_clear();
    pub fn asmgl_set_depth_test(mode: u8);
    pub fn asmgl_set_blend_mode(mode: u8);
    pub fn asmgl_enable_vertex_attrib_array(loc: i32);
    pub fn asmgl_disable_vertex_attrib_array(loc: i32);
    pub fn asmgl_vertex_attrib_pointer(loc: i32,
                                       count: usize,
                                       ty: u8,
                                       normalize: u8,
                                       stride: usize,
                                       offset: usize);
    pub fn asmgl_draw_arrays_triangles(start: usize, count: usize);

}
