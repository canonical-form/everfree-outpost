//! `asmgl` graphics API.  Despite the name, this works on both asm.js and native targets.  It
//! either calls `asmgl` functions directly through the FFI, or provides the same API using
//! wrappers around `outpost_gl`.

#[allow(unused)] #[macro_use] extern crate log;

#[cfg(not(asmjs))] extern crate outpost_gl;
#[cfg(not(asmjs))] extern crate png_file;
#[cfg(not(asmjs))] extern crate time;

#[cfg(not(asmjs))] mod native;
#[cfg(not(asmjs))] pub use self::native::*;

#[cfg(asmjs)] mod asmjs;
#[cfg(asmjs)] pub use self::asmjs::*;

#[repr(C)]
pub struct Size16 {
    pub x: u16,
    pub y: u16,
}

// TODO: change API to use these enums (they should be FFI-safe)

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum BufferTarget {
    Array = 0,
    #[allow(dead_code)]
    ElementArray = 1,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum TextureKind {
    RGBA = 0,
    Depth = 1,
    Luminance = 2,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum DataType {
    U8 = 0,
    U16 = 1,
    U32 = 2,
    I8 = 3,
    I16 = 4,
    I32 = 5,
    F32 = 6,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum BlendMode {
    None = 0,
    Alpha = 1,
    Add = 2,
    MultiplyInv = 3,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum DepthMode {
    Disable = 0,
    GEqual = 1,
    Equal = 2,
    Always = 3,
}

