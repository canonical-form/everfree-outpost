import _outpost_engine as ENG
from _outpost_types import *
from outpost.core.data import DATA

def check_type(obj, ty):
    if not isinstance(obj, ty):
        raise TypeError('expected %r, but got %r' % (ty, type(obj)))


class ObjectProxy(object):
    def __init__(self, id):
        assert isinstance(id, type(self).ID_TYPE)
        self.id = id

    def __hash__(self):
        return hash(self.id)

    def __repr__(self):
        return '<%s #%d>' % (type(self).__name__, self.id.raw)

    def __getitem__(self, key):
        return ENG.key_value_get(self.id, key)

    def get(self, key, default=None):
        return ENG.key_value_get_default(self.id, key, default)

    def __setitem__(self, key, value):
        return ENG.key_value_set(self.id, key, value)

    def __delitem__(self, key):
        return ENG.key_value_clear(self.id, key)

    def __contains__(self, key):
        return ENG.key_value_contains(self.id, key)

class Client(ObjectProxy):
    ID_TYPE = ClientId

    def stable_id(self):
        return ENG.client_stable_id(self.id)

    def name(self):
        return ENG.client_name(self.id)

    def pawn(self):
        eid = ENG.client_pawn_id(self.id)
        if eid is not None:
            return Entity(eid)
        else:
            return None

    @staticmethod
    def by_name(name):
        cid = ENG.client_by_name(name)
        if cid is None:
            raise KeyError('no client named %r' % name)
        return Client(cid)

    def kick(self, msg):
        ENG.client_kick(self.id, msg)

    def log_msg(self, msg):
        ENG.chat_send_log(self.id, msg)

    def is_superuser(self):
        return self.get('superuser', False)

    # TODO: open_dialog

    def open_container_dialog(self, inv0, inv1, s):
        ENG.dialog_open_container(self.id, inv0.id, inv1.id, s.id)

    def open_crafting_dialog(self, s, class_mask):
        ENG.dialog_open_crafting(self.id, s.id, class_mask)

    def open_celestial_crafting_dialog(self, s, i, class_mask):
        ENG.dialog_open_crafting_celestial(self.id, s.id, i.id, class_mask)

    def create_child_inventory(self, size):
        iid = ENG.client_create_child_inventory(self.id, size)
        return Inventory(iid)


class Entity(ObjectProxy):
    ID_TYPE = EntityId

    def stable_id(self):
        return ENG.entity_stable_id(self.id)

    # TODO: by_stable_id

    def controller(self):
        cid = ENG.entity_controller(self.id)
        if cid is not None:
            return Client(cid)
        else:
            return None

    def log_msg(self, msg):
        cid = ENG.entity_controller(self.id)
        ENG.chat_send_log(cid, msg)

    def pos(self):
        return ENG.entity_pos(self.id)

    def facing(self):
        return ENG.entity_facing(self.id)

    def plane(self):
        pid = ENG.entity_plane_id(self.id)
        return Plane(pid)

    def plane_id(self):
        return ENG.entity_plane_id(self.id)

    def appearance(self):
        return ENG.entity_appearance(self.id)

    def set_appearance(self, appearance):
        ENG.entity_set_appearance(self.id, appearance)

    def teleport(self, pos):
        ENG.entity_teleport(self.id, pos)

    # TODO: teleport_plane
    # TODO: set_activity
    # TODO: energy

    def main_inv(self):
        iid = ENG.char_inv_main(self.id)
        if iid is not None:
            return Inventory(iid)
        else:
            return None

    def ability_inv(self):
        iid = ENG.char_inv_ability(self.id)
        if iid is not None:
            return Inventory(iid)
        else:
            return None

    def equip_inv(self):
        iid = ENG.char_inv_equip(self.id)
        if iid is not None:
            return Inventory(iid)
        else:
            return None


class Inventory(ObjectProxy):
    ID_TYPE = InventoryId

    def stable_id(self):
        return ENG.inventory_stable_id(self.id)

    # TODO: add, remove (bulk)
    # TODO: __getitem__
    # TODO: count, count_space

    def is_empty(self):
        return ENG.inventory_is_empty(self.id)

    def is_public(self):
        return ENG.inventory_is_public(self.id)

    def set_public(self, public):
        return ENG.inventory_set_public(self.id, public)


class Plane(ObjectProxy):
    ID_TYPE = PlaneId

    def stable_id(self):
        return ENG.plane_stable_id(self.id)

    # TODO: by_stable_id

    # TODO: structure_at_point

    def create_structure(self, pos, template):
        sid = ENG.structure_create(self.id, pos, DATA.template(template).id)
        return Structure(sid)

    def add_ward(self, region, owner, owner_name):
        ENG.ward_map_add_ward(self.id, region, owner, owner_name)

    def remove_ward(self, owner):
        ENG.ward_map_remove_ward(self.id, owner)

    def has_ward_map(self):
        return ENG.ward_map_contains(self.id)

    def has_ward(self, owner):
        return ENG.ward_map_has_ward(self.id, owner)

    def ward_owner_name(self, owner):
        return ENG.ward_map_owner_name(self.id, owner)

    def ward_region(self, owner):
        return ENG.ward_map_region(self.id, owner)

    def set_looted_vault(self, vault_id):
        return ENG.plane_set_looted_vault(self.id, vault_id)

    def nearest_compass_target(self, pos):
        return ENG.plane_nearest_compass_target(self.id, pos)


class Structure(ObjectProxy):
    ID_TYPE = StructureId

    def stable_id(self):
        return ENG.structure_stable_id(self)

    @staticmethod
    def by_stable_id(stable_id):
        sid = ENG.structure_transient_id(stable_id)
        if sid is None:
            raise KeyError('no such structure: %r' % (stable_id,))
        return Structure(sid)

    def pos(self):
        return ENG.structure_pos(self.id)

    def plane(self):
        pid = ENG.structure_plane(self.id)
        return Plane(pid)

    def template(self):
        template_id = ENG.structure_template_id(self.id)
        return DATA.template(template_id)

    def destroy(self):
        ENG.structure_destroy(self.id)

    def create_child_inventory(self, size):
        iid = ENG.structure_create_child_inventory(self.id, size)
        return Inventory(iid)

    def set_contents(self, i):
        ENG.contents_set(self.id, i.id)

    def clear_contents(self):
        ENG.contents_clear(self.id)

    def contents(self):
        iid = ENG.contents_get(self.id)
        return Inventory(iid)

    def parent_vault(self):
        return ENG.structure_parent_vault(self.id)


def system_msg(msg):
    ENG.chat_send_system(msg)

def now():
    return ENG.now()

def create_inventory(size):
    iid = ENG.inventory_create(size)
    return Inventory(iid)

# TODO: num_clients / list_clients
# TODO: create_plane
# TODO: schedule_timer
# TODO: cancel_timer

ward_permit_add = ENG.ward_permit_add
ward_permit_remove = ENG.ward_permit_remove
ward_permit_list = ENG.ward_permit_list
ward_permit_granted = ENG.ward_permit_granted
