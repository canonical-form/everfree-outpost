import math

from _outpost_types import V2


def handle(obj, k):
    def decorate(f):
        setattr(obj, k, f)
        return f
    return decorate


NS1_NORMAL = V2(-2, 5)
NS2_NORMAL = V2(2, 5)

EW1_NORMAL = V2(5, -2)
EW2_NORMAL = V2(5, 2)

def vector_approx_8way(v):
    if v == 0:
        return V2(0, 0)

    ns1 = NS1_NORMAL.dot(v)
    ns2 = NS2_NORMAL.dot(v)

    ew1 = EW1_NORMAL.dot(v)
    ew2 = EW2_NORMAL.dot(v)

    if ns1 > 0 and ns2 > 0:
        dy = 1
    elif ns1 < 0 and ns2 < 0:
        dy = -1
    else:
        dy = 0

    if ew1 > 0 and ew2 > 0:
        dx = 1
    elif ew1 < 0 and ew2 < 0:
        dx = -1
    else:
        dx = 0

    return V2(dx, dy)

def dir_desc_8way(v):
    if v.y > 0:
        dir_ns = 'south'
    elif v.y < 0:
        dir_ns = 'north'
    else:
        dir_ns = ''

    if v.x > 0:
        dir_ew = 'east'
    elif v.x < 0:
        dir_ew = 'west'
    else:
        dir_ew = ''

    return dir_ns + dir_ew

def vector_desc(v):
    if v == 0:
        return 'right here'

    ns1 = NS1_NORMAL.dot(v)
    ns2 = NS2_NORMAL.dot(v)

    ew1 = EW1_NORMAL.dot(v)
    ew2 = EW2_NORMAL.dot(v)

    if ns1 > 0 and ns2 > 0:
        dir_ns = 'south'
    elif ns1 < 0 and ns2 < 0:
        dir_ns = 'north'
    else:
        dir_ns = ''

    if ew1 > 0 and ew2 > 0:
        dir_ew = 'east'
    elif ew1 < 0 and ew2 < 0:
        dir_ew = 'west'
    else:
        dir_ew = ''

    dir_str = dir_ns + dir_ew

    dist = max(abs(v.x), abs(v.y))
    if dist < 10:
        return 'nearby, to the %s' % dir_str
    elif dist < 100:
        return 'to the %s of here' % dir_str
    else:
        return 'far to the %s' % dir_str

