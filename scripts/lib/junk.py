from _outpost_types import *
from outpost.core.data import DATA
from outpost.core import logic
from outpost.lib import permission
from outpost.lib.util import handle


def register_interact(name, loot=[]):
    struct = DATA.template(name)
    loot = [(DATA.item(name), count) for name, count in loot]

    @handle(struct.handlers, 'can_interact')
    def struct_can_interact(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'structure_harvest'):
            return False

        inv = e.main_inv()
        if not logic.inventory.can_add_multi(inv, loot):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def struct_on_interact(s, e):
        for (item, count) in loot:
            logic.inventory.add(e.main_inv(), item, count)


def register_destroy(name, loot=[]):
    struct = DATA.template(name)
    loot = [(DATA.item(name), count) for name, count in loot]

    @handle(struct.handlers, 'can_destroy')
    def struct_can_destroy(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'structure_destroy'):
            return False

        inv = e.main_inv()
        if not logic.inventory.can_add_multi(inv, loot):
            return False

        return True

    @handle(struct.handlers, 'on_destroy')
    def struct_on_destroy(s, e):
        s.destroy()
        for (item, count) in loot:
            logic.inventory.add(e.main_inv(), item, count)

def register(name, gather_loot=None, break_loot=[]):
    if gather_loot is not None:
        register_interact(name, loot=gather_loot)
    register_destroy(name, loot=break_loot)

