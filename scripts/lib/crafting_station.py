from _outpost_types import *
from outpost.core.data import DATA
from outpost.lib import permission
from outpost.lib.util import handle

def make_on_create(inv_size=6):
    def on_create(s, e):
        i = s.create_child_inventory(inv_size)
        s.set_contents(i)

    return on_create

def register(name, station_classes):
    struct = DATA.template(name)

    class_names = [x.strip() for x in station_classes.split(',')]

    class_mask = 0
    for n in class_names:
        b = DATA.crafting_class_id(n)
        class_mask |= 1 << b

    @handle(struct.handlers, 'can_interact')
    def station_can_interact(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'crafting_station_open'):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def station_on_interact(s, e):
        e.controller().open_crafting_dialog(s, class_mask)


