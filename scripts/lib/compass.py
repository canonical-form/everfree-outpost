from _outpost_types import *
from outpost.core.data import DATA
from outpost.core import engine, logic
from outpost.lib import util
from outpost.lib.util import handle, vector_approx_8way

def register_compass(item, target_func, precision_func):
    '''Register a compass-like item.  `target_func(e)` should return
    the position of the target of this compass, as a V3 in pixel
    coordinates.  `precision_func(dist2)` should return the level of precision
    (0..3, with 3 being most precise) for the given squared distance from the
    target.  If `precision_func` is None, the compass simply reports the
    direction with no precision qualifier.'''
    item = DATA.item(item)

    @handle(item.handlers, 'can_use')
    def item_can_use(e, _pos):
        return True

    @handle(item.handlers, 'on_use')
    def item_on_use(e, _pos):
        target_pos = target_func(e)

        if target_pos is None:
            e.log_msg('The compass spins randomly.')
            return

        d = target_pos.reduce() / 16 - e.pos().reduce() / 16
        dist2 = d.dot(d)
        approx = util.vector_approx_8way(d)

        if dist2 == 0:
            e.log_msg('The compass points directly at the ground.')
            return

        if precision_func is None:
            e.log_msg('The compass points %s.' % util.dir_desc_8way(approx))
            return

        prec = precision_func(dist2)
        if prec <= 0:
            e.log_msg('The compass spins randomly.')
        elif prec == 1:
            e.log_msg('The compass wobbles uncertainly.')
        elif prec == 2:
            e.log_msg('The compass points vaguely %s.' % util.dir_desc_8way(approx))
        elif prec >= 3:
            e.log_msg('The compass points steadily %s.' % util.dir_desc_8way(approx))

def register_chaotic_compass(item):
    def target_func(e):
        nearest = e.plane().nearest_compass_target(e.pos())
        if nearest is None:
            return None
        _, vault_pos = nearest
        return vault_pos

    def precision_func(dist2):
        if dist2 <= 16 * 16:
            return 3
        elif dist2 <= 40 * 40:
            return 2
        elif dist2 <= 64 * 64:
            return 1
        else:
            return 0

    register_compass(item, target_func, precision_func)

def register_spawn_compass(item):
    def target_func(_e):
        return V3(0, 0, 0)

    register_compass(item, target_func, precision_func=None)
