'''Functions for registering common object behaviors.  The naming convention is
`(obj)_(act)_(desc)` for a function that registers handlers for an `obj`'s
`can_act`/`on_act` events, implementing behavior `desc`.  Example:
`item_use_place` sets `can_use` and `on_use` handlers for an `item` that
implement the "place structure" functionality.
'''
from _outpost_types import *
from outpost.core.data import DATA
from outpost.core import engine, logic
from outpost.lib import permission
from outpost.lib.util import handle

def item_use_place(item, struct, can_create=None, on_create=None):
    '''Item behavior: place `struct` at the use location, consuming 1 `item`
    from the actor's inventory.

    Permission: `structure_create`

    Hooks: `can_create(e, pos)`, `on_create(e, s)`
    '''
    item = DATA.item(item)
    struct = DATA.template(struct)

    @handle(item.handlers, 'can_use')
    def item_can_use_place(e, pos):
        bounds = Region3(pos, pos + struct.size)
        if not permission.check_region(e, bounds, 'structure_create'):
            return False

        if not logic.structure.can_place(struct, e.plane(), pos):
            return False

        inv = e.main_inv()
        if not logic.inventory.can_remove(inv, item, 1):
            return False

        if can_create is not None and not can_create(e, pos):
            return False

        return True

    @handle(item.handlers, 'on_use')
    def item_on_use_place(e, pos):
        s = e.plane().create_structure(pos, struct)
        logic.inventory.remove(e.main_inv(), item, 1)
        if on_create is not None:
            on_create(e, s)


def structure_destroy_take(struct, item, can_destroy=None, on_destroy=None):
    '''Structure behavior: pick up the structure, obtaining 1 `item`.

    Permission: `structure_destroy`

    Hooks: `can_destroy(e, s)`, `on_destroy(e, s)`
    '''
    struct = DATA.template(struct)
    item = DATA.item(item)

    @handle(struct.handlers, 'can_destroy')
    def structure_can_destroy_take(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'structure_destroy'):
            return False

        inv = e.main_inv()
        if not logic.inventory.can_add(inv, item, 1):
            return False

        if can_destroy is not None and not can_destroy(e, s):
            return False

        return True

    @handle(struct.handlers, 'on_destroy')
    def structure_on_destroy_take(s, e):
        if on_destroy is not None:
            on_destroy(e, s)
        s.destroy()
        logic.inventory.add(e.main_inv(), item, 1)


def structure_interact_container(struct, can_open=None):
    '''Structure behavior: open a container dialog on interaction.

    Permission: `structure_access_contents`

    Hooks: `can_open(e, s)`
    '''
    struct = DATA.template(struct)

    @handle(struct.handlers, 'can_interact')
    def struct_can_interact_container(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'structure_access_contents'):
            return False

        if can_open is not None and not can_open(e, s):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def struct_on_interact_container(s, e):
        e.controller().open_container_dialog(e.main_inv(), s.contents(), s)

def structure_interact_container_loot(struct, can_open=None):
    '''Structure behavior: open a container dialog on interaction, and mark its
    parent vault as looted.

    Permission: `structure_access_contents`

    Hooks: `can_open(e, s)`
    '''
    struct = DATA.template(struct)

    @handle(struct.handlers, 'can_interact')
    def struct_can_interact_container(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'structure_access_contents'):
            return False

        if can_open is not None and not can_open(e, s):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def struct_on_interact_container(s, e):
        vid = s.parent_vault()
        s.plane().set_looted_vault(vid)

        e.controller().open_container_dialog(e.main_inv(), s.contents(), s)


def convert_crafting_classes(classes):
    if classes is None:
        raise ValueError('classes must be a list of crafting class names')
    class_mask = 0
    for name in classes:
        c = DATA.get_crafting_class(name)
        if c is None:
            continue
        class_mask |= 1 << c.id

    return class_mask

def structure_interact_crafting(struct, classes, can_open=None):
    '''Structure behavior: open a crafting dialog, providing access to crafting
    classes `classes`.

    Permission: `structure_access_crafting`

    Hooks: `can_open(e, s)`
    '''
    struct = DATA.template(struct)
    class_mask = convert_crafting_classes(classes)

    @handle(struct.handlers, 'can_interact')
    def struct_can_interact(s, e):
        bounds = Region3(s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'structure_access_crafting'):
            return False

        if can_open is not None and not can_open(e, s):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def struct_on_interact(s, e):
        e.controller().open_crafting_dialog(s, class_mask)

CELESTIAL_INVENTORY_SIZE = {
    'crafting': 6,
    'chest': 30,
}

def get_celestial_inventory(c, name):
    iid = c.get('celestial_%s' % name)
    if iid is not None:
        i = engine.Inventory(iid)
    else:
        i = c.create_child_inventory(CELESTIAL_INVENTORY_SIZE[name])
        c['celestia_%s' % name] = i.id
    return i

def structure_interact_crafting_celestial(struct, classes, can_open=None):
    '''Structure behavior: open a crafting dialog, providing access to crafting
    classes `classes`.  

    Permission: `structure_access_crafting`

    Hooks: `can_open(e, s)`
    '''
    struct = DATA.template(struct)

    if classes is None:
        raise ValueError('classes must be a list of crafting class names')
    class_mask = 0
    for name in classes:
        c = DATA.get_crafting_class(name)
        if c is None:
            continue
        class_mask |= 1 << c.id

    @handle(struct.handlers, 'can_interact')
    def struct_can_interact(s, e):
        if can_open is not None and not can_open(e, s):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def struct_on_interact(s, e):
        i = get_celestial_inventory(e.controller(), 'crafting')
        e.controller().open_celestial_crafting_dialog(s, i, class_mask)
