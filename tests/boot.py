
class Handlers:
    def __init__(self):
        self.__name__ = 'outpost.core.handlers'

    def _no_op(self, *args, **kwargs):
        return ()

    def __getattr__(self, k):
        return self._no_op


class Core:
    def __init__(self):
        self.__name__ = 'outpost.core'
        self.handlers = Handlers()

class Outpost:
    def __init__(self):
        self.__name__ = 'outpost'
        self.core = Core()

outpost = Outpost()

import sys
def do_import(m, name):
    if getattr(m, '__name__', None) != name:
        return

    sys.modules[name] = m
    for k, v in m.__dict__.items():
        do_import(v, name + '.' + k)

do_import(outpost, 'outpost')

try:
    import outpost.core.handlers
except Exception as e:
    print(e)
