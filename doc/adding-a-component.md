`src/libengine/component/mod.rs`:

 * Add the new component module
 * Add a new field to `struct Components`.
 * Add a `destroy` call to the appropriate `destroy_obj` cleanup method(s)


`src/libengine/update/macros.rs`:

 * Add info on the new component to the `obj_components` definition.


`src/libengine/update/components.rs`:

 * Add a `Tag` struct and `Component` impl for the new component.
 * `get` is required only if `store_new` is set to `true` in `macros.rs`.
 * `rewrite_obj_components` is required only if the new component contains
   transient object IDs.


`src/server2/save/macros.rs`:

 * Add info on the new component to the `obj_components` definition.


`src/server2/save/components.rs`:

 * Add a `Component` impl for the `Tag` struct.



For components with time-varying fields (like crafting progress - essentially a
`Gauge`):

`src/server2/save/memory.rs`:

 * Add a case to `update_obj` that updates the inputs to the time-varying fields.
 * Add a case to `build_obj` that computes the current values of the
   time-varying fields.


`src/server2/save/objects.rs`:

 * Add a new field to `Obj` to store the inputs for computing the values of
   time-varying components.
