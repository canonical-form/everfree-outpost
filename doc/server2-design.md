# Extension Traits

`libworld` allows client code to provide a type implementing the extension
trait `Ext` to be included in the world data.  The extension receives callbacks
for various events, such as creation and destruction of objects, allowing it to
update the extension state accordingly.  This is meant to allow clients to
extend the `World` with custom components (tables associating additional
data with certain object IDs) and custom caches, both of which require updating
when the main object data changes.

`libengine` provides a similar extension subtrait, which includes additional
callbacks for engine events, such as a client's map data finishing loading.


# `libengine` Entry Points

The entry points for `libengine` are:

 * `Engine::new`
 * The `on_xyz` functions found in various modules
 * The function `engine::post::post`

Client code may call any entry point at any time.  Entry points validate
arguments, logging errors if necessary, and never panic.

Many other `libengine` functions are public due to limitations of the Rust
module system.  These functions should not be called from outside `libengine`.
They often make assumptions about their arguments relative to `libengine`'s
internal invariants, and may panic if those assumptions are violated.


# The Engine's World Extensions

`libengine` implements the `libworld` extension trait to add important
components and caches, such as the terrain shape (collision detection) cache.

The extension trait should not be used for other purposes.  Complex operations
involving modifications to the world data can't be done directly in the handler
(it has only immutable access to the `World`).  The handler can queue events
for the post-processing step, but this is difficult to do correctly due to the
inherent asynchrony of the post-processing step.  If modifying one object
should trigger modification of a second object, then there should be a single
method that performs both modifications.


# The Engine's Post-Processing Step

`libengine` provides a method `post` which should be called frequently (for
example, after every call to an `on_xyz` entry point) to process asynchronous
events.  Various parts of the engine may queue events for later processing,
either because it is not possible to perform the operation immediately, or
because some operations can be coalesced for efficiency.  For example, the
`chunk_refs` module queues events for loading and unloading chunks, so that
"load" and "unload" operations on the same chunk can be coalesced into a no-op. 

Asynchrony itself adds complexity, so asynchronous operations should be kept as
simple as possible.  For example, `chunk_refs` only ever requests new chunks
from the engine extension and deletes existing chunks or planes.  Both
operations have minimal side effects.  Operations with major side effects,
especially calls to the script engine, should be performed synchronously if
possible, so that they occur at a predictable time and in a predictable order.

Note that `post` makes only a single pass over the currently queued events.  If
an event handler causes more events to be queued, there might be an arbitrary
delay before those new events are processed.  These issues should be avoidable
if asynchronous operations are kept simple.
