uniform sampler2D sheet_tex;
uniform sampler2D cavernTex;
uniform vec2 camera_pos;
uniform vec2 camera_size;
uniform vec2 sliceCenter;
uniform float sliceZ;
uniform bool clip_enabled;

varying vec2 tex_coord;
varying float baseZ;
varying float clipped;

#define cameraPos camera_pos
#define cameraSize camera_size
#include "slicing.inc"

void main(void) {
    if (sliceCheck()) {
        discard;
    }

    vec4 color = texture2D(sheet_tex, tex_coord);

#ifndef OUTPOST_SHADOW
    if (clip_enabled) {
        vec2 pos = gl_FragCoord.xy + vec2(-16.0, -16.0);
        vec2 center = camera_size / 2.0;
        vec2 dist = pos - center;
        if (dot(dist, dist) < 64.0 * 64.0 &&
                clipped > 0.0 &&
                mod(gl_FragCoord.x + gl_FragCoord.y, 2.0) < 1.0) {
            discard;
        }
    }

    if (color.a < 1.0) {
        discard;
    } else {
        WRITE_COLOR(0, color);
    }
#else
    if (color.a == 0.0 || color.a == 1.0) {
        discard;
    } else {
        WRITE_COLOR(0, color);
    }
#endif

    WRITE_DEPTH();
}
