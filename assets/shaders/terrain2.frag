uniform sampler2D atlasTex;
uniform sampler2D cavernTex;
uniform vec2 cameraPos;
uniform vec2 cameraSize;
uniform vec2 sliceCenter;
uniform float sliceZ;

varying vec2 texCoord;
varying float baseZ;

#include "slicing.inc"

void main(void) {
    if (sliceCheck()) {
        discard;
    }

    vec4 color = texture2D(atlasTex, texCoord);
    if (color.a == 0.0) {
        discard;
    } else {
        WRITE_COLOR(0, color);
    }

    WRITE_DEPTH();
}
