uniform sampler2D sheet_tex;
#ifndef UI_DISPLAY
uniform sampler2D depth_tex;
uniform sampler2D cavernTex;
#endif
// The UI atlas, used for rendering player names.
uniform sampler2D ui_tex;

uniform vec2 camera_size;
#ifndef UI_DISPLAY
uniform vec2 camera_pos;
uniform vec2 sliceCenter;
uniform float sliceZ;
#endif

varying vec2 tex_coord;
varying vec3 ref_pos;
varying float ref_size_z;
varying vec3 color_;
varying float sheet_;

#ifndef UI_DISPLAY
#define cameraPos camera_pos
#define cameraSize camera_size
#define baseZ floor(ref_pos.z / TILE_SIZE)

#include "slicing.inc"
#endif



#include "pony_shading.inc"

// Replace magenta pixels in the texture with variants of the layer color.
// Supports a palette of four variants, plus the layer color itself.  The
// variants are computed from the layer color using the coefficient values in
// `pony_shading.inc`.
vec3 compute_color(vec3 tex_color, vec3 layer_color) {
    if (tex_color.r == tex_color.b && tex_color.g == 0.0 && tex_color.r > 0.0) {
        float level = floor(tex_color.r * 255.0 + 0.5);

        vec4 coeffs;
        vec3 blend_color = vec3(0.0);

        if (level == 101.0) {
            // Border
            coeffs = SHADING_COEFFS_BORDER;
        } else if (level == 102.0) {
            // Border (light)
            coeffs = SHADING_COEFFS_BORDER_LIGHT;
        } else if (level == 103.0) {
            // Shadow
            coeffs = SHADING_COEFFS_SHADOW;
        } else if (level == 104.0) {
            // Highlight
            coeffs = SHADING_COEFFS_HIGHLIGHT;
            blend_color = vec3(1.0);
        } else {
            return layer_color;
        }

        float alpha = dot(vec4(layer_color, 1.0), coeffs);
        return layer_color * alpha + blend_color * (1.0 - alpha);
    } else {
        return tex_color;
    }
}


void main(void) {
#ifndef UI_DISPLAY
    if (sliceCheck()) {
        discard;
    }

    // Custom depth-test logic

    // NB: FragCoord origin is the bottom left, not the top left.  So it works
    // for texture lookups (they use the same origin), but it needs to be
    // flipped for coordinate calculations.
    vec3 pos = vec3(gl_FragCoord.x + camera_pos.x,
                    gl_FragCoord.y + camera_pos.y,
                    0.0);
    float depth = READ_DEPTH(depth_tex, gl_FragCoord.xy / camera_size).r;
    float z = floor(depth * 512.0);

    pos.y += z;
    pos.z = z;
    if (pos.z >= ref_pos.z + ref_size_z ||
            (pos.z > ref_pos.z && pos.y > ref_pos.y)) {
        discard;
    }
#endif

    if (sheet_ != -1.0) {
        vec4 tex_color = texture2D(sheet_tex, tex_coord);
        if (tex_color.a == 0.0) {
            discard;
        }
        WRITE_COLOR(0, vec4(compute_color(tex_color.rgb, color_), tex_color.a));
    } else {
        vec4 color = texture2D(ui_tex, tex_coord) * vec4(color_, 1.0);
        if (color.a == 0.0) {
            discard;
        }
        WRITE_COLOR(0, color);
    }

    WRITE_DEPTH();
}
