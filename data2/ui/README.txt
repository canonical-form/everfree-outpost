This directory contains definitions of graphical elements for the client UI.
These definitions are processed differently from the normal game data in
`data2/game/`, in that the data files generated from these definitions are
shipped with the client, not downloaded from the server.  Server-side mods
can't override these definitions.

These definitions are closely tied to the client UI code, and in particular,
the enums defined in `atlas.rs`.  There must be a corresponding definition for
each variant in those enums.
