import os
import stat
import textwrap


def mk_bindings(**kwargs):
    return '\n'.join('%s = %s' % (k, v) for k,v in kwargs.items())

def mk_rule(name, **kwargs):
    bindings_str = mk_bindings(**kwargs)
    return 'rule %s\n' % name + textwrap.indent(bindings_str, '  ')

def mk_build(targets, rule, inputs, implicit=(), order=(), **kwargs):
    targets = targets if isinstance(targets, str) else ' '.join(targets)
    inputs = inputs if isinstance(inputs, str) else ' '.join(inputs)
    implicit = ' | %s' % ' '.join(implicit) if implicit else ''
    order = ' || %s' % ' '.join(order) if order else ''
    bindings = '\n' + textwrap.indent(mk_bindings(**kwargs), '  ') if len(kwargs) > 0 else ''
    return 'build %s: %s %s%s%s%s' % (targets, rule, inputs, implicit, order, bindings)

def maybe(s, x, t=''):
    if x is not None:
        return s % x
    else:
        return t

def cond(x, a, b=''):
    if x:
        return a
    else:
        return b

def join(*args):
    return ' '.join(args)


def walk_dir(d, rel=''):
    '''Visit every file in subdirectories of `os.path.join(d, rel)`, yielding
    the path of each one relative to `d`.'''
    for f in os.listdir(os.path.join(d, rel)):
        if f.startswith('.'):
            continue

        st = os.stat(os.path.join(d, rel, f))

        if stat.S_ISREG(st.st_mode):
            yield os.path.join(rel, f)
        elif stat.S_ISDIR(st.st_mode):
            for q in walk_dir(d, os.path.join(rel, f)):
                yield q
