from configure.checks.context import ConfigError

def configure(ctx):
    out = ctx.file('exe')

    ctx.detect('pandoc', 'Pandoc', ('pandoc',), chk_pandoc)

    ctx.detect('patchelf', 'patchelf', ('patchelf',), chk_patchelf)
    ctx.copy_arg('patch_libpython_version', 'patch libpython version')
    ctx.copy_arg('patch_elf_loader', 'patch ELF loader')
    configure_patch_elf_loader_path(ctx)

def requirements(ctx):
    reqs = ('pandoc',)
    if ctx.args.patch_elf_loader or ctx.args.patch_libpython_version is not None:
        reqs += ('patchelf',)
    if ctx.args.patch_elf_loader:
        reqs += ('patch_elf_loader_path',)
    return reqs


def chk_pandoc(ctx, pandoc):
    if not ctx.run(pandoc, ('--version',)):
        raise ConfigError('not found')
    return True

def chk_patchelf(ctx, patchelf):
    if not ctx.run(patchelf, ('--version',)):
        raise ConfigError('not found')
    return True

def configure_patch_elf_loader_path(ctx):
    ctx.copy_arg('patch_elf_loader_path', 'path to replacement ELF loader')

    if ctx.info.patch_elf_loader_path is not None:
        return;

    if not ctx.info.patch_elf_loader:
        return

    if ctx.info.patchelf is None:
        ctx.warn_skip('patch_elf_loader_path', 'patchelf')
        return

    for binary in ('/bin/sh', '/bin/bash', '/usr/bin/env'):
        ctx.out_part('Checking for ELF loader of binary %r: ' % binary)
        out = ctx.run_output(ctx.info.patchelf, ('--print-interpreter', binary))
        if out is not None:
            ctx.out(out)
            ctx.info.patch_elf_loader_path = out.strip()
            break
        else:
            ctx.out('not found')
