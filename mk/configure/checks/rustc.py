import os
import shlex

from configure.checks.context import ConfigError

NEED_RUSTC_VERSION = '1.33'

NEED_RUST_LIBS = (
    # Keep these in dependency order.  That way necessary --extern flags will already be
    # set before they are needed for later checks.
    'libc',
    'bitflags',
    'rand',
    'log',
    'env_logger',
    'rustc_serialize',
    'time',
    'python3_sys',
    'linked_hash_map',
    'sdl2',
    'gl_generator',
    'png',
    )

NEED_RUST_LIB_SRC = (
        'core',
        'alloc',
        'libc',
        'unwind',
        'rustc_demangle',
        'compiler_builtins',
        'panic_abort',
        'dlmalloc',
        'std',
        'arena',
        'bitflags',
        'log',
        # If you extend this, also add a new check in configure()
        )

def configure(ctx):
    ctx.detect('rustc', 'Rust compiler', ('rustc',), chk_rustc)

    ctx.info.add('rust_externs', 'Rust library --extern flags')
    ext_arg = ctx.args.rust_lib_externs
    ctx.info.rust_externs = ext_arg.split(',') if ext_arg else []

    for lib in NEED_RUST_LIBS:
        configure_lib(ctx, lib)

    # Note: if you add another `detect_link_flags`, also update `requirements`.
    detect_link_flags(ctx, 'python3_sys', 'Py_Initialize()',
            ('', ctx.info.python3_libs or '', '-lpython3',
                '-lpython3.6', '-lpython3.5', '-lpython3.4', '-lpython3.3'))
    detect_link_flags(ctx, 'sdl2', 'init()',
            ('', '-lSDL2'))

    configure_lib_src(ctx, 'core', ctx.args.rust_home, 'src/libcore')
    configure_lib_src(ctx, 'std_unicode', ctx.args.rust_home, 'src/libstd_unicode')
    configure_lib_src(ctx, 'alloc', ctx.args.rust_home, 'src/liballoc')
    configure_lib_src(ctx, 'alloc_system', ctx.args.rust_home, 'src/liballoc_system')
    configure_lib_src(ctx, 'libc', ctx.args.rust_home, 'vendor/libc/src')
    configure_lib_src(ctx, 'unwind', ctx.args.rust_home, 'src/libunwind')
    configure_lib_src(ctx, 'rustc_demangle', ctx.args.rust_home, 'vendor/rustc-demangle/src')
    configure_lib_src(ctx, 'compiler_builtins', ctx.args.rust_home, 'vendor/compiler_builtins/src')
    configure_lib_src(ctx, 'panic_abort', ctx.args.rust_home, 'src/libpanic_abort')
    configure_lib_src(ctx, 'dlmalloc', ctx.args.rust_home, 'vendor/dlmalloc/src')
    configure_lib_src(ctx, 'std', ctx.args.rust_home, 'src/libstd')
    configure_lib_src(ctx, 'arena', ctx.args.rust_home, 'src/libarena')
    configure_lib_src(ctx, 'bitflags', ctx.args.bitflags_home)
    configure_lib_src(ctx, 'log', ctx.args.log_home)

    configure_unstable_features(ctx)

    ctx.copy_arg('rust_extra_libdir', 'Rust extra library directory')

def requirements(ctx):
    return ('rustc', 'rustc_feature_env',) + \
            tuple('rust_lib%s_path' % l for l in NEED_RUST_LIBS) + \
            tuple('rust_lib%s_src' % l for l in NEED_RUST_LIB_SRC) + \
            tuple('rust_lib%s_ldflags' % l for l in ('python3_sys', 'sdl2'))

def configure_unstable_features(ctx):
    ctx.info.add('rustc_feature_env', 'rustc settings for #![feature(...)]')

    src = ctx.write('rs', '#![feature(lang_items)] fn main() {}')
    out = ctx.file('exe')

    ctx.out_part('Checking for rustc #![feature(...)]: ')
    ok = ctx.run(ctx.info.rustc, (src, '-o', out))
    if ok:
        ctx.out('ok')
        ctx.info.rustc_feature_env = ''
        return
    else:
        ctx.out('not available')

    ctx.out_part('Checking for rustc #![feature(...)] (with RUSTC_BOOTSTRAP=1): ')
    ok = ctx.run(ctx.info.rustc, (src, '-o', out),
            env={'RUSTC_BOOTSTRAP': '1'})
    if ok:
        ctx.out('ok')
        ctx.info.rustc_feature_env = 'RUSTC_BOOTSTRAP=1'
        return
    else:
        ctx.out('not available')

    ctx.info.rustc_feature_env = None

def configure_lib(ctx, crate_name):
    key = 'rust_lib%s_path' % crate_name
    desc = 'Rust lib%s library path' % crate_name
    lib_desc = 'Rust lib%s library' % crate_name
    ctx.info.add(key, desc)

    if ctx.info.rustc is None:
        ctx.warn_skip(key, 'rustc')
        return

    # Set up for the tests
    src = ctx.write('rs', 'extern crate %s; fn main() {}' % crate_name)
    out = ctx.file('exe')

    lib_dir_flags = ()
    extern = None
    if ctx.args.rust_extra_libdir is not None:
        lib_dir_flags = ('-L', ctx.args.rust_extra_libdir)
        extern = os.path.join(ctx.args.rust_extra_libdir, 'lib%s.rlib' % crate_name)

    extern_flag = ('--extern', '%s=%s' % (crate_name, extern),)

    other_extern_flags = ()
    for e in ctx.info.rust_externs:
        other_extern_flags += ('--extern', e)


    path = getattr(ctx.args, key, None)

    if path is None:
        ctx.out_part('Checking for %s: ' % lib_desc)
        if ctx.run(ctx.info.rustc,
                lib_dir_flags + other_extern_flags + (src, '-o', out)):
            ctx.out('ok')
            path = ''
        else:
            ctx.out('not found')

    if path is None and extern is not None:
        ctx.out_part('Checking for %s (with --extern): ' % lib_desc)
        if ctx.run(ctx.info.rustc,
                lib_dir_flags + other_extern_flags + extern_flag + (src, '-o', out)):
            ctx.out('ok')
            path = extern
        else:
            ctx.out('not found')

    setattr(ctx.info, key, path)
    if path is not None and path != '':
        ctx.info.rust_externs.append('%s=%s' % (crate_name, extern))

def detect_link_flags(ctx, crate_name, expr, candidates):
    key = 'rust_lib%s_ldflags' % crate_name
    desc = 'Rust lib%s linker flags' % crate_name
    ctx.info.add(key, desc)

    src = ctx.write('rs', '''
            extern crate %s;
            use %s::*;
            fn main() {
                let _ = unsafe {
                    %s
                };
            }''' % (crate_name, crate_name, expr))
    out = ctx.file('exe')

    args = [src, '-o', out]
    if ctx.args.rust_extra_libdir is not None:
        args.extend(('-L', ctx.args.rust_extra_libdir))
    for extern in ctx.info.rust_externs:
        args.extend(('--extern', extern))


    def chk(ctx, flags):
        return ctx.run(ctx.info.rustc, args + shlex.split(flags))

    ctx.detect(key, desc, candidates, chk, deps=('rustc',))

def configure_lib_src(ctx, crate_name, home, rel_dir='src'):
    candidates = (os.path.join(home, rel_dir, 'lib.rs'),)

    def chk(ctx, path):
        if not os.path.isfile(path):
            raise ConfigError('not found')
        return True

    key = 'rust_lib%s_src' % crate_name
    desc = 'Rust lib%s source file' % crate_name
    ctx.detect(key, desc, candidates, chk)


def chk_rustc(ctx, rustc):
    out = ctx.run_output(rustc, ('--version',))
    if out is None:
        raise ConfigError('not found')

    ver = out.splitlines()[0]
    if NEED_RUSTC_VERSION not in ver:
        raise ConfigError('bad version %r (need %r)' % (ver.strip(), NEED_RUSTC_VERSION))

    return True

