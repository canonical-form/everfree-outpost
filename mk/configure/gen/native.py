import os

from minitemplate import template

from configure.util import cond, join, maybe, mk_build


def rules(i):
    common_cflags = join(
            '-MMD -MF $out.d',
            '$picflag',
            cond(i.debug, '-ggdb', '-O3'),
            )

    return template(r'''
        rule c_obj
            command = $cc -c $in -o $out -std=c99 %common_cflags $cflags $user_cflags
            depfile = $out.d
            description = CC $out

        rule cxx_obj
            command = $cxx -c $in -o $out $
                %{i.cxx14_flag} %common_cflags $cxxflags $user_cxxflags
            depfile = $out.d
            description = CXX $out

        rule link_bin
            command = $cxx $in -o $out $ldflags $user_ldflags $libs
            description = LD $out

        rule link_shlib
            command = $cxx -shared $in -o $out $ldflags $user_ldflags $libs
            description = LD $out
    ''', **locals())


def cxx(out_name, out_type, src_files, link_extra=[], **kwargs):
    builds = []
    def add_build(*args, **kwargs):
        builds.append(mk_build(*args, **kwargs))

    out_file = ('%s$_exe' if out_type == 'bin' else '%s$_so') % out_name
    out_path = '$b_native/%s' % out_file
    pic_flag = '' if out_type == 'bin' else '-fPIC'

    deps = []
    for f in src_files:
        obj_file = '$b_native/%s_objs/%s.o' % (out_name, os.path.basename(f))
        if f.endswith('.c'):
            build_type = 'c_obj'
        else:
            build_type = 'cxx_obj'
        add_build(obj_file, build_type, f, picflag=pic_flag, **kwargs)
        deps.append(obj_file)

    add_build(out_path, 'link_%s' % out_type, deps + link_extra, **kwargs)

    return '\n'.join(builds)

def outpost_gl_src(out_name, gen_prog):
    return template('''
        rule outpost_gl_src
            command = %gen_prog >$out
            description = GEN $out

        build %out_name: outpost_gl_src | %gen_prog
    ''', **locals())
