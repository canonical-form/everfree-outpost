import os

from minitemplate import template

from configure.util import cond, join, maybe, mk_build


def rules(i):
    return template('''
        rule js_compile_modules
            command = $python3 $root/mk/misc/pack_js_modules.py $
                    $js_dir $main_module $out $out.d
            description = MIN $out
            depfile = $out.d

        rule js_minify_file
            command = $
                %if not i.debug
                $uglifyjs $in -o $out -c -m
                %else
                cp $in $out
                %end
            description = MIN $out

        rule js_minify_asm
            command = $
                %if not i.debug
                $python3 $root/mk/misc/minify.py $in $out
                %else
                cp $in $out
                %end
            description = MIN $out
    ''', **locals())

def compile(i, out_file, js_dir, main_module):
    return template('''
        build %out_file: js_compile_modules $
            | $root/mk/misc/pack_js_modules.py
            js_dir = %js_dir
            main_module = %main_module
    ''', **locals())

def minify(i, out_file, js_src):
    return template('''
        build %out_file: js_minify_file %js_src
    ''', **locals())

def minify_asm(i, out_file, js_src):
    if out_file is None:
        out_file = '$b_js/%s' % os.path.basename(js_src)

    return template('''
        build %out_file: js_minify_file %js_src $
            | %if i.debug% $root/mk/misc/minify.py %end%
    ''', **locals())

def client_manifest(i, out_file):
    return template('''
        rule gen_client_manifest
            command = $python3 $root/mk/misc/gen_client_manifest.py $
                --file $b_asmjs/asmlibs.wasm $
                --file $root/src/client/client_parts.html $
                --file $b_js/outpost.js $
                --output $out
            description = GEN $out
            depfile = $out.d

        build %out_file: gen_client_manifest $
            | $root/mk/misc/gen_client_manifest.py $
              $b_asmjs/asmlibs.wasm $
              $b_js/outpost.js
    ''', **locals())
