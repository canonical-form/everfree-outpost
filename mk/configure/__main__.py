import argparse
import os
import shlex
import subprocess
import sys

from minitemplate import template

from configure import checks, rule, rust
from configure.gen import native, data, data2, js, dist, tests, www, sodium, misc
from configure.rust import RustCrate, RustTest


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('--root-dir', default=None,
            help='root of the project source tree')
    args.add_argument('--build-dir', default=None,
            help='directory to store build files')
    args.add_argument('--dist-dir', default=None,
            help='directory to store distribution image')

    args.add_argument('--reconfigure', action='store_true', default=False,
            help='reuse cached configuration info when possible')
    # Internal option, used when regenerating build.ninja on config changes
    args.add_argument('--regenerate', action='store_true', default=False,
            help=argparse.SUPPRESS)

    args.add_argument('--components', default='all',
            help='list of components to build')

    args.add_argument('--debug', action='store_true', default=False,
            help='produce a debug build')
    args.add_argument('--release', action='store_false', dest='debug',
            help='produce a release build (default)')

    args.add_argument('--rust-home', default='../rust',
            help='path to rust-lang/rust checkout')
    args.add_argument('--bitflags-home', default='../bitflags',
            help='path to rust-lang-nursery/bitflags checkout')
    args.add_argument('--log-home', default='../log',
            help='path to rust-lang-nursery/log checkout')
    args.add_argument('--rust-extra-libdir', default=None,
            help='additional search directory for Rust libraries')
    args.add_argument('--rust-lib-externs', default='',
            help='list of --extern flags for locating Rust libraries')
    args.add_argument('--compiler-rt-wasm-lib', default=None,
            help='path to Clang wasm32 support library `libclang_rt.builtins-wasm32.a`')

    args.add_argument('--rustc',
            help='name of the Rust compiler binary')
    args.add_argument('--cc',
            help='name of the C compiler binary')
    args.add_argument('--cxx',
            help='name of the C++ compiler binary')
    args.add_argument('--python3',
            help='name of the Python 3 interpreter binary')
    args.add_argument('--python3-config',
            help='name of the Python 3 build configuration helper binary')
    args.add_argument('--uglifyjs',
            help='name of the uglifyjs binary')
    args.add_argument('--pandoc',
            help='name of the Pandoc binary')

    args.add_argument('--force', action='store_true', default=False,
            help='proceed even if there are configuration errors')

    args.add_argument('--cflags',
            help='extra flags for the C compiler')
    args.add_argument('--cxxflags',
            help='extra flags for the C++ compiler')
    args.add_argument('--ldflags',
            help='extra flags for the C/C++ linker')

    args.add_argument('--site-config',
            help='YAML file containing site-specific config')

    args.add_argument('--set', action='append', default=[], metavar='KEY=VALUE',
            help='''(advanced) Set an additional configuration argument.
                Useful for overriding autodetection of things that don't have
                regular flags.''')

    args.add_argument('--patch-elf-loader', action='store_true', default=False,
            help='''(advanced) patch compiled binaries to remove references
                to nix paths''')
    args.add_argument('--patch-elf-loader-path', metavar='LD_LINUX',
            help='''(advanced) use this path as the replacement elf loader''')
    args.add_argument('--patch-libpython-version', metavar='VERSION',
            help='''(advanced) patch compiled binaries to change required
                libpython version''')

    return args


def header(i):
    def b(*args):
        return os.path.normpath(os.path.join(i.build_dir, *args))

    return template('''
        # Root of the source tree.  This used to be called $src, but that would
        # be confusing now that $root/src is an actual directory.
        root = %{os.path.normpath(i.root_dir)}
        # Note: (1) `build` is a ninja keyword; (2) `builddir` is a special
        # variable that determines where `.ninja_log` is stored.
        builddir = %{os.path.normpath(i.build_dir)}
        dist = %{os.path.normpath(i.dist_dir)}

        _exe = %{'' if not i.win32 else '.exe'}
        _so = %{'.so' if not i.win32 else '.dll'}
        _a = .a

        b_native = %{b('native')}
        b_asmjs = %{b('asmjs')}
        b_data = %{b('data')}
        b_data2 = %{b('data2')}
        b_js = %{b('js')}
        b_scripts = %{b('scripts')}
        b_www = %{b('www')}
        b_tests = %{b('tests')}
        b_misc = %{b('misc')}

        rustc = %{i.rustc}
        cc = %{i.cc}
        cxx = %{i.cxx}
        python3 = %{i.python3}
        uglifyjs = %{i.uglifyjs}

        user_cflags = %{i.cflags}
        user_cxxflags = %{i.cxxflags}
        user_ldflags = %{i.ldflags}

        version = dev
        site_config = %{i.site_config_path}
    ''', os=os, **locals())

def regenerate_rule(i, raw_args):
    args = ' '.join(shlex.quote(arg) for arg in raw_args
            if arg not in ('--regenerate', '--reconfigure'))

    dep_list = []
    for k, v in sys.modules.items():
        if k.startswith('configure.') or k == '__main__':
            path = getattr(v, '__file__', None)
            if path is not None:
                rel_path = os.path.relpath(path, i.root_dir)
                dep_list.append(os.path.join('$root', rel_path))
    dep_list.extend(misc.config_deps())
    dep_list.extend('$root/mk/%s.manifest' % c for c in i.components)
    dep_list.sort()
    deps = ' '.join(dep_list)

    return template('''
        rule configure
            command = ./configure %args --regenerate
            generator = 1

        build build.ninja: configure | %deps
    ''', **locals())


if __name__ == '__main__':
    log = open('config.log', 'w')

    raw_args = sys.argv[1:]
    if '--regenerate' not in raw_args and 'OUTPOST_CONFIGURE_ARGS' in os.environ:
        env_args = shlex.split(os.environ['OUTPOST_CONFIGURE_ARGS'])
        lines = ['Extra args from $OUTPOST_CONFIGURE_ARGS:'] + \
                ['  %r' % a for a in env_args]
        for l in lines:
            print(l)
            log.write(l)
        raw_args.extend(env_args)
    log.write('Arguments: %r\n\n' % (raw_args,))

    parser = build_parser()
    args = parser.parse_args(raw_args)


    # Patch up args a bit

    if args.regenerate:
        args.reconfigure = True

    for kv in args.set:
        k, _, v = kv.partition('=')
        k = k.replace('-', '_')
        setattr(args, k, v)


    i, ok = checks.run(args, log)
    if not ok:
        if i.force:
            print('Ignoring errors due to --force')
        else:
            sys.exit(1)


    rust_crates = [
        RustCrate('syntax_exts', ()),

        # Common
        RustCrate('common',
            ('common_crypto', 'common_data', 'common_physics',
                'common_proto', 'common_types', 'common_util')),
        RustCrate('common_crypto', ('common_util',)),
        RustCrate('common_data', ('common_util',)),
        RustCrate('common_physics', ('common_types', 'common_util')),
        RustCrate('common_proto', ('common_types', 'common_util')),
        RustCrate('common_types', (),
            plugin_deps=('syntax_exts',)),
        RustCrate('common_util', ('common_types',)),

        RustCrate('physics', ('common', 'common_types',)),

        # Server
        RustCrate('server_bundle',
            ('common', 'physics',
                'server_config', 'server_extra', 'server_types', 'server_world_types'),
            extra_flags='--cfg ffi'),
        RustCrate('server_config',
            ('common', 'physics', 'server_types')),
        RustCrate('server_extra', ('common', 'server_types',),
            plugin_deps=('syntax_exts',)),
        RustCrate('server_types', ('common',)),
        RustCrate('server_world_types', ('server_types',)),

        RustCrate('multipython',
            (),
            plugin_deps=('syntax_exts',),
            extra_flags=i.rust_libpython3_sys_ldflags),

        RustCrate('server_python_conv',
            ('multipython', 'common', 'physics',
                'server_config', 'server_extra', 'server_types', 'server_world_types'),
            # Uses syntax_exts for tests
            plugin_deps=('syntax_exts',)),

        RustCrate('world',
            ('common', 'server_bundle', 'server_config',
                'server_extra', 'server_types', 'server_world_types'),
            plugin_deps=('syntax_exts',)),

        RustCrate('engine',
            ('common', 'multipython', 'physics',
             'server_bundle', 'server_config', 'server_extra', 'server_types',
             'server_python_conv',
             'world'),
            plugin_deps=('syntax_exts',)),

        RustCrate('server2',
            ('common', 'engine', 'server_config', 'server_types'),
            src_file='$root/src/server2/main.rs'),

        # Wrapper
        RustCrate('wrapper_crypto',
                ('common',),
                # Note that both `wrapper_crypto` and `launcher_crypto` are
                # built from the same source code.  `launcher_crypto` has an
                # extra dependency, though, so we define them as separate
                # crates.
                src_file='$root/src/libcommon_crypto_bindings/lib.rs',
                test=False),

        # Terrain gen
        RustCrate('terrain_gen_algo', ('server_types',), force_optimize=True),
        RustCrate('generate_terrain',
            ('common', 'physics',
                'server_bundle', 'server_config', 'server_extra', 'server_types',
                'server_world_types',
                'terrain_gen_algo'),
            src_file='$root/src/generate_terrain/main.rs'),

        # Client
        RustCrate('ui', ()),
        RustCrate('colorspace', ()),
        RustCrate('asmgl', (),
                native_deps=('outpost_gl', 'png_file')),
        RustCrate('render', ('asmgl', 'rustc_arena')),

        RustCrate('client',
                ('physics', 'common', 'client_data', 'client_fonts',
                    'colorspace', 'ui', 'render')),
        RustCrate('client_data',
                ('common', 'physics')),
        RustCrate('client_fonts', (),
                src_file='$b_data/fonts_metrics.rs'),

        # Native client
        RustCrate('gen_outpost_gl', (), src_file='mk/misc/gen_outpost_gl.rs'),
        RustCrate('outpost_gl', (), src_file='$b_native/outpost_gl.rs',
                extra_flags='-A warnings'),
        RustCrate('png_file', (), force_optimize=True),

        RustCrate('client_native',
                ('client', 'outpost_gl', 'physics', 'png_file'),
                extra_flags=i.rust_libsdl2_ldflags,
                src_file='$root/src/client_native/main.rs'),

        RustCrate('client_native_crypto',
                ('common',),
                src_file='$root/src/libcommon_crypto_bindings/lib.rs',
                extra_flags='-C link-arg=-lsodium',
                test=False),

        # Asmlibs
        RustCrate('core', (), external_deps=(),
                src_file=i.rust_libcore_src,
                test=False),
        RustCrate('compiler_builtins', (), external_deps=('core',),
                src_file=i.rust_libcompiler_builtins_src,
                extra_flags=
                    '--cfg \'feature="compiler-builtins"\' '
                    '--cap-lints allow',
                test=False),
        RustCrate('alloc', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_liballoc_src,
                test=False),
        RustCrate('dlmalloc', (), external_deps=('core', 'alloc', 'compiler_builtins'),
                src_file=i.rust_libdlmalloc_src,
                test=False),
        RustCrate('libc', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_liblibc_src,
                extra_flags='--cap-lints allow',
                test=False),
        RustCrate('unwind', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_libunwind_src,
                test=False),
        RustCrate('rustc_demangle', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_librustc_demangle_src,
                test=False),
        RustCrate('panic_abort', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_libpanic_abort_src,
                test=False),
        RustCrate('std', (),
                external_deps=('core', 'alloc', 'dlmalloc', 'libc',
                    'unwind', 'rustc_demangle', 'compiler_builtins'),
                src_file=i.rust_libstd_src,
                extra_flags=
                    #'--cfg \'feature="backtrace"\' '
                    '--cfg \'feature="wasm_syscall"\'',
                test=False),

        RustCrate('bitflags', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_libbitflags_src,
                test=False,
                # Don't error on `pub use core as __core`
                extra_flags='--cap-lints allow'),
        RustCrate('log', (), external_deps=('core', 'compiler_builtins'),
                src_file=i.rust_liblog_src,
                test=False),

        RustCrate('asmlogger', (), external_deps=('std', 'log'),
                test=False),

        RustCrate('asmlibs',
                ('asmlogger', 'physics', 'client'),
                src_file='$root/src/asmlibs/lib.rs',
                test=False),

        RustCrate('launcher_crypto',
                ('common',),
                src_file='$root/src/libcommon_crypto_bindings/lib.rs',
                c_deps=('minisodium',),
                test=False),

        # Misc
        RustCrate('equip_sprites_render', ('physics',),
                src_file='$root/src/gen/equip_sprites/render.rs'),

        RustCrate('gen_phf', (),
                src_file='$root/src/gen/gen_phf.rs',
                force_optimize=True),

        RustCrate('uvedit_asm', ('common',),
                src_file='$root/src/uvedit/lib.rs',
                test=False),

        RustCrate('rustc_arena', ()),

        # libterrain_gen doesn't exist, so we can't build libterrain_gen_ffi
        #RustCrate('terrain_gen_ffi',
        #    ('server_config', 'server_types', 'terrain_gen'),
        #    src_file='$root/src/test_terrain_gen/ffi.rs'),

        #RustCrate('backend',
        #    ('physics', 'python',
        #        'common', 'common_proto', 'common_util',
        #        'server_bundle', 'server_config', 'server_extra',
        #        'server_types', 'server_world_types',),
        #    plugin_deps=('syntax_exts',),
        #    src_file='$root/src/server/main.rs'),

        RustTest('smallset', ('common',)),
        RustTest('smallvec', ('common',)),
        RustTest('prediction', ('client', 'client_data', 'common', 'server_config')),
        RustTest('crafting',
                ('common', 'engine', 'physics', 'server_bundle',
                    'server_config', 'server_types', 'world')),
        RustTest('lifecycle',
                ('engine', 'server_bundle', 'server_config', 'server_types')),
        RustTest('wards',
                ('engine', 'server_config', 'server_types')),
        RustTest('collide_2d', ('common',)),
        RustTest('movement', ('common',)),
        RustTest('engine_movement',
                ('common', 'engine', 'server_config', 'server_types')),

        # Migrations
        RustCrate('dump_bundle',
                ('server_bundle', 'server_extra'),
                src_file='$root/src/migrations/dump_bundle.rs',
                test=False),
    ]

    rust_crates = dict((c.name, c) for c in rust_crates)
    test_crate_names = sorted(c.name for c in rust_crates.values() if c.test)

    #rust_ctx = rust.RustContext(i, rust_crates)


    r = rule.Context()

    misc.rules(i, r)
    rust.rules(i, r)
    rust.register_providers(r, rust_crates)
    sodium.rules(i, r)


    sodium.lib(i, r, 'asmrt')
    sodium.lib(i, r, 'minisodium',
            # Paranoid CFLAGS used by the real `libsodium` build scripts
            extra_flags='-fno-strict-aliasing -fwrapv')

    # Custom build command for libtest_paths, which needs special env vars.
    r.rust_native_rlib.build(
            ('$b_native/rust-libs/libtest_paths.rlib',),
            ('$root/src/libtest_paths/lib.rs',),
            crate_name='test_paths',
            flags='$rust_opt_flags',
            rustc='''
                OUTPOST_FULL_SERVER_DATA=$b_data2/server_data.bin
                OUTPOST_FULL_CLIENT_DATA=$b_data2/client_data.bin
                OUTPOST_FULL_BOOT_PY=$b_data2/preboot.py
                OUTPOST_TEST_SERVER_DATA=$b_data2/server_test_data.bin
                OUTPOST_TEST_CLIENT_DATA=$b_data2/client_test_data.bin
                OUTPOST_TEST_BOOT_PY=$b_data2/preboot_test.py
                $rustc
            '''.replace('\n', ' '))

    # All preboot and test_paths paths are relative to `build.ninja`.
    r.gen_preboot.build(('$b_data2/preboot.py',),
            ('$root/scripts/boot.py', '$b_data2/game/game.py'))
    r.gen_preboot.build(('$b_data2/preboot_test.py',),
            ('$root/tests/boot.py', '$b_data2/test/game.py'))

    r.require(rust.asm_wasm_path('asmlibs'))
    r.require(rust.asm_wasm_path('launcher_crypto'))
    r.require(rust.asm_wasm_path('uvedit_asm'))

    r.require(rust.native_bin_path('server2'))
    r.require(rust.native_bin_path('generate_terrain'))
    r.require(rust.native_bin_path('gen_phf'))
    r.require(rust.native_bin_path('gen_outpost_gl'))
    r.require(rust.native_bin_path('dump_bundle'))
    r.require(rust.native_cdylib_path('equip_sprites_render'))
    r.require(rust.native_bin_path('client_native'))
    r.require(rust.native_staticlib_path('wrapper_crypto'))
    r.require(rust.native_cdylib_path('client_native_crypto'))

    test_exes = []
    for crate in test_crate_names:
        test_exes.append(r.require(rust.native_test_path(crate)))


    dist.rules2(r)

    data2.pony_shading(r)
    data2.vaults(r)


    # Build shader list
    misc.list_files(i, r, '$b_misc/shaders.txt',
            ('$root/assets/shaders',
                '$b_data2/misc/pony_shading.inc'))


    content = header(i)
    content += '\n\n'.join((
        r.code,

        '',
        '# Dist',
        dist.rules(i),
        '',

        '# Server - backend',
        native.rules(i),
        '',

        '# Server - wrapper',
        native.cxx('wrapper', 'bin',
            ('$root/src/wrapper/%s' % f
                for f in os.listdir(os.path.join(i.root_dir, 'src', 'wrapper'))
                if f.endswith('.cpp')),
            cxxflags='-DWEBSOCKETPP_STRICT_MASKING -I$root/src/libcommon_crypto_bindings/',
            # --gc-sections to remove unused rust libstd parts
            ldflags='-static -L$b_native -Wl,--gc-sections',
            # TODO: check for libsodium during config
            libs='-lwrapper_crypto -lsodium -ldl' +
            # TODO: detect these lib flags
                (' -lboost_system -lpthread' if not i.win32 else
                    ' -lboost_system-mt -lpthread -lwsock32 -lws2_32'),
            link_extra=['$b_native/libwrapper_crypto.a']),
        '',

        '# Python libs',
        native.cxx('outpost_savegame', 'shlib',
            ('$root/util/savegame_py/%s' % f
                for f in os.listdir(os.path.join(i.root_dir, 'util/savegame_py'))
                if f.endswith('.c')),
            cflags=i.python3_includes,
            ldflags=i.python3_ldflags,
            ),

        native.cxx('outpost_terrain_gen', 'shlib',
            ('$root/src/test_terrain_gen/py.c',),
            cflags=i.python3_includes,
            ldflags=i.python3_ldflags,
            link_extra=['$b_native/libterrain_gen_ffi$_a'],
            ),

        'build pymodules: phony '
            '$b_native/outpost_savegame$_so '
            '$b_native/outpost_terrain_gen$_so',
        '',

        '# Equipment sprite generator',
        dist.copy('$b_native/libequip_sprites_render$_so',
                  '$b_native/equip_sprites_render$_so'),
        '',
        '',

        '# Client - asmlibs',
        '# Client - Javascript',
        js.rules(i),
        js.compile(i, '$b_js/outpost.packed.js', '$root/src/client/js', 'main'),
        js.minify(i, '$b_js/outpost.js', '$b_js/outpost.packed.js'),
        js.compile(i, '$b_js/configedit.packed.js', '$root/src/client/js', 'configedit'),
        js.minify(i, '$b_js/configedit.js', '$b_js/configedit.packed.js'),
        js.client_manifest(i, '$b_js/manifest.json'),
        '',

        '# uvedit',

        js.minify_asm(i, '$b_js/uvedit_asm.js', '$b_asmjs/uvedit_asm.js'),
        js.compile(i, '$b_js/uvedit.packed.js', '$root/src/uvedit', 'main'),
        js.minify(i, '$b_js/uvedit.js', '$b_js/uvedit.packed.js'),
        '',

        '# Client - Native',
        native.outpost_gl_src('$b_native/outpost_gl.rs', '$b_native/gen_outpost_gl'),

        '# Data',
        data.rules(i),

        data.font('name', '$root/assets/misc/NeoSans.png'),
        data.font_stack('$b_data/fonts', ('name',)),
        data.server_json('$b_data/server.json'),

        data.pack(),

        data.credits('$b_data/credits.html'),
        '',

        '# New data compiler',
        data2.rules(i),

        data2.compile(i, '$root/data2/game', '$b_data2/game'),
        data2.compile_ui(i, '$root/data2/ui', '$b_data2/ui'),
        data2.binary_defs('$b_data2/client_data.bin', 'client',
                '$b_data2/game', '$b_data2/ui'),
        data2.binary_defs('$b_data2/server_data.bin', 'server', '$b_data2/game'),
        data2.binary_defs('$b_data2/vaults.bin', 'vaults'),

        data2.compile(i, '$root/tests/data', '$b_data2/test'),
        data2.binary_defs('$b_data2/client_test_data.bin', 'client',
                '$b_data2/test', '$b_data2/ui'),
        data2.binary_defs('$b_data2/server_test_data.bin', 'server',
                '$b_data2/test'),

        '',

        '# Launcher',
        www.rules(i),

        www.render_template('$b_www/index.html', '$root/src/www/index.html'),
        www.render_template('$b_www/main.css', '$root/src/www/main.css'),
        www.render_template('$b_www/serverlist.html', '$root/src/launcher/serverlist.html'),
        www.render_template('$b_www/launcher.html', '$root/src/launcher/launcher.html',
                hash_files={
                    'encsocket.js': '$root/src/launcher/encsocket.js',
                    'crypto.wasm': '$b_asmjs/launcher_crypto.wasm',
                }),
        www.render_markdown('$b_www/changelog.html', '$root/doc/changelog.md'),

        www.render_template('$b_www/templates/_base.html',
                '$root/src/auth/server/templates/_base.html'),

        www.collect_img_lists('$b_www/img/all.txt',
                ('index.html', 'main.css', 'serverlist.html', 'launcher.html',
                    'templates/_base.html')),

        # Actually part of the client, but it gets rendered the same way.
        www.render_template('$b_www/bugreport.html', '$root/src/bugreport/bugreport.html'),
        '',

        '# Tests',
        tests.tests(i, test_exes),


        '# Distribution',
        # dist rules go at the top so other parts can refer to `dist.copy`
        dist.components(i, i.components),
        '',

        '# Misc',
        regenerate_rule(i, raw_args),

        'default $builddir/dist.stamp',
        '', # ensure there's a newline after the last command
        ))

    with open('build.ninja', 'w') as f:
        f.write(content)

    print('Generated build.ninja')
    print('Run `ninja` to build')
