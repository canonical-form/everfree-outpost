import os
import sys

def list_files(path):
    if os.path.isdir(path):
        for name in os.listdir(path):
            if name.startswith('.'):
                continue
            yield os.path.join(path, name)
    else:
        yield path

def main(out_path, in_paths):
    out_dir = os.path.dirname(out_path)
    with open(out_path, 'w') as f:
        for in_path in in_paths:
            for file_path in list_files(in_path):
                rel_path = os.path.relpath(file_path, out_dir)
                print(file_path, out_dir, rel_path)
                f.write('%s\n' % rel_path)

if __name__ == '__main__':
    out_path = sys.argv[1]
    in_paths = sys.argv[2:]
    main(out_path, in_paths)
