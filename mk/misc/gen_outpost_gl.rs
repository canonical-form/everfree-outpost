extern crate gl_generator;

use std::io;
use gl_generator::{Registry, Api, Profile, Fallbacks, GlobalGenerator};

fn main() {
    Registry::new(Api::Gl, (2, 0), Profile::Core, Fallbacks::All, [
        "GL_ARB_framebuffer_object",
    ]).write_bindings(GlobalGenerator, &mut io::stdout()).unwrap();
}

