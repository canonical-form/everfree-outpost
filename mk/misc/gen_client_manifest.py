import argparse
import base64
import json
import hashlib
import os
import re
import sys


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('--file', action='append', default=[],
            help='include a file in the manifest')
    args.add_argument('--walk-js-file', action='append', default=[],
            help='include a Javascript file and all its dependencies')
    args.add_argument('--output',
            help='output file path')

    return args


REQUIRE_RE = re.compile(r'''require\(['"]([a-zA-Z0-9_/.]+)['"]\)''')

def collect_deps(path):
    deps = set()
    with open(path, 'r') as f:
        for line in f:
            for match in REQUIRE_RE.finditer(line):
                deps.add(match.group(1))
    deps = sorted(deps)
    return deps

def walk_js_deps(root_path):
    root_dir = os.path.dirname(root_path)

    seen = set()
    order = []
    def walk(name):
        nonlocal root_dir, seen, order

        if name in seen:
            return
        seen.add(name)

        path = os.path.join(root_dir, '%s.js' % name)
        deps = collect_deps(path)

        for dep in deps:
            dep_path = os.path.join(os.path.dirname(path), dep)
            dep_name = os.path.relpath(os.path.normpath(dep_path), root_dir)
            walk(dep_name)
        order.append(name)

    root_file = os.path.basename(root_path)
    root_name, _, _ = root_file.partition('.')
    walk(root_name)

    return order

def main(args):
    ns = build_parser().parse_args(args)

    file_path = {}
    order = []

    for path in ns.file:
        rel = os.path.basename(path)
        if rel in file_path:
            raise ValueError('duplicate file with same basename %r' % rel)
        file_path[rel] = path
        order.append(rel)

    for path in ns.walk_js_file:
        path, _, prefix = path.partition('::')
        dir_ = os.path.dirname(path)
        for f in walk_js_deps(path):
            f += '.js'
            rel = os.path.join(prefix, f)
            full = os.path.join(dir_, f)
            if rel in file_path:
                raise ValueError('duplicate file with same relative path %r' % rel)
            file_path[rel] = full
            order.append(rel)

    file_info = []
    #for rel, full in file_path.items():
    for rel in order:
        full = file_path[rel]
        with open(full, 'rb') as f:
            b = f.read()
            size = len(b)
            h = hashlib.sha256(b).digest()
            h_str = base64.b64encode(h).decode('ascii')
            b = None

        file_info.append([rel, size, h_str])

    with open(ns.output, 'w') as f:
        json.dump({
            'files': file_info,
            'redirects': {
                'configedit': 'configedit.html',
                'changelog': 'changelog.html',
                'bugreport': 'bugreport.html',
                },
            }, f)

    with open(ns.output + '.d', 'w') as f:
        f.write('%s:\\\n' % ns.output)
        for dep in file_path.values():
            f.write('    %s\\\n' % dep)

if __name__ == '__main__':
    main(sys.argv[1:])
