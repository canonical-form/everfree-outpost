import os
import re
import sys


REQUIRE_RE = re.compile(r'''require\(['"]([a-zA-Z0-9_/.]+)['"]\)''')

BAD_CHAR_RE = re.compile(r'[^a-zA-Z0-9_$]')

def clean_name(n):
    n = n.replace('/', '$')
    n = BAD_CHAR_RE.sub('_', n)
    return n

def wrap(src, before='', after=''):
    return '\n\n'.join((
            '(function() {',
            before,
            src,
            after,
            '})();\n'))

def pack_deps(root_dir, entry_module):
    module_code = {}
    postorder = []

    def walk(mod_name):
        if mod_name in module_code:
            return

        src_path = os.path.join(root_dir, '%s.js' % mod_name)
        with open(src_path) as f:
            src = f.read()

        mod_parent = os.path.dirname(mod_name)
        deps = set()
        def repl(m):
            target_mod = os.path.normpath(os.path.join(mod_parent, m.group(1)))
            deps.add(target_mod)
            return '$MODULES.%s' % clean_name(target_mod)
        src = REQUIRE_RE.sub(repl, src)
        src = wrap(src,
                before='var exports = {};',
                after='$MODULES.%s = exports;' % clean_name(mod_name))
        module_code[mod_name] = src

        for dep in deps:
            walk(dep)

        postorder.append(mod_name)

    walk(entry_module)

    code = wrap('\n\n'.join(module_code[m] for m in postorder),
            before='var $MODULES = {};')
    return code, postorder


def main(js_dir, entry_module, out_file, dep_file):
    code, order = pack_deps(js_dir, entry_module)

    with open(out_file, 'w') as f:
        f.write(code)

    with open(dep_file, 'w') as f:
        f.write('%s:\\\n' % out_file)
        for d in order:
            f.write('    %s\\\n' % os.path.join(js_dir, d + '.js'))

if __name__ == '__main__':
    js_dir, entry_module, out_file, dep_file = sys.argv[1:]
    main(js_dir, entry_module, out_file, dep_file)
